-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 16, 2013 at 01:17 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ocast_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `set_category`
--

CREATE TABLE IF NOT EXISTS `set_category` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `SortingOrder` int(5) DEFAULT NULL,
  `LastUpdate` datetime DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `set_categorylang`
--

CREATE TABLE IF NOT EXISTS `set_categorylang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `CategoryID` int(5) DEFAULT NULL,
  `Description` varchar(45) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `categoryID_idx` (`CategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `set_component`
--

CREATE TABLE IF NOT EXISTS `set_component` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `set_faq`
--

CREATE TABLE IF NOT EXISTS `set_faq` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `SortingOrder` int(5) DEFAULT NULL,
  `FAQCategoryID` int(5) DEFAULT NULL,
  `IsEnable` enum('0','1') DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FAQCategoryID_idx` (`FAQCategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `set_faqcategory`
--

CREATE TABLE IF NOT EXISTS `set_faqcategory` (
  `ID` int(5) NOT NULL,
  `SortingOrder` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `set_faqcategorylang`
--

CREATE TABLE IF NOT EXISTS `set_faqcategorylang` (
  `ID` int(5) NOT NULL,
  `FAQCategoryID` int(5) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CategoryName` varchar(250) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FAQCategoryID_idx` (`FAQCategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `set_faqlang`
--

CREATE TABLE IF NOT EXISTS `set_faqlang` (
  `ID` int(5) NOT NULL,
  `FaqID` int(5) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `Question` text,
  `Answer` text,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `fqaID_idx` (`FaqID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `set_format`
--

CREATE TABLE IF NOT EXISTS `set_format` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `set_formatlang`
--

CREATE TABLE IF NOT EXISTS `set_formatlang` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `FormatID` int(5) DEFAULT NULL,
  `Langcode` varchar(5) DEFAULT NULL,
  `Title` varchar(200) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FormatID_idx` (`FormatID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `set_menuitem`
--

CREATE TABLE IF NOT EXISTS `set_menuitem` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `MenuName` varchar(100) DEFAULT NULL,
  `ParentID` int(5) NOT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `set_pricetype`
--

CREATE TABLE IF NOT EXISTS `set_pricetype` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `set_pricetypelang`
--

CREATE TABLE IF NOT EXISTS `set_pricetypelang` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `text` varchar(100) DEFAULT NULL,
  `priceTypeID` int(5) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `creationDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `lastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `set_subscriptionplan`
--

CREATE TABLE IF NOT EXISTS `set_subscriptionplan` (
  `ID` int(5) NOT NULL,
  `SortingOrder` int(5) DEFAULT NULL,
  `Duration` int(2) DEFAULT NULL,
  `Price` decimal(5,2) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `set_subscriptionplanlang`
--

CREATE TABLE IF NOT EXISTS `set_subscriptionplanlang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `PlanID` int(5) DEFAULT NULL,
  `Description` varchar(250) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` date DEFAULT NULL,
  `CreatedBy` int(5) NOT NULL,
  `LastModifiedDate` date DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `planId_idx` (`PlanID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `set_user`
--

CREATE TABLE IF NOT EXISTS `set_user` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(150) DEFAULT NULL,
  `Password` text,
  `Fname` varchar(20) DEFAULT NULL,
  `Lname` varchar(20) DEFAULT NULL,
  `AciveStatus` varchar(20) DEFAULT NULL,
  `UserTypeId` int(5) DEFAULT NULL,
  `CompanyId` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `userNameUNQ` (`UserName`),
  KEY `companyId_idx` (`CompanyId`),
  KEY `userTypeId_idx` (`UserTypeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `set_userpermissions`
--

CREATE TABLE IF NOT EXISTS `set_userpermissions` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `MenuItemID` int(5) DEFAULT NULL,
  `UserTypeID` int(5) DEFAULT NULL,
  `IsAllowed` enum('0','1') DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `menuItemId_idx` (`MenuItemID`),
  KEY `userTypeId_idx` (`UserTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `set_usersite`
--

CREATE TABLE IF NOT EXISTS `set_usersite` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `UserID` int(5) DEFAULT NULL,
  `SiteID` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `userId_idx` (`UserID`),
  KEY `siteId_idx` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `set_usertype`
--

CREATE TABLE IF NOT EXISTS `set_usertype` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `UserTypeName` varchar(25) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sys_country`
--

CREATE TABLE IF NOT EXISTS `sys_country` (
  `ID` int(5) NOT NULL,
  `ISO` varchar(2) NOT NULL,
  `CountryCode` varchar(10) DEFAULT NULL,
  `IsActive` enum('0','1') DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`,`ISO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sys_countrylang`
--

CREATE TABLE IF NOT EXISTS `sys_countrylang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `CountryID` int(5) DEFAULT NULL,
  `CountryName` varchar(150) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `countryId_idx` (`CountryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sys_device`
--

CREATE TABLE IF NOT EXISTS `sys_device` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `SortingOrder` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sys_devicelang`
--

CREATE TABLE IF NOT EXISTS `sys_devicelang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `DeviceID` int(5) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `DeviceName` varchar(45) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `deviceId_idx` (`DeviceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sys_language`
--

CREATE TABLE IF NOT EXISTS `sys_language` (
  `LangCode` varchar(5) NOT NULL,
  `LangNameEng` varchar(50) DEFAULT NULL,
  `LangName` varchar(70) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`LangCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sys_registrationhistory`
--

CREATE TABLE IF NOT EXISTS `sys_registrationhistory` (
  `ID` int(5) NOT NULL,
  `UserID` int(5) DEFAULT NULL,
  `StepNumber` int(1) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Fk_userId` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `trn_analytics`
--

CREATE TABLE IF NOT EXISTS `trn_analytics` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteId` bigint(20) DEFAULT NULL,
  `UniqueVisits` int(11) DEFAULT NULL,
  `PageViews` int(11) DEFAULT NULL,
  `Visits` int(11) DEFAULT NULL,
  `Date` date DEFAULT NULL,
  `UpdatedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `siteID` (`SiteId`),
  KEY `date` (`Date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_case`
--

CREATE TABLE IF NOT EXISTS `trn_case` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `Image` varchar(100) DEFAULT NULL,
  `SortingOrder` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_caselang`
--

CREATE TABLE IF NOT EXISTS `trn_caselang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CaseID` bigint(20) DEFAULT NULL,
  `Title` varchar(250) DEFAULT NULL,
  `Description` text,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `caseId_idx` (`CaseID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_company`
--

CREATE TABLE IF NOT EXISTS `trn_company` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Name` varchar(150) DEFAULT NULL,
  `Phone` varchar(30) DEFAULT NULL,
  `Address1` varchar(200) DEFAULT NULL,
  `Address2` varchar(200) DEFAULT NULL,
  `zip` varchar(6) DEFAULT NULL,
  `CountryId` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ind_country` (`CountryId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_companylang`
--

CREATE TABLE IF NOT EXISTS `trn_companylang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CompanyId` bigint(20) DEFAULT NULL,
  `Name` varchar(150) DEFAULT NULL,
  `Address1` varchar(200) DEFAULT NULL,
  `Address2` varchar(200) DEFAULT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `companyId_idx` (`CompanyId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_demographicdetail`
--

CREATE TABLE IF NOT EXISTS `trn_demographicdetail` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `HeaderId` bigint(20) DEFAULT NULL,
  `Points` decimal(4,2) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `header_id` (`HeaderId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_demographicdetaillang`
--

CREATE TABLE IF NOT EXISTS `trn_demographicdetaillang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DemographicDetailId` bigint(20) DEFAULT NULL,
  `Answer` varchar(250) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `demographicDetailId_idx` (`DemographicDetailId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_demographicheader`
--

CREATE TABLE IF NOT EXISTS `trn_demographicheader` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteId` bigint(20) DEFAULT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_demographicheaderlang`
--

CREATE TABLE IF NOT EXISTS `trn_demographicheaderlang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DemographicHeaderID` bigint(20) NOT NULL,
  `HeaderLine` varchar(250) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `demographicHeaderID_idx` (`DemographicHeaderID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_format`
--

CREATE TABLE IF NOT EXISTS `trn_format` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `TypeID` bigint(20) DEFAULT NULL,
  `PriceTypeID` bigint(20) DEFAULT NULL,
  `Image` varchar(100) DEFAULT NULL,
  `Price` decimal(6,2) DEFAULT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`),
  KEY `price_type_id` (`PriceTypeID`),
  KEY `FK_type_id_idx` (`TypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_googleprofile`
--

CREATE TABLE IF NOT EXISTS `trn_googleprofile` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `GoogleUserID` varchar(100) DEFAULT NULL,
  `GoogleProfile` bigint(20) DEFAULT NULL,
  `ProfileName` varchar(100) DEFAULT NULL,
  `Token` text,
  `Status` varchar(10) DEFAULT NULL,
  `CompanyId` int(5) DEFAULT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `google_profile` (`GoogleProfile`),
  KEY `user_id` (`GoogleUserID`),
  KEY `companyId_idx` (`CompanyId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_invoice`
--

CREATE TABLE IF NOT EXISTS `trn_invoice` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `Description` varchar(250) DEFAULT NULL,
  `Amount` decimal(5,2) DEFAULT NULL,
  `Duedate` datetime DEFAULT NULL,
  `InvoceNo` varchar(25) DEFAULT NULL,
  `PaidDate` datetime DEFAULT NULL,
  `TransactionID` varchar(20) DEFAULT NULL,
  `PaymentStatus` int(1) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `siteId_idx` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_quickfact`
--

CREATE TABLE IF NOT EXISTS `trn_quickfact` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `LastUpdated` date DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_quickfactlang`
--

CREATE TABLE IF NOT EXISTS `trn_quickfactlang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `QuickFactId` bigint(20) NOT NULL,
  `Text` text,
  `LangCode` varchar(5) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `quickFactId_idx` (`QuickFactId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_site`
--

CREATE TABLE IF NOT EXISTS `trn_site` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ParentID` bigint(20) DEFAULT NULL,
  `ProfileID` bigint(20) DEFAULT NULL,
  `GoogleSiteID` varchar(12) DEFAULT NULL,
  `URL` varchar(200) DEFAULT NULL,
  `Logo` varchar(100) DEFAULT NULL,
  `TargetAge` varchar(45) DEFAULT NULL,
  `MalePercentage` decimal(5,2) DEFAULT NULL,
  `IsMobile` enum('0','1') DEFAULT NULL,
  `SubscriptionPlanId` int(3) DEFAULT NULL,
  `ValidPeriod` int(2) DEFAULT NULL,
  `ActivatedDate` date DEFAULT NULL,
  `ActivationStatus` varchar(15) DEFAULT NULL,
  `companyId` int(5) DEFAULT NULL,
  `ExpiryDate` date DEFAULT NULL,
  `LastGoogleUpdate` datetime DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `profileId_idx` (`ProfileID`),
  KEY `goog_site_id` (`GoogleSiteID`),
  KEY `company_id` (`companyId`),
  KEY `subscriptionPlanId_idx` (`SubscriptionPlanId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_sitecategory`
--

CREATE TABLE IF NOT EXISTS `trn_sitecategory` (
  `ID` bigint(20) NOT NULL,
  `SiteID` bigint(20) DEFAULT NULL,
  `CategoryID` bigint(20) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`),
  KEY `category_id` (`CategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `trn_sitecontact`
--

CREATE TABLE IF NOT EXISTS `trn_sitecontact` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `Fname` varchar(45) DEFAULT NULL,
  `IsDefault` enum('0','1') DEFAULT NULL,
  `Lname` varchar(45) DEFAULT NULL,
  `Email` varchar(75) DEFAULT NULL,
  `ConcatNumber` varchar(25) DEFAULT NULL,
  `ProfilePic` varchar(45) DEFAULT NULL,
  `ContactCountryCode` varchar(15) DEFAULT NULL,
  `SortingOrder` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`),
  KEY `FK_site_id_idx` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_sitecontactlang`
--

CREATE TABLE IF NOT EXISTS `trn_sitecontactlang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteContactID` bigint(20) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `Designation` varchar(45) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `lanCode` (`LangCode`),
  KEY `SiteContactID_idx` (`SiteContactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_sitecountry`
--

CREATE TABLE IF NOT EXISTS `trn_sitecountry` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CountryID` bigint(20) DEFAULT NULL,
  `SiteID` bigint(20) NOT NULL,
  `IsDefault` enum('0','1') DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `country_id` (`CountryID`),
  KEY `fk_trn_siteCountry_trn_site1_idx` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_sitedevice`
--

CREATE TABLE IF NOT EXISTS `trn_sitedevice` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `DeviceID` bigint(20) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `siteId_idx` (`SiteID`),
  KEY `deviceId_idx` (`DeviceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_sitelang`
--

CREATE TABLE IF NOT EXISTS `trn_sitelang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `Description` text,
  `Audience` varchar(250) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode_idx` (`LangCode`),
  KEY `siteId_idx` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_testimonial`
--

CREATE TABLE IF NOT EXISTS `trn_testimonial` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `SiteID` int(5) DEFAULT NULL,
  `UpdateDate` date DEFAULT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_testimoniallang`
--

CREATE TABLE IF NOT EXISTS `trn_testimoniallang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `TestimonialID` int(5) DEFAULT NULL,
  `Quote` text,
  `ByWhom` varchar(45) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `testimonialId_idx` (`TestimonialID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_userhistory`
--

CREATE TABLE IF NOT EXISTS `trn_userhistory` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `Title` varchar(250) DEFAULT NULL,
  `UserID` int(5) DEFAULT NULL,
  `Description` varchar(250) DEFAULT NULL,
  `URL` text,
  `LastUpdate` datetime DEFAULT NULL,
  `SiteID` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `userId` (`SiteID`),
  KEY `siteId_idx` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
