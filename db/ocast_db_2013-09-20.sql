/*
Navicat MySQL Data Transfer

Source Server         : Local
Source Server Version : 50527
Source Host           : localhost:3306
Source Database       : ocast_db

Target Server Type    : MYSQL
Target Server Version : 50527
File Encoding         : 65001

Date: 2013-09-20 20:58:44
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `set_category`
-- ----------------------------
DROP TABLE IF EXISTS `set_category`;
CREATE TABLE `set_category` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `SortingOrder` int(5) DEFAULT NULL,
  `LastUpdate` datetime DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of set_category
-- ----------------------------

-- ----------------------------
-- Table structure for `set_category_lang`
-- ----------------------------
DROP TABLE IF EXISTS `set_category_lang`;
CREATE TABLE `set_category_lang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `CategoryID` int(5) DEFAULT NULL,
  `Description` varchar(45) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `categoryID_idx` (`CategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of set_category_lang
-- ----------------------------

-- ----------------------------
-- Table structure for `set_faq`
-- ----------------------------
DROP TABLE IF EXISTS `set_faq`;
CREATE TABLE `set_faq` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `SortingOrder` int(5) DEFAULT NULL,
  `FAQCategoryID` int(5) DEFAULT NULL,
  `IsEnable` enum('0','1') DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FAQCategoryID_idx` (`FAQCategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of set_faq
-- ----------------------------

-- ----------------------------
-- Table structure for `set_faq_category`
-- ----------------------------
DROP TABLE IF EXISTS `set_faq_category`;
CREATE TABLE `set_faq_category` (
  `ID` int(5) NOT NULL,
  `SortingOrder` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of set_faq_category
-- ----------------------------

-- ----------------------------
-- Table structure for `set_faq_category_lang`
-- ----------------------------
DROP TABLE IF EXISTS `set_faq_category_lang`;
CREATE TABLE `set_faq_category_lang` (
  `ID` int(5) NOT NULL,
  `FAQCategoryID` int(5) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CategoryName` varchar(250) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FAQCategoryID_idx` (`FAQCategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of set_faq_category_lang
-- ----------------------------

-- ----------------------------
-- Table structure for `set_faq_lang`
-- ----------------------------
DROP TABLE IF EXISTS `set_faq_lang`;
CREATE TABLE `set_faq_lang` (
  `ID` int(5) NOT NULL,
  `FaqID` int(5) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `Question` text,
  `Answer` text,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `fqaID_idx` (`FaqID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of set_faq_lang
-- ----------------------------

-- ----------------------------
-- Table structure for `set_format`
-- ----------------------------
DROP TABLE IF EXISTS `set_format`;
CREATE TABLE `set_format` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of set_format
-- ----------------------------

-- ----------------------------
-- Table structure for `set_format_lang`
-- ----------------------------
DROP TABLE IF EXISTS `set_format_lang`;
CREATE TABLE `set_format_lang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `FormatID` int(5) DEFAULT NULL,
  `Langcode` varchar(5) DEFAULT NULL,
  `Title` varchar(200) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FormatID_idx` (`FormatID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of set_format_lang
-- ----------------------------

-- ----------------------------
-- Table structure for `set_menu_item`
-- ----------------------------
DROP TABLE IF EXISTS `set_menu_item`;
CREATE TABLE `set_menu_item` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `MenuName` varchar(100) DEFAULT NULL,
  `ParentID` int(5) NOT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of set_menu_item
-- ----------------------------

-- ----------------------------
-- Table structure for `set_price_type`
-- ----------------------------
DROP TABLE IF EXISTS `set_price_type`;
CREATE TABLE `set_price_type` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of set_price_type
-- ----------------------------

-- ----------------------------
-- Table structure for `set_price_type_lang`
-- ----------------------------
DROP TABLE IF EXISTS `set_price_type_lang`;
CREATE TABLE `set_price_type_lang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `text` varchar(100) DEFAULT NULL,
  `priceTypeID` int(5) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `creationDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `lastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of set_price_type_lang
-- ----------------------------

-- ----------------------------
-- Table structure for `set_subscription_plan`
-- ----------------------------
DROP TABLE IF EXISTS `set_subscription_plan`;
CREATE TABLE `set_subscription_plan` (
  `ID` int(5) NOT NULL,
  `SortingOrder` int(5) DEFAULT NULL,
  `Duration` int(2) DEFAULT NULL,
  `Price` decimal(5,2) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of set_subscription_plan
-- ----------------------------

-- ----------------------------
-- Table structure for `set_subscription_plan_lang`
-- ----------------------------
DROP TABLE IF EXISTS `set_subscription_plan_lang`;
CREATE TABLE `set_subscription_plan_lang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `PlanID` int(5) DEFAULT NULL,
  `Description` varchar(250) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` date DEFAULT NULL,
  `CreatedBy` int(5) NOT NULL,
  `LastModifiedDate` date DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `planId_idx` (`PlanID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of set_subscription_plan_lang
-- ----------------------------

-- ----------------------------
-- Table structure for `set_user`
-- ----------------------------
DROP TABLE IF EXISTS `set_user`;
CREATE TABLE `set_user` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(150) DEFAULT NULL,
  `Password` text,
  `Fname` varchar(20) DEFAULT NULL,
  `Lname` varchar(20) DEFAULT NULL,
  `AciveStatus` varchar(20) DEFAULT NULL,
  `UserTypeID` int(5) DEFAULT NULL,
  `CompanyID` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `PasswordResetCode` varchar(250) DEFAULT NULL,
  `ResetRequestDate` datetime DEFAULT NULL,
  `LastLogin` datetime DEFAULT NULL,
  `LastLoginIP` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `userNameUNQ` (`UserName`),
  KEY `companyId_idx` (`CompanyID`),
  KEY `userTypeId_idx` (`UserTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of set_user
-- ----------------------------

-- ----------------------------
-- Table structure for `set_user_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `set_user_permissions`;
CREATE TABLE `set_user_permissions` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `ModuleID` int(5) DEFAULT NULL,
  `UserTypeID` int(5) DEFAULT NULL,
  `IsAllowed` enum('0','1') DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `userTypeId_idx` (`UserTypeID`),
  KEY `ModuleID_idx` (`ModuleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of set_user_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for `set_user_site`
-- ----------------------------
DROP TABLE IF EXISTS `set_user_site`;
CREATE TABLE `set_user_site` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `UserID` int(5) DEFAULT NULL,
  `SiteID` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `userId_idx` (`UserID`),
  KEY `siteId_idx` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of set_user_site
-- ----------------------------

-- ----------------------------
-- Table structure for `set_user_type`
-- ----------------------------
DROP TABLE IF EXISTS `set_user_type`;
CREATE TABLE `set_user_type` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `UserTypeName` varchar(25) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of set_user_type
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_country`
-- ----------------------------
DROP TABLE IF EXISTS `sys_country`;
CREATE TABLE `sys_country` (
  `ID` int(5) NOT NULL,
  `ISO` varchar(2) NOT NULL,
  `CountryCode` varchar(10) DEFAULT NULL,
  `IsActive` enum('0','1') DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`,`ISO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_country
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_country_lang`
-- ----------------------------
DROP TABLE IF EXISTS `sys_country_lang`;
CREATE TABLE `sys_country_lang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `CountryID` int(5) DEFAULT NULL,
  `CountryName` varchar(150) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `countryId_idx` (`CountryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_country_lang
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_device`
-- ----------------------------
DROP TABLE IF EXISTS `sys_device`;
CREATE TABLE `sys_device` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `SortingOrder` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_device
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_device_lang`
-- ----------------------------
DROP TABLE IF EXISTS `sys_device_lang`;
CREATE TABLE `sys_device_lang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `DeviceID` int(5) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `DeviceName` varchar(45) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `deviceId_idx` (`DeviceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_device_lang
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_language`
-- ----------------------------
DROP TABLE IF EXISTS `sys_language`;
CREATE TABLE `sys_language` (
  `LangCode` varchar(5) NOT NULL,
  `LangNameEng` varchar(50) DEFAULT NULL,
  `LangName` varchar(70) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`LangCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_language
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_module`
-- ----------------------------
DROP TABLE IF EXISTS `sys_module`;
CREATE TABLE `sys_module` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `ModuleName` varchar(45) DEFAULT NULL,
  `ModuleCode` varchar(10) DEFAULT NULL,
  `DateCreated` datetime DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_module
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_registration_history`
-- ----------------------------
DROP TABLE IF EXISTS `sys_registration_history`;
CREATE TABLE `sys_registration_history` (
  `ID` int(5) NOT NULL,
  `UserID` int(5) DEFAULT NULL,
  `StepNumber` int(1) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Fk_userId` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_registration_history
-- ----------------------------

-- ----------------------------
-- Table structure for `tmp_account_info`
-- ----------------------------
DROP TABLE IF EXISTS `tmp_account_info`;
CREATE TABLE `tmp_account_info` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `ActivationCode` varchar(250) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `UserName` varchar(150) DEFAULT NULL,
  `Password` text,
  `Fname` varchar(20) DEFAULT NULL,
  `Lname` varchar(20) DEFAULT NULL,
  `Company` varchar(100) DEFAULT NULL,
  `PhoneCountryCode` varchar(6) DEFAULT NULL,
  `PhoneNumber` varchar(10) DEFAULT NULL,
  `Address1` varchar(100) DEFAULT NULL,
  `Address2` varchar(100) DEFAULT NULL,
  `Zip` int(8) DEFAULT NULL,
  `City` varchar(45) DEFAULT NULL,
  `CountryID` int(1) DEFAULT NULL,
  `AciveStatus` enum('active','pending','inactive') DEFAULT NULL,
  `SessionID` varchar(100) DEFAULT NULL,
  `RemorteIP` varchar(15) DEFAULT NULL,
  `UserAgent` varchar(100) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ActivationCode` (`ActivationCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tmp_account_info
-- ----------------------------

-- ----------------------------
-- Table structure for `trn_analytics`
-- ----------------------------
DROP TABLE IF EXISTS `trn_analytics`;
CREATE TABLE `trn_analytics` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `UniqueVisits` int(11) DEFAULT NULL,
  `PageViews` int(11) DEFAULT NULL,
  `Visits` int(11) DEFAULT NULL,
  `Date` date DEFAULT NULL,
  `UpdatedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `siteID` (`SiteID`),
  KEY `date` (`Date`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trn_analytics
-- ----------------------------
INSERT INTO `trn_analytics` VALUES ('1', '14', '8', '8', '8', '2013-09-02', '2013-09-09 17:12:58');
INSERT INTO `trn_analytics` VALUES ('2', '14', '12', '16', '12', '2013-09-03', '2013-09-09 17:12:59');
INSERT INTO `trn_analytics` VALUES ('3', '14', '5', '24', '5', '2013-09-04', '2013-09-09 17:13:00');
INSERT INTO `trn_analytics` VALUES ('4', '14', '3', '10', '3', '2013-09-05', '2013-09-09 17:13:01');
INSERT INTO `trn_analytics` VALUES ('5', '14', '3', '3', '3', '2013-09-06', '2013-09-09 17:13:03');
INSERT INTO `trn_analytics` VALUES ('6', '14', '1', '4', '2', '2013-09-07', '2013-09-09 17:13:04');
INSERT INTO `trn_analytics` VALUES ('7', '14', '0', '0', '0', '2013-09-08', '2013-09-09 17:13:05');
INSERT INTO `trn_analytics` VALUES ('8', '14', '1', '1', '1', '2013-09-09', '2013-09-09 17:13:06');
INSERT INTO `trn_analytics` VALUES ('9', '14', '4', '5', '4', '2013-09-10', '2013-09-09 17:13:07');
INSERT INTO `trn_analytics` VALUES ('10', '14', '1', '1', '1', '2013-09-11', '2013-09-09 17:13:08');
INSERT INTO `trn_analytics` VALUES ('11', '14', '1', '5', '1', '2013-09-12', '2013-09-09 17:13:09');
INSERT INTO `trn_analytics` VALUES ('12', '14', '5', '17', '6', '2013-09-13', '2013-09-09 17:13:10');
INSERT INTO `trn_analytics` VALUES ('13', '14', '2', '2', '2', '2013-09-14', '2013-09-09 17:13:11');
INSERT INTO `trn_analytics` VALUES ('14', '14', '2', '2', '2', '2013-09-15', '2013-09-09 17:13:12');
INSERT INTO `trn_analytics` VALUES ('15', '14', '3', '9', '3', '2013-09-16', '2013-09-09 17:13:14');
INSERT INTO `trn_analytics` VALUES ('16', '14', '2', '3', '2', '2013-09-17', '2013-09-09 17:13:15');
INSERT INTO `trn_analytics` VALUES ('17', '14', '2', '4', '2', '2013-09-18', '2013-09-09 17:13:16');
INSERT INTO `trn_analytics` VALUES ('18', '14', null, null, null, '2013-09-19', '2013-09-09 17:13:17');
INSERT INTO `trn_analytics` VALUES ('19', '14', '1', '1', '1', '2013-09-11', '2013-09-09 17:19:58');
INSERT INTO `trn_analytics` VALUES ('20', '14', '1', '5', '1', '2013-09-12', '2013-09-09 17:19:59');
INSERT INTO `trn_analytics` VALUES ('21', '14', '5', '17', '6', '2013-09-13', '2013-09-09 17:20:00');
INSERT INTO `trn_analytics` VALUES ('22', '14', '2', '2', '2', '2013-09-14', '2013-09-09 17:20:01');
INSERT INTO `trn_analytics` VALUES ('23', '14', '2', '2', '2', '2013-09-15', '2013-09-09 17:20:02');
INSERT INTO `trn_analytics` VALUES ('24', '14', '3', '9', '3', '2013-09-16', '2013-09-09 17:20:03');
INSERT INTO `trn_analytics` VALUES ('25', '14', '2', '3', '2', '2013-09-17', '2013-09-09 17:20:04');
INSERT INTO `trn_analytics` VALUES ('26', '14', '2', '4', '2', '2013-09-18', '2013-09-09 17:20:05');
INSERT INTO `trn_analytics` VALUES ('27', '14', null, null, null, '2013-09-19', '2013-09-09 17:20:06');
INSERT INTO `trn_analytics` VALUES ('28', '14', '1', '1', '1', '2013-09-11', '2012-09-09 00:00:00');
INSERT INTO `trn_analytics` VALUES ('29', '14', '1', '5', '1', '2013-09-12', '2012-09-09 00:00:00');
INSERT INTO `trn_analytics` VALUES ('30', '14', '5', '17', '6', '2013-09-13', '2012-09-09 00:00:00');
INSERT INTO `trn_analytics` VALUES ('31', '14', '2', '2', '2', '2013-09-14', '2012-09-09 00:00:00');
INSERT INTO `trn_analytics` VALUES ('32', '14', '2', '2', '2', '2013-09-15', '2012-09-09 00:00:00');
INSERT INTO `trn_analytics` VALUES ('33', '14', '3', '9', '3', '2013-09-16', '2012-09-09 00:00:00');
INSERT INTO `trn_analytics` VALUES ('34', '14', '2', '3', '2', '2013-09-17', '2012-09-09 00:00:00');
INSERT INTO `trn_analytics` VALUES ('35', '14', '2', '4', '2', '2013-09-18', '2012-09-09 00:00:00');
INSERT INTO `trn_analytics` VALUES ('36', '14', null, null, null, '2013-09-19', '2012-09-09 00:00:00');
INSERT INTO `trn_analytics` VALUES ('37', '14', '1', '1', '1', '2013-09-11', '2013-09-09 00:00:00');
INSERT INTO `trn_analytics` VALUES ('38', '14', '1', '5', '1', '2013-09-12', '2013-09-09 00:00:00');
INSERT INTO `trn_analytics` VALUES ('39', '14', '5', '17', '6', '2013-09-13', '2013-09-09 00:00:00');
INSERT INTO `trn_analytics` VALUES ('40', '14', '2', '2', '2', '2013-09-14', '2013-09-09 00:00:00');
INSERT INTO `trn_analytics` VALUES ('41', '14', '2', '2', '2', '2013-09-15', '2013-09-09 00:00:00');
INSERT INTO `trn_analytics` VALUES ('42', '14', '3', '9', '3', '2013-09-16', '2013-09-09 00:00:00');
INSERT INTO `trn_analytics` VALUES ('43', '14', '2', '3', '2', '2013-09-17', '2013-09-09 00:00:00');
INSERT INTO `trn_analytics` VALUES ('44', '14', '2', '4', '2', '2013-09-18', '2013-09-09 00:00:00');
INSERT INTO `trn_analytics` VALUES ('45', '14', null, null, null, '2013-09-19', '2013-09-09 00:00:00');

-- ----------------------------
-- Table structure for `trn_analytics_archive`
-- ----------------------------
DROP TABLE IF EXISTS `trn_analytics_archive`;
CREATE TABLE `trn_analytics_archive` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `UniqueVisits` int(11) DEFAULT NULL,
  `PageViews` int(11) DEFAULT NULL,
  `Visits` int(11) DEFAULT NULL,
  `Date` date DEFAULT NULL,
  `UpdatedTime` datetime DEFAULT NULL,
  `AnalyticsID` bigint(20) DEFAULT NULL,
  `TransferredDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `siteID` (`SiteID`),
  KEY `date` (`Date`),
  KEY `AnalyticsID` (`AnalyticsID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trn_analytics_archive
-- ----------------------------

-- ----------------------------
-- Table structure for `trn_case`
-- ----------------------------
DROP TABLE IF EXISTS `trn_case`;
CREATE TABLE `trn_case` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `Image` varchar(100) DEFAULT NULL,
  `SortingOrder` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trn_case
-- ----------------------------

-- ----------------------------
-- Table structure for `trn_case_lang`
-- ----------------------------
DROP TABLE IF EXISTS `trn_case_lang`;
CREATE TABLE `trn_case_lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CaseID` bigint(20) DEFAULT NULL,
  `Title` varchar(250) DEFAULT NULL,
  `Description` text,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `caseId_idx` (`CaseID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trn_case_lang
-- ----------------------------

-- ----------------------------
-- Table structure for `trn_company`
-- ----------------------------
DROP TABLE IF EXISTS `trn_company`;
CREATE TABLE `trn_company` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Phone` varchar(30) DEFAULT NULL,
  `zip` varchar(6) DEFAULT NULL,
  `CountryID` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ind_country` (`CountryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trn_company
-- ----------------------------

-- ----------------------------
-- Table structure for `trn_company_lang`
-- ----------------------------
DROP TABLE IF EXISTS `trn_company_lang`;
CREATE TABLE `trn_company_lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CompanyID` bigint(20) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `Name` varchar(150) DEFAULT NULL,
  `Address1` varchar(200) DEFAULT NULL,
  `Address2` varchar(200) DEFAULT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `companyId_idx` (`CompanyID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trn_company_lang
-- ----------------------------

-- ----------------------------
-- Table structure for `trn_demographic_detail`
-- ----------------------------
DROP TABLE IF EXISTS `trn_demographic_detail`;
CREATE TABLE `trn_demographic_detail` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `HeaderID` bigint(20) DEFAULT NULL,
  `Points` decimal(4,2) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `header_id` (`HeaderID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trn_demographic_detail
-- ----------------------------

-- ----------------------------
-- Table structure for `trn_demographic_detail_lang`
-- ----------------------------
DROP TABLE IF EXISTS `trn_demographic_detail_lang`;
CREATE TABLE `trn_demographic_detail_lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DemographicDetailID` bigint(20) DEFAULT NULL,
  `Answer` varchar(250) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `demographicDetailId_idx` (`DemographicDetailID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trn_demographic_detail_lang
-- ----------------------------

-- ----------------------------
-- Table structure for `trn_demographic_header`
-- ----------------------------
DROP TABLE IF EXISTS `trn_demographic_header`;
CREATE TABLE `trn_demographic_header` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trn_demographic_header
-- ----------------------------

-- ----------------------------
-- Table structure for `trn_demographic_header_lang`
-- ----------------------------
DROP TABLE IF EXISTS `trn_demographic_header_lang`;
CREATE TABLE `trn_demographic_header_lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DemographicHeaderID` bigint(20) NOT NULL,
  `HeadLine` varchar(250) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `demographicHeaderID_idx` (`DemographicHeaderID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trn_demographic_header_lang
-- ----------------------------

-- ----------------------------
-- Table structure for `trn_format`
-- ----------------------------
DROP TABLE IF EXISTS `trn_format`;
CREATE TABLE `trn_format` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `TypeID` bigint(20) DEFAULT NULL,
  `PriceTypeID` bigint(20) DEFAULT NULL,
  `Image` varchar(100) DEFAULT NULL,
  `Price` decimal(6,2) DEFAULT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`),
  KEY `price_type_id` (`PriceTypeID`),
  KEY `FK_type_id_idx` (`TypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trn_format
-- ----------------------------

-- ----------------------------
-- Table structure for `trn_google_profile`
-- ----------------------------
DROP TABLE IF EXISTS `trn_google_profile`;
CREATE TABLE `trn_google_profile` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `GoogleUserID` varchar(100) DEFAULT NULL,
  `GoogleProfile` bigint(20) DEFAULT NULL,
  `ProfileName` varchar(100) DEFAULT NULL,
  `Token` text,
  `Status` varchar(10) DEFAULT NULL,
  `CompanyId` int(5) DEFAULT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `AnalyticsCreatedDate` date DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `google_profile` (`GoogleProfile`),
  KEY `user_id` (`GoogleUserID`),
  KEY `companyId_idx` (`CompanyId`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trn_google_profile
-- ----------------------------
INSERT INTO `trn_google_profile` VALUES ('11', null, null, null, '{\"access_token\":\"ya29.AHES6ZQhKaw3M6kr7W8imvCiR89KyaKpSqnDnH6oU1TqPp6Z4DO86A\",\"token_type\":\"Bearer\",\"expires_in\":3600,\"refresh_token\":\"1\\/0UnZguSWgnjMFhNcFy4jhjrJCCvg50wHjsseMVrbL8o\",\"created\":1379689459}', null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for `trn_invoice`
-- ----------------------------
DROP TABLE IF EXISTS `trn_invoice`;
CREATE TABLE `trn_invoice` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `Description` varchar(250) DEFAULT NULL,
  `Amount` decimal(5,2) DEFAULT NULL,
  `Duedate` datetime DEFAULT NULL,
  `InvoceNo` varchar(25) DEFAULT NULL,
  `PaidDate` datetime DEFAULT NULL,
  `TransactionID` varchar(20) DEFAULT NULL,
  `PaymentStatus` int(1) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `siteId_idx` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trn_invoice
-- ----------------------------

-- ----------------------------
-- Table structure for `trn_quick_fact`
-- ----------------------------
DROP TABLE IF EXISTS `trn_quick_fact`;
CREATE TABLE `trn_quick_fact` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `LastUpdated` date DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trn_quick_fact
-- ----------------------------

-- ----------------------------
-- Table structure for `trn_quick_fact_lang`
-- ----------------------------
DROP TABLE IF EXISTS `trn_quick_fact_lang`;
CREATE TABLE `trn_quick_fact_lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `QuickFactID` bigint(20) NOT NULL,
  `Text` text,
  `LangCode` varchar(5) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `quickFactId_idx` (`QuickFactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trn_quick_fact_lang
-- ----------------------------

-- ----------------------------
-- Table structure for `trn_site`
-- ----------------------------
DROP TABLE IF EXISTS `trn_site`;
CREATE TABLE `trn_site` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ParentID` bigint(20) DEFAULT NULL,
  `ProfileID` bigint(20) DEFAULT NULL,
  `GoogleSiteID` varchar(12) DEFAULT NULL,
  `URL` varchar(200) DEFAULT NULL,
  `Logo` varchar(100) DEFAULT NULL,
  `AverageAge` int(2) DEFAULT NULL,
  `MalePercentage` decimal(5,2) DEFAULT NULL,
  `IsMobile` enum('0','1') DEFAULT NULL,
  `SubscriptionPlanID` int(3) DEFAULT NULL,
  `AnalyticsCreatedDate` date DEFAULT NULL,
  `ValidPeriod` int(2) DEFAULT NULL,
  `ActivatedDate` date DEFAULT NULL,
  `ActivationStatus` varchar(15) DEFAULT NULL,
  `CompanyID` int(5) DEFAULT NULL,
  `ExpiryDate` date DEFAULT NULL,
  `LastGoogleUpdate` datetime DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `profileId_idx` (`ProfileID`),
  KEY `goog_site_id` (`GoogleSiteID`),
  KEY `company_id` (`CompanyID`),
  KEY `subscriptionPlanId_idx` (`SubscriptionPlanID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trn_site
-- ----------------------------
INSERT INTO `trn_site` VALUES ('14', null, '11', '24741292', 'http://www.b2blk.com', null, null, null, null, null, '2009-12-26', null, null, null, null, null, '2013-09-19 00:00:00', null, null, null, null);

-- ----------------------------
-- Table structure for `trn_site_category`
-- ----------------------------
DROP TABLE IF EXISTS `trn_site_category`;
CREATE TABLE `trn_site_category` (
  `ID` bigint(20) NOT NULL,
  `SiteID` bigint(20) DEFAULT NULL,
  `CategoryID` bigint(20) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`),
  KEY `category_id` (`CategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trn_site_category
-- ----------------------------

-- ----------------------------
-- Table structure for `trn_site_contact`
-- ----------------------------
DROP TABLE IF EXISTS `trn_site_contact`;
CREATE TABLE `trn_site_contact` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `Fname` varchar(45) DEFAULT NULL,
  `IsDefault` enum('0','1') DEFAULT NULL,
  `Lname` varchar(45) DEFAULT NULL,
  `Email` varchar(75) DEFAULT NULL,
  `ConcatNumber` varchar(25) DEFAULT NULL,
  `ProfilePic` varchar(45) DEFAULT NULL,
  `ContactCountryCode` varchar(15) DEFAULT NULL,
  `SortingOrder` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`),
  KEY `FK_site_id_idx` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trn_site_contact
-- ----------------------------

-- ----------------------------
-- Table structure for `trn_site_contact_lang`
-- ----------------------------
DROP TABLE IF EXISTS `trn_site_contact_lang`;
CREATE TABLE `trn_site_contact_lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteContactID` bigint(20) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `Designation` varchar(45) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `lanCode` (`LangCode`),
  KEY `SiteContactID_idx` (`SiteContactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trn_site_contact_lang
-- ----------------------------

-- ----------------------------
-- Table structure for `trn_site_country`
-- ----------------------------
DROP TABLE IF EXISTS `trn_site_country`;
CREATE TABLE `trn_site_country` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CountryID` bigint(20) DEFAULT NULL,
  `SiteID` bigint(20) NOT NULL,
  `IsDefault` enum('0','1') DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `country_id` (`CountryID`),
  KEY `fk_trn_siteCountry_trn_site1_idx` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trn_site_country
-- ----------------------------

-- ----------------------------
-- Table structure for `trn_site_device`
-- ----------------------------
DROP TABLE IF EXISTS `trn_site_device`;
CREATE TABLE `trn_site_device` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `DeviceID` bigint(20) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `siteId_idx` (`SiteID`),
  KEY `deviceId_idx` (`DeviceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trn_site_device
-- ----------------------------

-- ----------------------------
-- Table structure for `trn_site_lang`
-- ----------------------------
DROP TABLE IF EXISTS `trn_site_lang`;
CREATE TABLE `trn_site_lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `Description` text,
  `Audience` varchar(250) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode_idx` (`LangCode`),
  KEY `siteId_idx` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trn_site_lang
-- ----------------------------

-- ----------------------------
-- Table structure for `trn_testimonial`
-- ----------------------------
DROP TABLE IF EXISTS `trn_testimonial`;
CREATE TABLE `trn_testimonial` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `SiteID` int(5) DEFAULT NULL,
  `UpdateDate` date DEFAULT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trn_testimonial
-- ----------------------------

-- ----------------------------
-- Table structure for `trn_testimonial_lang`
-- ----------------------------
DROP TABLE IF EXISTS `trn_testimonial_lang`;
CREATE TABLE `trn_testimonial_lang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `TestimonialID` int(5) DEFAULT NULL,
  `Quote` text,
  `ByWhom` varchar(45) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `testimonialId_idx` (`TestimonialID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trn_testimonial_lang
-- ----------------------------

-- ----------------------------
-- Table structure for `trn_user_history`
-- ----------------------------
DROP TABLE IF EXISTS `trn_user_history`;
CREATE TABLE `trn_user_history` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `ModuleID` int(5) DEFAULT NULL,
  `UserID` int(5) DEFAULT NULL,
  `Operation` varchar(150) DEFAULT NULL,
  `Description` varchar(250) DEFAULT NULL,
  `URL` text,
  `LastUpdate` datetime DEFAULT NULL,
  `SiteID` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `userId` (`SiteID`),
  KEY `siteId_idx` (`UserID`),
  KEY `ModuleID_idx` (`ModuleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trn_user_history
-- ----------------------------
