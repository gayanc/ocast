<<<<<<< HEAD
-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 24, 2013 at 07:10 PM
-- Server version: 5.5.29
-- PHP Version: 5.3.10-1ubuntu3.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ocast_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `set_Category`
--

CREATE TABLE IF NOT EXISTS `set_Category` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `SortingOrder` int(5) DEFAULT NULL,
  `LastUpdate` datetime DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `set_Category_Lang`
--

CREATE TABLE IF NOT EXISTS `set_Category_Lang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `CategoryID` int(5) DEFAULT NULL,
  `Description` varchar(45) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `categoryID_idx` (`CategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `set_FAQ`
--

CREATE TABLE IF NOT EXISTS `set_FAQ` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `SortingOrder` int(5) DEFAULT NULL,
  `FAQCategoryID` int(5) DEFAULT NULL,
  `IsEnable` enum('0','1') DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FAQCategoryID_idx` (`FAQCategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `set_FAQ_Category`
--

CREATE TABLE IF NOT EXISTS `set_FAQ_Category` (
  `ID` int(5) NOT NULL,
  `SortingOrder` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `set_FAQ_Category_Lang`
--

CREATE TABLE IF NOT EXISTS `set_FAQ_Category_Lang` (
  `ID` int(5) NOT NULL,
  `FAQCategoryID` int(5) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CategoryName` varchar(250) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FAQCategoryID_idx` (`FAQCategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `set_FAQ_Lang`
--

CREATE TABLE IF NOT EXISTS `set_FAQ_Lang` (
  `ID` int(5) NOT NULL,
  `FaqID` int(5) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `Question` text,
  `Answer` text,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `fqaID_idx` (`FaqID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `set_Format`
--

CREATE TABLE IF NOT EXISTS `set_Format` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `set_Format_Lang`
--

CREATE TABLE IF NOT EXISTS `set_Format_Lang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `FormatID` int(5) DEFAULT NULL,
  `Langcode` varchar(5) DEFAULT NULL,
  `Title` varchar(200) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FormatID_idx` (`FormatID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `set_Menu_Item`
--

CREATE TABLE IF NOT EXISTS `set_Menu_Item` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `MenuName` varchar(100) DEFAULT NULL,
  `ParentID` int(5) NOT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `set_Price_Type`
--

CREATE TABLE IF NOT EXISTS `set_Price_Type` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `set_Price_Type_Lang`
--

CREATE TABLE IF NOT EXISTS `set_Price_Type_Lang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `text` varchar(100) DEFAULT NULL,
  `priceTypeID` int(5) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `creationDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `lastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `set_Subscription_Plan`
--

CREATE TABLE IF NOT EXISTS `set_Subscription_Plan` (
  `ID` int(5) NOT NULL,
  `SortingOrder` int(5) DEFAULT NULL,
  `Duration` int(2) DEFAULT NULL,
  `Price` decimal(5,2) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `set_Subscription_Plan_Lang`
--

CREATE TABLE IF NOT EXISTS `set_Subscription_Plan_Lang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `PlanID` int(5) DEFAULT NULL,
  `Description` varchar(250) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` date DEFAULT NULL,
  `CreatedBy` int(5) NOT NULL,
  `LastModifiedDate` date DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `planId_idx` (`PlanID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `set_User`
--

CREATE TABLE IF NOT EXISTS `set_User` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(150) DEFAULT NULL,
  `Password` text,
  `Fname` varchar(20) DEFAULT NULL,
  `Lname` varchar(20) DEFAULT NULL,
  `AciveStatus` varchar(20) DEFAULT NULL,
  `UserTypeID` int(5) DEFAULT NULL,
  `CompanyID` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `PasswordResetCode` varchar(250) DEFAULT NULL,
  `ResetRequestDate` datetime DEFAULT NULL,
  `LastLogin` datetime DEFAULT NULL,
  `LastLoginIP` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `userNameUNQ` (`UserName`),
  KEY `companyId_idx` (`CompanyID`),
  KEY `userTypeId_idx` (`UserTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `set_User_Permissions`
--

CREATE TABLE IF NOT EXISTS `set_User_Permissions` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `ModuleID` int(5) DEFAULT NULL,
  `UserTypeID` int(5) DEFAULT NULL,
  `IsAllowed` enum('0','1') DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `userTypeId_idx` (`UserTypeID`),
  KEY `ModuleID_idx` (`ModuleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `set_User_Site`
--

CREATE TABLE IF NOT EXISTS `set_User_Site` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `UserID` int(5) DEFAULT NULL,
  `SiteID` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `userId_idx` (`UserID`),
  KEY `siteId_idx` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `set_User_Type`
--

CREATE TABLE IF NOT EXISTS `set_User_Type` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `UserTypeName` varchar(25) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sys_Country`
--

CREATE TABLE IF NOT EXISTS `sys_Country` (
  `ID` int(5) NOT NULL,
  `ISO` varchar(2) NOT NULL,
  `CountryCode` varchar(10) DEFAULT NULL,
  `IsActive` enum('0','1') DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`,`ISO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sys_Country`
--

INSERT INTO `sys_Country` (`ID`, `ISO`, `CountryCode`, `IsActive`, `LastModifiedDate`, `CreatedDate`) VALUES
(0, 'SL', 'SL', '1', '2013-09-24 00:00:00', '2013-09-24 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `sys_Country_Lang`
--

CREATE TABLE IF NOT EXISTS `sys_Country_Lang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `CountryID` int(5) DEFAULT NULL,
  `CountryName` varchar(150) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `countryId_idx` (`CountryID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `sys_Country_Lang`
--

INSERT INTO `sys_Country_Lang` (`ID`, `CountryID`, `CountryName`, `LangCode`, `CreationDate`, `LastModifiedDate`) VALUES
(1, 0, 'Sri Lanka', 'en', '2013-09-24 00:00:00', '2013-09-24 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `sys_Device`
--

CREATE TABLE IF NOT EXISTS `sys_Device` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `SortingOrder` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sys_Device_Lang`
--

CREATE TABLE IF NOT EXISTS `sys_Device_Lang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `DeviceID` int(5) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `DeviceName` varchar(45) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `deviceId_idx` (`DeviceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sys_Language`
--

CREATE TABLE IF NOT EXISTS `sys_Language` (
  `LangCode` varchar(5) NOT NULL,
  `LangNameEng` varchar(50) DEFAULT NULL,
  `LangName` varchar(70) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`LangCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sys_Module`
--

CREATE TABLE IF NOT EXISTS `sys_Module` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `ModuleCode` varchar(10) DEFAULT NULL,
  `ParentID` int(5) DEFAULT NULL,
  `URL` text,
  `SortingOrder` int(5) DEFAULT NULL,
  `DateCreated` datetime DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sys_Module_lang`
--

CREATE TABLE IF NOT EXISTS `sys_Module_lang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `ModuleName` varchar(45) DEFAULT NULL,
  `ModuleID` int(5) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `DateCreated` datetime DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ModuleID_idx` (`ModuleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sys_Registration_History`
--

CREATE TABLE IF NOT EXISTS `sys_Registration_History` (
  `ID` int(5) NOT NULL,
  `UserID` int(5) DEFAULT NULL,
  `StepNumber` int(1) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Fk_userId` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tmp_Account_info`
--

CREATE TABLE IF NOT EXISTS `tmp_Account_info` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `ActivationCode` varchar(250) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `UserName` varchar(150) DEFAULT NULL,
  `Password` text,
  `Fname` varchar(20) DEFAULT NULL,
  `Lname` varchar(20) DEFAULT NULL,
  `Company` varchar(100) DEFAULT NULL,
  `PhoneCountryCode` varchar(6) DEFAULT NULL,
  `PhoneNumber` varchar(10) DEFAULT NULL,
  `Address1` varchar(100) DEFAULT NULL,
  `Address2` varchar(100) DEFAULT NULL,
  `Zip` int(8) DEFAULT NULL,
  `City` varchar(45) DEFAULT NULL,
  `CountryID` int(1) DEFAULT NULL,
  `AciveStatus` enum('active','pending','inactive') DEFAULT NULL,
  `SessionID` varchar(100) DEFAULT NULL,
  `RemorteIP` varchar(15) DEFAULT NULL,
  `UserAgent` varchar(100) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ActivationCode` (`ActivationCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_Analytics`
--

CREATE TABLE IF NOT EXISTS `trn_Analytics` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `UniqueVisits` int(11) DEFAULT NULL,
  `PageViews` int(11) DEFAULT NULL,
  `Visits` int(11) DEFAULT NULL,
  `Date` date DEFAULT NULL,
  `UpdatedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `siteID` (`SiteID`),
  KEY `date` (`Date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_Analytics_Archive`
--

CREATE TABLE IF NOT EXISTS `trn_Analytics_Archive` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `UniqueVisits` int(11) DEFAULT NULL,
  `PageViews` int(11) DEFAULT NULL,
  `Visits` int(11) DEFAULT NULL,
  `Date` date DEFAULT NULL,
  `UpdatedTime` datetime DEFAULT NULL,
  `AnalyticsID` bigint(20) DEFAULT NULL,
  `TransferredDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `siteID` (`SiteID`),
  KEY `date` (`Date`),
  KEY `AnalyticsID` (`AnalyticsID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_Case`
--

CREATE TABLE IF NOT EXISTS `trn_Case` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `Image` varchar(100) DEFAULT NULL,
  `SortingOrder` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_Case_Lang`
--

CREATE TABLE IF NOT EXISTS `trn_Case_Lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CaseID` bigint(20) DEFAULT NULL,
  `Title` varchar(250) DEFAULT NULL,
  `Description` text,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `caseId_idx` (`CaseID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_Company`
--

CREATE TABLE IF NOT EXISTS `trn_Company` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Phone` varchar(30) DEFAULT NULL,
  `zip` varchar(6) DEFAULT NULL,
  `CountryID` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ind_country` (`CountryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_Company_Lang`
--

CREATE TABLE IF NOT EXISTS `trn_Company_Lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CompanyID` bigint(20) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `Name` varchar(150) DEFAULT NULL,
  `Address1` varchar(200) DEFAULT NULL,
  `Address2` varchar(200) DEFAULT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `companyId_idx` (`CompanyID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_Demographic_Detail`
--

CREATE TABLE IF NOT EXISTS `trn_Demographic_Detail` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `HeaderID` bigint(20) DEFAULT NULL,
  `Points` decimal(4,2) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `header_id` (`HeaderID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_Demographic_Detail_Lang`
--

CREATE TABLE IF NOT EXISTS `trn_Demographic_Detail_Lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DemographicDetailID` bigint(20) DEFAULT NULL,
  `Answer` varchar(250) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `demographicDetailId_idx` (`DemographicDetailID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_Demographic_Header`
--

CREATE TABLE IF NOT EXISTS `trn_Demographic_Header` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_Demographic_Header_Lang`
--

CREATE TABLE IF NOT EXISTS `trn_Demographic_Header_Lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DemographicHeaderID` bigint(20) NOT NULL,
  `HeadLine` varchar(250) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `demographicHeaderID_idx` (`DemographicHeaderID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_Format`
--

CREATE TABLE IF NOT EXISTS `trn_Format` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `TypeID` bigint(20) DEFAULT NULL,
  `PriceTypeID` bigint(20) DEFAULT NULL,
  `Image` varchar(100) DEFAULT NULL,
  `Price` decimal(6,2) DEFAULT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`),
  KEY `price_type_id` (`PriceTypeID`),
  KEY `FK_type_id_idx` (`TypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_Google_Profile`
--

CREATE TABLE IF NOT EXISTS `trn_Google_Profile` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `GoogleUserID` varchar(100) DEFAULT NULL,
  `GoogleProfile` bigint(20) DEFAULT NULL,
  `ProfileName` varchar(100) DEFAULT NULL,
  `Token` text,
  `Status` varchar(10) DEFAULT NULL,
  `CompanyId` int(5) DEFAULT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `google_profile` (`GoogleProfile`),
  KEY `user_id` (`GoogleUserID`),
  KEY `companyId_idx` (`CompanyId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_Invoice`
--

CREATE TABLE IF NOT EXISTS `trn_Invoice` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `Description` varchar(250) DEFAULT NULL,
  `Amount` decimal(5,2) DEFAULT NULL,
  `Duedate` datetime DEFAULT NULL,
  `InvoceNo` varchar(25) DEFAULT NULL,
  `PaidDate` datetime DEFAULT NULL,
  `TransactionID` varchar(20) DEFAULT NULL,
  `PaymentStatus` int(1) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `siteId_idx` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_Quick_Fact`
--

CREATE TABLE IF NOT EXISTS `trn_Quick_Fact` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `LastUpdated` date DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_Quick_Fact_Lang`
--

CREATE TABLE IF NOT EXISTS `trn_Quick_Fact_Lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `QuickFactID` bigint(20) NOT NULL,
  `Text` text,
  `LangCode` varchar(5) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `quickFactId_idx` (`QuickFactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_Site`
--

CREATE TABLE IF NOT EXISTS `trn_Site` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ParentID` bigint(20) DEFAULT NULL,
  `ProfileID` bigint(20) DEFAULT NULL,
  `GoogleSiteID` varchar(12) DEFAULT NULL,
  `AnalyticsCreatedDate` date DEFAULT NULL,
  `URL` varchar(200) DEFAULT NULL,
  `Logo` varchar(100) DEFAULT NULL,
  `AverageAge` int(2) DEFAULT NULL,
  `MalePercentage` decimal(5,2) DEFAULT NULL,
  `IsMobile` enum('0','1') DEFAULT NULL,
  `SubscriptionPlanID` int(3) DEFAULT NULL,
  `ValidPeriod` int(2) DEFAULT NULL,
  `ActivatedDate` date DEFAULT NULL,
  `ActivationStatus` varchar(15) DEFAULT NULL,
  `CompanyID` int(5) DEFAULT NULL,
  `ExpiryDate` date DEFAULT NULL,
  `LastGoogleUpdate` datetime DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Isdownloading` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `profileId_idx` (`ProfileID`),
  KEY `goog_site_id` (`GoogleSiteID`),
  KEY `company_id` (`CompanyID`),
  KEY `subscriptionPlanId_idx` (`SubscriptionPlanID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_Site_Category`
--

CREATE TABLE IF NOT EXISTS `trn_Site_Category` (
  `ID` bigint(20) NOT NULL,
  `SiteID` bigint(20) DEFAULT NULL,
  `CategoryID` bigint(20) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`),
  KEY `category_id` (`CategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `trn_Site_Contact`
--

CREATE TABLE IF NOT EXISTS `trn_Site_Contact` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `Fname` varchar(45) DEFAULT NULL,
  `IsDefault` enum('0','1') DEFAULT NULL,
  `Lname` varchar(45) DEFAULT NULL,
  `Email` varchar(75) DEFAULT NULL,
  `ConcatNumber` varchar(25) DEFAULT NULL,
  `ProfilePic` varchar(45) DEFAULT NULL,
  `ContactCountryCode` varchar(15) DEFAULT NULL,
  `SortingOrder` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`),
  KEY `FK_site_id_idx` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_Site_Contact_Lang`
--

CREATE TABLE IF NOT EXISTS `trn_Site_Contact_Lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteContactID` bigint(20) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `Designation` varchar(45) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `lanCode` (`LangCode`),
  KEY `SiteContactID_idx` (`SiteContactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_Site_Country`
--

CREATE TABLE IF NOT EXISTS `trn_Site_Country` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CountryID` bigint(20) DEFAULT NULL,
  `SiteID` bigint(20) NOT NULL,
  `IsDefault` enum('0','1') DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `country_id` (`CountryID`),
  KEY `fk_trn_siteCountry_trn_site1_idx` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_Site_Device`
--

CREATE TABLE IF NOT EXISTS `trn_Site_Device` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `DeviceID` bigint(20) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `siteId_idx` (`SiteID`),
  KEY `deviceId_idx` (`DeviceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_Site_Lang`
--

CREATE TABLE IF NOT EXISTS `trn_Site_Lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `Description` text,
  `Audience` varchar(250) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode_idx` (`LangCode`),
  KEY `siteId_idx` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_Testimonial`
--

CREATE TABLE IF NOT EXISTS `trn_Testimonial` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `SiteID` int(5) DEFAULT NULL,
  `UpdateDate` date DEFAULT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_Testimonial_Lang`
--

CREATE TABLE IF NOT EXISTS `trn_Testimonial_Lang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `TestimonialID` int(5) DEFAULT NULL,
  `Quote` text,
  `ByWhom` varchar(45) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `testimonialId_idx` (`TestimonialID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_User_History`
--

CREATE TABLE IF NOT EXISTS `trn_User_History` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `ModuleID` int(5) DEFAULT NULL,
  `UserID` int(5) DEFAULT NULL,
  `Operation` varchar(150) DEFAULT NULL,
  `Description` varchar(250) DEFAULT NULL,
  `LastUpdate` datetime DEFAULT NULL,
  `SiteID` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `userId` (`SiteID`),
  KEY `siteId_idx` (`UserID`),
  KEY `ModuleID_idx` (`ModuleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
=======
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `ocast_db` ;
CREATE SCHEMA IF NOT EXISTS `ocast_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `ocast_db` ;

-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Company`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Company` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Company` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `Phone` VARCHAR(30) NULL ,
  `zip` VARCHAR(6) NULL ,
  `CountryID` INT(5) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `ind_country` ON `ocast_db`.`trn_Company` (`CountryID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Google_Profile`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Google_Profile` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Google_Profile` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `GoogleUserID` VARCHAR(100) NULL ,
  `GoogleProfile` BIGINT NULL ,
  `ProfileName` VARCHAR(100) NULL ,
  `Token` TEXT NULL ,
  `Status` VARCHAR(10) NULL ,
  `CompanyId` INT(5) NULL ,
  `CreationDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `google_profile` ON `ocast_db`.`trn_Google_Profile` (`GoogleProfile` ASC) ;

CREATE INDEX `user_id` ON `ocast_db`.`trn_Google_Profile` (`GoogleUserID` ASC) ;

CREATE INDEX `companyId_idx` ON `ocast_db`.`trn_Google_Profile` (`CompanyId` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_Subscription_Plan`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_Subscription_Plan` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_Subscription_Plan` (
  `ID` INT(5) NOT NULL ,
  `SortingOrder` INT(5) NULL ,
  `Duration` INT(2) NULL ,
  `Price` DECIMAL(5,2) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Site`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Site` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Site` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `ParentID` BIGINT NULL ,
  `ProfileID` BIGINT NULL ,
  `GoogleSiteID` VARCHAR(12) NULL ,
  `AnalyticsCreatedDate` DATE NULL ,
  `URL` VARCHAR(200) NULL ,
  `Logo` VARCHAR(100) NULL ,
  `AverageAge` INT(2) NULL ,
  `MalePercentage` DECIMAL(5,2) NULL ,
  `IsMobile` ENUM('0','1') NULL ,
  `SubscriptionPlanID` INT(3) NULL ,
  `ValidPeriod` INT(2) NULL ,
  `ActivatedDate` DATE NULL ,
  `ActivationStatus` VARCHAR(15) NULL ,
  `CompanyID` INT(5) NULL ,
  `ExpiryDate` DATE NULL ,
  `LastGoogleUpdate` DATETIME NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  `Isdownloading` ENUM('0','1') NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `profileId_idx` ON `ocast_db`.`trn_Site` (`ProfileID` ASC) ;

CREATE INDEX `goog_site_id` ON `ocast_db`.`trn_Site` (`GoogleSiteID` ASC) ;

CREATE INDEX `company_id` ON `ocast_db`.`trn_Site` (`CompanyID` ASC) ;

CREATE INDEX `subscriptionPlanId_idx` ON `ocast_db`.`trn_Site` (`SubscriptionPlanID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Site_Contact`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Site_Contact` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Site_Contact` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `SiteID` BIGINT NULL ,
  `Fname` VARCHAR(45) NULL ,
  `IsDefault` ENUM('0','1') NULL ,
  `Lname` VARCHAR(45) NULL ,
  `Email` VARCHAR(75) NULL ,
  `ConcatNumber` VARCHAR(25) NULL ,
  `ProfilePic` VARCHAR(45) NULL ,
  `ContactCountryCode` VARCHAR(15) NULL ,
  `SortingOrder` INT(5) NULL ,
  `CreatedBy` INT(5) NULL ,
  `CreatedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `site_id` ON `ocast_db`.`trn_Site_Contact` (`SiteID` ASC) ;

CREATE INDEX `FK_site_id_idx` ON `ocast_db`.`trn_Site_Contact` (`SiteID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_FAQ_Category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_FAQ_Category` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_FAQ_Category` (
  `ID` INT(5) NOT NULL ,
  `SortingOrder` INT(5) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastUpdated` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_FAQ`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_FAQ` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_FAQ` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `SortingOrder` INT(5) NULL ,
  `FAQCategoryID` INT(5) NULL ,
  `IsEnable` ENUM('0','1') NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `FAQCategoryID_idx` ON `ocast_db`.`set_FAQ` (`FAQCategoryID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_Category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_Category` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_Category` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `SortingOrder` INT(5) NULL ,
  `LastUpdate` DATETIME NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Site_Category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Site_Category` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Site_Category` (
  `ID` BIGINT NOT NULL ,
  `SiteID` BIGINT NULL ,
  `CategoryID` BIGINT NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `site_id` ON `ocast_db`.`trn_Site_Category` (`SiteID` ASC) ;

CREATE INDEX `category_id` ON `ocast_db`.`trn_Site_Category` (`CategoryID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`sys_Country`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`sys_Country` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`sys_Country` (
  `ID` INT(5) NOT NULL ,
  `ISO` VARCHAR(2) NOT NULL ,
  `CountryCode` VARCHAR(10) NULL ,
  `IsActive` ENUM('0','1') NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `CreatedDate` DATETIME NULL ,
  PRIMARY KEY (`ID`, `ISO`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Site_Country`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Site_Country` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Site_Country` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `CountryID` BIGINT NULL ,
  `SiteID` BIGINT NOT NULL ,
  `IsDefault` ENUM('0','1') NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `country_id` ON `ocast_db`.`trn_Site_Country` (`CountryID` ASC) ;

CREATE INDEX `fk_trn_siteCountry_trn_site1_idx` ON `ocast_db`.`trn_Site_Country` (`SiteID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Quick_Fact`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Quick_Fact` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Quick_Fact` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `SiteID` BIGINT NULL ,
  `LastUpdated` DATE NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `site_id` ON `ocast_db`.`trn_Quick_Fact` (`SiteID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Testimonial`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Testimonial` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Testimonial` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `SiteID` INT(5) NULL ,
  `UpdateDate` DATE NULL ,
  `CreationDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `site_id` ON `ocast_db`.`trn_Testimonial` (`SiteID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Demographic_Header`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Demographic_Header` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Demographic_Header` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `SiteID` BIGINT NULL ,
  `CreationDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastUpdated` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `site_id` ON `ocast_db`.`trn_Demographic_Header` (`SiteID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Demographic_Detail`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Demographic_Detail` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Demographic_Detail` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `HeaderID` BIGINT NULL ,
  `Points` DECIMAL(4,2) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `header_id` ON `ocast_db`.`trn_Demographic_Detail` (`HeaderID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_Price_Type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_Price_Type` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_Price_Type` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_Format`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_Format` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_Format` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Format`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Format` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Format` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `SiteID` BIGINT NULL ,
  `TypeID` BIGINT NULL ,
  `PriceTypeID` BIGINT NULL ,
  `Image` VARCHAR(100) NULL ,
  `Price` DECIMAL(6,2) NULL ,
  `CreationDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `site_id` ON `ocast_db`.`trn_Format` (`SiteID` ASC) ;

CREATE INDEX `price_type_id` ON `ocast_db`.`trn_Format` (`PriceTypeID` ASC) ;

CREATE INDEX `FK_type_id_idx` ON `ocast_db`.`trn_Format` (`TypeID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Case`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Case` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Case` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `SiteID` BIGINT NULL ,
  `Image` VARCHAR(100) NULL ,
  `SortingOrder` INT(5) NULL ,
  `CreatedBy` INT(5) NULL ,
  `CreatedDate` DATETIME NULL ,
  `LastUpdated` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `site_id` ON `ocast_db`.`trn_Case` (`SiteID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Analytics`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Analytics` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Analytics` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `SiteID` BIGINT NULL ,
  `UniqueVisits` INT NULL ,
  `PageViews` INT NULL ,
  `Visits` INT NULL ,
  `Date` DATE NULL ,
  `UpdatedTime` DATETIME NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `siteID` ON `ocast_db`.`trn_Analytics` (`SiteID` ASC) ;

CREATE INDEX `date` ON `ocast_db`.`trn_Analytics` (`Date` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_User_Type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_User_Type` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_User_Type` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `UserTypeName` VARCHAR(25) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_User`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_User` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_User` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `UserName` VARCHAR(150) NULL ,
  `Password` TEXT NULL ,
  `Fname` VARCHAR(20) NULL ,
  `Lname` VARCHAR(20) NULL ,
  `AciveStatus` VARCHAR(20) NULL ,
  `UserTypeID` INT(5) NULL ,
  `CompanyID` INT(5) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  `PasswordResetCode` VARCHAR(250) NULL ,
  `ResetRequestDate` DATETIME NULL ,
  `LastLogin` DATETIME NULL ,
  `LastLoginIP` VARCHAR(15) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE UNIQUE INDEX `userNameUNQ` ON `ocast_db`.`set_User` (`UserName` ASC) ;

CREATE INDEX `companyId_idx` ON `ocast_db`.`set_User` (`CompanyID` ASC) ;

CREATE INDEX `userTypeId_idx` ON `ocast_db`.`set_User` (`UserTypeID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`sys_Language`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`sys_Language` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`sys_Language` (
  `LangCode` VARCHAR(5) NOT NULL ,
  `LangNameEng` VARCHAR(50) NULL ,
  `LangName` VARCHAR(70) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL ,
  `CreatedDate` DATETIME NULL ,
  `LastModifiedDate` DATETIME NULL ,
  PRIMARY KEY (`LangCode`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ocast_db`.`sys_Module`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`sys_Module` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`sys_Module` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `ModuleCode` VARCHAR(10) NULL ,
  `ParentID` INT(5) NULL ,
  `URL` TEXT NULL ,
  `SortingOrder` INT(5) NULL ,
  `DateCreated` DATETIME NULL ,
  `LastUpdated` DATETIME NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_User_History`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_User_History` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_User_History` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `ModuleID` INT(5) NULL ,
  `UserID` INT(5) NULL ,
  `Operation` VARCHAR(150) NULL ,
  `Description` VARCHAR(250) NULL ,
  `LastUpdate` DATETIME NULL ,
  `SiteID` INT(5) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `userId` ON `ocast_db`.`trn_User_History` (`SiteID` ASC) ;

CREATE INDEX `siteId_idx` ON `ocast_db`.`trn_User_History` (`UserID` ASC) ;

CREATE INDEX `ModuleID_idx` ON `ocast_db`.`trn_User_History` (`ModuleID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_FAQ_Lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_FAQ_Lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_FAQ_Lang` (
  `ID` INT(5) NOT NULL ,
  `FaqID` INT(5) NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `Question` TEXT CHARACTER SET 'utf8' NULL ,
  `Answer` TEXT CHARACTER SET 'utf8' NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `langCode` ON `ocast_db`.`set_FAQ_Lang` (`LangCode` ASC) ;

CREATE INDEX `fqaID_idx` ON `ocast_db`.`set_FAQ_Lang` (`FaqID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Case_Lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Case_Lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Case_Lang` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `CaseID` BIGINT NULL ,
  `Title` VARCHAR(250) CHARACTER SET 'utf8' NULL ,
  `Description` TEXT CHARACTER SET 'utf8' NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `langCode` ON `ocast_db`.`trn_Case_Lang` (`LangCode` ASC) ;

CREATE INDEX `caseId_idx` ON `ocast_db`.`trn_Case_Lang` (`CaseID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Company_Lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Company_Lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Company_Lang` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `CompanyID` BIGINT NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `Name` VARCHAR(150) CHARACTER SET 'utf8' NULL ,
  `Address1` VARCHAR(200) CHARACTER SET 'utf8' NULL ,
  `Address2` VARCHAR(200) NULL ,
  `CreationDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `companyId_idx` ON `ocast_db`.`trn_Company_Lang` (`CompanyID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Demographic_Header_Lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Demographic_Header_Lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Demographic_Header_Lang` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `DemographicHeaderID` BIGINT NOT NULL ,
  `HeadLine` VARCHAR(250) CHARACTER SET 'utf8' NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `CreationDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `langCode` ON `ocast_db`.`trn_Demographic_Header_Lang` (`LangCode` ASC) ;

CREATE INDEX `demographicHeaderID_idx` ON `ocast_db`.`trn_Demographic_Header_Lang` (`DemographicHeaderID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Demographic_Detail_Lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Demographic_Detail_Lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Demographic_Detail_Lang` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `DemographicDetailID` BIGINT NULL ,
  `Answer` VARCHAR(250) CHARACTER SET 'utf8' NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `langCode` ON `ocast_db`.`trn_Demographic_Detail_Lang` (`LangCode` ASC) ;

CREATE INDEX `demographicDetailId_idx` ON `ocast_db`.`trn_Demographic_Detail_Lang` (`DemographicDetailID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Quick_Fact_Lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Quick_Fact_Lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Quick_Fact_Lang` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `QuickFactID` BIGINT NOT NULL ,
  `Text` TEXT CHARACTER SET 'utf8' NULL ,
  `LangCode` VARCHAR(5) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `langCode` ON `ocast_db`.`trn_Quick_Fact_Lang` (`LangCode` ASC) ;

CREATE INDEX `quickFactId_idx` ON `ocast_db`.`trn_Quick_Fact_Lang` (`QuickFactID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Site_Contact_Lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Site_Contact_Lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Site_Contact_Lang` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `SiteContactID` BIGINT NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `Designation` VARCHAR(45) CHARACTER SET 'utf8' NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `lanCode` ON `ocast_db`.`trn_Site_Contact_Lang` (`LangCode` ASC) ;

CREATE INDEX `SiteContactID_idx` ON `ocast_db`.`trn_Site_Contact_Lang` (`SiteContactID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Testimonial_Lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Testimonial_Lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Testimonial_Lang` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `TestimonialID` INT(5) NULL ,
  `Quote` TEXT CHARACTER SET 'utf8' NULL ,
  `ByWhom` VARCHAR(45) CHARACTER SET 'utf8' NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `langCode` ON `ocast_db`.`trn_Testimonial_Lang` (`LangCode` ASC) ;

CREATE INDEX `testimonialId_idx` ON `ocast_db`.`trn_Testimonial_Lang` (`TestimonialID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_User_Site`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_User_Site` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_User_Site` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `UserID` INT(5) NULL ,
  `SiteID` INT(5) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `userId_idx` ON `ocast_db`.`set_User_Site` (`UserID` ASC) ;

CREATE INDEX `siteId_idx` ON `ocast_db`.`set_User_Site` (`SiteID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_User_Permissions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_User_Permissions` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_User_Permissions` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `ModuleID` INT(5) NULL ,
  `UserTypeID` INT(5) NULL ,
  `IsAllowed` ENUM('0','1') NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `userTypeId_idx` ON `ocast_db`.`set_User_Permissions` (`UserTypeID` ASC) ;

CREATE INDEX `ModuleID_idx` ON `ocast_db`.`set_User_Permissions` (`ModuleID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_Menu_Item`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_Menu_Item` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_Menu_Item` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `MenuName` VARCHAR(100) NULL ,
  `ParentID` INT(5) NOT NULL ,
  `CreatedDate` DATETIME NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  `CreatedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ocast_db`.`sys_Device`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`sys_Device` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`sys_Device` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `SortingOrder` INT(5) NULL ,
  `CreatedDate` DATETIME NULL ,
  `LastModifiedDate` DATETIME NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Site_Device`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Site_Device` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Site_Device` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `SiteID` BIGINT NULL ,
  `DeviceID` BIGINT NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `siteId_idx` ON `ocast_db`.`trn_Site_Device` (`SiteID` ASC) ;

CREATE INDEX `deviceId_idx` ON `ocast_db`.`trn_Site_Device` (`DeviceID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Site_Lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Site_Lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Site_Lang` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `SiteID` BIGINT NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `Description` TEXT NULL ,
  `Audience` VARCHAR(250) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `langCode_idx` ON `ocast_db`.`trn_Site_Lang` (`LangCode` ASC) ;

CREATE INDEX `siteId_idx` ON `ocast_db`.`trn_Site_Lang` (`SiteID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`sys_Registration_History`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`sys_Registration_History` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`sys_Registration_History` (
  `ID` INT(5) NOT NULL ,
  `UserID` INT(5) NULL ,
  `StepNumber` INT(1) NULL ,
  `CreatedDate` DATETIME NULL ,
  `LastModifiedDate` DATETIME NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `Fk_userId` ON `ocast_db`.`sys_Registration_History` (`UserID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_Subscription_Plan_Lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_Subscription_Plan_Lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_Subscription_Plan_Lang` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `PlanID` INT(5) NULL ,
  `Description` VARCHAR(250) NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `CreatedDate` DATE NULL ,
  `CreatedBy` INT(5) NOT NULL ,
  `LastModifiedDate` DATE NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `planId_idx` ON `ocast_db`.`set_Subscription_Plan_Lang` (`PlanID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Invoice`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Invoice` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Invoice` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `SiteID` BIGINT NULL ,
  `Description` VARCHAR(250) NULL ,
  `Amount` DECIMAL(5,2) NULL ,
  `Duedate` DATETIME NULL ,
  `InvoceNo` VARCHAR(25) NULL ,
  `PaidDate` DATETIME NULL ,
  `TransactionID` VARCHAR(20) NULL ,
  `PaymentStatus` INT(1) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `siteId_idx` ON `ocast_db`.`trn_Invoice` (`SiteID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`sys_Country_Lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`sys_Country_Lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`sys_Country_Lang` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `CountryID` INT(5) NULL ,
  `CountryName` VARCHAR(150) CHARACTER SET 'utf8' NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `CreationDate` DATETIME NULL ,
  `LastModifiedDate` DATETIME NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `countryId_idx` ON `ocast_db`.`sys_Country_Lang` (`CountryID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`sys_Device_Lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`sys_Device_Lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`sys_Device_Lang` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `DeviceID` INT(5) NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `DeviceName` VARCHAR(45) NULL ,
  `CreatedDate` DATETIME NULL ,
  `LastModifiedDate` DATETIME NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `deviceId_idx` ON `ocast_db`.`sys_Device_Lang` (`DeviceID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_Category_Lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_Category_Lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_Category_Lang` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `CategoryID` INT(5) NULL ,
  `Description` VARCHAR(45) NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `categoryID_idx` ON `ocast_db`.`set_Category_Lang` (`CategoryID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_Format_Lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_Format_Lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_Format_Lang` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `FormatID` INT(5) NULL ,
  `Langcode` VARCHAR(5) NULL ,
  `Title` VARCHAR(200) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `FormatID_idx` ON `ocast_db`.`set_Format_Lang` (`FormatID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_Price_Type_Lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_Price_Type_Lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_Price_Type_Lang` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `text` VARCHAR(100) NULL ,
  `priceTypeID` INT(5) NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `creationDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `lastModifiedDate` DATETIME NULL ,
  `lastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_FAQ_Category_Lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_FAQ_Category_Lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_FAQ_Category_Lang` (
  `ID` INT(5) NOT NULL ,
  `FAQCategoryID` INT(5) NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `CategoryName` VARCHAR(250) CHARACTER SET 'utf8' NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastUpdated` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `FAQCategoryID_idx` ON `ocast_db`.`set_FAQ_Category_Lang` (`FAQCategoryID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Analytics_Archive`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Analytics_Archive` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Analytics_Archive` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `SiteID` BIGINT NULL ,
  `UniqueVisits` INT NULL ,
  `PageViews` INT NULL ,
  `Visits` INT NULL ,
  `Date` DATE NULL ,
  `UpdatedTime` DATETIME NULL ,
  `AnalyticsID` BIGINT NULL ,
  `TransferredDate` DATETIME NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `siteID` ON `ocast_db`.`trn_Analytics_Archive` (`SiteID` ASC) ;

CREATE INDEX `date` ON `ocast_db`.`trn_Analytics_Archive` (`Date` ASC) ;

CREATE INDEX `AnalyticsID` ON `ocast_db`.`trn_Analytics_Archive` (`AnalyticsID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`tmp_Account_info`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`tmp_Account_info` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`tmp_Account_info` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `ActivationCode` VARCHAR(250) NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `UserName` VARCHAR(150) NULL ,
  `Password` TEXT NULL ,
  `Fname` VARCHAR(20) NULL ,
  `Lname` VARCHAR(20) NULL ,
  `Company` VARCHAR(100) NULL ,
  `PhoneCountryCode` VARCHAR(6) NULL ,
  `PhoneNumber` VARCHAR(10) NULL ,
  `Address1` VARCHAR(100) NULL ,
  `Address2` VARCHAR(100) NULL ,
  `Zip` INT(8) NULL ,
  `City` VARCHAR(45) NULL ,
  `CountryID` INT(1) NULL ,
  `AciveStatus` ENUM('active', 'pending', 'inactive') NULL ,
  `SessionID` VARCHAR(100) NULL ,
  `RemorteIP` VARCHAR(15) NULL ,
  `UserAgent` VARCHAR(100) NULL ,
  `CreatedDate` DATETIME NULL ,
  `LastModifiedDate` DATETIME NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE UNIQUE INDEX `ActivationCode` ON `ocast_db`.`tmp_Account_info` (`ActivationCode` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`sys_Module_lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`sys_Module_lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`sys_Module_lang` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `ModuleName` VARCHAR(45) NULL ,
  `ModuleID` INT(5) NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `DateCreated` DATETIME NULL ,
  `LastUpdated` DATETIME NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `ModuleID_idx` ON `ocast_db`.`sys_Module_lang` (`ModuleID` ASC) ;

USE `ocast_db` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
>>>>>>> 47c1b3c39a3c648039726db8a102014da5674ba7
