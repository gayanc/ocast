SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `ocast_db` ;
CREATE SCHEMA IF NOT EXISTS `ocast_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `ocast_db` ;

-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Company`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Company` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Company` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `Name` VARCHAR(150) NULL ,
  `Phone` VARCHAR(30) NULL ,
  `Address1` VARCHAR(200) NULL ,
  `Address2` VARCHAR(200) NULL ,
  `zip` VARCHAR(6) NULL ,
  `CountryId` INT(5) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `ind_country` ON `ocast_db`.`trn_Company` (`CountryId` ASC);

-- -----------------------------------------------------
-- Table `ocast_db`.`trn_GoogleProfile`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_GoogleProfile` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_GoogleProfile` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `GoogleUserID` VARCHAR(100) NULL ,
  `GoogleProfile` BIGINT NULL ,
  `ProfileName` VARCHAR(100) NULL ,
  `Token` TEXT NULL ,
  `Status` VARCHAR(10) NULL ,
  `CompanyId` INT(5) NULL ,
  `CreationDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `google_profile` ON `ocast_db`.`trn_GoogleProfile` (`GoogleProfile` ASC) ;

CREATE INDEX `user_id` ON `ocast_db`.`trn_GoogleProfile` (`GoogleUserID` ASC) ;

CREATE INDEX `companyId_idx` ON `ocast_db`.`trn_GoogleProfile` (`CompanyId` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_SubscriptionPlan`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_SubscriptionPlan` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_SubscriptionPlan` (
  `ID` INT(5) NOT NULL ,
  `SortingOrder` INT(5) NULL ,
  `Duration` INT(2) NULL ,
  `Price` DECIMAL(5,2) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Site`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Site` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Site` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `ParentID` BIGINT NULL ,
  `ProfileID` BIGINT NULL ,
  `GoogleSiteID` VARCHAR(12) NULL ,
  `URL` VARCHAR(200) NULL ,
  `Logo` VARCHAR(100) NULL ,
  `TargetAge` VARCHAR(45) NULL ,
  `MalePercentage` DECIMAL(5,2) NULL ,
  `IsMobile` ENUM('0','1') NULL ,
  `SubscriptionPlanId` INT(3) NULL ,
  `ValidPeriod` INT(2) NULL ,
  `ActivatedDate` DATE NULL ,
  `ActivationStatus` VARCHAR(15) NULL ,
  `companyId` INT(5) NULL ,
  `ExpiryDate` DATE NULL ,
  `LastGoogleUpdate` DATETIME NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `profileId_idx` ON `ocast_db`.`trn_Site` (`ProfileID` ASC) ;

CREATE INDEX `goog_site_id` ON `ocast_db`.`trn_Site` (`GoogleSiteID` ASC) ;

CREATE INDEX `company_id` ON `ocast_db`.`trn_Site` (`companyId` ASC) ;

CREATE INDEX `subscriptionPlanId_idx` ON `ocast_db`.`trn_Site` (`SubscriptionPlanId` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_SiteContact`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_SiteContact` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_SiteContact` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `SiteID` BIGINT NULL ,
  `Fname` VARCHAR(45) NULL ,
  `IsDefault` ENUM('0','1') NULL ,
  `Lname` VARCHAR(45) NULL ,
  `Email` VARCHAR(75) NULL ,
  `ConcatNumber` VARCHAR(25) NULL ,
  `ProfilePic` VARCHAR(45) NULL ,
  `ContactCountryCode` VARCHAR(15) NULL ,
  `SortingOrder` INT(5) NULL ,
  `CreatedBy` INT(5) NULL ,
  `CreatedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `site_id` ON `ocast_db`.`trn_SiteContact` (`SiteID` ASC) ;

CREATE INDEX `FK_site_id_idx` ON `ocast_db`.`trn_SiteContact` (`SiteID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_FAQCategory`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_FAQCategory` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_FAQCategory` (
  `ID` INT(5) NOT NULL ,
  `SortingOrder` INT(5) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastUpdated` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_FAQ`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_FAQ` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_FAQ` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `SortingOrder` INT(5) NULL ,
  `FAQCategoryID` INT(5) NULL ,
  `IsEnable` ENUM('0','1') NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `FAQCategoryID_idx` ON `ocast_db`.`set_FAQ` (`FAQCategoryID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_Category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_Category` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_Category` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `SortingOrder` INT(5) NULL ,
  `LastUpdate` DATETIME NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_SiteCategory`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_SiteCategory` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_SiteCategory` (
  `ID` BIGINT NOT NULL ,
  `SiteID` BIGINT NULL ,
  `CategoryID` BIGINT NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `site_id` ON `ocast_db`.`trn_SiteCategory` (`SiteID` ASC) ;

CREATE INDEX `category_id` ON `ocast_db`.`trn_SiteCategory` (`CategoryID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`sys_Country`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`sys_Country` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`sys_Country` (
  `ID` INT(5) NOT NULL ,
  `ISO` VARCHAR(2) NOT NULL ,
  `CountryCode` VARCHAR(10) NULL ,
  `IsActive` ENUM('0','1') NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `CreatedDate` DATETIME NULL ,
  PRIMARY KEY (`ID`, `ISO`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_SiteCountry`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_SiteCountry` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_SiteCountry` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `CountryID` BIGINT NULL ,
  `SiteID` BIGINT NOT NULL ,
  `IsDefault` ENUM('0','1') NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `country_id` ON `ocast_db`.`trn_SiteCountry` (`CountryID` ASC) ;

CREATE INDEX `fk_trn_siteCountry_trn_site1_idx` ON `ocast_db`.`trn_SiteCountry` (`SiteID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_QuickFact`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_QuickFact` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_QuickFact` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `SiteID` BIGINT NULL ,
  `LastUpdated` DATE NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `site_id` ON `ocast_db`.`trn_QuickFact` (`SiteID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Testimonial`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Testimonial` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Testimonial` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `SiteID` INT(5) NULL ,
  `UpdateDate` DATE NULL ,
  `CreationDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `site_id` ON `ocast_db`.`trn_Testimonial` (`SiteID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_DemographicHeader`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_DemographicHeader` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_DemographicHeader` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `SiteId` BIGINT NULL ,
  `CreationDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastUpdated` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `site_id` ON `ocast_db`.`trn_DemographicHeader` (`SiteId` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_DemographicDetail`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_DemographicDetail` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_DemographicDetail` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `HeaderId` BIGINT NULL ,
  `Points` DECIMAL(4,2) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `header_id` ON `ocast_db`.`trn_DemographicDetail` (`HeaderId` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_PriceType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_PriceType` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_PriceType` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_Format`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_Format` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_Format` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Format`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Format` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Format` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `SiteID` BIGINT NULL ,
  `TypeID` BIGINT NULL ,
  `PriceTypeID` BIGINT NULL ,
  `Image` VARCHAR(100) NULL ,
  `Price` DECIMAL(6,2) NULL ,
  `CreationDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `site_id` ON `ocast_db`.`trn_Format` (`SiteID` ASC) ;

CREATE INDEX `price_type_id` ON `ocast_db`.`trn_Format` (`PriceTypeID` ASC) ;

CREATE INDEX `FK_type_id_idx` ON `ocast_db`.`trn_Format` (`TypeID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Case`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Case` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Case` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `SiteID` BIGINT NULL ,
  `Image` VARCHAR(100) NULL ,
  `SortingOrder` INT(5) NULL ,
  `CreatedBy` INT(5) NULL ,
  `CreatedDate` DATETIME NULL ,
  `LastUpdated` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `site_id` ON `ocast_db`.`trn_Case` (`SiteID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Analytics`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Analytics` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Analytics` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `SiteId` BIGINT NULL ,
  `UniqueVisits` INT NULL ,
  `PageViews` INT NULL ,
  `Visits` INT NULL ,
  `Date` DATE NULL ,
  `UpdatedTime` DATETIME NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `siteID` ON `ocast_db`.`trn_Analytics` (`SiteId` ASC) ;

CREATE INDEX `date` ON `ocast_db`.`trn_Analytics` (`Date` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_UserType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_UserType` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_UserType` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `UserTypeName` VARCHAR(25) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_User`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_User` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_User` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `UserName` VARCHAR(150) NULL ,
  `Password` TEXT NULL ,
  `Fname` VARCHAR(20) NULL ,
  `Lname` VARCHAR(20) NULL ,
  `AciveStatus` VARCHAR(20) NULL ,
  `UserTypeId` INT(5) NULL ,
  `CompanyId` INT(5) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE UNIQUE INDEX `userNameUNQ` ON `ocast_db`.`set_User` (`UserName` ASC) ;

CREATE INDEX `companyId_idx` ON `ocast_db`.`set_User` (`CompanyId` ASC) ;

CREATE INDEX `userTypeId_idx` ON `ocast_db`.`set_User` (`UserTypeId` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`sys_Language`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`sys_Language` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`sys_Language` (
  `LangCode` VARCHAR(5) NOT NULL ,
  `LangNameEng` VARCHAR(50) NULL ,
  `LangName` VARCHAR(70) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL ,
  `CreatedDate` DATETIME NULL ,
  `LastModifiedDate` DATETIME NULL ,
  PRIMARY KEY (`LangCode`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_UserHistory`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_UserHistory` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_UserHistory` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `Title` VARCHAR(250) NULL ,
  `UserID` INT(5) NULL ,
  `Description` VARCHAR(250) NULL ,
  `URL` TEXT NULL ,
  `LastUpdate` DATETIME NULL ,
  `SiteID` INT(5) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `userId` ON `ocast_db`.`trn_UserHistory` (`SiteID` ASC) ;

CREATE INDEX `siteId_idx` ON `ocast_db`.`trn_UserHistory` (`UserID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_FAQLang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_FAQLang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_FAQLang` (
  `ID` INT(5) NOT NULL ,
  `FaqID` INT(5) NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `Question` TEXT CHARACTER SET 'utf8' NULL ,
  `Answer` TEXT CHARACTER SET 'utf8' NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `langCode` ON `ocast_db`.`set_FAQLang` (`LangCode` ASC) ;

CREATE INDEX `fqaID_idx` ON `ocast_db`.`set_FAQLang` (`FaqID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_CaseLang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_CaseLang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_CaseLang` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `CaseID` BIGINT NULL ,
  `Title` VARCHAR(250) CHARACTER SET 'utf8' NULL ,
  `Description` TEXT CHARACTER SET 'utf8' NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `langCode` ON `ocast_db`.`trn_CaseLang` (`LangCode` ASC) ;

CREATE INDEX `caseId_idx` ON `ocast_db`.`trn_CaseLang` (`CaseID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_CompanyLang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_CompanyLang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_CompanyLang` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `CompanyId` BIGINT NULL ,
  `Name` VARCHAR(150) CHARACTER SET 'utf8' NULL ,
  `Address1` VARCHAR(200) CHARACTER SET 'utf8' NULL ,
  `Address2` VARCHAR(200) NULL ,
  `CreationDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `companyId_idx` ON `ocast_db`.`trn_CompanyLang` (`CompanyId` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_DemographicHeaderLang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_DemographicHeaderLang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_DemographicHeaderLang` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `DemographicHeaderID` BIGINT NOT NULL ,
  `HeaderLine` VARCHAR(250) CHARACTER SET 'utf8' NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `CreationDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `langCode` ON `ocast_db`.`trn_DemographicHeaderLang` (`LangCode` ASC) ;

CREATE INDEX `demographicHeaderID_idx` ON `ocast_db`.`trn_DemographicHeaderLang` (`DemographicHeaderID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_DemographicDetailLang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_DemographicDetailLang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_DemographicDetailLang` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `DemographicDetailId` BIGINT NULL ,
  `Answer` VARCHAR(250) CHARACTER SET 'utf8' NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `langCode` ON `ocast_db`.`trn_DemographicDetailLang` (`LangCode` ASC) ;

CREATE INDEX `demographicDetailId_idx` ON `ocast_db`.`trn_DemographicDetailLang` (`DemographicDetailId` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_QuickFactLang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_QuickFactLang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_QuickFactLang` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `QuickFactId` BIGINT NOT NULL ,
  `Text` TEXT CHARACTER SET 'utf8' NULL ,
  `LangCode` VARCHAR(5) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `langCode` ON `ocast_db`.`trn_QuickFactLang` (`LangCode` ASC) ;

CREATE INDEX `quickFactId_idx` ON `ocast_db`.`trn_QuickFactLang` (`QuickFactId` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_SiteContactLang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_SiteContactLang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_SiteContactLang` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `SiteContactID` BIGINT NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `Designation` VARCHAR(45) CHARACTER SET 'utf8' NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `lanCode` ON `ocast_db`.`trn_SiteContactLang` (`LangCode` ASC) ;

CREATE INDEX `SiteContactID_idx` ON `ocast_db`.`trn_SiteContactLang` (`SiteContactID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_TestimonialLang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_TestimonialLang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_TestimonialLang` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `TestimonialID` INT(5) NULL ,
  `Quote` TEXT CHARACTER SET 'utf8' NULL ,
  `ByWhom` VARCHAR(45) CHARACTER SET 'utf8' NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `langCode` ON `ocast_db`.`trn_TestimonialLang` (`LangCode` ASC) ;

CREATE INDEX `testimonialId_idx` ON `ocast_db`.`trn_TestimonialLang` (`TestimonialID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_UserSite`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_UserSite` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_UserSite` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `UserID` INT(5) NULL ,
  `SiteID` INT(5) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `userId_idx` ON `ocast_db`.`set_UserSite` (`UserID` ASC) ;

CREATE INDEX `siteId_idx` ON `ocast_db`.`set_UserSite` (`SiteID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_MenuItem`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_MenuItem` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_MenuItem` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `MenuName` VARCHAR(100) NULL ,
  `ParentID` INT(5) NOT NULL ,
  `CreatedDate` DATETIME NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  `CreatedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_UserPermissions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_UserPermissions` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_UserPermissions` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `MenuItemID` INT(5) NULL ,
  `UserTypeID` INT(5) NULL ,
  `IsAllowed` ENUM('0','1') NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `menuItemId_idx` ON `ocast_db`.`set_UserPermissions` (`MenuItemID` ASC) ;

CREATE INDEX `userTypeId_idx` ON `ocast_db`.`set_UserPermissions` (`UserTypeID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_Component`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_Component` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_Component` (
  `id` INT(5) NOT NULL AUTO_INCREMENT ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ocast_db`.`sys_Device`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`sys_Device` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`sys_Device` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `SortingOrder` INT(5) NULL ,
  `CreatedDate` DATETIME NULL ,
  `LastModifiedDate` DATETIME NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_SiteDevice`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_SiteDevice` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_SiteDevice` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `SiteID` BIGINT NULL ,
  `DeviceID` BIGINT NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `siteId_idx` ON `ocast_db`.`trn_SiteDevice` (`SiteID` ASC) ;

CREATE INDEX `deviceId_idx` ON `ocast_db`.`trn_SiteDevice` (`DeviceID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_SiteLang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_SiteLang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_SiteLang` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `SiteID` BIGINT NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `Description` TEXT NULL ,
  `Audience` VARCHAR(250) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `langCode_idx` ON `ocast_db`.`trn_SiteLang` (`LangCode` ASC) ;

CREATE INDEX `siteId_idx` ON `ocast_db`.`trn_SiteLang` (`SiteID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`sys_RegistrationHistory`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`sys_RegistrationHistory` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`sys_RegistrationHistory` (
  `ID` INT(5) NOT NULL ,
  `UserID` INT(5) NULL ,
  `StepNumber` INT(1) NULL ,
  `CreatedDate` DATETIME NULL ,
  `LastModifiedDate` DATETIME NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `Fk_userId` ON `ocast_db`.`sys_RegistrationHistory` (`UserID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_SubscriptionPlanLang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_SubscriptionPlanLang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_SubscriptionPlanLang` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `PlanID` INT(5) NULL ,
  `Description` VARCHAR(250) NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `CreatedDate` DATE NULL ,
  `CreatedBy` INT(5) NOT NULL ,
  `LastModifiedDate` DATE NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `planId_idx` ON `ocast_db`.`set_SubscriptionPlanLang` (`PlanID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Invoice`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Invoice` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Invoice` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `SiteID` BIGINT NULL ,
  `Description` VARCHAR(250) NULL ,
  `Amount` DECIMAL(5,2) NULL ,
  `Duedate` DATETIME NULL ,
  `InvoceNo` VARCHAR(25) NULL ,
  `PaidDate` DATETIME NULL ,
  `TransactionID` VARCHAR(20) NULL ,
  `PaymentStatus` INT(1) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `siteId_idx` ON `ocast_db`.`trn_Invoice` (`SiteID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`sys_CountryLang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`sys_CountryLang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`sys_CountryLang` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `CountryID` INT(5) NULL ,
  `CountryName` VARCHAR(150) CHARACTER SET 'utf8' NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `CreationDate` DATETIME NULL ,
  `LastModifiedDate` DATETIME NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `countryId_idx` ON `ocast_db`.`sys_CountryLang` (`CountryID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`sys_DeviceLang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`sys_DeviceLang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`sys_DeviceLang` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `DeviceID` INT(5) NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `DeviceName` VARCHAR(45) NULL ,
  `CreatedDate` DATETIME NULL ,
  `LastModifiedDate` DATETIME NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `deviceId_idx` ON `ocast_db`.`sys_DeviceLang` (`DeviceID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_CategoryLang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_CategoryLang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_CategoryLang` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `CategoryID` INT(5) NULL ,
  `Description` VARCHAR(45) NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `categoryID_idx` ON `ocast_db`.`set_CategoryLang` (`CategoryID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_FormatLang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_FormatLang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_FormatLang` (
  `id` INT(5) NOT NULL AUTO_INCREMENT ,
  `FormatID` INT(5) NULL ,
  `Langcode` VARCHAR(5) NULL ,
  `Title` VARCHAR(200) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;

CREATE INDEX `FormatID_idx` ON `ocast_db`.`set_FormatLang` (`FormatID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_PriceTypeLang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_PriceTypeLang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_PriceTypeLang` (
  `id` INT(5) NOT NULL AUTO_INCREMENT ,
  `text` VARCHAR(100) NULL ,
  `priceTypeID` INT(5) NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `creationDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `lastModifiedDate` DATETIME NULL ,
  `lastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_FAQCategoryLang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_FAQCategoryLang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_FAQCategoryLang` (
  `ID` INT(5) NOT NULL ,
  `FAQCategoryID` INT(5) NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `CategoryName` VARCHAR(250) CHARACTER SET 'utf8' NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastUpdated` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `FAQCategoryID_idx` ON `ocast_db`.`set_FAQCategoryLang` (`FAQCategoryID` ASC) ;

USE `ocast_db` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
