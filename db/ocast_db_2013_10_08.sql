-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: ocstepxdb
-- ------------------------------------------------------
-- Server version	5.1.69

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `set_category`
--

DROP TABLE IF EXISTS `set_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `set_category` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `SortingOrder` int(5) DEFAULT NULL,
  `LastUpdate` datetime DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `set_category_lang`
--

DROP TABLE IF EXISTS `set_category_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `set_category_lang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `CategoryID` int(5) DEFAULT NULL,
  `Description` varchar(45) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `categoryID_idx` (`CategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

 

--
-- Table structure for table `set_component`
--

DROP TABLE IF EXISTS `set_component`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `set_component` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `set_faq`
--

DROP TABLE IF EXISTS `set_faq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `set_faq` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `SortingOrder` int(5) DEFAULT NULL,
  `FAQCategoryID` int(5) DEFAULT NULL,
  `IsEnable` enum('0','1') DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FAQCategoryID_idx` (`FAQCategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `set_faq_category`
--

DROP TABLE IF EXISTS `set_faq_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `set_faq_category` (
  `ID` int(5) NOT NULL,
  `SortingOrder` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `set_faq_category_lang`
--

DROP TABLE IF EXISTS `set_faq_category_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `set_faq_category_lang` (
  `ID` int(5) NOT NULL,
  `FAQCategoryID` int(5) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CategoryName` varchar(250) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FAQCategoryID_idx` (`FAQCategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `set_faq_lang`
--

DROP TABLE IF EXISTS `set_faq_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `set_faq_lang` (
  `ID` int(5) NOT NULL,
  `FaqID` int(5) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `Question` text,
  `Answer` text,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `fqaID_idx` (`FaqID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `set_faqlang`
--

DROP TABLE IF EXISTS `set_faqlang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `set_faqlang` (
  `ID` int(5) NOT NULL,
  `FaqID` int(5) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `Title` varchar(250) DEFAULT NULL,
  `Description` text,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `fqaID_idx` (`FaqID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `set_format`
--

DROP TABLE IF EXISTS `set_format`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `set_format` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `set_format_lang`
--

DROP TABLE IF EXISTS `set_format_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `set_format_lang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `FormatID` int(5) DEFAULT NULL,
  `Langcode` varchar(5) DEFAULT NULL,
  `Title` varchar(200) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FormatID_idx` (`FormatID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `set_formatlang`
--

DROP TABLE IF EXISTS `set_formatlang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `set_formatlang` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `Title` varchar(200) DEFAULT NULL,
  `FormatID` int(5) DEFAULT NULL,
  `Langcode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `lastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FormatID_idx` (`FormatID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `set_location`
--

DROP TABLE IF EXISTS `set_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `set_location` (
  `location_id` int(5) NOT NULL AUTO_INCREMENT,
  `ISO2` char(2) NOT NULL,
  `location_name` varchar(100) NOT NULL,
  `location_long_name` varchar(100) NOT NULL,
  `ISO3` char(3) NOT NULL,
  `NumCode` varchar(6) NOT NULL,
  `UNMemberState` varchar(12) NOT NULL,
  `CallingCode` varchar(8) NOT NULL,
  `CCTLD` varchar(5) NOT NULL,
  `enable` enum('1','0') NOT NULL DEFAULT '0',
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`location_id`),
  KEY `indlocation_id` (`location_id`),
  KEY `indlocation_name` (`location_name`)
) ENGINE=InnoDB AUTO_INCREMENT=251 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `set_menu_item`
--

DROP TABLE IF EXISTS `set_menu_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `set_menu_item` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `MenuName` varchar(100) DEFAULT NULL,
  `ParentID` int(5) NOT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `set_menuitem`
--

DROP TABLE IF EXISTS `set_menuitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `set_menuitem` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `MenuName` varchar(100) DEFAULT NULL,
  `ParentID` int(5) NOT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `set_price_type`
--

DROP TABLE IF EXISTS `set_price_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `set_price_type` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `set_price_type_lang`
--

DROP TABLE IF EXISTS `set_price_type_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `set_price_type_lang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `text` varchar(100) DEFAULT NULL,
  `priceTypeID` int(5) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `lastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `set_pricetype`
--

DROP TABLE IF EXISTS `set_pricetype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `set_pricetype` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `set_pricetypelang`
--

DROP TABLE IF EXISTS `set_pricetypelang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `set_pricetypelang` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `text` varchar(100) DEFAULT NULL,
  `priceTypeID` int(5) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `lastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `set_subscription_plan`
--

DROP TABLE IF EXISTS `set_subscription_plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `set_subscription_plan` (
  `ID` int(5) NOT NULL,
  `SortingOrder` int(5) DEFAULT NULL,
  `Duration` int(2) DEFAULT NULL,
  `Price` decimal(5,2) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `set_subscription_plan_lang`
--

DROP TABLE IF EXISTS `set_subscription_plan_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `set_subscription_plan_lang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `PlanID` int(5) DEFAULT NULL,
  `Description` varchar(250) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` date DEFAULT NULL,
  `CreatedBy` int(5) NOT NULL,
  `LastModifiedDate` date DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `planId_idx` (`PlanID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `set_subscriptionplan`
--

DROP TABLE IF EXISTS `set_subscriptionplan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `set_subscriptionplan` (
  `ID` int(5) NOT NULL,
  `Duration` int(2) DEFAULT NULL,
  `Price` decimal(5,2) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `set_subscriptionplanlang`
--

DROP TABLE IF EXISTS `set_subscriptionplanlang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `set_subscriptionplanlang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `PlanID` int(5) DEFAULT NULL,
  `Description` varchar(250) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` date DEFAULT NULL,
  `LastModifiedDate` date DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `planId_idx` (`PlanID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `set_user`
--

DROP TABLE IF EXISTS `set_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `set_user` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(150) DEFAULT NULL,
  `Password` text,
  `Fname` varchar(20) DEFAULT NULL,
  `Lname` varchar(20) DEFAULT NULL,
  `AciveStatus` varchar(20) DEFAULT NULL,
  `UserTypeID` int(5) DEFAULT NULL,
  `CompanyID` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `PasswordResetCode` varchar(250) DEFAULT NULL,
  `ResetRequestDate` datetime DEFAULT NULL,
  `LastLogin` datetime DEFAULT NULL,
  `LastLoginIP` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `userNameUNQ` (`UserName`),
  KEY `companyId_idx` (`CompanyID`),
  KEY `userTypeId_idx` (`UserTypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `set_user_permissions`
--

DROP TABLE IF EXISTS `set_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `set_user_permissions` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `ModuleID` int(5) DEFAULT NULL,
  `UserTypeID` int(5) DEFAULT NULL,
  `IsAllowed` enum('0','1') DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `userTypeId_idx` (`UserTypeID`),
  KEY `ModuleID_idx` (`ModuleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `set_user_site`
--

DROP TABLE IF EXISTS `set_user_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `set_user_site` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `UserID` int(5) DEFAULT NULL,
  `SiteID` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `userId_idx` (`UserID`),
  KEY `siteId_idx` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `set_user_type`
--

DROP TABLE IF EXISTS `set_user_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `set_user_type` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `UserTypeName` varchar(25) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `set_userpermissions`
--

DROP TABLE IF EXISTS `set_userpermissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `set_userpermissions` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `MenuItemID` int(5) DEFAULT NULL,
  `UserTypeID` int(5) DEFAULT NULL,
  `IsAllowed` int(1) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `menuItemId_idx` (`MenuItemID`),
  KEY `userTypeId_idx` (`UserTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `set_usersite`
--

DROP TABLE IF EXISTS `set_usersite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `set_usersite` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `UserID` int(5) DEFAULT NULL,
  `SiteID` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `userId_idx` (`UserID`),
  KEY `siteId_idx` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `set_usertype`
--

DROP TABLE IF EXISTS `set_usertype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `set_usertype` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `UserTypeName` varchar(25) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_country`
--

DROP TABLE IF EXISTS `sys_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_country` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `ISO` varchar(2) NOT NULL,
  `CallingCode` varchar(8) DEFAULT NULL,
  `IsActive` enum('0','1') DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`,`ISO`)
) ENGINE=InnoDB AUTO_INCREMENT=251 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_country_lang`
--

DROP TABLE IF EXISTS `sys_country_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_country_lang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `CountryID` int(5) DEFAULT NULL,
  `CountryName` varchar(150) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `countryId_idx` (`CountryID`)
) ENGINE=InnoDB AUTO_INCREMENT=751 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_countrylang`
--

DROP TABLE IF EXISTS `sys_countrylang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_countrylang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `CountryID` int(5) DEFAULT NULL,
  `CountryName` varchar(150) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `countryId_idx` (`CountryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_device`
--

DROP TABLE IF EXISTS `sys_device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_device` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `SortingOrder` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_device_lang`
--

DROP TABLE IF EXISTS `sys_device_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_device_lang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `DeviceID` int(5) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `DeviceName` varchar(45) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `deviceId_idx` (`DeviceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_devicelang`
--

DROP TABLE IF EXISTS `sys_devicelang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_devicelang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `DeviceID` int(5) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `DeviceName` varchar(45) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `deviceId_idx` (`DeviceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_failed_payment`
--

DROP TABLE IF EXISTS `sys_failed_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_failed_payment` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `InvoiceID` bigint(20) DEFAULT NULL,
  `FaileddDate` date DEFAULT NULL,
  `Status` enum('Active','Inactive','Pending') DEFAULT NULL,
  `IsPaid` enum('0','1') DEFAULT NULL,
  `Notes` text,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `InvoiceID_idx` (`InvoiceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_language`
--

DROP TABLE IF EXISTS `sys_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_language` (
  `LangCode` varchar(5) NOT NULL,
  `LangNameEng` varchar(50) DEFAULT NULL,
  `LangName` varchar(70) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`LangCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_module`
--

DROP TABLE IF EXISTS `sys_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_module` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `ModuleCode` varchar(10) DEFAULT NULL,
  `ParentID` int(5) DEFAULT NULL,
  `URL` text,
  `SortingOrder` int(5) DEFAULT NULL,
  `DateCreated` datetime DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_module_lang`
--

DROP TABLE IF EXISTS `sys_module_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_module_lang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `ModuleName` varchar(45) DEFAULT NULL,
  `ModuleID` int(5) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `DateCreated` datetime DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ModuleID_idx` (`ModuleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_registration_history`
--

DROP TABLE IF EXISTS `sys_registration_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_registration_history` (
  `ID` int(5) NOT NULL,
  `UserID` int(5) DEFAULT NULL,
  `StepNumber` int(1) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Fk_userId` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_registrationhistory`
--

DROP TABLE IF EXISTS `sys_registrationhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_registrationhistory` (
  `ID` int(5) NOT NULL,
  `UserID` int(5) DEFAULT NULL,
  `StepNumber` int(1) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Fk_userId` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_account_info`
--

DROP TABLE IF EXISTS `tmp_account_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_account_info` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `ActivationCode` varchar(250) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `UserName` varchar(150) DEFAULT NULL,
  `Password` text,
  `Fname` varchar(20) DEFAULT NULL,
  `Lname` varchar(20) DEFAULT NULL,
  `Company` varchar(100) DEFAULT NULL,
  `PhoneCountryCode` varchar(6) DEFAULT NULL,
  `PhoneNumber` varchar(10) DEFAULT NULL,
  `Address1` varchar(100) DEFAULT NULL,
  `Address2` varchar(100) DEFAULT NULL,
  `Zip` int(8) DEFAULT NULL,
  `City` varchar(45) DEFAULT NULL,
  `CountryID` int(1) DEFAULT NULL,
  `AciveStatus` enum('active','pending','inactive') DEFAULT NULL,
  `SessionID` varchar(100) DEFAULT NULL,
  `RemorteIP` varchar(15) DEFAULT NULL,
  `UserAgent` varchar(100) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `RequestedURL` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ActivationCode` (`ActivationCode`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_case`
--

DROP TABLE IF EXISTS `tmp_case`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_case` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `SiteID` bigint(20) DEFAULT NULL,
  `Image` varchar(100) DEFAULT NULL,
  `SortingOrder` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_case_lang`
--

DROP TABLE IF EXISTS `tmp_case_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_case_lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `CaseID` bigint(20) DEFAULT NULL,
  `Title` varchar(250) DEFAULT NULL,
  `Description` text,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `caseId_idx` (`CaseID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_company`
--

DROP TABLE IF EXISTS `tmp_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_company` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `Phone` varchar(30) DEFAULT NULL,
  `zip` varchar(6) DEFAULT NULL,
  `CountryID` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  `CountryCallingCode` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ind_country` (`CountryID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_company_lang`
--

DROP TABLE IF EXISTS `tmp_company_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_company_lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `CompanyID` bigint(20) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `Name` varchar(150) DEFAULT NULL,
  `Address1` varchar(200) DEFAULT NULL,
  `Address2` varchar(200) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `companyId_idx` (`CompanyID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_demographic_detail`
--

DROP TABLE IF EXISTS `tmp_demographic_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_demographic_detail` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `HeaderID` bigint(20) DEFAULT NULL,
  `Points` decimal(4,2) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `header_id` (`HeaderID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_demographic_detail_lang`
--

DROP TABLE IF EXISTS `tmp_demographic_detail_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_demographic_detail_lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `DemographicDetailID` bigint(20) DEFAULT NULL,
  `Answer` varchar(250) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `demographicDetailId_idx` (`DemographicDetailID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_demographic_header`
--

DROP TABLE IF EXISTS `tmp_demographic_header`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_demographic_header` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `SiteID` bigint(20) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_demographic_header_lang`
--

DROP TABLE IF EXISTS `tmp_demographic_header_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_demographic_header_lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `DemographicHeaderID` bigint(20) NOT NULL,
  `HeadLine` varchar(250) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `demographicHeaderID_idx` (`DemographicHeaderID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_format`
--

DROP TABLE IF EXISTS `tmp_format`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_format` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `SiteID` bigint(20) DEFAULT NULL,
  `TypeID` bigint(20) DEFAULT NULL,
  `PriceTypeID` bigint(20) DEFAULT NULL,
  `Image` varchar(100) DEFAULT NULL,
  `Price` decimal(6,2) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`),
  KEY `price_type_id` (`PriceTypeID`),
  KEY `FK_type_id_idx` (`TypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_quick_fact`
--

DROP TABLE IF EXISTS `tmp_quick_fact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_quick_fact` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `SiteID` bigint(20) DEFAULT NULL,
  `LastUpdated` date DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_quick_fact_lang`
--

DROP TABLE IF EXISTS `tmp_quick_fact_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_quick_fact_lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `QuickFactID` bigint(20) NOT NULL,
  `Text` text,
  `LangCode` varchar(5) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `quickFactId_idx` (`QuickFactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_site`
--

DROP TABLE IF EXISTS `tmp_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_site` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `ParentID` bigint(20) DEFAULT NULL,
  `ProfileID` bigint(20) DEFAULT NULL,
  `GoogleSiteID` varchar(12) DEFAULT NULL,
  `AnalyticsCreatedDate` date DEFAULT NULL,
  `URL` varchar(200) DEFAULT NULL,
  `Logo` varchar(100) DEFAULT NULL,
  `AverageAge` int(2) DEFAULT NULL,
  `MalePercentage` decimal(5,2) DEFAULT NULL,
  `IsMobile` enum('0','1') DEFAULT NULL,
  `SubscriptionPlanID` int(3) DEFAULT NULL,
  `ValidPeriod` int(2) DEFAULT NULL,
  `ActivatedDate` date DEFAULT NULL,
  `ActivationStatus` varchar(15) DEFAULT NULL,
  `CompanyID` int(5) DEFAULT NULL,
  `ExpiryDate` date DEFAULT NULL,
  `LastGoogleUpdate` datetime DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Isdownloading` enum('0','1') DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `profileId_idx` (`ProfileID`),
  KEY `goog_site_id` (`GoogleSiteID`),
  KEY `company_id` (`CompanyID`),
  KEY `subscriptionPlanId_idx` (`SubscriptionPlanID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_site_category`
--

DROP TABLE IF EXISTS `tmp_site_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_site_category` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `SiteID` bigint(20) DEFAULT NULL,
  `CategoryID` bigint(20) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`),
  KEY `category_id` (`CategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_site_contact`
--

DROP TABLE IF EXISTS `tmp_site_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_site_contact` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `SiteID` bigint(20) DEFAULT NULL,
  `Fname` varchar(45) DEFAULT NULL,
  `IsDefault` enum('0','1') DEFAULT NULL,
  `Lname` varchar(45) DEFAULT NULL,
  `Email` varchar(75) DEFAULT NULL,
  `ConcatNumber` varchar(25) DEFAULT NULL,
  `ProfilePic` varchar(45) DEFAULT NULL,
  `ContactCountryCode` varchar(15) DEFAULT NULL,
  `SortingOrder` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`),
  KEY `FK_site_id_idx` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_site_contact_lang`
--

DROP TABLE IF EXISTS `tmp_site_contact_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_site_contact_lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `SiteContactID` bigint(20) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `Designation` varchar(45) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `lanCode` (`LangCode`),
  KEY `SiteContactID_idx` (`SiteContactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_site_country`
--

DROP TABLE IF EXISTS `tmp_site_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_site_country` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `CountryID` bigint(20) DEFAULT NULL,
  `SiteID` bigint(20) NOT NULL,
  `IsDefault` enum('0','1') DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `country_id` (`CountryID`),
  KEY `fk_tmp_siteCountry_tmp_site1_idx` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_site_device`
--

DROP TABLE IF EXISTS `tmp_site_device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_site_device` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `SiteID` bigint(20) DEFAULT NULL,
  `DeviceID` bigint(20) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `siteId_idx` (`SiteID`),
  KEY `deviceId_idx` (`DeviceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_site_lang`
--

DROP TABLE IF EXISTS `tmp_site_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_site_lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `SiteID` bigint(20) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `Description` text,
  `MetaDescription` varchar(250) DEFAULT NULL,
  `Meta_Keywords` varchar(250) DEFAULT NULL,
  `Audience` varchar(250) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode_idx` (`LangCode`),
  KEY `siteId_idx` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_testimonial`
--

DROP TABLE IF EXISTS `tmp_testimonial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_testimonial` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `SiteID` int(5) DEFAULT NULL,
  `UpdateDate` date DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_testimonial_lang`
--

DROP TABLE IF EXISTS `tmp_testimonial_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_testimonial_lang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `TestimonialID` int(5) DEFAULT NULL,
  `Quote` text,
  `ByWhom` varchar(45) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `testimonialId_idx` (`TestimonialID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_analytics`
--

DROP TABLE IF EXISTS `trn_analytics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_analytics` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `UniqueVisits` int(11) NOT NULL,
  `PageViews` int(11) NOT NULL,
  `Visits` int(11) NOT NULL,
  `UniqueVisitsMobile` int(11) NOT NULL,
  `PageViewsMobile` int(11) NOT NULL,
  `VisitsMobile` int(11) NOT NULL,
  `UpdatedTime` datetime DEFAULT NULL,
  `Date` date DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `siteID` (`SiteID`),
  KEY `date` (`Date`)
) ENGINE=InnoDB AUTO_INCREMENT=1577 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_analytics_archive`
--

DROP TABLE IF EXISTS `trn_analytics_archive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_analytics_archive` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `UniqueVisits` int(11) DEFAULT NULL,
  `PageViews` int(11) DEFAULT NULL,
  `Visits` int(11) DEFAULT NULL,
  `UniqueVisitsMobile` int(11) DEFAULT NULL,
  `PageViewsMobile` int(11) DEFAULT NULL,
  `VisitsMobile` int(11) DEFAULT NULL,
  `Date` date DEFAULT NULL,
  `UpdatedTime` datetime DEFAULT NULL,
  `AnalyticsID` bigint(20) DEFAULT NULL,
  `TransferredDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `siteID` (`SiteID`),
  KEY `date` (`Date`),
  KEY `AnalyticsID` (`AnalyticsID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_case`
--

DROP TABLE IF EXISTS `trn_case`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_case` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `Image` varchar(100) DEFAULT NULL,
  `SortingOrder` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_case_lang`
--

DROP TABLE IF EXISTS `trn_case_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_case_lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CaseID` bigint(20) DEFAULT NULL,
  `Title` varchar(250) DEFAULT NULL,
  `Description` text,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `caseId_idx` (`CaseID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_caselang`
--

DROP TABLE IF EXISTS `trn_caselang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_caselang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `CaseID` int(5) DEFAULT NULL,
  `Title` varchar(250) DEFAULT NULL,
  `Description` text,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `caseId_idx` (`CaseID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_company`
--

DROP TABLE IF EXISTS `trn_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_company` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Phone` varchar(30) DEFAULT NULL,
  `zip` varchar(6) DEFAULT NULL,
  `CountryID` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `CountryCallingCode` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ind_country` (`CountryID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_company_lang`
--

DROP TABLE IF EXISTS `trn_company_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_company_lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CompanyID` bigint(20) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `Name` varchar(150) DEFAULT NULL,
  `Address1` varchar(200) DEFAULT NULL,
  `Address2` varchar(200) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `companyId_idx` (`CompanyID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_companylang`
--

DROP TABLE IF EXISTS `trn_companylang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_companylang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `CompanyId` int(5) DEFAULT NULL,
  `Name` varchar(150) DEFAULT NULL,
  `Address1` varchar(200) DEFAULT NULL,
  `Address2` varchar(200) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `companyId_idx` (`CompanyId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_countries`
--

DROP TABLE IF EXISTS `trn_countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_countries` (
  `geonameID` int(11) NOT NULL,
  `code` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `en_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `en_slug` text COLLATE utf8_unicode_ci NOT NULL,
  `sv_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sv_slug` text COLLATE utf8_unicode_ci NOT NULL,
  `pl_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pl_slug` text COLLATE utf8_unicode_ci NOT NULL,
  `yr_url` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`geonameID`),
  UNIQUE KEY `code_UNIQUE` (`code`),
  KEY `en_name` (`en_name`),
  KEY `sv_name` (`sv_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_demographic_detail`
--

DROP TABLE IF EXISTS `trn_demographic_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_demographic_detail` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `HeaderID` bigint(20) DEFAULT NULL,
  `Points` decimal(4,2) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `header_id` (`HeaderID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_demographic_detail_lang`
--

DROP TABLE IF EXISTS `trn_demographic_detail_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_demographic_detail_lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DemographicDetailID` bigint(20) DEFAULT NULL,
  `Answer` varchar(250) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `demographicDetailId_idx` (`DemographicDetailID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_demographic_header`
--

DROP TABLE IF EXISTS `trn_demographic_header`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_demographic_header` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_demographic_header_lang`
--

DROP TABLE IF EXISTS `trn_demographic_header_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_demographic_header_lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DemographicHeaderID` bigint(20) NOT NULL,
  `HeadLine` varchar(250) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `demographicHeaderID_idx` (`DemographicHeaderID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_demographicdetail`
--

DROP TABLE IF EXISTS `trn_demographicdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_demographicdetail` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `HeaderId` int(5) DEFAULT NULL,
  `Points` decimal(4,2) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `header_id` (`HeaderId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_demographicdetaillang`
--

DROP TABLE IF EXISTS `trn_demographicdetaillang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_demographicdetaillang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `DemographicDetailId` int(5) DEFAULT NULL,
  `Answer` varchar(250) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `demographicDetailId_idx` (`DemographicDetailId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_demographicheader`
--

DROP TABLE IF EXISTS `trn_demographicheader`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_demographicheader` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `SiteId` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_demographicheaderlang`
--

DROP TABLE IF EXISTS `trn_demographicheaderlang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_demographicheaderlang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `DemographicHeaderID` int(5) NOT NULL,
  `HeaderLine` varchar(250) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `demographicHeaderID_idx` (`DemographicHeaderID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_format`
--

DROP TABLE IF EXISTS `trn_format`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_format` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `TypeID` bigint(20) DEFAULT NULL,
  `PriceTypeID` bigint(20) DEFAULT NULL,
  `Image` varchar(100) DEFAULT NULL,
  `Price` decimal(6,2) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`),
  KEY `price_type_id` (`PriceTypeID`),
  KEY `FK_type_id_idx` (`TypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_google_profile`
--

DROP TABLE IF EXISTS `trn_google_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_google_profile` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `GoogleUserID` varchar(100) DEFAULT NULL,
  `GoogleProfile` bigint(20) DEFAULT NULL,
  `ProfileName` varchar(100) DEFAULT NULL,
  `Token` text,
  `Status` varchar(10) DEFAULT NULL,
  `CompanyId` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `google_profile` (`GoogleProfile`),
  KEY `user_id` (`GoogleUserID`),
  KEY `companyId_idx` (`CompanyId`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_invoice`
--

DROP TABLE IF EXISTS `trn_invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_invoice` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `Description` varchar(250) DEFAULT NULL,
  `Amount` decimal(5,2) DEFAULT NULL,
  `Duedate` datetime DEFAULT NULL,
  `InvoceNo` varchar(25) DEFAULT NULL,
  `PaidDate` datetime DEFAULT NULL,
  `TransactionID` varchar(20) DEFAULT NULL,
  `PaymentStatus` int(1) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `siteId_idx` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_quick_fact`
--

DROP TABLE IF EXISTS `trn_quick_fact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_quick_fact` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `LastUpdated` date DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_quick_fact_lang`
--

DROP TABLE IF EXISTS `trn_quick_fact_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_quick_fact_lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `QuickFactID` bigint(20) NOT NULL,
  `Text` text,
  `LangCode` varchar(5) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `quickFactId_idx` (`QuickFactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_quickfact`
--

DROP TABLE IF EXISTS `trn_quickfact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_quickfact` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `SiteID` int(5) DEFAULT NULL,
  `LastUpdated` date DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_quickfactlang`
--

DROP TABLE IF EXISTS `trn_quickfactlang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_quickfactlang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `QuickFactId` int(5) NOT NULL,
  `Text` text,
  `LangCode` varchar(5) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `quickFactId_idx` (`QuickFactId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_site`
--

DROP TABLE IF EXISTS `trn_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_site` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ParentID` bigint(20) DEFAULT NULL,
  `ProfileID` bigint(20) DEFAULT NULL,
  `GoogleSiteID` varchar(12) DEFAULT NULL,
  `URL` varchar(200) DEFAULT NULL,
  `Logo` varchar(100) DEFAULT NULL,
  `AverageAge` int(2) DEFAULT NULL,
  `MalePercentage` decimal(5,2) DEFAULT NULL,
  `IsMobile` enum('0','1') DEFAULT NULL,
  `SubscriptionPlanID` int(3) DEFAULT NULL,
  `AnalyticsCreatedDate` date DEFAULT NULL,
  `ValidPeriod` int(2) DEFAULT NULL,
  `ActivatedDate` date DEFAULT NULL,
  `ActivationStatus` varchar(15) DEFAULT NULL,
  `CompanyID` int(5) DEFAULT NULL,
  `ExpiryDate` date DEFAULT NULL,
  `LastGoogleUpdate` datetime DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Isdownloading` enum('1','0') DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `profileId_idx` (`ProfileID`),
  KEY `goog_site_id` (`GoogleSiteID`),
  KEY `company_id` (`CompanyID`),
  KEY `subscriptionPlanId_idx` (`SubscriptionPlanID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_site_category`
--

DROP TABLE IF EXISTS `trn_site_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_site_category` (
  `ID` bigint(20) NOT NULL,
  `SiteID` bigint(20) DEFAULT NULL,
  `CategoryID` bigint(20) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`),
  KEY `category_id` (`CategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_site_contact`
--

DROP TABLE IF EXISTS `trn_site_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_site_contact` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `Fname` varchar(45) DEFAULT NULL,
  `IsDefault` enum('0','1') DEFAULT NULL,
  `Lname` varchar(45) DEFAULT NULL,
  `Email` varchar(75) DEFAULT NULL,
  `ConcatNumber` varchar(25) DEFAULT NULL,
  `ProfilePic` varchar(45) DEFAULT NULL,
  `ContactCountryCode` varchar(15) DEFAULT NULL,
  `SortingOrder` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`),
  KEY `FK_site_id_idx` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_site_contact_lang`
--

DROP TABLE IF EXISTS `trn_site_contact_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_site_contact_lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteContactID` bigint(20) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `Designation` varchar(45) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `lanCode` (`LangCode`),
  KEY `SiteContactID_idx` (`SiteContactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_site_country`
--

DROP TABLE IF EXISTS `trn_site_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_site_country` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CountryID` bigint(20) DEFAULT NULL,
  `SiteID` bigint(20) NOT NULL,
  `IsDefault` enum('0','1') DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `country_id` (`CountryID`),
  KEY `fk_trn_siteCountry_trn_site1_idx` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_site_device`
--

DROP TABLE IF EXISTS `trn_site_device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_site_device` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `DeviceID` bigint(20) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `siteId_idx` (`SiteID`),
  KEY `deviceId_idx` (`DeviceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_site_lang`
--

DROP TABLE IF EXISTS `trn_site_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_site_lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `Description` text,
  `MetaDescription` varchar(250) DEFAULT NULL,
  `Meta_Keywords` varchar(250) DEFAULT NULL,
  `Audience` varchar(250) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode_idx` (`LangCode`),
  KEY `siteId_idx` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_sitecategory`
--

DROP TABLE IF EXISTS `trn_sitecategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_sitecategory` (
  `ID` int(5) NOT NULL,
  `SiteID` int(5) DEFAULT NULL,
  `CategoryID` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`),
  KEY `category_id` (`CategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_sitecontact`
--

DROP TABLE IF EXISTS `trn_sitecontact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_sitecontact` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `SiteID` int(5) DEFAULT NULL,
  `IsDefault` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`),
  KEY `FK_site_id_idx` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_sitecontactlang`
--

DROP TABLE IF EXISTS `trn_sitecontactlang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_sitecontactlang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `SiteContactId` int(5) DEFAULT NULL,
  `FName` varchar(45) DEFAULT NULL,
  `LName` varchar(45) DEFAULT NULL,
  `Email` varchar(90) DEFAULT NULL,
  `Designation` varchar(45) DEFAULT NULL,
  `ProfilePic` varchar(45) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `lanCode` (`LangCode`),
  KEY `siteContactId_idx` (`SiteContactId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_sitecountry`
--

DROP TABLE IF EXISTS `trn_sitecountry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_sitecountry` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `CountryID` int(5) DEFAULT NULL,
  `SiteID` int(5) NOT NULL,
  `iSDefault` enum('yes','no') DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `country_id` (`CountryID`),
  KEY `fk_trn_siteCountry_trn_site1_idx` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_sitedevice`
--

DROP TABLE IF EXISTS `trn_sitedevice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_sitedevice` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `SiteID` int(5) DEFAULT NULL,
  `DeviceID` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `siteId_idx` (`SiteID`),
  KEY `deviceId_idx` (`DeviceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_sitelang`
--

DROP TABLE IF EXISTS `trn_sitelang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_sitelang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `SiteID` int(5) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `Description` text,
  `Audience` varchar(250) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode_idx` (`LangCode`),
  KEY `siteId_idx` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_testimonial`
--

DROP TABLE IF EXISTS `trn_testimonial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_testimonial` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `SiteID` int(5) DEFAULT NULL,
  `UpdateDate` date DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_testimonial_lang`
--

DROP TABLE IF EXISTS `trn_testimonial_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_testimonial_lang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `TestimonialID` int(5) DEFAULT NULL,
  `Quote` text,
  `ByWhom` varchar(45) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `testimonialId_idx` (`TestimonialID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_testimoniallang`
--

DROP TABLE IF EXISTS `trn_testimoniallang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_testimoniallang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `TestimonialID` int(5) DEFAULT NULL,
  `Quote` text,
  `ByWhom` varchar(45) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `testimonialId_idx` (`TestimonialID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_user_history`
--

DROP TABLE IF EXISTS `trn_user_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_user_history` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `ModuleID` int(5) DEFAULT NULL,
  `UserID` int(5) DEFAULT NULL,
  `Operation` varchar(150) DEFAULT NULL,
  `Description` varchar(250) DEFAULT NULL,
  `LastUpdate` datetime DEFAULT NULL,
  `SiteID` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `userId` (`SiteID`),
  KEY `siteId_idx` (`UserID`),
  KEY `ModuleID_idx` (`ModuleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trn_userhistory`
--

DROP TABLE IF EXISTS `trn_userhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_userhistory` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `Title` varchar(250) DEFAULT NULL,
  `UserID` int(5) DEFAULT NULL,
  `Description` varchar(250) DEFAULT NULL,
  `URL` text,
  `LastUpdate` datetime DEFAULT NULL,
  `SiteID` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `userId` (`SiteID`),
  KEY `siteId_idx` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-10-08  6:52:10
