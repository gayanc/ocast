SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `ocast_db` ;
CREATE SCHEMA IF NOT EXISTS `ocast_db` DEFAULT CHARACTER SET utf8 ;
USE `ocast_db` ;

-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Company`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Company` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Company` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `Phone` VARCHAR(30) NULL ,
  `zip` VARCHAR(6) NULL ,
  `CountryID` INT(5) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `ind_country` ON `ocast_db`.`trn_Company` (`CountryID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Google_Profile`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Google_Profile` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Google_Profile` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `GoogleUserID` VARCHAR(100) NULL ,
  `GoogleProfile` BIGINT NULL ,
  `ProfileName` VARCHAR(100) NULL ,
  `Token` TEXT NULL ,
  `Status` VARCHAR(10) NULL ,
  `CompanyId` INT(5) NULL ,
  `CreationDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `google_profile` ON `ocast_db`.`trn_Google_Profile` (`GoogleProfile` ASC) ;

CREATE INDEX `user_id` ON `ocast_db`.`trn_Google_Profile` (`GoogleUserID` ASC) ;

CREATE INDEX `companyId_idx` ON `ocast_db`.`trn_Google_Profile` (`CompanyId` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_Subscription_Plan`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_Subscription_Plan` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_Subscription_Plan` (
  `ID` INT(5) NOT NULL ,
  `SortingOrder` INT(5) NULL ,
  `Duration` INT(2) NULL ,
  `Price` DECIMAL(5,2) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Site`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Site` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Site` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `ParentID` BIGINT NULL ,
  `ProfileID` BIGINT NULL ,
  `GoogleSiteID` VARCHAR(12) NULL ,
  `AnalyticsCreatedDate` DATE NULL ,
  `URL` VARCHAR(200) NULL ,
  `Logo` VARCHAR(100) NULL ,
  `AverageAge` INT(2) NULL ,
  `MalePercentage` DECIMAL(5,2) NULL ,
  `IsMobile` ENUM('0','1') NULL ,
  `SubscriptionPlanID` INT(3) NULL ,
  `ValidPeriod` INT(2) NULL ,
  `ActivatedDate` DATE NULL ,
  `ActivationStatus` VARCHAR(15) NULL ,
  `CompanyID` INT(5) NULL ,
  `ExpiryDate` DATE NULL ,
  `LastGoogleUpdate` DATETIME NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  `Isdownloading` ENUM('0','1') NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `profileId_idx` ON `ocast_db`.`trn_Site` (`ProfileID` ASC) ;

CREATE INDEX `goog_site_id` ON `ocast_db`.`trn_Site` (`GoogleSiteID` ASC) ;

CREATE INDEX `company_id` ON `ocast_db`.`trn_Site` (`CompanyID` ASC) ;

CREATE INDEX `subscriptionPlanId_idx` ON `ocast_db`.`trn_Site` (`SubscriptionPlanID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Site_Contact`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Site_Contact` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Site_Contact` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `SiteID` BIGINT NULL ,
  `Fname` VARCHAR(45) NULL ,
  `IsDefault` ENUM('0','1') NULL ,
  `Lname` VARCHAR(45) NULL ,
  `Email` VARCHAR(75) NULL ,
  `ConcatNumber` VARCHAR(25) NULL ,
  `ProfilePic` VARCHAR(45) NULL ,
  `ContactCountryCode` VARCHAR(15) NULL ,
  `SortingOrder` INT(5) NULL ,
  `CreatedBy` INT(5) NULL ,
  `CreatedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `site_id` ON `ocast_db`.`trn_Site_Contact` (`SiteID` ASC) ;

CREATE INDEX `FK_site_id_idx` ON `ocast_db`.`trn_Site_Contact` (`SiteID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_FAQ_Category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_FAQ_Category` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_FAQ_Category` (
  `ID` INT(5) NOT NULL ,
  `SortingOrder` INT(5) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastUpdated` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_FAQ`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_FAQ` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_FAQ` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `SortingOrder` INT(5) NULL ,
  `FAQCategoryID` INT(5) NULL ,
  `IsEnable` ENUM('0','1') NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `FAQCategoryID_idx` ON `ocast_db`.`set_FAQ` (`FAQCategoryID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_Category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_Category` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_Category` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `SortingOrder` INT(5) NULL ,
  `LastUpdate` DATETIME NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Site_Category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Site_Category` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Site_Category` (
  `ID` BIGINT NOT NULL ,
  `SiteID` BIGINT NULL ,
  `CategoryID` BIGINT NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `site_id` ON `ocast_db`.`trn_Site_Category` (`SiteID` ASC) ;

CREATE INDEX `category_id` ON `ocast_db`.`trn_Site_Category` (`CategoryID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`sys_Country`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`sys_Country` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`sys_Country` (
  `ID` INT(5) NOT NULL ,
  `ISO` VARCHAR(2) NOT NULL ,
  `CountryCode` VARCHAR(10) NULL ,
  `IsActive` ENUM('0','1') NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `CreatedDate` DATETIME NULL ,
  PRIMARY KEY (`ID`, `ISO`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Site_Country`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Site_Country` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Site_Country` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `CountryID` BIGINT NULL ,
  `SiteID` BIGINT NOT NULL ,
  `IsDefault` ENUM('0','1') NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `country_id` ON `ocast_db`.`trn_Site_Country` (`CountryID` ASC) ;

CREATE INDEX `fk_trn_siteCountry_trn_site1_idx` ON `ocast_db`.`trn_Site_Country` (`SiteID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Quick_Fact`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Quick_Fact` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Quick_Fact` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `SiteID` BIGINT NULL ,
  `LastUpdated` DATE NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `site_id` ON `ocast_db`.`trn_Quick_Fact` (`SiteID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Testimonial`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Testimonial` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Testimonial` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `SiteID` INT(5) NULL ,
  `UpdateDate` DATE NULL ,
  `CreationDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `site_id` ON `ocast_db`.`trn_Testimonial` (`SiteID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Demographic_Header`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Demographic_Header` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Demographic_Header` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `SiteID` BIGINT NULL ,
  `CreationDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastUpdated` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `site_id` ON `ocast_db`.`trn_Demographic_Header` (`SiteID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Demographic_Detail`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Demographic_Detail` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Demographic_Detail` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `HeaderID` BIGINT NULL ,
  `Points` DECIMAL(4,2) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `header_id` ON `ocast_db`.`trn_Demographic_Detail` (`HeaderID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_Price_Type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_Price_Type` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_Price_Type` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_Format`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_Format` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_Format` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Format`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Format` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Format` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `SiteID` BIGINT NULL ,
  `TypeID` BIGINT NULL ,
  `PriceTypeID` BIGINT NULL ,
  `Image` VARCHAR(100) NULL ,
  `Price` DECIMAL(6,2) NULL ,
  `CreationDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `site_id` ON `ocast_db`.`trn_Format` (`SiteID` ASC) ;

CREATE INDEX `price_type_id` ON `ocast_db`.`trn_Format` (`PriceTypeID` ASC) ;

CREATE INDEX `FK_type_id_idx` ON `ocast_db`.`trn_Format` (`TypeID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Case`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Case` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Case` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `SiteID` BIGINT NULL ,
  `Image` VARCHAR(100) NULL ,
  `SortingOrder` INT(5) NULL ,
  `CreatedBy` INT(5) NULL ,
  `CreatedDate` DATETIME NULL ,
  `LastUpdated` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `site_id` ON `ocast_db`.`trn_Case` (`SiteID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Analytics`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Analytics` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Analytics` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `SiteID` BIGINT NULL ,
  `UniqueVisits` INT NULL ,
  `PageViews` INT NULL ,
  `Visits` INT NULL ,
  `UniqueVisitsMobile` INT NULL ,
  `PageViewsMobile` INT NULL ,
  `VisitsMobile` INT NULL ,
  `Date` DATE NULL ,
  `UpdatedTime` DATETIME NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `siteID` ON `ocast_db`.`trn_Analytics` (`SiteID` ASC) ;

CREATE INDEX `date` ON `ocast_db`.`trn_Analytics` (`Date` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_User_Type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_User_Type` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_User_Type` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `UserTypeName` VARCHAR(25) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_User`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_User` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_User` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `UserName` VARCHAR(150) NULL ,
  `Password` TEXT NULL ,
  `Fname` VARCHAR(20) NULL ,
  `Lname` VARCHAR(20) NULL ,
  `AciveStatus` VARCHAR(20) NULL ,
  `UserTypeID` INT(5) NULL ,
  `CompanyID` INT(5) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  `PasswordResetCode` VARCHAR(250) NULL ,
  `ResetRequestDate` DATETIME NULL ,
  `LastLogin` DATETIME NULL ,
  `LastLoginIP` VARCHAR(15) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE UNIQUE INDEX `userNameUNQ` ON `ocast_db`.`set_User` (`UserName` ASC) ;

CREATE INDEX `companyId_idx` ON `ocast_db`.`set_User` (`CompanyID` ASC) ;

CREATE INDEX `userTypeId_idx` ON `ocast_db`.`set_User` (`UserTypeID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`sys_Language`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`sys_Language` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`sys_Language` (
  `LangCode` VARCHAR(5) NOT NULL ,
  `LangNameEng` VARCHAR(50) NULL ,
  `LangName` VARCHAR(70) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL ,
  `CreatedDate` DATETIME NULL ,
  `LastModifiedDate` DATETIME NULL ,
  PRIMARY KEY (`LangCode`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ocast_db`.`sys_Module`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`sys_Module` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`sys_Module` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `ModuleCode` VARCHAR(10) NULL ,
  `ParentID` INT(5) NULL ,
  `URL` TEXT NULL ,
  `SortingOrder` INT(5) NULL ,
  `DateCreated` DATETIME NULL ,
  `LastUpdated` DATETIME NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_User_History`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_User_History` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_User_History` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `ModuleID` INT(5) NULL ,
  `UserID` INT(5) NULL ,
  `Operation` VARCHAR(150) NULL ,
  `Description` VARCHAR(250) NULL ,
  `LastUpdate` DATETIME NULL ,
  `SiteID` INT(5) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `userId` ON `ocast_db`.`trn_User_History` (`SiteID` ASC) ;

CREATE INDEX `siteId_idx` ON `ocast_db`.`trn_User_History` (`UserID` ASC) ;

CREATE INDEX `ModuleID_idx` ON `ocast_db`.`trn_User_History` (`ModuleID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_FAQ_Lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_FAQ_Lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_FAQ_Lang` (
  `ID` INT(5) NOT NULL ,
  `FaqID` INT(5) NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `Question` TEXT CHARACTER SET 'utf8' NULL ,
  `Answer` TEXT CHARACTER SET 'utf8' NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `langCode` ON `ocast_db`.`set_FAQ_Lang` (`LangCode` ASC) ;

CREATE INDEX `fqaID_idx` ON `ocast_db`.`set_FAQ_Lang` (`FaqID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Case_Lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Case_Lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Case_Lang` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `CaseID` BIGINT NULL ,
  `Title` VARCHAR(250) CHARACTER SET 'utf8' NULL ,
  `Description` TEXT CHARACTER SET 'utf8' NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `langCode` ON `ocast_db`.`trn_Case_Lang` (`LangCode` ASC) ;

CREATE INDEX `caseId_idx` ON `ocast_db`.`trn_Case_Lang` (`CaseID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Company_Lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Company_Lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Company_Lang` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `CompanyID` BIGINT NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `Name` VARCHAR(150) CHARACTER SET 'utf8' NULL ,
  `Address1` VARCHAR(200) CHARACTER SET 'utf8' NULL ,
  `Address2` VARCHAR(200) NULL ,
  `CreationDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `companyId_idx` ON `ocast_db`.`trn_Company_Lang` (`CompanyID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Demographic_Header_Lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Demographic_Header_Lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Demographic_Header_Lang` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `DemographicHeaderID` BIGINT NOT NULL ,
  `HeadLine` VARCHAR(250) CHARACTER SET 'utf8' NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `CreationDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `langCode` ON `ocast_db`.`trn_Demographic_Header_Lang` (`LangCode` ASC) ;

CREATE INDEX `demographicHeaderID_idx` ON `ocast_db`.`trn_Demographic_Header_Lang` (`DemographicHeaderID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Demographic_Detail_Lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Demographic_Detail_Lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Demographic_Detail_Lang` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `DemographicDetailID` BIGINT NULL ,
  `Answer` VARCHAR(250) CHARACTER SET 'utf8' NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `langCode` ON `ocast_db`.`trn_Demographic_Detail_Lang` (`LangCode` ASC) ;

CREATE INDEX `demographicDetailId_idx` ON `ocast_db`.`trn_Demographic_Detail_Lang` (`DemographicDetailID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Quick_Fact_Lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Quick_Fact_Lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Quick_Fact_Lang` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `QuickFactID` BIGINT NOT NULL ,
  `Text` TEXT CHARACTER SET 'utf8' NULL ,
  `LangCode` VARCHAR(5) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `langCode` ON `ocast_db`.`trn_Quick_Fact_Lang` (`LangCode` ASC) ;

CREATE INDEX `quickFactId_idx` ON `ocast_db`.`trn_Quick_Fact_Lang` (`QuickFactID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Site_Contact_Lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Site_Contact_Lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Site_Contact_Lang` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `SiteContactID` BIGINT NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `Designation` VARCHAR(45) CHARACTER SET 'utf8' NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `lanCode` ON `ocast_db`.`trn_Site_Contact_Lang` (`LangCode` ASC) ;

CREATE INDEX `SiteContactID_idx` ON `ocast_db`.`trn_Site_Contact_Lang` (`SiteContactID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Testimonial_Lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Testimonial_Lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Testimonial_Lang` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `TestimonialID` INT(5) NULL ,
  `Quote` TEXT CHARACTER SET 'utf8' NULL ,
  `ByWhom` VARCHAR(45) CHARACTER SET 'utf8' NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `langCode` ON `ocast_db`.`trn_Testimonial_Lang` (`LangCode` ASC) ;

CREATE INDEX `testimonialId_idx` ON `ocast_db`.`trn_Testimonial_Lang` (`TestimonialID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_User_Site`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_User_Site` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_User_Site` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `UserID` INT(5) NULL ,
  `SiteID` INT(5) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `userId_idx` ON `ocast_db`.`set_User_Site` (`UserID` ASC) ;

CREATE INDEX `siteId_idx` ON `ocast_db`.`set_User_Site` (`SiteID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_User_Permissions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_User_Permissions` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_User_Permissions` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `ModuleID` INT(5) NULL ,
  `UserTypeID` INT(5) NULL ,
  `IsAllowed` ENUM('0','1') NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `userTypeId_idx` ON `ocast_db`.`set_User_Permissions` (`UserTypeID` ASC) ;

CREATE INDEX `ModuleID_idx` ON `ocast_db`.`set_User_Permissions` (`ModuleID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_Menu_Item`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_Menu_Item` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_Menu_Item` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `MenuName` VARCHAR(100) NULL ,
  `ParentID` INT(5) NOT NULL ,
  `CreatedDate` DATETIME NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  `CreatedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ocast_db`.`sys_Device`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`sys_Device` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`sys_Device` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `SortingOrder` INT(5) NULL ,
  `CreatedDate` DATETIME NULL ,
  `LastModifiedDate` DATETIME NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Site_Device`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Site_Device` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Site_Device` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `SiteID` BIGINT NULL ,
  `DeviceID` BIGINT NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `siteId_idx` ON `ocast_db`.`trn_Site_Device` (`SiteID` ASC) ;

CREATE INDEX `deviceId_idx` ON `ocast_db`.`trn_Site_Device` (`DeviceID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Site_Lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Site_Lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Site_Lang` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `SiteID` BIGINT NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `Description` TEXT NULL ,
  `MetaDescription` VARCHAR(250) CHARACTER SET 'utf8' NULL ,
  `Meta_Keywords` VARCHAR(250) CHARACTER SET 'utf8' NULL ,
  `Audience` VARCHAR(250) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `langCode_idx` ON `ocast_db`.`trn_Site_Lang` (`LangCode` ASC) ;

CREATE INDEX `siteId_idx` ON `ocast_db`.`trn_Site_Lang` (`SiteID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`sys_Registration_History`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`sys_Registration_History` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`sys_Registration_History` (
  `ID` INT(5) NOT NULL ,
  `UserID` INT(5) NULL ,
  `StepNumber` INT(1) NULL ,
  `CreatedDate` DATETIME NULL ,
  `LastModifiedDate` DATETIME NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `Fk_userId` ON `ocast_db`.`sys_Registration_History` (`UserID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_Subscription_Plan_Lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_Subscription_Plan_Lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_Subscription_Plan_Lang` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `PlanID` INT(5) NULL ,
  `Description` VARCHAR(250) NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `CreatedDate` DATE NULL ,
  `CreatedBy` INT(5) NOT NULL ,
  `LastModifiedDate` DATE NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `planId_idx` ON `ocast_db`.`set_Subscription_Plan_Lang` (`PlanID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Invoice`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Invoice` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Invoice` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `SiteID` BIGINT NULL ,
  `Description` VARCHAR(250) NULL ,
  `Amount` DECIMAL(5,2) NULL ,
  `Duedate` DATETIME NULL ,
  `InvoceNo` VARCHAR(25) NULL ,
  `PaidDate` DATETIME NULL ,
  `TransactionID` VARCHAR(20) NULL ,
  `PaymentStatus` INT(1) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `siteId_idx` ON `ocast_db`.`trn_Invoice` (`SiteID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`sys_Country_Lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`sys_Country_Lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`sys_Country_Lang` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `CountryID` INT(5) NULL ,
  `CountryName` VARCHAR(150) CHARACTER SET 'utf8' NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `CreationDate` DATETIME NULL ,
  `LastModifiedDate` DATETIME NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `countryId_idx` ON `ocast_db`.`sys_Country_Lang` (`CountryID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`sys_Device_Lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`sys_Device_Lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`sys_Device_Lang` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `DeviceID` INT(5) NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `DeviceName` VARCHAR(45) NULL ,
  `CreatedDate` DATETIME NULL ,
  `LastModifiedDate` DATETIME NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `deviceId_idx` ON `ocast_db`.`sys_Device_Lang` (`DeviceID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_Category_Lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_Category_Lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_Category_Lang` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `CategoryID` INT(5) NULL ,
  `Description` VARCHAR(45) NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `categoryID_idx` ON `ocast_db`.`set_Category_Lang` (`CategoryID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_Format_Lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_Format_Lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_Format_Lang` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `FormatID` INT(5) NULL ,
  `Langcode` VARCHAR(5) NULL ,
  `Title` VARCHAR(200) NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastModifiedDate` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `FormatID_idx` ON `ocast_db`.`set_Format_Lang` (`FormatID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_Price_Type_Lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_Price_Type_Lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_Price_Type_Lang` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `text` VARCHAR(100) NULL ,
  `priceTypeID` INT(5) NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `creationDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `lastModifiedDate` DATETIME NULL ,
  `lastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ocast_db`.`set_FAQ_Category_Lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`set_FAQ_Category_Lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`set_FAQ_Category_Lang` (
  `ID` INT(5) NOT NULL ,
  `FAQCategoryID` INT(5) NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `CategoryName` VARCHAR(250) CHARACTER SET 'utf8' NULL ,
  `CreatedDate` DATETIME NULL ,
  `CreatedBy` INT(5) NULL ,
  `LastUpdated` DATETIME NULL ,
  `LastModifiedBy` INT(5) NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `FAQCategoryID_idx` ON `ocast_db`.`set_FAQ_Category_Lang` (`FAQCategoryID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`trn_Analytics_Archive`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`trn_Analytics_Archive` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`trn_Analytics_Archive` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `SiteID` BIGINT NULL ,
  `UniqueVisits` INT NULL ,
  `PageViews` INT NULL ,
  `Visits` INT NULL ,
  `UniqueVisitsMobile` INT NULL ,
  `PageViewsMobile` INT NULL ,
  `VisitsMobile` INT NULL ,
  `Date` DATE NULL ,
  `UpdatedTime` DATETIME NULL ,
  `AnalyticsID` BIGINT NULL ,
  `TransferredDate` DATETIME NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `siteID` ON `ocast_db`.`trn_Analytics_Archive` (`SiteID` ASC) ;

CREATE INDEX `date` ON `ocast_db`.`trn_Analytics_Archive` (`Date` ASC) ;

CREATE INDEX `AnalyticsID` ON `ocast_db`.`trn_Analytics_Archive` (`AnalyticsID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`tmp_Account_info`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`tmp_Account_info` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`tmp_Account_info` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `ActivationCode` VARCHAR(250) NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `UserName` VARCHAR(150) NULL ,
  `Password` TEXT NULL ,
  `Fname` VARCHAR(20) NULL ,
  `Lname` VARCHAR(20) NULL ,
  `Company` VARCHAR(100) NULL ,
  `PhoneCountryCode` VARCHAR(6) NULL ,
  `PhoneNumber` VARCHAR(10) NULL ,
  `Address1` VARCHAR(100) NULL ,
  `Address2` VARCHAR(100) NULL ,
  `Zip` INT(8) NULL ,
  `City` VARCHAR(45) NULL ,
  `CountryID` INT(1) NULL ,
  `AciveStatus` ENUM('active', 'pending', 'inactive') NULL ,
  `SessionID` VARCHAR(100) NULL ,
  `RemorteIP` VARCHAR(15) NULL ,
  `UserAgent` VARCHAR(100) NULL ,
  `CreatedDate` DATETIME NULL ,
  `LastModifiedDate` DATETIME NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE UNIQUE INDEX `ActivationCode` ON `ocast_db`.`tmp_Account_info` (`ActivationCode` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`sys_Module_lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`sys_Module_lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`sys_Module_lang` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `ModuleName` VARCHAR(45) NULL ,
  `ModuleID` INT(5) NULL ,
  `LangCode` VARCHAR(5) NULL ,
  `DateCreated` DATETIME NULL ,
  `LastUpdated` DATETIME NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `ModuleID_idx` ON `ocast_db`.`sys_Module_lang` (`ModuleID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`sys_Failed_Payment`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`sys_Failed_Payment` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`sys_Failed_Payment` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT ,
  `InvoiceID` BIGINT NULL ,
  `FaileddDate` DATE NULL ,
  `Status` ENUM('Active' , 'Inactive' , 'Pending') NULL ,
  `IsPaid` ENUM('0','1') NULL ,
  `Notes` TEXT NULL ,
  `CreatedDate` DATETIME NULL ,
  `LastModifiedDate` DATETIME NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB;

CREATE INDEX `InvoiceID_idx` ON `ocast_db`.`sys_Failed_Payment` (`InvoiceID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`tmp_case`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`tmp_case` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`tmp_case` (
  `ID` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `TransactionID` BIGINT(20) NULL DEFAULT NULL ,
  `SiteID` BIGINT(20) NULL DEFAULT NULL ,
  `Image` VARCHAR(100) NULL DEFAULT NULL ,
  `SortingOrder` INT(5) NULL DEFAULT NULL ,
  `CreatedBy` INT(5) NULL DEFAULT NULL ,
  `CreatedDate` DATETIME NULL DEFAULT NULL ,
  `LastUpdated` DATETIME NULL DEFAULT NULL ,
  `LastModifiedBy` INT(5) NULL DEFAULT NULL ,
  `Status` ENUM('0','1') NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE INDEX `site_id` ON `ocast_db`.`tmp_case` (`SiteID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`tmp_case_lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`tmp_case_lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`tmp_case_lang` (
  `ID` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `TransactionID` BIGINT(20) NULL DEFAULT NULL ,
  `CaseID` BIGINT(20) NULL DEFAULT NULL ,
  `Title` VARCHAR(250) NULL DEFAULT NULL ,
  `Description` TEXT NULL DEFAULT NULL ,
  `LangCode` VARCHAR(5) NULL DEFAULT NULL ,
  `CreatedDate` DATETIME NULL DEFAULT NULL ,
  `CreatedBy` INT(5) NULL DEFAULT NULL ,
  `LastModifiedDate` DATETIME NULL DEFAULT NULL ,
  `LastModifiedBy` INT(5) NULL DEFAULT NULL ,
  `Status` ENUM('0','1') NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE INDEX `langCode` ON `ocast_db`.`tmp_case_lang` (`LangCode` ASC) ;

CREATE INDEX `caseId_idx` ON `ocast_db`.`tmp_case_lang` (`CaseID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`tmp_company`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`tmp_company` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`tmp_company` (
  `ID` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `TransactionID` BIGINT(20) NULL DEFAULT NULL ,
  `Phone` VARCHAR(30) NULL DEFAULT NULL ,
  `zip` VARCHAR(6) NULL DEFAULT NULL ,
  `CountryID` INT(5) NULL DEFAULT NULL ,
  `CreatedDate` DATETIME NULL DEFAULT NULL ,
  `CreatedBy` INT(5) NULL DEFAULT NULL ,
  `LastModifiedDate` DATETIME NULL DEFAULT NULL ,
  `LastModifiedBy` INT(5) NULL DEFAULT NULL ,
  `Status` ENUM('0','1') NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE INDEX `ind_country` ON `ocast_db`.`tmp_company` (`CountryID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`tmp_company_lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`tmp_company_lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`tmp_company_lang` (
  `ID` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `TransactionID` BIGINT(20) NULL DEFAULT NULL ,
  `CompanyID` BIGINT(20) NULL DEFAULT NULL ,
  `LangCode` VARCHAR(5) NULL DEFAULT NULL ,
  `Name` VARCHAR(150) NULL DEFAULT NULL ,
  `Address1` VARCHAR(200) NULL DEFAULT NULL ,
  `Address2` VARCHAR(200) NULL DEFAULT NULL ,
  `CreationDate` DATETIME NULL DEFAULT NULL ,
  `CreatedBy` INT(5) NULL DEFAULT NULL ,
  `LastModifiedDate` DATETIME NULL DEFAULT NULL ,
  `LastModifiedBy` INT(5) NULL DEFAULT NULL ,
  `Status` ENUM('0','1') NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE INDEX `companyId_idx` ON `ocast_db`.`tmp_company_lang` (`CompanyID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`tmp_demographic_detail`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`tmp_demographic_detail` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`tmp_demographic_detail` (
  `ID` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `TransactionID` BIGINT(20) NULL DEFAULT NULL ,
  `HeaderID` BIGINT(20) NULL DEFAULT NULL ,
  `Points` DECIMAL(4,2) NULL DEFAULT NULL ,
  `CreatedDate` DATETIME NULL DEFAULT NULL ,
  `CreatedBy` INT(5) NULL DEFAULT NULL ,
  `LastModifiedDate` DATETIME NULL DEFAULT NULL ,
  `LastModifiedBy` INT(5) NULL DEFAULT NULL ,
  `Status` ENUM('0','1') NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE INDEX `header_id` ON `ocast_db`.`tmp_demographic_detail` (`HeaderID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`tmp_demographic_detail_lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`tmp_demographic_detail_lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`tmp_demographic_detail_lang` (
  `ID` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `TransactionID` BIGINT(20) NULL DEFAULT NULL ,
  `DemographicDetailID` BIGINT(20) NULL DEFAULT NULL ,
  `Answer` VARCHAR(250) NULL DEFAULT NULL ,
  `LangCode` VARCHAR(5) NULL DEFAULT NULL ,
  `CreatedDate` DATETIME NULL DEFAULT NULL ,
  `CreatedBy` INT(5) NULL DEFAULT NULL ,
  `LastModifiedDate` DATETIME NULL DEFAULT NULL ,
  `LastModifiedBy` INT(5) NULL DEFAULT NULL ,
  `Status` ENUM('0','1') NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE INDEX `langCode` ON `ocast_db`.`tmp_demographic_detail_lang` (`LangCode` ASC) ;

CREATE INDEX `demographicDetailId_idx` ON `ocast_db`.`tmp_demographic_detail_lang` (`DemographicDetailID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`tmp_demographic_header`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`tmp_demographic_header` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`tmp_demographic_header` (
  `ID` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `TransactionID` BIGINT(20) NULL DEFAULT NULL ,
  `SiteID` BIGINT(20) NULL DEFAULT NULL ,
  `CreationDate` DATETIME NULL DEFAULT NULL ,
  `CreatedBy` INT(5) NULL DEFAULT NULL ,
  `LastUpdated` DATETIME NULL DEFAULT NULL ,
  `LastModifiedBy` INT(5) NULL DEFAULT NULL ,
  `Status` ENUM('0','1') NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE INDEX `site_id` ON `ocast_db`.`tmp_demographic_header` (`SiteID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`tmp_demographic_header_lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`tmp_demographic_header_lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`tmp_demographic_header_lang` (
  `ID` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `TransactionID` BIGINT(20) NULL DEFAULT NULL ,
  `DemographicHeaderID` BIGINT(20) NOT NULL ,
  `HeadLine` VARCHAR(250) NULL DEFAULT NULL ,
  `LangCode` VARCHAR(5) NULL DEFAULT NULL ,
  `CreationDate` DATETIME NULL DEFAULT NULL ,
  `CreatedBy` INT(5) NULL DEFAULT NULL ,
  `LastModifiedDate` DATETIME NULL DEFAULT NULL ,
  `LastModifiedBy` INT(5) NULL DEFAULT NULL ,
  `Status` ENUM('0','1') NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE INDEX `langCode` ON `ocast_db`.`tmp_demographic_header_lang` (`LangCode` ASC) ;

CREATE INDEX `demographicHeaderID_idx` ON `ocast_db`.`tmp_demographic_header_lang` (`DemographicHeaderID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`tmp_format`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`tmp_format` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`tmp_format` (
  `ID` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `TransactionID` BIGINT(20) NULL DEFAULT NULL ,
  `SiteID` BIGINT(20) NULL DEFAULT NULL ,
  `TypeID` BIGINT(20) NULL DEFAULT NULL ,
  `PriceTypeID` BIGINT(20) NULL DEFAULT NULL ,
  `Image` VARCHAR(100) NULL DEFAULT NULL ,
  `Price` DECIMAL(6,2) NULL DEFAULT NULL ,
  `CreationDate` DATETIME NULL DEFAULT NULL ,
  `CreatedBy` INT(5) NULL DEFAULT NULL ,
  `LastModifiedDate` DATETIME NULL DEFAULT NULL ,
  `LastModifiedBy` INT(5) NULL DEFAULT NULL ,
  `Status` ENUM('0','1') NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE INDEX `site_id` ON `ocast_db`.`tmp_format` (`SiteID` ASC) ;

CREATE INDEX `price_type_id` ON `ocast_db`.`tmp_format` (`PriceTypeID` ASC) ;

CREATE INDEX `FK_type_id_idx` ON `ocast_db`.`tmp_format` (`TypeID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`tmp_quick_fact`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`tmp_quick_fact` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`tmp_quick_fact` (
  `ID` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `TransactionID` BIGINT(20) NULL DEFAULT NULL ,
  `SiteID` BIGINT(20) NULL DEFAULT NULL ,
  `LastUpdated` DATE NULL DEFAULT NULL ,
  `CreatedDate` DATETIME NULL DEFAULT NULL ,
  `CreatedBy` INT(5) NULL DEFAULT NULL ,
  `LastModifiedDate` DATETIME NULL DEFAULT NULL ,
  `LastModifiedBy` INT(5) NULL DEFAULT NULL ,
  `Status` ENUM('0','1') NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE INDEX `site_id` ON `ocast_db`.`tmp_quick_fact` (`SiteID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`tmp_quick_fact_lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`tmp_quick_fact_lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`tmp_quick_fact_lang` (
  `ID` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `TransactionID` BIGINT(20) NULL DEFAULT NULL ,
  `QuickFactID` BIGINT(20) NOT NULL ,
  `Text` TEXT NULL DEFAULT NULL ,
  `LangCode` VARCHAR(5) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `CreatedDate` DATETIME NULL DEFAULT NULL ,
  `CreatedBy` INT(5) NULL DEFAULT NULL ,
  `LastModifiedDate` DATETIME NULL DEFAULT NULL ,
  `LastModifiedBy` INT(5) NULL DEFAULT NULL ,
  `Status` ENUM('0','1') NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE INDEX `langCode` ON `ocast_db`.`tmp_quick_fact_lang` (`LangCode` ASC) ;

CREATE INDEX `quickFactId_idx` ON `ocast_db`.`tmp_quick_fact_lang` (`QuickFactID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`tmp_site`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`tmp_site` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`tmp_site` (
  `ID` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `TransactionID` BIGINT(20) NULL DEFAULT NULL ,
  `ParentID` BIGINT(20) NULL DEFAULT NULL ,
  `ProfileID` BIGINT(20) NULL DEFAULT NULL ,
  `GoogleSiteID` VARCHAR(12) NULL DEFAULT NULL ,
  `AnalyticsCreatedDate` DATE NULL DEFAULT NULL ,
  `URL` VARCHAR(200) NULL DEFAULT NULL ,
  `Logo` VARCHAR(100) NULL DEFAULT NULL ,
  `AverageAge` INT(2) NULL DEFAULT NULL ,
  `MalePercentage` DECIMAL(5,2) NULL DEFAULT NULL ,
  `IsMobile` ENUM('0','1') NULL DEFAULT NULL ,
  `SubscriptionPlanID` INT(3) NULL DEFAULT NULL ,
  `ValidPeriod` INT(2) NULL DEFAULT NULL ,
  `ActivatedDate` DATE NULL DEFAULT NULL ,
  `ActivationStatus` VARCHAR(15) NULL DEFAULT NULL ,
  `CompanyID` INT(5) NULL DEFAULT NULL ,
  `ExpiryDate` DATE NULL DEFAULT NULL ,
  `LastGoogleUpdate` DATETIME NULL DEFAULT NULL ,
  `CreatedDate` DATETIME NULL DEFAULT NULL ,
  `CreatedBy` INT(5) NULL DEFAULT NULL ,
  `LastModifiedDate` DATETIME NULL DEFAULT NULL ,
  `LastModifiedBy` INT(5) NULL DEFAULT NULL ,
  `Isdownloading` ENUM('0','1') NULL DEFAULT NULL ,
  `Status` ENUM('0','1') NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE INDEX `profileId_idx` ON `ocast_db`.`tmp_site` (`ProfileID` ASC) ;

CREATE INDEX `goog_site_id` ON `ocast_db`.`tmp_site` (`GoogleSiteID` ASC) ;

CREATE INDEX `company_id` ON `ocast_db`.`tmp_site` (`CompanyID` ASC) ;

CREATE INDEX `subscriptionPlanId_idx` ON `ocast_db`.`tmp_site` (`SubscriptionPlanID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`tmp_site_category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`tmp_site_category` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`tmp_site_category` (
  `ID` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `TransactionID` BIGINT(20) NULL DEFAULT NULL ,
  `SiteID` BIGINT(20) NULL DEFAULT NULL ,
  `CategoryID` BIGINT(20) NULL DEFAULT NULL ,
  `CreatedDate` DATETIME NULL DEFAULT NULL ,
  `CreatedBy` INT(5) NULL DEFAULT NULL ,
  `LastModifiedDate` DATETIME NULL DEFAULT NULL ,
  `LastModifiedBy` INT(5) NULL DEFAULT NULL ,
  `Status` ENUM('0','1') NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE INDEX `site_id` ON `ocast_db`.`tmp_site_category` (`SiteID` ASC) ;

CREATE INDEX `category_id` ON `ocast_db`.`tmp_site_category` (`CategoryID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`tmp_site_contact`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`tmp_site_contact` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`tmp_site_contact` (
  `ID` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `TransactionID` BIGINT(20) NULL DEFAULT NULL ,
  `SiteID` BIGINT(20) NULL DEFAULT NULL ,
  `Fname` VARCHAR(45) NULL DEFAULT NULL ,
  `IsDefault` ENUM('0','1') NULL DEFAULT NULL ,
  `Lname` VARCHAR(45) NULL DEFAULT NULL ,
  `Email` VARCHAR(75) NULL DEFAULT NULL ,
  `ConcatNumber` VARCHAR(25) NULL DEFAULT NULL ,
  `ProfilePic` VARCHAR(45) NULL DEFAULT NULL ,
  `ContactCountryCode` VARCHAR(15) NULL DEFAULT NULL ,
  `SortingOrder` INT(5) NULL DEFAULT NULL ,
  `CreatedBy` INT(5) NULL DEFAULT NULL ,
  `CreatedDate` DATETIME NULL DEFAULT NULL ,
  `LastModifiedBy` INT(5) NULL DEFAULT NULL ,
  `LastModifiedDate` DATETIME NULL DEFAULT NULL ,
  `Status` ENUM('0','1') NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE INDEX `site_id` ON `ocast_db`.`tmp_site_contact` (`SiteID` ASC) ;

CREATE INDEX `FK_site_id_idx` ON `ocast_db`.`tmp_site_contact` (`SiteID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`tmp_site_contact_lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`tmp_site_contact_lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`tmp_site_contact_lang` (
  `ID` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `TransactionID` BIGINT(20) NULL DEFAULT NULL ,
  `SiteContactID` BIGINT(20) NULL DEFAULT NULL ,
  `LangCode` VARCHAR(5) NULL DEFAULT NULL ,
  `Designation` VARCHAR(45) NULL DEFAULT NULL ,
  `CreatedDate` DATETIME NULL DEFAULT NULL ,
  `CreatedBy` INT(5) NULL DEFAULT NULL ,
  `LastModifiedDate` DATETIME NULL DEFAULT NULL ,
  `LastModifiedBy` INT(5) NULL DEFAULT NULL ,
  `Status` ENUM('0','1') NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE INDEX `lanCode` ON `ocast_db`.`tmp_site_contact_lang` (`LangCode` ASC) ;

CREATE INDEX `SiteContactID_idx` ON `ocast_db`.`tmp_site_contact_lang` (`SiteContactID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`tmp_site_country`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`tmp_site_country` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`tmp_site_country` (
  `ID` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `TransactionID` BIGINT(20) NULL DEFAULT NULL ,
  `CountryID` BIGINT(20) NULL DEFAULT NULL ,
  `SiteID` BIGINT(20) NOT NULL ,
  `IsDefault` ENUM('0','1') NULL DEFAULT NULL ,
  `CreatedDate` DATETIME NULL DEFAULT NULL ,
  `CreatedBy` INT(5) NULL DEFAULT NULL ,
  `LastModifiedDate` DATETIME NULL DEFAULT NULL ,
  `LastModifiedBy` INT(5) NULL DEFAULT NULL ,
  `Status` ENUM('0','1') NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE INDEX `country_id` ON `ocast_db`.`tmp_site_country` (`CountryID` ASC) ;

CREATE INDEX `fk_tmp_siteCountry_tmp_site1_idx` ON `ocast_db`.`tmp_site_country` (`SiteID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`tmp_site_device`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`tmp_site_device` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`tmp_site_device` (
  `ID` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `TransactionID` BIGINT(20) NULL DEFAULT NULL ,
  `SiteID` BIGINT(20) NULL DEFAULT NULL ,
  `DeviceID` BIGINT(20) NULL DEFAULT NULL ,
  `CreatedDate` DATETIME NULL DEFAULT NULL ,
  `CreatedBy` INT(5) NULL DEFAULT NULL ,
  `LastModifiedDate` DATETIME NULL DEFAULT NULL ,
  `LastModifiedBy` INT(5) NULL DEFAULT NULL ,
  `Status` ENUM('0','1') NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE INDEX `siteId_idx` ON `ocast_db`.`tmp_site_device` (`SiteID` ASC) ;

CREATE INDEX `deviceId_idx` ON `ocast_db`.`tmp_site_device` (`DeviceID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`tmp_site_lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`tmp_site_lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`tmp_site_lang` (
  `ID` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `TransactionID` BIGINT(20) NULL DEFAULT NULL ,
  `SiteID` BIGINT(20) NULL DEFAULT NULL ,
  `LangCode` VARCHAR(5) NULL DEFAULT NULL ,
  `Description` TEXT NULL DEFAULT NULL ,
  `MetaDescription` VARCHAR(250) NULL DEFAULT NULL ,
  `Meta_Keywords` VARCHAR(250) NULL DEFAULT NULL ,
  `Audience` VARCHAR(250) NULL DEFAULT NULL ,
  `CreatedDate` DATETIME NULL DEFAULT NULL ,
  `CreatedBy` INT(5) NULL DEFAULT NULL ,
  `LastModifiedDate` DATETIME NULL DEFAULT NULL ,
  `LastModifiedBy` INT(5) NULL DEFAULT NULL ,
  `Status` ENUM('0','1') NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE INDEX `langCode_idx` ON `ocast_db`.`tmp_site_lang` (`LangCode` ASC) ;

CREATE INDEX `siteId_idx` ON `ocast_db`.`tmp_site_lang` (`SiteID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`tmp_testimonial`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`tmp_testimonial` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`tmp_testimonial` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `TransactionID` BIGINT(20) NULL DEFAULT NULL ,
  `SiteID` INT(5) NULL DEFAULT NULL ,
  `UpdateDate` DATE NULL DEFAULT NULL ,
  `CreationDate` DATETIME NULL DEFAULT NULL ,
  `CreatedBy` INT(5) NULL DEFAULT NULL ,
  `LastModifiedDate` DATETIME NULL DEFAULT NULL ,
  `LastModifiedBy` INT(5) NULL DEFAULT NULL ,
  `Status` ENUM('0','1') NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE INDEX `site_id` ON `ocast_db`.`tmp_testimonial` (`SiteID` ASC) ;


-- -----------------------------------------------------
-- Table `ocast_db`.`tmp_testimonial_lang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ocast_db`.`tmp_testimonial_lang` ;

CREATE  TABLE IF NOT EXISTS `ocast_db`.`tmp_testimonial_lang` (
  `ID` INT(5) NOT NULL AUTO_INCREMENT ,
  `TransactionID` BIGINT(20) NULL DEFAULT NULL ,
  `TestimonialID` INT(5) NULL DEFAULT NULL ,
  `Quote` TEXT NULL DEFAULT NULL ,
  `ByWhom` VARCHAR(45) NULL DEFAULT NULL ,
  `LangCode` VARCHAR(5) NULL DEFAULT NULL ,
  `CreatedDate` DATETIME NULL DEFAULT NULL ,
  `CreatedBy` INT(5) NULL DEFAULT NULL ,
  `LastModifiedDate` DATETIME NULL DEFAULT NULL ,
  `LastModifiedBy` INT(5) NULL DEFAULT NULL ,
  `Status` ENUM('0','1') NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE INDEX `langCode` ON `ocast_db`.`tmp_testimonial_lang` (`LangCode` ASC) ;

CREATE INDEX `testimonialId_idx` ON `ocast_db`.`tmp_testimonial_lang` (`TestimonialID` ASC) ;

USE `ocast_db` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
