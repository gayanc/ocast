truncate set_category;
truncate set_category_lang;
truncate set_format;
truncate set_format_lang;
truncate set_price_type;
truncate set_price_type_lang;

INSERT INTO `set_category` (`ID`, `SortingOrder`, `LastUpdate`, `CreatedDate`, `CreatedBy`, `LastModifiedDate`, `LastModifiedBy`) VALUES
(1, 1, '2013-10-08 12:08:00', '2013-10-08 12:08:02', NULL, '0000-00-00 00:00:00', NULL),
(2, 1, '2013-10-08 12:08:00', '2013-10-08 12:08:02', NULL, '0000-00-00 00:00:00', NULL),
(3, 1, '2013-10-08 12:08:00', '2013-10-08 12:08:02', NULL, '0000-00-00 00:00:00', NULL);

INSERT INTO `set_category_lang` (`ID`, `CategoryID`, `Description`, `LangCode`, `CreatedDate`, `CreatedBy`, `LastModifiedDate`, `LastModifiedBy`) VALUES
(1, 1, 'Travel Websites', 'en', '2013-10-08 12:13:52', NULL, '2013-10-08 12:13:52', NULL),
(2, 2, 'Mobile app', 'en', '2013-10-08 12:13:52', NULL, '2013-10-08 12:13:52', NULL),
(3, 3, 'Music Search Engines', 'en', '2013-10-08 12:13:52', NULL, '2013-10-08 12:13:52', NULL),
(4, 4, 'Online Ad Servers', 'en', '2013-10-08 12:13:52', NULL, '2013-10-08 12:13:52', NULL),
(5, 1, 'Resor webbplatser', 'sv', '2013-10-08 12:13:52', NULL, '2013-10-08 12:13:52', NULL),
(6, 2, 'Mobile app', 'sv', '2013-10-08 12:13:52', NULL, '2013-10-08 12:13:52', NULL),
(7, 3, 'Musik Sökmotorer', 'sv', '2013-10-08 12:13:52', NULL, '2013-10-08 12:13:52', NULL),
(8, 4, 'Online Ad Servers', 'sv', '2013-10-08 12:13:52', NULL, '2013-10-08 12:13:52', NULL);

INSERT INTO `set_format` (`ID`, `CreatedDate`, `CreatedBy`, `LastModifiedDate`, `LastModifiedBy`) VALUES
(1, '2013-10-14 00:00:00', NULL, '2013-10-14 00:00:00', NULL),
(2, '2013-10-16 10:00:02', NULL, NULL, NULL),
(3, '2013-10-16 10:00:02', NULL, NULL, NULL),
(4, '2013-10-16 10:00:02', NULL, NULL, NULL),
(5, '2013-10-16 10:00:02', NULL, NULL, NULL),
(6, '2013-10-16 10:00:02', NULL, NULL, NULL),
(7, '2013-10-16 10:00:02', NULL, NULL, NULL),
(8, '2013-10-16 10:00:02', NULL, NULL, NULL);

INSERT INTO `set_format_lang` (`ID`, `FormatID`, `LangCode`, `Title`, `CreatedDate`, `CreatedBy`, `LastModifiedDate`, `LastModifiedBy`) VALUES
(1, 1, 'en', 'Banner', NULL, NULL, NULL, NULL),
(2, 2, 'en', 'Billboard ', NULL, NULL, NULL, NULL),
(3, 1, 'sv', 'Banner', NULL, NULL, NULL, NULL),
(4, 2, 'sv', 'Billboard', NULL, NULL, NULL, NULL);

INSERT INTO `set_price_type` (`ID`, `CreatedDate`, `CreatedBy`, `LastModifiedDate`, `LastModifiedBy`) VALUES
(1, NULL, NULL, NULL, NULL),
(2, NULL, NULL, NULL, NULL);


INSERT INTO `set_price_type_lang` (`ID`, `Text`, `PriceTypeID`, `LangCode`, `CreatedDate`, `CreatedBy`, `lastModifiedDate`, `lastModifiedBy`) VALUES
(1, 'Request Price', 1, 'en', NULL, NULL, NULL, NULL),
(2, 'Negotiable', 2, 'en', NULL, NULL, NULL, NULL);