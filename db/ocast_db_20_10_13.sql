-- phpMyAdmin SQL Dump
-- version 3.3.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 20, 2013 at 09:02 PM
-- Server version: 5.1.54
-- PHP Version: 5.3.5-1ubuntu7.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ocast_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `set_category`
--

CREATE TABLE IF NOT EXISTS `set_category` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `SortingOrder` int(5) DEFAULT NULL,
  `LastUpdate` datetime DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `set_category`
--

INSERT INTO `set_category` (`ID`, `SortingOrder`, `LastUpdate`, `CreatedDate`, `CreatedBy`, `LastModifiedDate`, `LastModifiedBy`) VALUES
(1, 1, '2013-10-08 12:08:00', '2013-10-08 12:08:02', NULL, '0000-00-00 00:00:00', NULL),
(2, 1, '2013-10-08 12:08:00', '2013-10-08 12:08:02', NULL, '0000-00-00 00:00:00', NULL),
(3, 1, '2013-10-08 12:08:00', '2013-10-08 12:08:02', NULL, '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `set_category_lang`
--

CREATE TABLE IF NOT EXISTS `set_category_lang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `CategoryID` int(5) DEFAULT NULL,
  `Description` varchar(45) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `categoryID_idx` (`CategoryID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `set_category_lang`
--

INSERT INTO `set_category_lang` (`ID`, `CategoryID`, `Description`, `LangCode`, `CreatedDate`, `CreatedBy`, `LastModifiedDate`, `LastModifiedBy`) VALUES
(1, 1, 'Travel Websites', 'en', '2013-10-08 12:13:52', NULL, '2013-10-08 12:13:52', NULL),
(2, 2, 'Mobile app', 'en', '2013-10-08 12:13:52', NULL, '2013-10-08 12:13:52', NULL),
(3, 3, 'Music Search Engines', 'en', '2013-10-08 12:13:52', NULL, '2013-10-08 12:13:52', NULL),
(4, 4, 'Online Ad Servers', 'en', '2013-10-08 12:13:52', NULL, '2013-10-08 12:13:52', NULL),
(5, 1, 'Resor webbplatser', 'sv', '2013-10-08 12:13:52', NULL, '2013-10-08 12:13:52', NULL),
(6, 2, 'Mobile app', 'sv', '2013-10-08 12:13:52', NULL, '2013-10-08 12:13:52', NULL),
(7, 3, 'Musik Sökmotorer', 'sv', '2013-10-08 12:13:52', NULL, '2013-10-08 12:13:52', NULL),
(8, 4, 'Online Ad Servers', 'sv', '2013-10-08 12:13:52', NULL, '2013-10-08 12:13:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `set_component`
--

CREATE TABLE IF NOT EXISTS `set_component` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `set_component`
--


-- --------------------------------------------------------

--
-- Table structure for table `set_faq`
--

CREATE TABLE IF NOT EXISTS `set_faq` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `SortingOrder` int(5) DEFAULT NULL,
  `FAQCategoryID` int(5) DEFAULT NULL,
  `IsEnable` enum('0','1') DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FAQCategoryID_idx` (`FAQCategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `set_faq`
--


-- --------------------------------------------------------

--
-- Table structure for table `set_faq_category`
--

CREATE TABLE IF NOT EXISTS `set_faq_category` (
  `ID` int(5) NOT NULL,
  `SortingOrder` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `set_faq_category`
--


-- --------------------------------------------------------

--
-- Table structure for table `set_faq_category_lang`
--

CREATE TABLE IF NOT EXISTS `set_faq_category_lang` (
  `ID` int(5) NOT NULL,
  `FAQCategoryID` int(5) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CategoryName` varchar(250) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FAQCategoryID_idx` (`FAQCategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `set_faq_category_lang`
--


-- --------------------------------------------------------

--
-- Table structure for table `set_faq_lang`
--

CREATE TABLE IF NOT EXISTS `set_faq_lang` (
  `ID` int(5) NOT NULL,
  `FaqID` int(5) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `Question` text,
  `Answer` text,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `fqaID_idx` (`FaqID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `set_faq_lang`
--


-- --------------------------------------------------------

--
-- Table structure for table `set_format`
--

CREATE TABLE IF NOT EXISTS `set_format` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `set_format`
--

INSERT INTO `set_format` (`ID`, `CreatedDate`, `CreatedBy`, `LastModifiedDate`, `LastModifiedBy`) VALUES
(1, '2013-10-14 00:00:00', NULL, '2013-10-14 00:00:00', NULL),
(2, '2013-10-16 10:00:02', NULL, NULL, NULL),
(3, '2013-10-16 10:00:02', NULL, NULL, NULL),
(4, '2013-10-16 10:00:02', NULL, NULL, NULL),
(5, '2013-10-16 10:00:02', NULL, NULL, NULL),
(6, '2013-10-16 10:00:02', NULL, NULL, NULL),
(7, '2013-10-16 10:00:02', NULL, NULL, NULL),
(8, '2013-10-16 10:00:02', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `set_format_lang`
--

CREATE TABLE IF NOT EXISTS `set_format_lang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `FormatID` int(5) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `Title` varchar(200) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FormatID_idx` (`FormatID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `set_format_lang`
--

INSERT INTO `set_format_lang` (`ID`, `FormatID`, `LangCode`, `Title`, `CreatedDate`, `CreatedBy`, `LastModifiedDate`, `LastModifiedBy`) VALUES
(1, 1, 'en', 'Banner', NULL, NULL, NULL, NULL),
(2, 2, 'en', 'Billboard ', NULL, NULL, NULL, NULL),
(3, 1, 'sv', 'Banner', NULL, NULL, NULL, NULL),
(4, 2, 'sv', 'Billboard', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `set_location`
--

CREATE TABLE IF NOT EXISTS `set_location` (
  `location_id` int(5) NOT NULL AUTO_INCREMENT,
  `ISO2` char(2) NOT NULL,
  `location_name` varchar(100) NOT NULL,
  `location_long_name` varchar(100) NOT NULL,
  `ISO3` char(3) NOT NULL,
  `NumCode` varchar(6) NOT NULL,
  `UNMemberState` varchar(12) NOT NULL,
  `CallingCode` varchar(8) NOT NULL,
  `CCTLD` varchar(5) NOT NULL,
  `enable` enum('1','0') NOT NULL DEFAULT '0',
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`location_id`),
  KEY `indlocation_id` (`location_id`),
  KEY `indlocation_name` (`location_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `set_location`
--


-- --------------------------------------------------------

--
-- Table structure for table `set_menuitem`
--

CREATE TABLE IF NOT EXISTS `set_menuitem` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `MenuName` varchar(100) DEFAULT NULL,
  `ParentID` int(5) NOT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `set_menuitem`
--


-- --------------------------------------------------------

--
-- Table structure for table `set_menu_item`
--

CREATE TABLE IF NOT EXISTS `set_menu_item` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `MenuName` varchar(100) DEFAULT NULL,
  `ParentID` int(5) NOT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `set_menu_item`
--


-- --------------------------------------------------------

--
-- Table structure for table `set_price_type`
--

CREATE TABLE IF NOT EXISTS `set_price_type` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `set_price_type`
--

INSERT INTO `set_price_type` (`ID`, `CreatedDate`, `CreatedBy`, `LastModifiedDate`, `LastModifiedBy`) VALUES
(1, NULL, NULL, NULL, NULL),
(2, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `set_price_type_lang`
--

CREATE TABLE IF NOT EXISTS `set_price_type_lang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `Text` varchar(100) DEFAULT NULL,
  `PriceTypeID` int(5) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `lastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `set_price_type_lang`
--

INSERT INTO `set_price_type_lang` (`ID`, `Text`, `PriceTypeID`, `LangCode`, `CreatedDate`, `CreatedBy`, `lastModifiedDate`, `lastModifiedBy`) VALUES
(1, 'Request Price', 1, 'en', NULL, NULL, NULL, NULL),
(2, 'Negotiable', 2, 'en', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `set_subscription_plan`
--

CREATE TABLE IF NOT EXISTS `set_subscription_plan` (
  `ID` int(5) NOT NULL,
  `SortingOrder` int(5) DEFAULT NULL,
  `Duration` int(2) DEFAULT NULL,
  `Price` decimal(5,2) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `set_subscription_plan`
--


-- --------------------------------------------------------

--
-- Table structure for table `set_subscription_plan_lang`
--

CREATE TABLE IF NOT EXISTS `set_subscription_plan_lang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `PlanID` int(5) DEFAULT NULL,
  `Description` varchar(250) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` date DEFAULT NULL,
  `CreatedBy` int(5) NOT NULL,
  `LastModifiedDate` date DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `planId_idx` (`PlanID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `set_subscription_plan_lang`
--


-- --------------------------------------------------------

--
-- Table structure for table `set_user`
--

CREATE TABLE IF NOT EXISTS `set_user` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(150) DEFAULT NULL,
  `Password` varchar(250) DEFAULT NULL,
  `Salt` varchar(250) DEFAULT NULL,
  `Fname` varchar(20) DEFAULT NULL,
  `Lname` varchar(20) DEFAULT NULL,
  `AciveStatus` varchar(20) DEFAULT NULL,
  `UserTypeID` int(5) DEFAULT NULL,
  `CompanyID` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `PasswordResetCode` varchar(250) DEFAULT NULL,
  `ResetRequestDate` datetime DEFAULT NULL,
  `LastLogin` datetime DEFAULT NULL,
  `LastLoginIP` varchar(15) DEFAULT NULL,
  `LoginKey` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `userNameUNQ` (`UserName`),
  KEY `companyId_idx` (`CompanyID`),
  KEY `userTypeId_idx` (`UserTypeID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `set_user`
--

INSERT INTO `set_user` (`ID`, `UserName`, `Password`, `Salt`, `Fname`, `Lname`, `AciveStatus`, `UserTypeID`, `CompanyID`, `CreatedDate`, `CreatedBy`, `LastModifiedDate`, `LastModifiedBy`, `PasswordResetCode`, `ResetRequestDate`, `LastLogin`, `LastLoginIP`, `LoginKey`) VALUES
(1, 'lasith.g@eyepax.com', '21232f297a57a5a743894a0e4a801fc3', '26374842573d1e51ba1e0e859adc06e89d771fcd', 'Lasith', 'Gunawardana', 'active', 1, 1, '2013-10-08 00:00:00', 1, '2013-10-08 00:00:00', 1, NULL, NULL, '2013-10-14 08:48:11', '127.0.0.1', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `set_user_permissions`
--

CREATE TABLE IF NOT EXISTS `set_user_permissions` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `ModuleID` int(5) DEFAULT NULL,
  `UserTypeID` int(5) DEFAULT NULL,
  `IsAllowed` enum('0','1') DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `userTypeId_idx` (`UserTypeID`),
  KEY `ModuleID_idx` (`ModuleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `set_user_permissions`
--


-- --------------------------------------------------------

--
-- Table structure for table `set_user_site`
--

CREATE TABLE IF NOT EXISTS `set_user_site` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `UserID` int(5) DEFAULT NULL,
  `SiteID` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `userId_idx` (`UserID`),
  KEY `siteId_idx` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `set_user_site`
--


-- --------------------------------------------------------

--
-- Table structure for table `set_user_type`
--

CREATE TABLE IF NOT EXISTS `set_user_type` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `UserTypeName` varchar(25) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `set_user_type`
--


-- --------------------------------------------------------

--
-- Table structure for table `sys_country`
--

CREATE TABLE IF NOT EXISTS `sys_country` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `ISO` varchar(2) NOT NULL,
  `CallingCode` varchar(8) DEFAULT NULL,
  `IsActive` enum('0','1') DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`,`ISO`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=251 ;

--
-- Dumping data for table `sys_country`
--

INSERT INTO `sys_country` (`ID`, `ISO`, `CallingCode`, `IsActive`, `LastModifiedDate`, `CreatedDate`) VALUES
(1, 'AD', '376', NULL, NULL, NULL),
(2, 'AE', '971', NULL, NULL, NULL),
(3, 'AF', '93', NULL, NULL, NULL),
(4, 'AG', '1+268', NULL, NULL, NULL),
(5, 'AI', '1+264', NULL, NULL, NULL),
(6, 'AL', '355', NULL, NULL, NULL),
(7, 'AM', '374', NULL, NULL, NULL),
(8, 'AO', '244', NULL, NULL, NULL),
(9, 'AQ', '672', NULL, NULL, NULL),
(10, 'AR', '54', NULL, NULL, NULL),
(11, 'AS', '1+684', NULL, NULL, NULL),
(12, 'AT', '43', NULL, NULL, NULL),
(13, 'AU', '61', NULL, NULL, NULL),
(14, 'AW', '297', NULL, NULL, NULL),
(15, 'AX', '358', NULL, NULL, NULL),
(16, 'AZ', '994', NULL, NULL, NULL),
(17, 'BA', '387', NULL, NULL, NULL),
(18, 'BB', '1+246', NULL, NULL, NULL),
(19, 'BD', '880', NULL, NULL, NULL),
(20, 'BE', '32', NULL, NULL, NULL),
(21, 'BF', '226', NULL, NULL, NULL),
(22, 'BG', '359', NULL, NULL, NULL),
(23, 'BH', '973', NULL, NULL, NULL),
(24, 'BI', '257', NULL, NULL, NULL),
(25, 'BJ', '229', NULL, NULL, NULL),
(26, 'BL', '590', NULL, NULL, NULL),
(27, 'BM', '1+441', NULL, NULL, NULL),
(28, 'BN', '673', NULL, NULL, NULL),
(29, 'BO', '591', NULL, NULL, NULL),
(30, 'BQ', '599', NULL, NULL, NULL),
(31, 'BR', '55', NULL, NULL, NULL),
(32, 'BS', '1+242', NULL, NULL, NULL),
(33, 'BT', '975', NULL, NULL, NULL),
(34, 'BV', 'NONE', NULL, NULL, NULL),
(35, 'BW', '267', NULL, NULL, NULL),
(36, 'BY', '375', NULL, NULL, NULL),
(37, 'BZ', '501', NULL, NULL, NULL),
(38, 'CA', '1', NULL, NULL, NULL),
(39, 'CC', '61', NULL, NULL, NULL),
(40, 'CD', '243', NULL, NULL, NULL),
(41, 'CF', '236', NULL, NULL, NULL),
(42, 'CG', '242', NULL, NULL, NULL),
(43, 'CH', '41', NULL, NULL, NULL),
(44, 'CI', '225', NULL, NULL, NULL),
(45, 'CK', '682', NULL, NULL, NULL),
(46, 'CL', '56', NULL, NULL, NULL),
(47, 'CM', '237', NULL, NULL, NULL),
(48, 'CN', '86', NULL, NULL, NULL),
(49, 'CO', '57', NULL, NULL, NULL),
(50, 'CR', '506', NULL, NULL, NULL),
(51, 'CU', '53', NULL, NULL, NULL),
(52, 'CV', '238', NULL, NULL, NULL),
(53, 'CW', '599', NULL, NULL, NULL),
(54, 'CX', '61', NULL, NULL, NULL),
(55, 'CY', '357', NULL, NULL, NULL),
(56, 'CZ', '420', NULL, NULL, NULL),
(57, 'DE', '49', NULL, NULL, NULL),
(58, 'DJ', '253', NULL, NULL, NULL),
(59, 'DK', '45', NULL, NULL, NULL),
(60, 'DM', '1+767', NULL, NULL, NULL),
(61, 'DO', '1+809, 8', NULL, NULL, NULL),
(62, 'DZ', '213', NULL, NULL, NULL),
(63, 'EC', '593', NULL, NULL, NULL),
(64, 'EE', '372', NULL, NULL, NULL),
(65, 'EG', '20', NULL, NULL, NULL),
(66, 'EH', '212', NULL, NULL, NULL),
(67, 'ER', '291', NULL, NULL, NULL),
(68, 'ES', '34', NULL, NULL, NULL),
(69, 'ET', '251', NULL, NULL, NULL),
(70, 'FI', '358', NULL, NULL, NULL),
(71, 'FJ', '679', NULL, NULL, NULL),
(72, 'FK', '500', NULL, NULL, NULL),
(73, 'FM', '691', NULL, NULL, NULL),
(74, 'FO', '298', NULL, NULL, NULL),
(75, 'FR', '33', NULL, NULL, NULL),
(76, 'GA', '241', NULL, NULL, NULL),
(77, 'GB', '44', NULL, NULL, NULL),
(78, 'GD', '1+473', NULL, NULL, NULL),
(79, 'GE', '995', NULL, NULL, NULL),
(80, 'GF', '594', NULL, NULL, NULL),
(81, 'GG', '44', NULL, NULL, NULL),
(82, 'GH', '233', NULL, NULL, NULL),
(83, 'GI', '350', NULL, NULL, NULL),
(84, 'GL', '299', NULL, NULL, NULL),
(85, 'GM', '220', NULL, NULL, NULL),
(86, 'GN', '224', NULL, NULL, NULL),
(87, 'GP', '590', NULL, NULL, NULL),
(88, 'GQ', '240', NULL, NULL, NULL),
(89, 'GR', '30', NULL, NULL, NULL),
(90, 'GS', '500', NULL, NULL, NULL),
(91, 'GT', '502', NULL, NULL, NULL),
(92, 'GU', '1+671', NULL, NULL, NULL),
(93, 'GW', '245', NULL, NULL, NULL),
(94, 'GY', '592', NULL, NULL, NULL),
(95, 'HK', '852', NULL, NULL, NULL),
(96, 'HM', 'NONE', NULL, NULL, NULL),
(97, 'HN', '504', NULL, NULL, NULL),
(98, 'HR', '385', NULL, NULL, NULL),
(99, 'HT', '509', NULL, NULL, NULL),
(100, 'HU', '36', NULL, NULL, NULL),
(101, 'ID', '62', NULL, NULL, NULL),
(102, 'IE', '353', NULL, NULL, NULL),
(103, 'IL', '972', NULL, NULL, NULL),
(104, 'IM', '44', NULL, NULL, NULL),
(105, 'IN', '91', NULL, NULL, NULL),
(106, 'IO', '246', NULL, NULL, NULL),
(107, 'IQ', '964', NULL, NULL, NULL),
(108, 'IR', '98', NULL, NULL, NULL),
(109, 'IS', '354', NULL, NULL, NULL),
(110, 'IT', '39', NULL, NULL, NULL),
(111, 'JE', '44', NULL, NULL, NULL),
(112, 'JM', '1+876', NULL, NULL, NULL),
(113, 'JO', '962', NULL, NULL, NULL),
(114, 'JP', '81', NULL, NULL, NULL),
(115, 'KE', '254', NULL, NULL, NULL),
(116, 'KG', '996', NULL, NULL, NULL),
(117, 'KH', '855', NULL, NULL, NULL),
(118, 'KI', '686', NULL, NULL, NULL),
(119, 'KM', '269', NULL, NULL, NULL),
(120, 'KN', '1+869', NULL, NULL, NULL),
(121, 'KP', '850', NULL, NULL, NULL),
(122, 'KR', '82', NULL, NULL, NULL),
(123, 'KW', '965', NULL, NULL, NULL),
(124, 'KY', '1+345', NULL, NULL, NULL),
(125, 'KZ', '7', NULL, NULL, NULL),
(126, 'LA', '856', NULL, NULL, NULL),
(127, 'LB', '961', NULL, NULL, NULL),
(128, 'LC', '1+758', NULL, NULL, NULL),
(129, 'LI', '423', NULL, NULL, NULL),
(130, 'LK', '94', NULL, NULL, NULL),
(131, 'LR', '231', NULL, NULL, NULL),
(132, 'LS', '266', NULL, NULL, NULL),
(133, 'LT', '370', NULL, NULL, NULL),
(134, 'LU', '352', NULL, NULL, NULL),
(135, 'LV', '371', NULL, NULL, NULL),
(136, 'LY', '218', NULL, NULL, NULL),
(137, 'MA', '212', NULL, NULL, NULL),
(138, 'MC', '377', NULL, NULL, NULL),
(139, 'MD', '373', NULL, NULL, NULL),
(140, 'ME', '382', NULL, NULL, NULL),
(141, 'MF', '590', NULL, NULL, NULL),
(142, 'MG', '261', NULL, NULL, NULL),
(143, 'MH', '692', NULL, NULL, NULL),
(144, 'MK', '389', NULL, NULL, NULL),
(145, 'ML', '223', NULL, NULL, NULL),
(146, 'MM', '95', NULL, NULL, NULL),
(147, 'MN', '976', NULL, NULL, NULL),
(148, 'MO', '853', NULL, NULL, NULL),
(149, 'MP', '1+670', NULL, NULL, NULL),
(150, 'MQ', '596', NULL, NULL, NULL),
(151, 'MR', '222', NULL, NULL, NULL),
(152, 'MS', '1+664', NULL, NULL, NULL),
(153, 'MT', '356', NULL, NULL, NULL),
(154, 'MU', '230', NULL, NULL, NULL),
(155, 'MV', '960', NULL, NULL, NULL),
(156, 'MW', '265', NULL, NULL, NULL),
(157, 'MX', '52', NULL, NULL, NULL),
(158, 'MY', '60', NULL, NULL, NULL),
(159, 'MZ', '258', NULL, NULL, NULL),
(160, 'NA', '264', NULL, NULL, NULL),
(161, 'NC', '687', NULL, NULL, NULL),
(162, 'NE', '227', NULL, NULL, NULL),
(163, 'NF', '672', NULL, NULL, NULL),
(164, 'NG', '234', NULL, NULL, NULL),
(165, 'NI', '505', NULL, NULL, NULL),
(166, 'NL', '31', NULL, NULL, NULL),
(167, 'NO', '47', NULL, NULL, NULL),
(168, 'NP', '977', NULL, NULL, NULL),
(169, 'NR', '674', NULL, NULL, NULL),
(170, 'NU', '683', NULL, NULL, NULL),
(171, 'NZ', '64', NULL, NULL, NULL),
(172, 'OM', '968', NULL, NULL, NULL),
(173, 'PA', '507', NULL, NULL, NULL),
(174, 'PE', '51', NULL, NULL, NULL),
(175, 'PF', '689', NULL, NULL, NULL),
(176, 'PG', '675', NULL, NULL, NULL),
(177, 'PH', '63', NULL, NULL, NULL),
(178, 'PK', '92', NULL, NULL, NULL),
(179, 'PL', '48', NULL, NULL, NULL),
(180, 'PM', '508', NULL, NULL, NULL),
(181, 'PN', 'NONE', NULL, NULL, NULL),
(182, 'PR', '1+939', NULL, NULL, NULL),
(183, 'PS', '970', NULL, NULL, NULL),
(184, 'PT', '351', NULL, NULL, NULL),
(185, 'PW', '680', NULL, NULL, NULL),
(186, 'PY', '595', NULL, NULL, NULL),
(187, 'QA', '974', NULL, NULL, NULL),
(188, 'RE', '262', NULL, NULL, NULL),
(189, 'RO', '40', NULL, NULL, NULL),
(190, 'RS', '381', NULL, NULL, NULL),
(191, 'RU', '7', NULL, NULL, NULL),
(192, 'RW', '250', NULL, NULL, NULL),
(193, 'SA', '966', NULL, NULL, NULL),
(194, 'SB', '677', NULL, NULL, NULL),
(195, 'SC', '248', NULL, NULL, NULL),
(196, 'SD', '249', NULL, NULL, NULL),
(197, 'SE', '46', NULL, NULL, NULL),
(198, 'SG', '65', NULL, NULL, NULL),
(199, 'SH', '290', NULL, NULL, NULL),
(200, 'SI', '386', NULL, NULL, NULL),
(201, 'SJ', '47', NULL, NULL, NULL),
(202, 'SK', '421', NULL, NULL, NULL),
(203, 'SL', '232', NULL, NULL, NULL),
(204, 'SM', '378', NULL, NULL, NULL),
(205, 'SN', '221', NULL, NULL, NULL),
(206, 'SO', '252', NULL, NULL, NULL),
(207, 'SR', '597', NULL, NULL, NULL),
(208, 'SS', '211', NULL, NULL, NULL),
(209, 'ST', '239', NULL, NULL, NULL),
(210, 'SV', '503', NULL, NULL, NULL),
(211, 'SX', '1+721', NULL, NULL, NULL),
(212, 'SY', '963', NULL, NULL, NULL),
(213, 'SZ', '268', NULL, NULL, NULL),
(214, 'TC', '1+649', NULL, NULL, NULL),
(215, 'TD', '235', NULL, NULL, NULL),
(216, 'TF', '', NULL, NULL, NULL),
(217, 'TG', '228', NULL, NULL, NULL),
(218, 'TH', '66', NULL, NULL, NULL),
(219, 'TJ', '992', NULL, NULL, NULL),
(220, 'TK', '690', NULL, NULL, NULL),
(221, 'TL', '670', NULL, NULL, NULL),
(222, 'TM', '993', NULL, NULL, NULL),
(223, 'TN', '216', NULL, NULL, NULL),
(224, 'TO', '676', NULL, NULL, NULL),
(225, 'TR', '90', NULL, NULL, NULL),
(226, 'TT', '1+868', NULL, NULL, NULL),
(227, 'TV', '688', NULL, NULL, NULL),
(228, 'TW', '886', NULL, NULL, NULL),
(229, 'TZ', '255', NULL, NULL, NULL),
(230, 'UA', '380', NULL, NULL, NULL),
(231, 'UG', '256', NULL, NULL, NULL),
(232, 'UM', 'NONE', NULL, NULL, NULL),
(233, 'US', '1', NULL, NULL, NULL),
(234, 'UY', '598', NULL, NULL, NULL),
(235, 'UZ', '998', NULL, NULL, NULL),
(236, 'VA', '39', NULL, NULL, NULL),
(237, 'VC', '1+784', NULL, NULL, NULL),
(238, 'VE', '58', NULL, NULL, NULL),
(239, 'VG', '1+284', NULL, NULL, NULL),
(240, 'VI', '1+340', NULL, NULL, NULL),
(241, 'VN', '84', NULL, NULL, NULL),
(242, 'VU', '678', NULL, NULL, NULL),
(243, 'WF', '681', NULL, NULL, NULL),
(244, 'WS', '685', NULL, NULL, NULL),
(245, 'XK', '381', NULL, NULL, NULL),
(246, 'YE', '967', NULL, NULL, NULL),
(247, 'YT', '262', NULL, NULL, NULL),
(248, 'ZA', '27', NULL, NULL, NULL),
(249, 'ZM', '260', NULL, NULL, NULL),
(250, 'ZW', '263', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sys_country_lang`
--

CREATE TABLE IF NOT EXISTS `sys_country_lang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `CountryID` int(5) DEFAULT NULL,
  `CountryName` varchar(150) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `countryId_idx` (`CountryID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=751 ;

--
-- Dumping data for table `sys_country_lang`
--

INSERT INTO `sys_country_lang` (`ID`, `CountryID`, `CountryName`, `LangCode`, `CreatedDate`, `LastModifiedDate`) VALUES
(1, 1, 'Andorra', 'en', NULL, NULL),
(2, 2, 'United Arab Emirates', 'en', NULL, NULL),
(3, 3, 'Afghanistan', 'en', NULL, NULL),
(4, 4, 'Antigua and Barbuda', 'en', NULL, NULL),
(5, 5, 'Anguilla', 'en', NULL, NULL),
(6, 6, 'Albania', 'en', NULL, NULL),
(7, 7, 'Armenia', 'en', NULL, NULL),
(8, 8, 'Angola', 'en', NULL, NULL),
(9, 9, 'Antarctica', 'en', NULL, NULL),
(10, 10, 'Argentina', 'en', NULL, NULL),
(11, 11, 'American Samoa', 'en', NULL, NULL),
(12, 12, 'Austria', 'en', NULL, NULL),
(13, 13, 'Australia', 'en', NULL, NULL),
(14, 14, 'Aruba', 'en', NULL, NULL),
(15, 15, 'Aland Islands', 'en', NULL, NULL),
(16, 16, 'Azerbaijan', 'en', NULL, NULL),
(17, 17, 'Bosnia and Herzegovina', 'en', NULL, NULL),
(18, 18, 'Barbados', 'en', NULL, NULL),
(19, 19, 'Bangladesh', 'en', NULL, NULL),
(20, 20, 'Belgium', 'en', NULL, NULL),
(21, 21, 'Burkina Faso', 'en', NULL, NULL),
(22, 22, 'Bulgaria', 'en', NULL, NULL),
(23, 23, 'Bahrain', 'en', NULL, NULL),
(24, 24, 'Burundi', 'en', NULL, NULL),
(25, 25, 'Benin', 'en', NULL, NULL),
(26, 26, 'Saint Barthelemy', 'en', NULL, NULL),
(27, 27, 'Bermuda', 'en', NULL, NULL),
(28, 28, 'Brunei', 'en', NULL, NULL),
(29, 29, 'Bolivia', 'en', NULL, NULL),
(30, 30, 'Bonaire, Saint Eustatius and Saba ', 'en', NULL, NULL),
(31, 31, 'Brazil', 'en', NULL, NULL),
(32, 32, 'Bahamas', 'en', NULL, NULL),
(33, 33, 'Bhutan', 'en', NULL, NULL),
(34, 34, 'Bouvet Island', 'en', NULL, NULL),
(35, 35, 'Botswana', 'en', NULL, NULL),
(36, 36, 'Belarus', 'en', NULL, NULL),
(37, 37, 'Belize', 'en', NULL, NULL),
(38, 38, 'Canada', 'en', NULL, NULL),
(39, 39, 'Cocos Islands', 'en', NULL, NULL),
(40, 40, 'Democratic Republic of the Congo', 'en', NULL, NULL),
(41, 41, 'Central African Republic', 'en', NULL, NULL),
(42, 42, 'Republic of the Congo', 'en', NULL, NULL),
(43, 43, 'Switzerland', 'en', NULL, NULL),
(44, 44, 'Ivory Coast', 'en', NULL, NULL),
(45, 45, 'Cook Islands', 'en', NULL, NULL),
(46, 46, 'Chile', 'en', NULL, NULL),
(47, 47, 'Cameroon', 'en', NULL, NULL),
(48, 48, 'China', 'en', NULL, NULL),
(49, 49, 'Colombia', 'en', NULL, NULL),
(50, 50, 'Costa Rica', 'en', NULL, NULL),
(51, 51, 'Cuba', 'en', NULL, NULL),
(52, 52, 'Cape Verde', 'en', NULL, NULL),
(53, 53, 'Curacao', 'en', NULL, NULL),
(54, 54, 'Christmas Island', 'en', NULL, NULL),
(55, 55, 'Cyprus', 'en', NULL, NULL),
(56, 56, 'Czech Republic', 'en', NULL, NULL),
(57, 57, 'Germany', 'en', NULL, NULL),
(58, 58, 'Djibouti', 'en', NULL, NULL),
(59, 59, 'Denmark', 'en', NULL, NULL),
(60, 60, 'Dominica', 'en', NULL, NULL),
(61, 61, 'Dominican Republic', 'en', NULL, NULL),
(62, 62, 'Algeria', 'en', NULL, NULL),
(63, 63, 'Ecuador', 'en', NULL, NULL),
(64, 64, 'Estonia', 'en', NULL, NULL),
(65, 65, 'Egypt', 'en', NULL, NULL),
(66, 66, 'Western Sahara', 'en', NULL, NULL),
(67, 67, 'Eritrea', 'en', NULL, NULL),
(68, 68, 'Spain', 'en', NULL, NULL),
(69, 69, 'Ethiopia', 'en', NULL, NULL),
(70, 70, 'Finland', 'en', NULL, NULL),
(71, 71, 'Fiji', 'en', NULL, NULL),
(72, 72, 'Falkland Islands', 'en', NULL, NULL),
(73, 73, 'Micronesia', 'en', NULL, NULL),
(74, 74, 'Faroe Islands', 'en', NULL, NULL),
(75, 75, 'France', 'en', NULL, NULL),
(76, 76, 'Gabon', 'en', NULL, NULL),
(77, 77, 'United Kingdom', 'en', NULL, NULL),
(78, 78, 'Grenada', 'en', NULL, NULL),
(79, 79, 'Georgia', 'en', NULL, NULL),
(80, 80, 'French Guiana', 'en', NULL, NULL),
(81, 81, 'Guernsey', 'en', NULL, NULL),
(82, 82, 'Ghana', 'en', NULL, NULL),
(83, 83, 'Gibraltar', 'en', NULL, NULL),
(84, 84, 'Greenland', 'en', NULL, NULL),
(85, 85, 'Gambia', 'en', NULL, NULL),
(86, 86, 'Guinea', 'en', NULL, NULL),
(87, 87, 'Guadeloupe', 'en', NULL, NULL),
(88, 88, 'Equatorial Guinea', 'en', NULL, NULL),
(89, 89, 'Greece', 'en', NULL, NULL),
(90, 90, 'South Georgia and the South Sandwich Islands', 'en', NULL, NULL),
(91, 91, 'Guatemala', 'en', NULL, NULL),
(92, 92, 'Guam', 'en', NULL, NULL),
(93, 93, 'Guinea-Bissau', 'en', NULL, NULL),
(94, 94, 'Guyana', 'en', NULL, NULL),
(95, 95, 'Hong Kong', 'en', NULL, NULL),
(96, 96, 'Heard Island and McDonald Islands', 'en', NULL, NULL),
(97, 97, 'Honduras', 'en', NULL, NULL),
(98, 98, 'Croatia', 'en', NULL, NULL),
(99, 99, 'Haiti', 'en', NULL, NULL),
(100, 100, 'Hungary', 'en', NULL, NULL),
(101, 101, 'Indonesia', 'en', NULL, NULL),
(102, 102, 'Ireland', 'en', NULL, NULL),
(103, 103, 'Israel', 'en', NULL, NULL),
(104, 104, 'Isle of Man', 'en', NULL, NULL),
(105, 105, 'India', 'en', NULL, NULL),
(106, 106, 'British Indian Ocean Territory', 'en', NULL, NULL),
(107, 107, 'Iraq', 'en', NULL, NULL),
(108, 108, 'Iran', 'en', NULL, NULL),
(109, 109, 'Iceland', 'en', NULL, NULL),
(110, 110, 'Italy', 'en', NULL, NULL),
(111, 111, 'Jersey', 'en', NULL, NULL),
(112, 112, 'Jamaica', 'en', NULL, NULL),
(113, 113, 'Jordan', 'en', NULL, NULL),
(114, 114, 'Japan', 'en', NULL, NULL),
(115, 115, 'Kenya', 'en', NULL, NULL),
(116, 116, 'Kyrgyzstan', 'en', NULL, NULL),
(117, 117, 'Cambodia', 'en', NULL, NULL),
(118, 118, 'Kiribati', 'en', NULL, NULL),
(119, 119, 'Comoros', 'en', NULL, NULL),
(120, 120, 'Saint Kitts and Nevis', 'en', NULL, NULL),
(121, 121, 'North Korea', 'en', NULL, NULL),
(122, 122, 'South Korea', 'en', NULL, NULL),
(123, 123, 'Kuwait', 'en', NULL, NULL),
(124, 124, 'Cayman Islands', 'en', NULL, NULL),
(125, 125, 'Kazakhstan', 'en', NULL, NULL),
(126, 126, 'Laos', 'en', NULL, NULL),
(127, 127, 'Lebanon', 'en', NULL, NULL),
(128, 128, 'Saint Lucia', 'en', NULL, NULL),
(129, 129, 'Liechtenstein', 'en', NULL, NULL),
(130, 130, 'Sri Lanka', 'en', NULL, NULL),
(131, 131, 'Liberia', 'en', NULL, NULL),
(132, 132, 'Lesotho', 'en', NULL, NULL),
(133, 133, 'Lithuania', 'en', NULL, NULL),
(134, 134, 'Luxembourg', 'en', NULL, NULL),
(135, 135, 'Latvia', 'en', NULL, NULL),
(136, 136, 'Libya', 'en', NULL, NULL),
(137, 137, 'Morocco', 'en', NULL, NULL),
(138, 138, 'Monaco', 'en', NULL, NULL),
(139, 139, 'Moldova', 'en', NULL, NULL),
(140, 140, 'Montenegro', 'en', NULL, NULL),
(141, 141, 'Saint Martin', 'en', NULL, NULL),
(142, 142, 'Madagascar', 'en', NULL, NULL),
(143, 143, 'Marshall Islands', 'en', NULL, NULL),
(144, 144, 'Macedonia', 'en', NULL, NULL),
(145, 145, 'Mali', 'en', NULL, NULL),
(146, 146, 'Myanmar', 'en', NULL, NULL),
(147, 147, 'Mongolia', 'en', NULL, NULL),
(148, 148, 'Macao', 'en', NULL, NULL),
(149, 149, 'Northern Mariana Islands', 'en', NULL, NULL),
(150, 150, 'Martinique', 'en', NULL, NULL),
(151, 151, 'Mauritania', 'en', NULL, NULL),
(152, 152, 'Montserrat', 'en', NULL, NULL),
(153, 153, 'Malta', 'en', NULL, NULL),
(154, 154, 'Mauritius', 'en', NULL, NULL),
(155, 155, 'Maldives', 'en', NULL, NULL),
(156, 156, 'Malawi', 'en', NULL, NULL),
(157, 157, 'Mexico', 'en', NULL, NULL),
(158, 158, 'Malaysia', 'en', NULL, NULL),
(159, 159, 'Mozambique', 'en', NULL, NULL),
(160, 160, 'Namibia', 'en', NULL, NULL),
(161, 161, 'New Caledonia', 'en', NULL, NULL),
(162, 162, 'Niger', 'en', NULL, NULL),
(163, 163, 'Norfolk Island', 'en', NULL, NULL),
(164, 164, 'Nigeria', 'en', NULL, NULL),
(165, 165, 'Nicaragua', 'en', NULL, NULL),
(166, 166, 'Netherlands', 'en', NULL, NULL),
(167, 167, 'Norway', 'en', NULL, NULL),
(168, 168, 'Nepal', 'en', NULL, NULL),
(169, 169, 'Nauru', 'en', NULL, NULL),
(170, 170, 'Niue', 'en', NULL, NULL),
(171, 171, 'New Zealand', 'en', NULL, NULL),
(172, 172, 'Oman', 'en', NULL, NULL),
(173, 173, 'Panama', 'en', NULL, NULL),
(174, 174, 'Peru', 'en', NULL, NULL),
(175, 175, 'French Polynesia', 'en', NULL, NULL),
(176, 176, 'Papua New Guinea', 'en', NULL, NULL),
(177, 177, 'Philippines', 'en', NULL, NULL),
(178, 178, 'Pakistan', 'en', NULL, NULL),
(179, 179, 'Poland', 'en', NULL, NULL),
(180, 180, 'Saint Pierre and Miquelon', 'en', NULL, NULL),
(181, 181, 'Pitcairn', 'en', NULL, NULL),
(182, 182, 'Puerto Rico', 'en', NULL, NULL),
(183, 183, 'Palestinian Territory', 'en', NULL, NULL),
(184, 184, 'Portugal', 'en', NULL, NULL),
(185, 185, 'Palau', 'en', NULL, NULL),
(186, 186, 'Paraguay', 'en', NULL, NULL),
(187, 187, 'Qatar', 'en', NULL, NULL),
(188, 188, 'Reunion', 'en', NULL, NULL),
(189, 189, 'Romania', 'en', NULL, NULL),
(190, 190, 'Serbia', 'en', NULL, NULL),
(191, 191, 'Russia', 'en', NULL, NULL),
(192, 192, 'Rwanda', 'en', NULL, NULL),
(193, 193, 'Saudi Arabia', 'en', NULL, NULL),
(194, 194, 'Solomon Islands', 'en', NULL, NULL),
(195, 195, 'Seychelles', 'en', NULL, NULL),
(196, 196, 'Sudan', 'en', NULL, NULL),
(197, 197, 'Sweden', 'en', NULL, NULL),
(198, 198, 'Singapore', 'en', NULL, NULL),
(199, 199, 'Saint Helena', 'en', NULL, NULL),
(200, 200, 'Slovenia', 'en', NULL, NULL),
(201, 201, 'Svalbard and Jan Mayen', 'en', NULL, NULL),
(202, 202, 'Slovakia', 'en', NULL, NULL),
(203, 203, 'Sierra Leone', 'en', NULL, NULL),
(204, 204, 'San Marino', 'en', NULL, NULL),
(205, 205, 'Senegal', 'en', NULL, NULL),
(206, 206, 'Somalia', 'en', NULL, NULL),
(207, 207, 'Suriname', 'en', NULL, NULL),
(208, 208, 'South Sudan', 'en', NULL, NULL),
(209, 209, 'Sao Tome and Principe', 'en', NULL, NULL),
(210, 210, 'El Salvador', 'en', NULL, NULL),
(211, 211, 'Sint Maarten', 'en', NULL, NULL),
(212, 212, 'Syria', 'en', NULL, NULL),
(213, 213, 'Swaziland', 'en', NULL, NULL),
(214, 214, 'Turks and Caicos Islands', 'en', NULL, NULL),
(215, 215, 'Chad', 'en', NULL, NULL),
(216, 216, 'French Southern Territories', 'en', NULL, NULL),
(217, 217, 'Togo', 'en', NULL, NULL),
(218, 218, 'Thailand', 'en', NULL, NULL),
(219, 219, 'Tajikistan', 'en', NULL, NULL),
(220, 220, 'Tokelau', 'en', NULL, NULL),
(221, 221, 'East Timor', 'en', NULL, NULL),
(222, 222, 'Turkmenistan', 'en', NULL, NULL),
(223, 223, 'Tunisia', 'en', NULL, NULL),
(224, 224, 'Tonga', 'en', NULL, NULL),
(225, 225, 'Turkey', 'en', NULL, NULL),
(226, 226, 'Trinidad and Tobago', 'en', NULL, NULL),
(227, 227, 'Tuvalu', 'en', NULL, NULL),
(228, 228, 'Taiwan', 'en', NULL, NULL),
(229, 229, 'Tanzania', 'en', NULL, NULL),
(230, 230, 'Ukraine', 'en', NULL, NULL),
(231, 231, 'Uganda', 'en', NULL, NULL),
(232, 232, 'United States Minor Outlying Islands', 'en', NULL, NULL),
(233, 233, 'United States', 'en', NULL, NULL),
(234, 234, 'Uruguay', 'en', NULL, NULL),
(235, 235, 'Uzbekistan', 'en', NULL, NULL),
(236, 236, 'Vatican', 'en', NULL, NULL),
(237, 237, 'Saint Vincent and the Grenadines', 'en', NULL, NULL),
(238, 238, 'Venezuela', 'en', NULL, NULL),
(239, 239, 'British Virgin Islands', 'en', NULL, NULL),
(240, 240, 'U.S. Virgin Islands', 'en', NULL, NULL),
(241, 241, 'Vietnam', 'en', NULL, NULL),
(242, 242, 'Vanuatu', 'en', NULL, NULL),
(243, 243, 'Wallis and Futuna', 'en', NULL, NULL),
(244, 244, 'Samoa', 'en', NULL, NULL),
(245, 245, 'Kosovo', 'en', NULL, NULL),
(246, 246, 'Yemen', 'en', NULL, NULL),
(247, 247, 'Mayotte', 'en', NULL, NULL),
(248, 248, 'South Africa', 'en', NULL, NULL),
(249, 249, 'Zambia', 'en', NULL, NULL),
(250, 250, 'Zimbabwe', 'en', NULL, NULL),
(251, 1, 'Andorra', 'sv', NULL, NULL),
(252, 2, 'Förenade Arabemiraten', 'sv', NULL, NULL),
(253, 3, 'Afghanistan', 'sv', NULL, NULL),
(254, 4, 'Antigua och Barbuda', 'sv', NULL, NULL),
(255, 5, 'Anguilla', 'sv', NULL, NULL),
(256, 6, 'Albanien', 'sv', NULL, NULL),
(257, 7, 'Armenien', 'sv', NULL, NULL),
(258, 8, 'Angola', 'sv', NULL, NULL),
(259, 9, 'Antarktis', 'sv', NULL, NULL),
(260, 10, 'Argentina', 'sv', NULL, NULL),
(261, 11, 'Amerikanska Samoa', 'sv', NULL, NULL),
(262, 12, 'Österrike', 'sv', NULL, NULL),
(263, 13, 'Australien', 'sv', NULL, NULL),
(264, 14, 'Aruba', 'sv', NULL, NULL),
(265, 15, 'Ålands län', 'sv', NULL, NULL),
(266, 16, 'Azerbajdzjan', 'sv', NULL, NULL),
(267, 17, 'Bosnien och Hercegovina', 'sv', NULL, NULL),
(268, 18, 'Barbados', 'sv', NULL, NULL),
(269, 19, 'Bangladesh', 'sv', NULL, NULL),
(270, 20, 'Belgien', 'sv', NULL, NULL),
(271, 21, 'Burkina Faso', 'sv', NULL, NULL),
(272, 22, 'Bulgarien', 'sv', NULL, NULL),
(273, 23, 'Bahrain', 'sv', NULL, NULL),
(274, 24, 'Burundi', 'sv', NULL, NULL),
(275, 25, 'Benin', 'sv', NULL, NULL),
(276, 26, 'S:t Barthélemy', 'sv', NULL, NULL),
(277, 27, 'Bermuda', 'sv', NULL, NULL),
(278, 28, 'Brunei', 'sv', NULL, NULL),
(279, 29, 'Bolivia', 'sv', NULL, NULL),
(280, 30, 'Bonaire, Saint Eustatius and Saba ', 'sv', NULL, NULL),
(281, 31, 'Brasilien', 'sv', NULL, NULL),
(282, 32, 'Bahamas', 'sv', NULL, NULL),
(283, 33, 'Bhutan', 'sv', NULL, NULL),
(284, 34, 'Bouvetön', 'sv', NULL, NULL),
(285, 35, 'Botswana', 'sv', NULL, NULL),
(286, 36, 'Vitryssland', 'sv', NULL, NULL),
(287, 37, 'Belize', 'sv', NULL, NULL),
(288, 38, 'Kanada', 'sv', NULL, NULL),
(289, 39, 'Kokosöarna', 'sv', NULL, NULL),
(290, 40, 'Demokratiska Republiken Kongo', 'sv', NULL, NULL),
(291, 41, 'Centralafrikanska republiken', 'sv', NULL, NULL),
(292, 42, 'Kongo', 'sv', NULL, NULL),
(293, 43, 'Schweiz', 'sv', NULL, NULL),
(294, 44, 'Elfenbenskusten', 'sv', NULL, NULL),
(295, 45, 'Cooköarna', 'sv', NULL, NULL),
(296, 46, 'Chile', 'sv', NULL, NULL),
(297, 47, 'Kamerun', 'sv', NULL, NULL),
(298, 48, 'Kina', 'sv', NULL, NULL),
(299, 49, 'Colombia', 'sv', NULL, NULL),
(300, 50, 'Costa Rica', 'sv', NULL, NULL),
(301, 51, 'Kuba', 'sv', NULL, NULL),
(302, 52, 'Kap Verde', 'sv', NULL, NULL),
(303, 53, 'Curacao', 'sv', NULL, NULL),
(304, 54, 'Julön', 'sv', NULL, NULL),
(305, 55, 'Cypern', 'sv', NULL, NULL),
(306, 56, 'Tjeckien', 'sv', NULL, NULL),
(307, 57, 'Tyskland', 'sv', NULL, NULL),
(308, 58, 'Djibouti', 'sv', NULL, NULL),
(309, 59, 'Danmark', 'sv', NULL, NULL),
(310, 60, 'Dominica', 'sv', NULL, NULL),
(311, 61, 'Dominikanska republiken', 'sv', NULL, NULL),
(312, 62, 'Algeriet', 'sv', NULL, NULL),
(313, 63, 'Ecuador', 'sv', NULL, NULL),
(314, 64, 'Estland', 'sv', NULL, NULL),
(315, 65, 'Egypten', 'sv', NULL, NULL),
(316, 66, 'Västsahara', 'sv', NULL, NULL),
(317, 67, 'Eritrea', 'sv', NULL, NULL),
(318, 68, 'Spanien', 'sv', NULL, NULL),
(319, 69, 'Etiopien', 'sv', NULL, NULL),
(320, 70, 'Finland', 'sv', NULL, NULL),
(321, 71, 'Fiji', 'sv', NULL, NULL),
(322, 72, 'Falklandsöarna', 'sv', NULL, NULL),
(323, 73, 'Mikronesiens', 'sv', NULL, NULL),
(324, 74, 'Färöarna', 'sv', NULL, NULL),
(325, 75, 'Frankrike', 'sv', NULL, NULL),
(326, 76, 'Gabon', 'sv', NULL, NULL),
(327, 77, 'Storbritannien', 'sv', NULL, NULL),
(328, 78, 'Grenada', 'sv', NULL, NULL),
(329, 79, 'Georgien', 'sv', NULL, NULL),
(330, 80, 'Franska Guyana', 'sv', NULL, NULL),
(331, 81, 'Guernsey', 'sv', NULL, NULL),
(332, 82, 'Ghana', 'sv', NULL, NULL),
(333, 83, 'Gibraltar', 'sv', NULL, NULL),
(334, 84, 'Grönland', 'sv', NULL, NULL),
(335, 85, 'Gambia', 'sv', NULL, NULL),
(336, 86, 'Guinea', 'sv', NULL, NULL),
(337, 87, 'Guadeloupe', 'sv', NULL, NULL),
(338, 88, 'Ekvatorialguinea', 'sv', NULL, NULL),
(339, 89, 'Grekland', 'sv', NULL, NULL),
(340, 90, 'Sydgeorgien och Sydsandwichöarna', 'sv', NULL, NULL),
(341, 91, 'Guatemala', 'sv', NULL, NULL),
(342, 92, 'Guam', 'sv', NULL, NULL),
(343, 93, 'Guinea-Bissau', 'sv', NULL, NULL),
(344, 94, 'Guyana', 'sv', NULL, NULL),
(345, 95, 'Hongkong', 'sv', NULL, NULL),
(346, 96, 'Heard- och McDonaldöarna', 'sv', NULL, NULL),
(347, 97, 'Honduras', 'sv', NULL, NULL),
(348, 98, 'Kroatien', 'sv', NULL, NULL),
(349, 99, 'Haiti', 'sv', NULL, NULL),
(350, 100, 'Ungern', 'sv', NULL, NULL),
(351, 101, 'Indonesien', 'sv', NULL, NULL),
(352, 102, 'Irland', 'sv', NULL, NULL),
(353, 103, 'Israel', 'sv', NULL, NULL),
(354, 104, 'Isle of Man', 'sv', NULL, NULL),
(355, 105, 'Indien', 'sv', NULL, NULL),
(356, 106, 'Brittiska Indiska oceanöarna', 'sv', NULL, NULL),
(357, 107, 'Irak', 'sv', NULL, NULL),
(358, 108, 'Iran', 'sv', NULL, NULL),
(359, 109, 'Island', 'sv', NULL, NULL),
(360, 110, 'Italien', 'sv', NULL, NULL),
(361, 111, 'Jersey', 'sv', NULL, NULL),
(362, 112, 'Jamaica', 'sv', NULL, NULL),
(363, 113, 'Jordanien', 'sv', NULL, NULL),
(364, 114, 'Japan', 'sv', NULL, NULL),
(365, 115, 'Kenya', 'sv', NULL, NULL),
(366, 116, 'Kirgizistan', 'sv', NULL, NULL),
(367, 117, 'Kambodja', 'sv', NULL, NULL),
(368, 118, 'Kiribati', 'sv', NULL, NULL),
(369, 119, 'Komorerna', 'sv', NULL, NULL),
(370, 120, 'S:t Kitts och Nevis', 'sv', NULL, NULL),
(371, 121, 'Nordkorea', 'sv', NULL, NULL),
(372, 122, 'Sydkorea', 'sv', NULL, NULL),
(373, 123, 'Kuwait', 'sv', NULL, NULL),
(374, 124, 'Caymanöarna', 'sv', NULL, NULL),
(375, 125, 'Kazakstan', 'sv', NULL, NULL),
(376, 126, 'Laos', 'sv', NULL, NULL),
(377, 127, 'Libanon', 'sv', NULL, NULL),
(378, 128, 'S:t Lucia', 'sv', NULL, NULL),
(379, 129, 'Liechtenstein', 'sv', NULL, NULL),
(380, 130, 'Sri Lanka', 'sv', NULL, NULL),
(381, 131, 'Liberia', 'sv', NULL, NULL),
(382, 132, 'Lesotho', 'sv', NULL, NULL),
(383, 133, 'Litauen', 'sv', NULL, NULL),
(384, 134, 'Luxemburg', 'sv', NULL, NULL),
(385, 135, 'Lettland', 'sv', NULL, NULL),
(386, 136, 'Libyen', 'sv', NULL, NULL),
(387, 137, 'Marocko', 'sv', NULL, NULL),
(388, 138, 'Monaco', 'sv', NULL, NULL),
(389, 139, 'Moldavien', 'sv', NULL, NULL),
(390, 140, 'Montenegro', 'sv', NULL, NULL),
(391, 141, 'S:t Martin', 'sv', NULL, NULL),
(392, 142, 'Madagaskar', 'sv', NULL, NULL),
(393, 143, 'Marshallöarna', 'sv', NULL, NULL),
(394, 144, 'Makedonien', 'sv', NULL, NULL),
(395, 145, 'Mali', 'sv', NULL, NULL),
(396, 146, 'Myanmar', 'sv', NULL, NULL),
(397, 147, 'Mongoliet', 'sv', NULL, NULL),
(398, 148, 'Macao', 'sv', NULL, NULL),
(399, 149, 'Nordmarianerna', 'sv', NULL, NULL),
(400, 150, 'Martinique', 'sv', NULL, NULL),
(401, 151, 'Mauretanien', 'sv', NULL, NULL),
(402, 152, 'Montserrat', 'sv', NULL, NULL),
(403, 153, 'Malta', 'sv', NULL, NULL),
(404, 154, 'Mauritius', 'sv', NULL, NULL),
(405, 155, 'Maldiverna', 'sv', NULL, NULL),
(406, 156, 'Malawi', 'sv', NULL, NULL),
(407, 157, 'Mexiko', 'sv', NULL, NULL),
(408, 158, 'Malaysia', 'sv', NULL, NULL),
(409, 159, 'Moçambique', 'sv', NULL, NULL),
(410, 160, 'Namibia', 'sv', NULL, NULL),
(411, 161, 'Nya Kaledonien', 'sv', NULL, NULL),
(412, 162, 'Niger', 'sv', NULL, NULL),
(413, 163, 'Norfolkön', 'sv', NULL, NULL),
(414, 164, 'Nigeria', 'sv', NULL, NULL),
(415, 165, 'Nicaragua', 'sv', NULL, NULL),
(416, 166, 'Nederländerna', 'sv', NULL, NULL),
(417, 167, 'Norge', 'sv', NULL, NULL),
(418, 168, 'Nepal', 'sv', NULL, NULL),
(419, 169, 'Nauru', 'sv', NULL, NULL),
(420, 170, 'Niue', 'sv', NULL, NULL),
(421, 171, 'Nya Zeeland', 'sv', NULL, NULL),
(422, 172, 'Oman', 'sv', NULL, NULL),
(423, 173, 'Panama', 'sv', NULL, NULL),
(424, 174, 'Peru', 'sv', NULL, NULL),
(425, 175, 'Franska Polynesien', 'sv', NULL, NULL),
(426, 176, 'Papua Nya Guinea', 'sv', NULL, NULL),
(427, 177, 'Filippinerna', 'sv', NULL, NULL),
(428, 178, 'Pakistan', 'sv', NULL, NULL),
(429, 179, 'Polen', 'sv', NULL, NULL),
(430, 180, 'S:t Pierre och Miquelon', 'sv', NULL, NULL),
(431, 181, 'Pitcairn', 'sv', NULL, NULL),
(432, 182, 'Puerto Rico', 'sv', NULL, NULL),
(433, 183, 'Palestinska territoriet', 'sv', NULL, NULL),
(434, 184, 'Portugal', 'sv', NULL, NULL),
(435, 185, 'Palau', 'sv', NULL, NULL),
(436, 186, 'Paraguay', 'sv', NULL, NULL),
(437, 187, 'Qatar', 'sv', NULL, NULL),
(438, 188, 'Réunion', 'sv', NULL, NULL),
(439, 189, 'Rumänien', 'sv', NULL, NULL),
(440, 190, 'Serbien', 'sv', NULL, NULL),
(441, 191, 'Ryssland', 'sv', NULL, NULL),
(442, 192, 'Rwanda', 'sv', NULL, NULL),
(443, 193, 'Saudiarabien', 'sv', NULL, NULL),
(444, 194, 'Salomonöarna', 'sv', NULL, NULL),
(445, 195, 'Seychellerna', 'sv', NULL, NULL),
(446, 196, 'Sudan', 'sv', NULL, NULL),
(447, 197, 'Sverige', 'sv', NULL, NULL),
(448, 198, 'Singapore', 'sv', NULL, NULL),
(449, 199, 'S:t Helena', 'sv', NULL, NULL),
(450, 200, 'Slovenien', 'sv', NULL, NULL),
(451, 201, 'Svalbard och Jan Mayen', 'sv', NULL, NULL),
(452, 202, 'Slovakien', 'sv', NULL, NULL),
(453, 203, 'Sierra Leone', 'sv', NULL, NULL),
(454, 204, 'San Marino', 'sv', NULL, NULL),
(455, 205, 'Senegal', 'sv', NULL, NULL),
(456, 206, 'Somalia', 'sv', NULL, NULL),
(457, 207, 'Surinam', 'sv', NULL, NULL),
(458, 208, 'Sydsudan', 'sv', NULL, NULL),
(459, 209, 'São Tomé och Príncipe', 'sv', NULL, NULL),
(460, 210, 'El Salvador', 'sv', NULL, NULL),
(461, 211, 'Sint Maarten', 'sv', NULL, NULL),
(462, 212, 'Syrien', 'sv', NULL, NULL),
(463, 213, 'Swaziland', 'sv', NULL, NULL),
(464, 214, 'Turks- och Caicosöarna', 'sv', NULL, NULL),
(465, 215, 'Tchad', 'sv', NULL, NULL),
(466, 216, 'Franska Sydterritorierna', 'sv', NULL, NULL),
(467, 217, 'Togo', 'sv', NULL, NULL),
(468, 218, 'Thailand', 'sv', NULL, NULL),
(469, 219, 'Tadzjikistan', 'sv', NULL, NULL),
(470, 220, 'Tokelau', 'sv', NULL, NULL),
(471, 221, 'Östtimor', 'sv', NULL, NULL),
(472, 222, 'Turkmenistan', 'sv', NULL, NULL),
(473, 223, 'Tunisien', 'sv', NULL, NULL),
(474, 224, 'Tonga', 'sv', NULL, NULL),
(475, 225, 'Turkiet', 'sv', NULL, NULL),
(476, 226, 'Trinidad och Tobago', 'sv', NULL, NULL),
(477, 227, 'Tuvalu', 'sv', NULL, NULL),
(478, 228, 'Taiwan', 'sv', NULL, NULL),
(479, 229, 'Tanzania', 'sv', NULL, NULL),
(480, 230, 'Ukraina', 'sv', NULL, NULL),
(481, 231, 'Uganda', 'sv', NULL, NULL),
(482, 232, 'USA:s yttre öar', 'sv', NULL, NULL),
(483, 233, 'USA', 'sv', NULL, NULL),
(484, 234, 'Uruguay', 'sv', NULL, NULL),
(485, 235, 'Uzbekistan', 'sv', NULL, NULL),
(486, 236, 'Vatikanen', 'sv', NULL, NULL),
(487, 237, 'S:t Vincent och Grenadinerna', 'sv', NULL, NULL),
(488, 238, 'Venezuela', 'sv', NULL, NULL),
(489, 239, 'Brittiska Jungfruöarna', 'sv', NULL, NULL),
(490, 240, 'Amerikanska Jungfruöarna', 'sv', NULL, NULL),
(491, 241, 'Vietnam', 'sv', NULL, NULL),
(492, 242, 'Vanuatu', 'sv', NULL, NULL),
(493, 243, 'Wallis- och Futunaöarna', 'sv', NULL, NULL),
(494, 244, 'Samoa', 'sv', NULL, NULL),
(495, 245, 'Kosovo', 'sv', NULL, NULL),
(496, 246, 'Jemen', 'sv', NULL, NULL),
(497, 247, 'Mayotte', 'sv', NULL, NULL),
(498, 248, 'Sydafrika', 'sv', NULL, NULL),
(499, 249, 'Zambia', 'sv', NULL, NULL),
(500, 250, 'Zimbabwe', 'sv', NULL, NULL),
(501, 1, 'Andora', 'pl', NULL, NULL),
(502, 2, 'Zjednoczone Emiraty Arabskie', 'pl', NULL, NULL),
(503, 3, 'Afganistan', 'pl', NULL, NULL),
(504, 4, 'Antigua i Barbuda', 'pl', NULL, NULL),
(505, 5, 'Anguilla', 'pl', NULL, NULL),
(506, 6, 'Albania', 'pl', NULL, NULL),
(507, 7, 'Armenia', 'pl', NULL, NULL),
(508, 8, 'Angola', 'pl', NULL, NULL),
(509, 9, 'Antarktyda', 'pl', NULL, NULL),
(510, 10, 'Argentyna', 'pl', NULL, NULL),
(511, 11, 'Samoa Ameryka?skie', 'pl', NULL, NULL),
(512, 12, 'Austria', 'pl', NULL, NULL),
(513, 13, 'Australia', 'pl', NULL, NULL),
(514, 14, 'Aruba', 'pl', NULL, NULL),
(515, 15, 'Wyspy Alandzkie', 'pl', NULL, NULL),
(516, 16, 'Azerbejd?an', 'pl', NULL, NULL),
(517, 17, 'Bo?nia i Hercegowina', 'pl', NULL, NULL),
(518, 18, 'Barbados', 'pl', NULL, NULL),
(519, 19, 'Bangladesz', 'pl', NULL, NULL),
(520, 20, 'Belgia', 'pl', NULL, NULL),
(521, 21, 'Burkina Faso', 'pl', NULL, NULL),
(522, 22, 'Bu?garia', 'pl', NULL, NULL),
(523, 23, 'Bahrajn', 'pl', NULL, NULL),
(524, 24, 'Burundi', 'pl', NULL, NULL),
(525, 25, 'Benin', 'pl', NULL, NULL),
(526, 26, 'Saint Barthélemy', 'pl', NULL, NULL),
(527, 27, 'Bermudy', 'pl', NULL, NULL),
(528, 28, 'Brunei Darussalam', 'pl', NULL, NULL),
(529, 29, 'Boliwia', 'pl', NULL, NULL),
(530, 30, 'Bonaire, Sint Eustatius i Saba', 'pl', NULL, NULL),
(531, 31, 'Brazylia', 'pl', NULL, NULL),
(532, 32, 'Bahamy', 'pl', NULL, NULL),
(533, 33, 'Bhutan', 'pl', NULL, NULL),
(534, 34, 'Wyspa Bouveta', 'pl', NULL, NULL),
(535, 35, 'Botswana', 'pl', NULL, NULL),
(536, 36, 'Bia?oru?', 'pl', NULL, NULL),
(537, 37, 'Belize', 'pl', NULL, NULL),
(538, 38, 'Kanada', 'pl', NULL, NULL),
(539, 39, 'Wyspy Kokosowe', 'pl', NULL, NULL),
(540, 40, 'Kongo, Republika Demokratyczna', 'pl', NULL, NULL),
(541, 41, 'Republika ?rodkowej Afryki', 'pl', NULL, NULL),
(542, 42, 'Kongo', 'pl', NULL, NULL),
(543, 43, 'Szwajcaria', 'pl', NULL, NULL),
(544, 44, 'Wybrze?e Ko?ci S?oniowej', 'pl', NULL, NULL),
(545, 45, 'Wyspy Cooka', 'pl', NULL, NULL),
(546, 46, 'Chile', 'pl', NULL, NULL),
(547, 47, 'Kamerun', 'pl', NULL, NULL),
(548, 48, 'Chiny', 'pl', NULL, NULL),
(549, 49, 'Kolumbia', 'pl', NULL, NULL),
(550, 50, 'Kostaryka', 'pl', NULL, NULL),
(551, 51, 'Kuba', 'pl', NULL, NULL),
(552, 52, 'Republika Zielonego Przyl?dka', 'pl', NULL, NULL),
(553, 53, 'Curaçao', 'pl', NULL, NULL),
(554, 54, 'Wyspa Bo?ego Narodzenia', 'pl', NULL, NULL),
(555, 55, 'Cypr', 'pl', NULL, NULL),
(556, 56, 'Republika Czeska', 'pl', NULL, NULL),
(557, 57, 'Niemcy', 'pl', NULL, NULL),
(558, 58, 'D?ibuti', 'pl', NULL, NULL),
(559, 59, 'Dania', 'pl', NULL, NULL),
(560, 60, 'Dominika', 'pl', NULL, NULL),
(561, 61, 'Republika Dominika?ska', 'pl', NULL, NULL),
(562, 62, 'Algieria', 'pl', NULL, NULL),
(563, 63, 'Ekwador', 'pl', NULL, NULL),
(564, 64, 'Estonia', 'pl', NULL, NULL),
(565, 65, 'Egipt', 'pl', NULL, NULL),
(566, 66, 'Sahara Zachodnia', 'pl', NULL, NULL),
(567, 67, 'Erytrea', 'pl', NULL, NULL),
(568, 68, 'Hiszpania', 'pl', NULL, NULL),
(569, 69, 'Etiopia', 'pl', NULL, NULL),
(570, 70, 'Finlandia', 'pl', NULL, NULL),
(571, 71, 'Fid?i', 'pl', NULL, NULL),
(572, 72, 'Falklandy', 'pl', NULL, NULL),
(573, 73, 'Mikronezja', 'pl', NULL, NULL),
(574, 74, 'Wyspy Owcze', 'pl', NULL, NULL),
(575, 75, 'Francja', 'pl', NULL, NULL),
(576, 76, 'Gabon', 'pl', NULL, NULL),
(577, 77, 'Wielka Brytania', 'pl', NULL, NULL),
(578, 78, 'Grenada', 'pl', NULL, NULL),
(579, 79, 'Gruzja', 'pl', NULL, NULL),
(580, 80, 'Gujana Francuska', 'pl', NULL, NULL),
(581, 81, 'Wyspa Guernsey', 'pl', NULL, NULL),
(582, 82, 'Ghana', 'pl', NULL, NULL),
(583, 83, 'Gibraltar', 'pl', NULL, NULL),
(584, 84, 'Grenlandia', 'pl', NULL, NULL),
(585, 85, 'Gambia', 'pl', NULL, NULL),
(586, 86, 'Gwinea', 'pl', NULL, NULL),
(587, 87, 'Gwadelupa', 'pl', NULL, NULL),
(588, 88, 'Gwinea Równikowa', 'pl', NULL, NULL),
(589, 89, 'Grecja', 'pl', NULL, NULL),
(590, 90, 'Georgia Po?udniowa i Sandwich Po?udniowy', 'pl', NULL, NULL),
(591, 91, 'Gwatemala', 'pl', NULL, NULL),
(592, 92, 'Guam', 'pl', NULL, NULL),
(593, 93, 'Gwinea Bissau', 'pl', NULL, NULL),
(594, 94, 'Gujana', 'pl', NULL, NULL),
(595, 95, 'Hongkong', 'pl', NULL, NULL),
(596, 96, 'Wyspy Heard i McDonalda', 'pl', NULL, NULL),
(597, 97, 'Honduras', 'pl', NULL, NULL),
(598, 98, 'Chorwacja', 'pl', NULL, NULL),
(599, 99, 'Haiti', 'pl', NULL, NULL),
(600, 100, 'W?gry', 'pl', NULL, NULL),
(601, 101, 'Indonezja', 'pl', NULL, NULL),
(602, 102, 'Irlandia', 'pl', NULL, NULL),
(603, 103, 'Izrael', 'pl', NULL, NULL),
(604, 104, 'Wyspa Man', 'pl', NULL, NULL),
(605, 105, 'Indie', 'pl', NULL, NULL),
(606, 106, 'Terytorium Brytyjskie Oceanu Indyjskiego', 'pl', NULL, NULL),
(607, 107, 'Irak', 'pl', NULL, NULL),
(608, 108, 'Iran', 'pl', NULL, NULL),
(609, 109, 'Islandia', 'pl', NULL, NULL),
(610, 110, 'W?ochy', 'pl', NULL, NULL),
(611, 111, 'Wyspa Jersey', 'pl', NULL, NULL),
(612, 112, 'Jamajka', 'pl', NULL, NULL),
(613, 113, 'Jordania', 'pl', NULL, NULL),
(614, 114, 'Japonia', 'pl', NULL, NULL),
(615, 115, 'Kenia', 'pl', NULL, NULL),
(616, 116, 'Kirgistan', 'pl', NULL, NULL),
(617, 117, 'Kambod?a', 'pl', NULL, NULL),
(618, 118, 'Kiribati', 'pl', NULL, NULL),
(619, 119, 'Komory', 'pl', NULL, NULL),
(620, 120, 'Saint Kitts i Nevis', 'pl', NULL, NULL),
(621, 121, 'Korea Pó?nocna', 'pl', NULL, NULL),
(622, 122, 'Korea Po?udniowa', 'pl', NULL, NULL),
(623, 123, 'Kuwejt', 'pl', NULL, NULL),
(624, 124, 'Kajmany', 'pl', NULL, NULL),
(625, 125, 'Kazachstan', 'pl', NULL, NULL),
(626, 126, 'Laos (Demokratyczna Republika Ludowa)', 'pl', NULL, NULL),
(627, 127, 'Liban', 'pl', NULL, NULL),
(628, 128, 'Saint Lucia', 'pl', NULL, NULL),
(629, 129, 'Liechtenstein', 'pl', NULL, NULL),
(630, 130, 'Sri Lanka', 'pl', NULL, NULL),
(631, 131, 'Liberia', 'pl', NULL, NULL),
(632, 132, 'Lesotho', 'pl', NULL, NULL),
(633, 133, 'Litwa', 'pl', NULL, NULL),
(634, 134, 'Luksemburg', 'pl', NULL, NULL),
(635, 135, '?otwa', 'pl', NULL, NULL),
(636, 136, 'Libijska', 'pl', NULL, NULL),
(637, 137, 'Maroko', 'pl', NULL, NULL),
(638, 138, 'Monako', 'pl', NULL, NULL),
(639, 139, 'Mo?dawia, Republika', 'pl', NULL, NULL),
(640, 140, 'Czarnogóra', 'pl', NULL, NULL),
(641, 141, 'Sint Maarten', 'pl', NULL, NULL),
(642, 142, 'Madagaskar', 'pl', NULL, NULL),
(643, 143, 'Wyspy Marshalla', 'pl', NULL, NULL),
(644, 144, 'Macedonia, Republika', 'pl', NULL, NULL),
(645, 145, 'Mali', 'pl', NULL, NULL),
(646, 146, 'Birma', 'pl', NULL, NULL),
(647, 147, 'Mongolia', 'pl', NULL, NULL),
(648, 148, 'Makau', 'pl', NULL, NULL),
(649, 149, 'Mariany Pó?nocne', 'pl', NULL, NULL),
(650, 150, 'Martynika', 'pl', NULL, NULL),
(651, 151, 'Mauretania', 'pl', NULL, NULL),
(652, 152, 'Montserrat', 'pl', NULL, NULL),
(653, 153, 'Malta', 'pl', NULL, NULL),
(654, 154, 'Mauritius', 'pl', NULL, NULL),
(655, 155, 'Malediwy', 'pl', NULL, NULL),
(656, 156, 'Malawi', 'pl', NULL, NULL),
(657, 157, 'Meksyk', 'pl', NULL, NULL),
(658, 158, 'Malezja', 'pl', NULL, NULL),
(659, 159, 'Mozambik', 'pl', NULL, NULL),
(660, 160, 'Namibia', 'pl', NULL, NULL),
(661, 161, 'Nowa Kaledonia', 'pl', NULL, NULL),
(662, 162, 'Niger', 'pl', NULL, NULL),
(663, 163, 'Norfolk', 'pl', NULL, NULL),
(664, 164, 'Nigeria', 'pl', NULL, NULL),
(665, 165, 'Nikaragua', 'pl', NULL, NULL),
(666, 166, 'Holandia', 'pl', NULL, NULL),
(667, 167, 'Norwegia', 'pl', NULL, NULL),
(668, 168, 'Nepal', 'pl', NULL, NULL),
(669, 169, 'Nauru', 'pl', NULL, NULL),
(670, 170, 'Niue', 'pl', NULL, NULL),
(671, 171, 'Nowa Zelandia', 'pl', NULL, NULL),
(672, 172, 'Oman', 'pl', NULL, NULL),
(673, 173, 'Panama', 'pl', NULL, NULL),
(674, 174, 'Peru', 'pl', NULL, NULL),
(675, 175, 'Polinezja Francuska', 'pl', NULL, NULL),
(676, 176, 'Papua Nowa Gwinea', 'pl', NULL, NULL),
(677, 177, 'Filipiny', 'pl', NULL, NULL),
(678, 178, 'Pakistan', 'pl', NULL, NULL),
(679, 179, 'Rzeczpospolita Polska', 'pl', NULL, NULL),
(680, 180, 'Saint-Pierre i Miquelon', 'pl', NULL, NULL),
(681, 181, 'Pitcairn', 'pl', NULL, NULL),
(682, 182, 'Portoryko', 'pl', NULL, NULL),
(683, 183, 'Terytoria Palesty?skie', 'pl', NULL, NULL),
(684, 184, 'Portugalia', 'pl', NULL, NULL),
(685, 185, 'Palau', 'pl', NULL, NULL),
(686, 186, 'Paragwaj', 'pl', NULL, NULL),
(687, 187, 'Katar', 'pl', NULL, NULL),
(688, 188, 'Reunion', 'pl', NULL, NULL),
(689, 189, 'Rumunia', 'pl', NULL, NULL),
(690, 190, 'Serbia', 'pl', NULL, NULL),
(691, 191, 'Rosja', 'pl', NULL, NULL),
(692, 192, 'Rwanda', 'pl', NULL, NULL),
(693, 193, 'Arabia Saudyjska', 'pl', NULL, NULL),
(694, 194, 'Wyspy Salomona', 'pl', NULL, NULL),
(695, 195, 'Seszele', 'pl', NULL, NULL),
(696, 196, 'Sudan', 'pl', NULL, NULL),
(697, 197, 'Szwecja', 'pl', NULL, NULL),
(698, 198, 'Singapur', 'pl', NULL, NULL),
(699, 199, 'Wyspa ?wi?tej Heleny', 'pl', NULL, NULL),
(700, 200, 'S?owenia', 'pl', NULL, NULL),
(701, 201, 'Svalbard i Jan Mayen', 'pl', NULL, NULL),
(702, 202, 'S?owacja', 'pl', NULL, NULL),
(703, 203, 'Sierra Leone', 'pl', NULL, NULL),
(704, 204, 'San Marino', 'pl', NULL, NULL),
(705, 205, 'Senegal', 'pl', NULL, NULL),
(706, 206, 'Somalia', 'pl', NULL, NULL),
(707, 207, 'Surinam', 'pl', NULL, NULL),
(708, 208, 'Sudan Po?udniowy', 'pl', NULL, NULL),
(709, 209, 'Wyspy ?wi?tego Tomasza i Ksi???ca', 'pl', NULL, NULL),
(710, 210, 'Salwador', 'pl', NULL, NULL),
(711, 211, 'Sint Maarten', 'pl', NULL, NULL),
(712, 212, 'Syria', 'pl', NULL, NULL),
(713, 213, 'Suazi', 'pl', NULL, NULL),
(714, 214, 'Turks i Caicos', 'pl', NULL, NULL),
(715, 215, 'Czad', 'pl', NULL, NULL),
(716, 216, 'Francuskie Terytoria Po?udniowe', 'pl', NULL, NULL),
(717, 217, 'Togo', 'pl', NULL, NULL),
(718, 218, 'Tajlandia', 'pl', NULL, NULL),
(719, 219, 'Tad?ykistan', 'pl', NULL, NULL),
(720, 220, 'Tokelau', 'pl', NULL, NULL),
(721, 221, 'Timor Wschodni', 'pl', NULL, NULL),
(722, 222, 'Turkmenia', 'pl', NULL, NULL),
(723, 223, 'Tunezja', 'pl', NULL, NULL),
(724, 224, 'Tonga', 'pl', NULL, NULL),
(725, 225, 'Turcja', 'pl', NULL, NULL),
(726, 226, 'Trinidad i Tobago', 'pl', NULL, NULL),
(727, 227, 'Tuvalu', 'pl', NULL, NULL),
(728, 228, 'Tajwan', 'pl', NULL, NULL),
(729, 229, 'Tanzania', 'pl', NULL, NULL),
(730, 230, 'Ukraina', 'pl', NULL, NULL),
(731, 231, 'Uganda', 'pl', NULL, NULL),
(732, 232, 'Dalekie Wyspy Mniejsze Stanów Zjednoczonych', 'pl', NULL, NULL),
(733, 233, 'Stany Zjednoczone', 'pl', NULL, NULL),
(734, 234, 'Urugwaj', 'pl', NULL, NULL),
(735, 235, 'Uzbekistan', 'pl', NULL, NULL),
(736, 236, 'Stolica Apostolska (Pa?stwo Watyka?skie)', 'pl', NULL, NULL),
(737, 237, 'Saint Vincent and the Grenadines', 'pl', NULL, NULL),
(738, 238, 'Wenezuela', 'pl', NULL, NULL),
(739, 239, 'Brytyjskie Wyspy Dziewicze', 'pl', NULL, NULL),
(740, 240, 'Wyspy Dziewicze Stanów Zjednoczonych', 'pl', NULL, NULL),
(741, 241, 'Wietnam', 'pl', NULL, NULL),
(742, 242, 'Vanuatu', 'pl', NULL, NULL),
(743, 243, 'Wallis i Futuna', 'pl', NULL, NULL),
(744, 244, 'Samoa', 'pl', NULL, NULL),
(745, 245, 'Kosowo', 'pl', NULL, NULL),
(746, 246, 'Jemen', 'pl', NULL, NULL),
(747, 247, 'Majotta', 'pl', NULL, NULL),
(748, 248, 'Afryka Po?udniowa', 'pl', NULL, NULL),
(749, 249, 'Zambia', 'pl', NULL, NULL),
(750, 250, 'Zimbabwe', 'pl', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sys_device`
--

CREATE TABLE IF NOT EXISTS `sys_device` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `SortingOrder` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `sys_device`
--

INSERT INTO `sys_device` (`ID`, `SortingOrder`, `CreatedDate`, `LastModifiedDate`) VALUES
(1, 1, '2013-10-15 17:56:30', NULL),
(2, 2, '2013-10-15 17:56:41', NULL),
(3, 3, '2013-10-15 17:56:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sys_device_lang`
--

CREATE TABLE IF NOT EXISTS `sys_device_lang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `DeviceID` int(5) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `DeviceName` varchar(45) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `deviceId_idx` (`DeviceID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `sys_device_lang`
--

INSERT INTO `sys_device_lang` (`ID`, `DeviceID`, `LangCode`, `DeviceName`, `CreatedDate`, `LastModifiedDate`) VALUES
(1, 1, 'en', 'Desktop', '2013-10-15 17:57:48', NULL),
(2, 2, 'en', 'Laptop', '2013-10-15 17:58:07', NULL),
(3, 3, 'en', 'Mobile', '2013-10-15 17:58:47', NULL),
(4, 1, 'sv', 'Desktop', '2013-10-15 18:00:03', NULL),
(5, 3, 'sv', 'Laptop', '2013-10-15 18:00:19', NULL),
(6, 3, 'sv', 'Mobil', '2013-10-15 18:03:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sys_failed_payment`
--

CREATE TABLE IF NOT EXISTS `sys_failed_payment` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `InvoiceID` bigint(20) DEFAULT NULL,
  `FaileddDate` date DEFAULT NULL,
  `Status` enum('Active','Inactive','Pending') DEFAULT NULL,
  `IsPaid` enum('0','1') DEFAULT NULL,
  `Notes` text,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `InvoiceID_idx` (`InvoiceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `sys_failed_payment`
--


-- --------------------------------------------------------

--
-- Table structure for table `sys_module`
--

CREATE TABLE IF NOT EXISTS `sys_module` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `ModuleCode` varchar(10) DEFAULT NULL,
  `ParentID` int(5) DEFAULT NULL,
  `URL` text,
  `SortingOrder` int(5) DEFAULT NULL,
  `DateCreated` datetime DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `sys_module`
--


-- --------------------------------------------------------

--
-- Table structure for table `sys_module_lang`
--

CREATE TABLE IF NOT EXISTS `sys_module_lang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `ModuleName` varchar(45) DEFAULT NULL,
  `ModuleID` int(5) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `DateCreated` datetime DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ModuleID_idx` (`ModuleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `sys_module_lang`
--


-- --------------------------------------------------------

--
-- Table structure for table `sys_registration_history`
--

CREATE TABLE IF NOT EXISTS `sys_registration_history` (
  `ID` int(5) NOT NULL,
  `UserID` int(5) DEFAULT NULL,
  `StepNumber` int(1) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Fk_userId` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sys_registration_history`
--


-- --------------------------------------------------------

--
-- Table structure for table `tmp_account_info`
--

CREATE TABLE IF NOT EXISTS `tmp_account_info` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `ActivationCode` varchar(250) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `UserName` varchar(150) DEFAULT NULL,
  `Password` text,
  `Salt` varchar(250) DEFAULT NULL,
  `Fname` varchar(20) DEFAULT NULL,
  `Lname` varchar(20) DEFAULT NULL,
  `Company` varchar(100) DEFAULT NULL,
  `PhoneCountryCode` varchar(6) DEFAULT NULL,
  `PhoneNumber` varchar(10) DEFAULT NULL,
  `Address1` varchar(100) DEFAULT NULL,
  `Address2` varchar(100) DEFAULT NULL,
  `Zip` int(8) DEFAULT NULL,
  `City` varchar(45) DEFAULT NULL,
  `CountryID` int(1) DEFAULT NULL,
  `AciveStatus` enum('active','pending','inactive') DEFAULT NULL,
  `SessionID` varchar(100) DEFAULT NULL,
  `RemorteIP` varchar(15) DEFAULT NULL,
  `UserAgent` varchar(100) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `RequestedURL` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ActivationCode` (`ActivationCode`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tmp_account_info`
--

INSERT INTO `tmp_account_info` (`ID`, `ActivationCode`, `LangCode`, `UserName`, `Password`, `Salt`, `Fname`, `Lname`, `Company`, `PhoneCountryCode`, `PhoneNumber`, `Address1`, `Address2`, `Zip`, `City`, `CountryID`, `AciveStatus`, `SessionID`, `RemorteIP`, `UserAgent`, `CreatedDate`, `LastModifiedDate`, `RequestedURL`) VALUES
(1, '0b232824faab375b8e4361b6ee59397badea22ed', 'en', 'gayan.c@eyepax.com', '4c9cf79b7b216d8562fe3776b2d96993', '97142e79684c90543335c901fedac03a87554244', 'Gayan ', 'Chathuranga', 'Eyepax', '', '770190352', 'Galle', '', 0, '', 0, 'pending', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.69 Safar', '2013-10-17 08:06:56', '2013-10-17 08:06:56', 'http://google.com'),
(2, '89d5a63b739765f78826974806f65149fdba3c8f', NULL, 'gayan.c@eye.como', '', '8594bd05e5ec351dbd4961fd2b3001749926d08a', 'Gayan', 'Chathuranga', '', '', '', '', '', 0, '', 0, 'pending', 'e890a56152d1077476bdeec1d5adff5a', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.69 Safar', '2013-10-17 13:45:14', '2013-10-17 13:45:14', 'http://google.com'),
(3, '392d5e98e75d454845d671e95c15ae9d98b8234e', 'en', 'gayan.cc@eyepax.com', '', 'd23a3da0ca762c26e5a1bd2e6114f26864b44911', 'Gayan', 'Chathuranga', '', '', '', '', '', 0, '', 0, 'pending', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.69 Safar', '2013-10-17 13:46:26', '2013-10-17 13:46:26', 'http://google.com');

-- --------------------------------------------------------

--
-- Table structure for table `tmp_case`
--

CREATE TABLE IF NOT EXISTS `tmp_case` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `SiteID` bigint(20) DEFAULT NULL,
  `Image` varchar(100) DEFAULT NULL,
  `SortingOrder` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tmp_case`
--


-- --------------------------------------------------------

--
-- Table structure for table `tmp_case_lang`
--

CREATE TABLE IF NOT EXISTS `tmp_case_lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `CaseID` bigint(20) DEFAULT NULL,
  `Title` varchar(250) DEFAULT NULL,
  `Description` text,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `caseId_idx` (`CaseID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tmp_case_lang`
--


-- --------------------------------------------------------

--
-- Table structure for table `tmp_company`
--

CREATE TABLE IF NOT EXISTS `tmp_company` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `Phone` varchar(30) DEFAULT NULL,
  `zip` varchar(6) DEFAULT NULL,
  `CountryID` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  `CountryCallingCode` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ind_country` (`CountryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tmp_company`
--


-- --------------------------------------------------------

--
-- Table structure for table `tmp_company_lang`
--

CREATE TABLE IF NOT EXISTS `tmp_company_lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `CompanyID` bigint(20) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `Name` varchar(150) DEFAULT NULL,
  `Address1` varchar(200) DEFAULT NULL,
  `Address2` varchar(200) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `companyId_idx` (`CompanyID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tmp_company_lang`
--


-- --------------------------------------------------------

--
-- Table structure for table `tmp_demographic_detail`
--

CREATE TABLE IF NOT EXISTS `tmp_demographic_detail` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `HeaderID` bigint(20) DEFAULT NULL,
  `Points` decimal(4,2) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `header_id` (`HeaderID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tmp_demographic_detail`
--


-- --------------------------------------------------------

--
-- Table structure for table `tmp_demographic_detail_lang`
--

CREATE TABLE IF NOT EXISTS `tmp_demographic_detail_lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `DemographicDetailID` bigint(20) DEFAULT NULL,
  `Answer` varchar(250) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `demographicDetailId_idx` (`DemographicDetailID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tmp_demographic_detail_lang`
--


-- --------------------------------------------------------

--
-- Table structure for table `tmp_demographic_header`
--

CREATE TABLE IF NOT EXISTS `tmp_demographic_header` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `SiteID` bigint(20) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tmp_demographic_header`
--


-- --------------------------------------------------------

--
-- Table structure for table `tmp_demographic_header_lang`
--

CREATE TABLE IF NOT EXISTS `tmp_demographic_header_lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `DemographicHeaderID` bigint(20) NOT NULL,
  `HeadLine` varchar(250) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `demographicHeaderID_idx` (`DemographicHeaderID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tmp_demographic_header_lang`
--


-- --------------------------------------------------------

--
-- Table structure for table `tmp_format`
--

CREATE TABLE IF NOT EXISTS `tmp_format` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `SiteID` bigint(20) DEFAULT NULL,
  `TypeID` bigint(20) DEFAULT NULL,
  `PriceTypeID` bigint(20) DEFAULT NULL,
  `Image` varchar(100) DEFAULT NULL,
  `Price` decimal(6,2) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`),
  KEY `price_type_id` (`PriceTypeID`),
  KEY `FK_type_id_idx` (`TypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tmp_format`
--


-- --------------------------------------------------------

--
-- Table structure for table `tmp_quick_fact`
--

CREATE TABLE IF NOT EXISTS `tmp_quick_fact` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `SiteID` bigint(20) DEFAULT NULL,
  `LastUpdated` date DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tmp_quick_fact`
--


-- --------------------------------------------------------

--
-- Table structure for table `tmp_quick_fact_lang`
--

CREATE TABLE IF NOT EXISTS `tmp_quick_fact_lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `QuickFactID` bigint(20) NOT NULL,
  `Text` text,
  `LangCode` varchar(5) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `quickFactId_idx` (`QuickFactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tmp_quick_fact_lang`
--


-- --------------------------------------------------------

--
-- Table structure for table `tmp_site`
--

CREATE TABLE IF NOT EXISTS `tmp_site` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `ParentID` bigint(20) DEFAULT NULL,
  `ProfileID` bigint(20) DEFAULT NULL,
  `GoogleSiteID` varchar(12) DEFAULT NULL,
  `AnalyticsCreatedDate` date DEFAULT NULL,
  `URL` varchar(200) DEFAULT NULL,
  `Logo` varchar(100) DEFAULT NULL,
  `AverageAge` int(2) DEFAULT NULL,
  `MalePercentage` decimal(5,2) DEFAULT NULL,
  `IsMobile` enum('0','1') DEFAULT NULL,
  `SubscriptionPlanID` int(3) DEFAULT NULL,
  `ValidPeriod` int(2) DEFAULT NULL,
  `ActivatedDate` date DEFAULT NULL,
  `ActivationStatus` varchar(15) DEFAULT NULL,
  `CompanyID` int(5) DEFAULT NULL,
  `ExpiryDate` date DEFAULT NULL,
  `LastGoogleUpdate` datetime DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Isdownloading` enum('0','1') DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `profileId_idx` (`ProfileID`),
  KEY `goog_site_id` (`GoogleSiteID`),
  KEY `company_id` (`CompanyID`),
  KEY `subscriptionPlanId_idx` (`SubscriptionPlanID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tmp_site`
--


-- --------------------------------------------------------

--
-- Table structure for table `tmp_site_category`
--

CREATE TABLE IF NOT EXISTS `tmp_site_category` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `SiteID` bigint(20) DEFAULT NULL,
  `CategoryID` bigint(20) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`),
  KEY `category_id` (`CategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tmp_site_category`
--


-- --------------------------------------------------------

--
-- Table structure for table `tmp_site_contact`
--

CREATE TABLE IF NOT EXISTS `tmp_site_contact` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `SiteID` bigint(20) DEFAULT NULL,
  `Fname` varchar(45) DEFAULT NULL,
  `IsDefault` enum('0','1') DEFAULT NULL,
  `Lname` varchar(45) DEFAULT NULL,
  `Email` varchar(75) DEFAULT NULL,
  `ConcatNumber` varchar(25) DEFAULT NULL,
  `ProfilePic` varchar(45) DEFAULT NULL,
  `ContactCountryCode` varchar(15) DEFAULT NULL,
  `SortingOrder` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`),
  KEY `FK_site_id_idx` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tmp_site_contact`
--


-- --------------------------------------------------------

--
-- Table structure for table `tmp_site_contact_lang`
--

CREATE TABLE IF NOT EXISTS `tmp_site_contact_lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `SiteContactID` bigint(20) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `Designation` varchar(45) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `lanCode` (`LangCode`),
  KEY `SiteContactID_idx` (`SiteContactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tmp_site_contact_lang`
--


-- --------------------------------------------------------

--
-- Table structure for table `tmp_site_country`
--

CREATE TABLE IF NOT EXISTS `tmp_site_country` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `CountryID` bigint(20) DEFAULT NULL,
  `SiteID` bigint(20) NOT NULL,
  `IsDefault` enum('0','1') DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `country_id` (`CountryID`),
  KEY `fk_tmp_siteCountry_tmp_site1_idx` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tmp_site_country`
--


-- --------------------------------------------------------

--
-- Table structure for table `tmp_site_device`
--

CREATE TABLE IF NOT EXISTS `tmp_site_device` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `SiteID` bigint(20) DEFAULT NULL,
  `DeviceID` bigint(20) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `siteId_idx` (`SiteID`),
  KEY `deviceId_idx` (`DeviceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tmp_site_device`
--


-- --------------------------------------------------------

--
-- Table structure for table `tmp_site_lang`
--

CREATE TABLE IF NOT EXISTS `tmp_site_lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `SiteID` bigint(20) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `Description` text,
  `MetaDescription` varchar(250) DEFAULT NULL,
  `Meta_Keywords` varchar(250) DEFAULT NULL,
  `Audience` varchar(250) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode_idx` (`LangCode`),
  KEY `siteId_idx` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tmp_site_lang`
--


-- --------------------------------------------------------

--
-- Table structure for table `tmp_testimonial`
--

CREATE TABLE IF NOT EXISTS `tmp_testimonial` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `SiteID` int(5) DEFAULT NULL,
  `UpdateDate` date DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tmp_testimonial`
--


-- --------------------------------------------------------

--
-- Table structure for table `tmp_testimonial_lang`
--

CREATE TABLE IF NOT EXISTS `tmp_testimonial_lang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `TransactionID` bigint(20) DEFAULT NULL,
  `TestimonialID` int(5) DEFAULT NULL,
  `Quote` text,
  `ByWhom` varchar(45) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `testimonialId_idx` (`TestimonialID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tmp_testimonial_lang`
--


-- --------------------------------------------------------

--
-- Table structure for table `trn_analytics`
--

CREATE TABLE IF NOT EXISTS `trn_analytics` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `UniqueVisits` int(11) NOT NULL,
  `PageViews` int(11) NOT NULL,
  `Visits` int(11) NOT NULL,
  `UniqueVisitsMobile` int(11) NOT NULL,
  `PageViewsMobile` int(11) NOT NULL,
  `VisitsMobile` int(11) NOT NULL,
  `UpdatedTime` datetime DEFAULT NULL,
  `Date` date DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `siteID` (`SiteID`),
  KEY `date` (`Date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `trn_analytics`
--


-- --------------------------------------------------------

--
-- Table structure for table `trn_analytics_archive`
--

CREATE TABLE IF NOT EXISTS `trn_analytics_archive` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `UniqueVisits` int(11) DEFAULT NULL,
  `PageViews` int(11) DEFAULT NULL,
  `Visits` int(11) DEFAULT NULL,
  `UniqueVisitsMobile` int(11) DEFAULT NULL,
  `PageViewsMobile` int(11) DEFAULT NULL,
  `VisitsMobile` int(11) DEFAULT NULL,
  `Date` date DEFAULT NULL,
  `UpdatedTime` datetime DEFAULT NULL,
  `AnalyticsID` bigint(20) DEFAULT NULL,
  `TransferredDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `siteID` (`SiteID`),
  KEY `date` (`Date`),
  KEY `AnalyticsID` (`AnalyticsID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `trn_analytics_archive`
--


-- --------------------------------------------------------

--
-- Table structure for table `trn_case`
--

CREATE TABLE IF NOT EXISTS `trn_case` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `Image` varchar(100) DEFAULT NULL,
  `SortingOrder` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `trn_case`
--

INSERT INTO `trn_case` (`ID`, `SiteID`, `Image`, `SortingOrder`, `CreatedBy`, `CreatedDate`, `LastUpdated`, `LastModifiedBy`) VALUES
(1, 1, '1382230501.295136_223169677792273_100002977267758_384315_770648566_n.jpg', 0, 8, '2013-10-20 00:55:38', '2013-10-20 00:55:38', 8),
(2, 1, '1382230529.429076_161661737277895_100003022083526_228654_1365398734_n.jpg', 0, 8, '2013-10-20 00:55:38', '2013-10-20 00:55:38', 8),
(3, 1, '1382230501.295136_223169677792273_100002977267758_384315_770648566_n.jpg', 0, 21343, '2013-10-20 14:02:54', '2013-10-20 14:02:54', 21343),
(4, 1, '1382230529.429076_161661737277895_100003022083526_228654_1365398734_n.jpg', 0, 21343, '2013-10-20 14:02:54', '2013-10-20 14:02:54', 21343);

-- --------------------------------------------------------

--
-- Table structure for table `trn_case_lang`
--

CREATE TABLE IF NOT EXISTS `trn_case_lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CaseID` bigint(20) DEFAULT NULL,
  `Title` varchar(250) DEFAULT NULL,
  `Description` text,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `caseId_idx` (`CaseID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `trn_case_lang`
--

INSERT INTO `trn_case_lang` (`ID`, `CaseID`, `Title`, `Description`, `LangCode`, `CreatedDate`, `CreatedBy`, `LastModifiedDate`, `LastModifiedBy`) VALUES
(1, 1, 'Title 1', 'DEsc', 'en', '2013-10-20 00:55:38', 8, '2013-10-20 00:55:38', 8),
(2, 2, 'Title2', 'Desc2', 'en', '2013-10-20 00:55:38', 8, '2013-10-20 00:55:38', 8),
(3, 3, 'Title 1', 'DEsc', 'en', '2013-10-20 14:02:54', 21343, '2013-10-20 14:02:54', 21343),
(4, 4, 'Title2', 'Desc2', 'en', '2013-10-20 14:02:54', 21343, '2013-10-20 14:02:54', 21343);

-- --------------------------------------------------------

--
-- Table structure for table `trn_company`
--

CREATE TABLE IF NOT EXISTS `trn_company` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Phone` varchar(30) DEFAULT NULL,
  `zip` varchar(6) DEFAULT NULL,
  `CountryID` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `CountryCallingCode` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ind_country` (`CountryID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `trn_company`
--

INSERT INTO `trn_company` (`ID`, `Phone`, `zip`, `CountryID`, `CreatedDate`, `CreatedBy`, `LastModifiedDate`, `LastModifiedBy`, `CountryCallingCode`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `trn_company_lang`
--

CREATE TABLE IF NOT EXISTS `trn_company_lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CompanyID` bigint(20) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `Name` varchar(150) DEFAULT NULL,
  `Address1` varchar(200) DEFAULT NULL,
  `Address2` varchar(200) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `companyId_idx` (`CompanyID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `trn_company_lang`
--


-- --------------------------------------------------------

--
-- Table structure for table `trn_countries`
--

CREATE TABLE IF NOT EXISTS `trn_countries` (
  `geonameID` int(11) NOT NULL,
  `code` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `en_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `en_slug` text COLLATE utf8_unicode_ci NOT NULL,
  `sv_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sv_slug` text COLLATE utf8_unicode_ci NOT NULL,
  `pl_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pl_slug` text COLLATE utf8_unicode_ci NOT NULL,
  `yr_url` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`geonameID`),
  UNIQUE KEY `code_UNIQUE` (`code`),
  KEY `en_name` (`en_name`),
  KEY `sv_name` (`sv_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `trn_countries`
--


-- --------------------------------------------------------

--
-- Table structure for table `trn_demographic_detail`
--

CREATE TABLE IF NOT EXISTS `trn_demographic_detail` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `HeaderID` bigint(20) DEFAULT NULL,
  `Points` decimal(4,2) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `header_id` (`HeaderID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `trn_demographic_detail`
--

INSERT INTO `trn_demographic_detail` (`ID`, `HeaderID`, `Points`, `CreatedDate`, `CreatedBy`, `LastModifiedDate`, `LastModifiedBy`) VALUES
(1, 1, 50.00, '2013-10-20 15:11:52', 0, '2013-10-20 15:11:52', 0),
(2, 1, 50.00, '2013-10-20 15:11:52', 0, '2013-10-20 15:11:52', 0);

-- --------------------------------------------------------

--
-- Table structure for table `trn_demographic_detail_lang`
--

CREATE TABLE IF NOT EXISTS `trn_demographic_detail_lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DemographicDetailID` bigint(20) DEFAULT NULL,
  `Answer` varchar(250) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `demographicDetailId_idx` (`DemographicDetailID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `trn_demographic_detail_lang`
--

INSERT INTO `trn_demographic_detail_lang` (`ID`, `DemographicDetailID`, `Answer`, `LangCode`, `CreatedDate`, `CreatedBy`, `LastModifiedDate`, `LastModifiedBy`) VALUES
(1, 1, '', 'en', '2013-10-20 15:11:52', 0, '2013-10-20 15:11:52', 0),
(2, 2, '', 'en', '2013-10-20 15:11:53', 0, '2013-10-20 15:11:53', 0);

-- --------------------------------------------------------

--
-- Table structure for table `trn_demographic_header`
--

CREATE TABLE IF NOT EXISTS `trn_demographic_header` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `trn_demographic_header`
--

INSERT INTO `trn_demographic_header` (`ID`, `SiteID`, `CreatedDate`, `CreatedBy`, `LastUpdated`, `LastModifiedBy`) VALUES
(1, 1, '2013-10-20 15:11:52', 0, '2013-10-20 15:11:52', 0);

-- --------------------------------------------------------

--
-- Table structure for table `trn_demographic_header_lang`
--

CREATE TABLE IF NOT EXISTS `trn_demographic_header_lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DemographicHeaderID` bigint(20) NOT NULL,
  `HeadLine` varchar(250) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `demographicHeaderID_idx` (`DemographicHeaderID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `trn_demographic_header_lang`
--

INSERT INTO `trn_demographic_header_lang` (`ID`, `DemographicHeaderID`, `HeadLine`, `LangCode`, `CreatedDate`, `CreatedBy`, `LastModifiedDate`, `LastModifiedBy`) VALUES
(1, 1, '', 'en', '2013-10-20 15:11:52', 0, '2013-10-20 15:11:52', 0);

-- --------------------------------------------------------

--
-- Table structure for table `trn_format`
--

CREATE TABLE IF NOT EXISTS `trn_format` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `TypeID` bigint(20) DEFAULT NULL,
  `Format` varchar(255) NOT NULL,
  `PriceTypeID` bigint(20) DEFAULT NULL,
  `Image` varchar(100) DEFAULT NULL,
  `Price` decimal(6,2) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`),
  KEY `price_type_id` (`PriceTypeID`),
  KEY `FK_type_id_idx` (`TypeID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `trn_format`
--

INSERT INTO `trn_format` (`ID`, `SiteID`, `TypeID`, `Format`, `PriceTypeID`, `Image`, `Price`, `CreatedDate`, `CreatedBy`, `LastModifiedDate`, `LastModifiedBy`) VALUES
(1, 1, 2, 'Helsida', 2, '', 100.00, '2013-10-19 23:46:27', 0, '2013-10-19 23:46:27', 0);

-- --------------------------------------------------------

--
-- Table structure for table `trn_google_profile`
--

CREATE TABLE IF NOT EXISTS `trn_google_profile` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `GoogleUserID` varchar(100) DEFAULT NULL,
  `GoogleProfile` bigint(20) DEFAULT NULL,
  `ProfileName` varchar(100) DEFAULT NULL,
  `Token` text,
  `Status` varchar(10) DEFAULT NULL,
  `CompanyId` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `google_profile` (`GoogleProfile`),
  KEY `user_id` (`GoogleUserID`),
  KEY `companyId_idx` (`CompanyId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `trn_google_profile`
--


-- --------------------------------------------------------

--
-- Table structure for table `trn_invoice`
--

CREATE TABLE IF NOT EXISTS `trn_invoice` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `Description` varchar(250) DEFAULT NULL,
  `Amount` decimal(5,2) DEFAULT NULL,
  `Duedate` datetime DEFAULT NULL,
  `InvoceNo` varchar(25) DEFAULT NULL,
  `PaidDate` datetime DEFAULT NULL,
  `TransactionID` varchar(20) DEFAULT NULL,
  `PaymentStatus` int(1) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `siteId_idx` (`SiteID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `trn_invoice`
--


-- --------------------------------------------------------

--
-- Table structure for table `trn_quick_fact`
--

CREATE TABLE IF NOT EXISTS `trn_quick_fact` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `LastUpdated` date DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `trn_quick_fact`
--

INSERT INTO `trn_quick_fact` (`ID`, `SiteID`, `LastUpdated`, `CreatedDate`, `CreatedBy`, `LastModifiedDate`, `LastModifiedBy`) VALUES
(1, 1, '2013-10-20', '2013-10-20 15:11:52', 0, '2013-10-20 15:11:52', 0);

-- --------------------------------------------------------

--
-- Table structure for table `trn_quick_fact_lang`
--

CREATE TABLE IF NOT EXISTS `trn_quick_fact_lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `QuickFactID` bigint(20) NOT NULL,
  `Text` text,
  `LangCode` varchar(5) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `quickFactId_idx` (`QuickFactID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `trn_quick_fact_lang`
--

INSERT INTO `trn_quick_fact_lang` (`ID`, `QuickFactID`, `Text`, `LangCode`, `CreatedDate`, `CreatedBy`, `LastModifiedDate`, `LastModifiedBy`) VALUES
(1, 1, '', 'en', '2013-10-20 15:11:52', 0, '2013-10-20 15:11:52', 0),
(2, 1, '', 'en', '2013-10-20 15:11:52', 0, '2013-10-20 15:11:52', 0),
(3, 1, '', 'en', '2013-10-20 15:11:52', 0, '2013-10-20 15:11:52', 0);

-- --------------------------------------------------------

--
-- Table structure for table `trn_site`
--

CREATE TABLE IF NOT EXISTS `trn_site` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ParentID` bigint(20) DEFAULT NULL,
  `ProfileID` bigint(20) DEFAULT NULL,
  `GoogleSiteID` varchar(12) DEFAULT NULL,
  `URL` varchar(200) DEFAULT NULL,
  `Logo` varchar(100) DEFAULT NULL,
  `AverageAge` int(2) DEFAULT NULL,
  `MalePercentage` decimal(5,2) DEFAULT NULL,
  `IsMobile` enum('0','1') DEFAULT NULL,
  `SubscriptionPlanID` int(3) DEFAULT NULL,
  `AnalyticsCreatedDate` date DEFAULT NULL,
  `ValidPeriod` int(2) DEFAULT NULL,
  `ActivatedDate` date DEFAULT NULL,
  `ActivationStatus` varchar(15) DEFAULT NULL,
  `CompanyID` int(5) DEFAULT NULL,
  `ExpiryDate` date DEFAULT NULL,
  `LastGoogleUpdate` datetime DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `Isdownloading` enum('1','0') DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `profileId_idx` (`ProfileID`),
  KEY `goog_site_id` (`GoogleSiteID`),
  KEY `company_id` (`CompanyID`),
  KEY `subscriptionPlanId_idx` (`SubscriptionPlanID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `trn_site`
--

INSERT INTO `trn_site` (`ID`, `ParentID`, `ProfileID`, `GoogleSiteID`, `URL`, `Logo`, `AverageAge`, `MalePercentage`, `IsMobile`, `SubscriptionPlanID`, `AnalyticsCreatedDate`, `ValidPeriod`, `ActivatedDate`, `ActivationStatus`, `CompanyID`, `ExpiryDate`, `LastGoogleUpdate`, `CreatedDate`, `CreatedBy`, `LastModifiedDate`, `LastModifiedBy`, `Isdownloading`) VALUES
(1, NULL, 1, NULL, NULL, '', 50, 50.00, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, '2013-10-20 15:11:52', 0, '0');

-- --------------------------------------------------------

--
-- Table structure for table `trn_site_category`
--

CREATE TABLE IF NOT EXISTS `trn_site_category` (
  `ID` bigint(20) NOT NULL,
  `SiteID` bigint(20) DEFAULT NULL,
  `CategoryID` bigint(20) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`),
  KEY `category_id` (`CategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `trn_site_category`
--


-- --------------------------------------------------------

--
-- Table structure for table `trn_site_contact`
--

CREATE TABLE IF NOT EXISTS `trn_site_contact` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `Fname` varchar(45) DEFAULT NULL,
  `IsDefault` enum('0','1') DEFAULT NULL,
  `Lname` varchar(45) DEFAULT NULL,
  `Email` varchar(75) DEFAULT NULL,
  `ConcatNumber` varchar(25) DEFAULT NULL,
  `ProfilePic` varchar(45) DEFAULT NULL,
  `ContactCountryCode` varchar(15) DEFAULT NULL,
  `SortingOrder` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`),
  KEY `FK_site_id_idx` (`SiteID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `trn_site_contact`
--

INSERT INTO `trn_site_contact` (`ID`, `SiteID`, `Fname`, `IsDefault`, `Lname`, `Email`, `ConcatNumber`, `ProfilePic`, `ContactCountryCode`, `SortingOrder`, `CreatedBy`, `CreatedDate`, `LastModifiedBy`, `LastModifiedDate`) VALUES
(1, 1, 'Gayan', '0', '', 'gayan.c@eyepax.com', '770190352', '', '971', NULL, 0, '2013-10-20 15:11:52', 0, '2013-10-20 15:11:52');

-- --------------------------------------------------------

--
-- Table structure for table `trn_site_contact_lang`
--

CREATE TABLE IF NOT EXISTS `trn_site_contact_lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteContactID` bigint(20) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `Designation` varchar(45) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `lanCode` (`LangCode`),
  KEY `SiteContactID_idx` (`SiteContactID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `trn_site_contact_lang`
--

INSERT INTO `trn_site_contact_lang` (`ID`, `SiteContactID`, `LangCode`, `Designation`, `CreatedDate`, `CreatedBy`, `LastModifiedDate`, `LastModifiedBy`) VALUES
(1, 1, 'en', '', '2013-10-20 15:11:52', 0, '2013-10-20 15:11:52', 0);

-- --------------------------------------------------------

--
-- Table structure for table `trn_site_country`
--

CREATE TABLE IF NOT EXISTS `trn_site_country` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CountryID` bigint(20) DEFAULT NULL,
  `SiteID` bigint(20) NOT NULL,
  `IsDefault` enum('0','1') DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `country_id` (`CountryID`),
  KEY `fk_trn_siteCountry_trn_site1_idx` (`SiteID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `trn_site_country`
--

INSERT INTO `trn_site_country` (`ID`, `CountryID`, `SiteID`, `IsDefault`, `CreatedDate`, `CreatedBy`, `LastModifiedDate`, `LastModifiedBy`) VALUES
(1, 0, 1, '0', '2013-10-20 15:11:52', 0, '2013-10-20 15:11:52', 0);

-- --------------------------------------------------------

--
-- Table structure for table `trn_site_device`
--

CREATE TABLE IF NOT EXISTS `trn_site_device` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `DeviceID` bigint(20) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `siteId_idx` (`SiteID`),
  KEY `deviceId_idx` (`DeviceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `trn_site_device`
--


-- --------------------------------------------------------

--
-- Table structure for table `trn_site_lang`
--

CREATE TABLE IF NOT EXISTS `trn_site_lang` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SiteID` bigint(20) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `Description` text,
  `MetaDescription` varchar(250) DEFAULT NULL,
  `Meta_Keywords` varchar(250) DEFAULT NULL,
  `Audience` varchar(250) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode_idx` (`LangCode`),
  KEY `siteId_idx` (`SiteID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `trn_site_lang`
--

INSERT INTO `trn_site_lang` (`ID`, `SiteID`, `LangCode`, `Description`, `MetaDescription`, `Meta_Keywords`, `Audience`, `CreatedDate`, `CreatedBy`, `LastModifiedDate`, `LastModifiedBy`) VALUES
(1, 1, 'en', '', '', '', '', NULL, NULL, '2013-10-20 15:11:52', 0);

-- --------------------------------------------------------

--
-- Table structure for table `trn_testimonial`
--

CREATE TABLE IF NOT EXISTS `trn_testimonial` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `SiteID` int(5) DEFAULT NULL,
  `UpdateDate` date DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `site_id` (`SiteID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `trn_testimonial`
--

INSERT INTO `trn_testimonial` (`ID`, `SiteID`, `UpdateDate`, `CreatedDate`, `CreatedBy`, `LastModifiedDate`, `LastModifiedBy`) VALUES
(1, 1, '2013-10-20', '2013-10-20 15:11:52', 0, '2013-10-20 15:11:52', 0);

-- --------------------------------------------------------

--
-- Table structure for table `trn_testimonial_lang`
--

CREATE TABLE IF NOT EXISTS `trn_testimonial_lang` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `TestimonialID` int(5) DEFAULT NULL,
  `Quote` text,
  `ByWhom` varchar(45) DEFAULT NULL,
  `LangCode` varchar(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `langCode` (`LangCode`),
  KEY `testimonialId_idx` (`TestimonialID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `trn_testimonial_lang`
--

INSERT INTO `trn_testimonial_lang` (`ID`, `TestimonialID`, `Quote`, `ByWhom`, `LangCode`, `CreatedDate`, `CreatedBy`, `LastModifiedDate`, `LastModifiedBy`) VALUES
(1, 1, '', '', 'en', '2013-10-20 15:11:52', 0, '2013-10-20 15:11:52', 0),
(2, 1, '', '', 'en', '2013-10-20 15:11:52', 0, '2013-10-20 15:11:52', 0),
(3, 1, '', '', 'en', '2013-10-20 15:11:52', 0, '2013-10-20 15:11:52', 0);

-- --------------------------------------------------------

--
-- Table structure for table `trn_userhistory`
--

CREATE TABLE IF NOT EXISTS `trn_userhistory` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `Title` varchar(250) DEFAULT NULL,
  `UserID` int(5) DEFAULT NULL,
  `Description` varchar(250) DEFAULT NULL,
  `URL` text,
  `LastUpdate` datetime DEFAULT NULL,
  `SiteID` int(5) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedBy` int(5) DEFAULT NULL,
  `CreatedBy` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `userId` (`SiteID`),
  KEY `siteId_idx` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `trn_userhistory`
--

