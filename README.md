Ocast CodeIgnitor Framework Bundle
===============================

Codeigniter Bundle with IonAuth2,RESTful,Base-Model,Twitter Bootstrap and jQuery

It contains:

- Codeigniter (`2.1.3`) 
- ionAuth2 (`#462`) Authentication Lib
- codeigniter-restserver (`#204`) RESTful API
- Base-Model (`#107`) CRUD Base Model
- Twitter Boostrap (`2.3.2`) Frontend Design Framework
- jQuery (`1.10.1`) 
- JQuery-HTML5-placeholder-shim

### Links for additional Info ###

[Codeigniter](http://ellislab.com/codeigniter)

[ionAuth2](https://github.com/benedmunds/CodeIgniter-Ion-Auth)

[codeigniter-restserver](https://github.com/philsturgeon/codeigniter-restserver)

[Base-Model](https://github.com/jamierumbelow/codeigniter-base-model)

[Twitter Bootstrap](http://http://twitter.github.io/bootstrap/)

[jQuery](http://jquery.com/) 

[JQuery-HTML5-placeholder-shim](https://github.com/parndt/jquery-html5-placeholder-shim/) 

### HTML Template Views #####

[views/_templates/header.tpl.php]() ---> Header

[views/_templates/html.tpl.php]() ---> Html

[views/_templates/footer.tpl.php]() ---> Footer

[views/user/siderbar.tpl.php]() ---> User Sidebar 


[views/site.tpl.php](http://ocast.eyepax.info/site/index)

[views/join.tpl.php](http://ocast.eyepax.info/site/join)

###Site Backend###
[views/user/site_lang.tpl.php](http://ocast.eyepax.info/site/language)

[views/user/site_meta_data.tpl.php](http://ocast.eyepax.info/site/metadata)

[views/user/over_view.tpl.php](http://ocast.eyepax.info/site/overview)

[views/user/account_info.tpl.php](http://ocast.eyepax.info/site/acc_info)

[views/user/payment_info.tpl.php](http://ocast.eyepax.info/site/payment_info)

[views/user/site_connect.tpl.php](http://ocast.eyepax.info/site/connect)

[views/user/site_info.tpl.php](http://ocast.eyepax.info/site/info)

[views/user/site_cases.tpl.php](http://ocast.eyepax.info/site/cases)

[views/user/site_formats.tpl.php](http://ocast.eyepax.info/site/formats)

[views/user/site_download.tpl.php](http://ocast.eyepax.info/site/download)

[views/user/site_admin.tpl.php](http://ocast.eyepax.info/site/admin)

###Payment###
[views/user/site_payment.tpl.php](http://ocast.eyepax.info/site/payment_method)

[views/user/site_payment_wrong.tpl.php](http://ocast.eyepax.info/site/payment_error)

[views/user/site_payment_confirm.tpl.php](http://ocast.eyepax.info/site/payment_confirm)

[views/user/site_payment_success.tpl.php](http://ocast.eyepax.info/site/payment_success)

###Client###

[views/user/client.tpl.php](http://ocast.eyepax.info/site/client)

###Search###

[views/site/search.tpl.php](http://ocast.eyepax.info/site/search)

[views/site/search_case.tpl.php](http://ocast.eyepax.info/site/search_case)

###Login###

[views/user/login.tpl.php](http://ocast.eyepax.info/user/login)

[views/user/reset_password.tpl.php](http://ocast.eyepax.info/user/reset_password)

[views/user/sent_password.tpl.php](http://ocast.eyepax.info/user/sent_password)

###About###

[views/ocast/about.tpl.php](http://ocast.eyepax.info/ocast/about)

[views/ocast/contact.tpl.php](http://ocast.eyepax.info/ocast/contact)

[views/ocast/questions.tpl.php](http://ocast.eyepax.info/ocast/questions)

[views/ocast/terms.tpl.php](http://ocast.eyepax.info/ocast/terms)

###Sign up###

[views/signup/add_account_information](http://ocast.eyepax.info/signup/add_infomation)

[views/signup/site_info.tpl.php](http://ocast.eyepax.info/signup/site_info)

[views/products_formats.tpl.php](http://ocast.eyepax.info/signup/product_formats)

[views/signup/signup_connected](http://ocast.eyepax.info/signup/connect)

[views/signup/signup_cases.tpl.php](http://ocast.eyepax.info/signup/add_cases)

[views/signup/preview.tpl.php](http://ocast.eyepax.info/signup/preview_site)













© Team R&D - Eyepax IT Consulting Pvt Ltd Colombo SL 2013