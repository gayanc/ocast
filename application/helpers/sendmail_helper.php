<?php

function email_notification($subject, $body, $to, $from=null, $from_name=null, $bcc=null) {
   
   $CI = get_instance();
    if ($from_name == "") {
        $from_name = lang('Admin');
    }
	
	
    if ($subject) {
        $CI->email->subject($subject);
    }
	  
    if ($body) {
        $CI->email->message($body);
    }

    if (!$from) {
        $from = $CI->config->item('admin_contact');
    }

    $CI->email->from($from, $from_name);

    if ($to) {
        $CI->email->to($to);
    }

    if ($bcc) {
        $CI->email->bcc($bcc);
    }
 
 
    if ($subject && $body && $from && $to) {
	   $CI->email->send();
	 
    }
	 
	
}

function email_contact($subject, $body, $to, $from=null, $from_name=null, $bcc=null) {
   
   $CI = get_instance();
    if ($from_name == "") {
        $from_name = lang('Admin');
    }
    
    
    if ($subject) {
        $CI->email->subject($subject);
    }
      
    if ($body) {
        $CI->email->message($body);
    }

    if (!$from) {
        $from = $CI->config->item('admin_contact');
    }

    $CI->email->from($from, $from_name);

    $CI->email->to(ADMIN_EMAIL);
  

    if ($bcc) {
        $CI->email->bcc($bcc);
    }
 
 
    if ($subject && $body && $from && $to) {
       $CI->email->send();

    }
     
    
}
?>
