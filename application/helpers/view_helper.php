<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('load_view')) {

    /**
     * Load the view
     *
     * @param  string    $controller  controller request comming from
     * @param  string  	 $view loading view
     * @param  object    $data  
     * @throws 
     * @return 
     */

    function load_view($controller, $view, $data = NULL, $bypassAuth = TRUE) {
        
        $controller->session->set_userdata('last_visit', current_url());
        
        if($controller->session->userdata('logged_in') || $bypassAuth){
            
            $CI = get_instance();
            $CI->load->model('category_model');
            $CI->load->model('country_model');
            $CI->load->model('device_model');
            $CI->load->model('cookie_model');
            $CI->load->model('user_model');
            $CI->load->model('Format_Model');
            $CI->load->helper('cookie');
            
            if(!$bypassAuth){
                if(!$CI->user_model->check_login_ip($controller->input->ip_address())){

                $controller->session->unset_userdata('logged_in');
                $controller->session->unset_userdata('last_visit');
                $controller->session->sess_destroy();
                delete_cookie('_lact');
                redirect('user/login', 'refresh');

                }
            }
            
            $data["userDetails"] = array();
            $CI->session->set_userdata('session_id', session_id());
            $CI->session->set_userdata('lang', lang('lcode'));
            if($CI->cookie_model->isReturningUser()){
                $data["AciveStatus"] =  $CI->session->userdata['userDetails']['AciveStatus'];
                $data["userDetails"] = $CI->session->userdata['userDetails'];
                $data["site_id"] = isset($CI->session->userdata['site_id']) ? $CI->session->userdata['site_id'] : '';
            }
            //var_dump($CI->session->userdata);
            $data['session_id'] = $CI->session->userdata['session_id'];
            $data['lang'] = $CI->session->userdata['lang'];


            if ($data == NULL) {
                $data = array();
            }

            $data["category_list"] = $CI->category_model->getAll();
            $data["country_list"] = $CI->country_model->getAll();
            $data["device_list"] = $CI->device_model->getAll();
            $data["type_list"] = $CI->Format_Model->getAllFormatTypes();



            $controller->load->view('_templates/html.tpl.php', $data);
            $controller->load->view('_templates/header.tpl.php', $data);
            $controller->load->view($view, $data);
            $controller->load->view('_templates/footer.tpl.php', $data);
            
        } else {
            redirect('user/login', 'refresh');
        }
    }

}
?>

