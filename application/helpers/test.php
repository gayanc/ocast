<script src="<?php print(ADMIN_BASE_URL);?>ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>jlibs/js/pluspro_validation/pages.js"></script>
<?php if($pageId == 5){?> 
<script type="text/javascript">
	$(document).ready(
		function()
		{
                        <?php for($uploaderX=0;$uploaderX<=1; $uploaderX++){?>
                        // the small image button
			var btnUpload=$('#btnImg<?php print($uploaderX);?>');
			new AjaxUpload(btnUpload, {
				action: 'uploadFile.php',
				name: 'uploadfile',
				onSubmit: function(file, ext){
					 if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
	                    // extension is not allowed
						status.text('Only JPG, PNG or GIF files are allowed');
						return false;
					}
					 var prg_image = "<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>images/uploader50.gif";
                     $("#btnImg<?php print($uploaderX);?>").attr("src",prg_image);
				},
				onComplete: function(file, response){
					//Add uploaded file to list
					if(response!="error"){
						//$('<li></li>').appendTo('#files').html('<img src="../content/uploads/photos/'+file+'" alt="" /><br />'+file).addClass('success');
	                                        var new_image = "<?php print(SITE_BASE_URL); ?>imgs/"+response;
	                                        $("#btnImg<?php print($uploaderX);?>").attr("src",new_image);
	                                        $("#txtSmallFileName<?php print($uploaderX);?>").attr("value", response);
					} else{
                                                alert("Error, when upload the file");
					}
				}
			});
                        <?php } ?>
                        
                        
		}
	);
	</script>
<?php } ?>
<div id="search_main_wrapper"  style="margin-bottom:55px;">

            <h2>Update Pages</h2>     
</div>

         <div id="error">
            Please fill in all the required fields and re-submit the form .<br>
        </div>

<div id="table_main_wrapper">
    <div id="dashboard" style="background-color:#FFF;">
        <div>
            <form  action="" method="post" name="pageForm" id="pageForm">

<div id="two">
                
               

                <div  <?php if($pageType == 'homePage'){?> style="display: none;" <?php } ?>>
                    <fieldset>

                        
                        <div id="separator">
                            <label for="pagename">Page Name : </label> 
                            <input name="pagename" type="text" id="pagename" tabindex="1" size="30" maxlength="30" value="<?php print($NAME);?>"/>
                        </div>




                     <div id="separator">
                            <label for="status">Page Status : </label> 
                            <select name="status" id="status" tabindex="2">
                                <option value="Published" <?php if($LIVE == 'Published'){?> selected="selected" <?php } ?>>Published</option>
                                <option value="Exclude_from_menu" <?php if($LIVE == 'Exclude_from_menu'){?> selected="selected" <?php } ?>>Exclude from menu</option>
                                <option value="Draft" <?php if($LIVE == 'Draft'){?> selected="selected" <?php } ?>>Draft</option>
                            </select>
                      </div>



                        <div id="separator">
                            <label for="contact">Add Contact: </label> 
                            <select name="contact" id="contact" tabindex="3">
                                <option value="Yes" <?php if($ISCONTACT == 'Yes'){?> selected="selected" <?php } ?>>Yes</option>
                                <option value="No" <?php if($ISCONTACT == 'No'){?> selected="selected" <?php } ?>>No</option>
                            </select>
                            <span>Select Yes if this page is a contact us page.</span>
                            
                        </div>
                    </fieldset>
                </div>
               

<h2>Header Info</h2>
                <div>   
                    <fieldset>

                       

                           <div id="separator">

                            <label for="pagetitle">Page Title : </label>
                            <input name="pagetitle" type="text" id="pagetitle" tabindex="4" size="64" style="width:600px;" value="<?php print($TITLE);?>"/>
                        </div>



                          <div id="separator">

                            <label for="keywords">Keywords : </label> 
                            <input name="keywords" type="text" id="keywords" tabindex="5" style="width:600px;" value="<?php print($KEYWORDS);?>"/>
                        </div>




                          <div id="separator">


                            <label for="description">Description : </label> 
                            <textarea name="description" style="width:600px; height:80px;" id="description" tabindex="6"><?php print($DESCRIPTION);?></textarea>
                        </div>  

                    </fieldset>



                </div>


     

         <?php if($pageId == '5' || $pageId == '9'){ 
	       if($pageId == '5'){ $ext = '_pc';}else{$ext = '';}
	 ?>

	  <!-- 
                    jWYSIWYG 0.97 User Manual
                    Copyright (c) 2009-2010 Juan M Martínez, 2011 Akzhan Abdulin and all contributors
                    #reference : https://github.com/akzhan/jwysiwyg#styling-the-content-inside-the-editor
                    #https://github.com/akzhan/jwysiwyg/blob/master/help/examples/10-custom-controls.html
                    -->
                    <script src="<?php print(ADMIN_BASE_URL);?>jwysiwyg/jquery.wysiwyg.js" type="text/javascript" charset="utf-8"></script>
                    <script src="<?php print(ADMIN_BASE_URL);?>jwysiwyg/src/dialogs/default.js" type="text/javascript" charset="utf-8"></script>
                    <script type="text/javascript" src="<?php print(ADMIN_BASE_URL);?>jwysiwyg/controls/wysiwyg.image.js"></script> 
                    <script type="text/javascript" src="<?php print(ADMIN_BASE_URL);?>jwysiwyg/controls/wysiwyg.link.js"></script> 
                    
                    <script type="text/javascript" charset="utf-8">
                        //<![CDATA[
                        $(document).ready(function(){
                            

			    $('#description_pc').wysiwyg({
                                //maxLength : 10,
                                //formHeight: 500,
                                initialContent: "",
                                rmUnusedControls: true,
                                rmMsWordMarkup: true,
                                controls:{
                                    
                                    
                                    bold: { visible : true },
                                    html: { visible : false },
                                    removeFormat: {visible: true},
                                    createLink : {visible: true},
                                    paragraph : {visible: true},
                                    italic : {visible: true},
                                    underline : {visible: true},
                                    justifyLeft : {visible: true},
                                    justifyCenter : {visible: true},
                                    justifyRight : {visible: true},
                                    justifyFull : {visible: true},
                                    subscript : {visible: true},
                                    superscript : {visible: true},
                                    paragraph : {visible: true}
                                }
                                
                            });
                        });
                        //]]>
                    </script>
                    <link rel="stylesheet" href="<?php print(ADMIN_BASE_URL);?>jwysiwyg/jquery.wysiwyg.css" type="text/css" media="screen" charset="utf-8" />
                    <link rel="stylesheet" href="<?php print(ADMIN_BASE_URL);?>jwysiwyg/src/dialogs/default.css" type="text/css" media="screen" charset="utf-8" /> 


      
      <h2>Page Columns</h2>      
        <div>
        
        
        <fieldset>
        
            
            <?php for($x = 1;$x<=1;$x++){
                 $homePageColumnsContent = "";
                 $homePageColumnsContent = $homePageColumnsContents[$x];
                ?>
         <div id="separator">

                            <label for="lcol_title">Top Col Heading : </label> 
                           <input type="hidden" id="txtSmallFileName" name="txtSmallFileName[]" value="NONE" />
                            <input name="pageContentId[]" type="hidden" id="pageContentId" tabindex="1" size="30" maxlength="30" <?php if($topContentDetails){?> value="<?php print($topContentDetails->id);?>" <?php } ?>/>
                            <input name="contentType[]" type="hidden" id="contentType" tabindex="1" size="30" maxlength="30" value="TOP_CONTENT"/>
                            <input name="contentDisplayOrder[]" type="hidden" id="contentDisplayOrder" tabindex="1" size="30" maxlength="30" value="<?php print($x);?>"/>
                            
                          
                            
                            <input name="pageContentsTitle[]" type="text" id="lcol_title" tabindex="3" style="width:600px;" <?php if($topContentDetails){?> value="<?php print($topContentDetails->title);?>" <?php } ?>/>
                        </div>




                          <div id="separator">


                            <label for="lcol_description">Top Col Text : </label> 
		      <div style="padding-left: 180px;">
                            <textarea name="pageContents[]" style="width:610px; height:80px;" id="description<?php print($ext);?>" ><?php if($topContentDetails){?> <?php print($topContentDetails->contents);?> <?php } ?></textarea>
                      </div>  
		      </div>  
                        
                <div style="clear:both; margin-top:25px;">&nbsp;</div>     
                                
         <?php } ?>
                        
             
        
        
        </fieldset>  
                
                
   </div> 
      
      <?php }?>  









 <?php if($pageId != 9){?>    
        <h2>Page Content</h2>

                <fieldset>
                 
                    <div id="separator">
                        <textarea name="mainbody"  id="mainbody" tabindex="7"><?php print($BODY);?></textarea>
                    </div>
                </fieldset>
                <p>
        <?php } ?>            
     
        <!-- start box contents -->
        
        <?php if($pageId == 9){?>    
        <h2>Page Box Columns</h2>      
        <div>
        
<script type="text/javascript" charset="utf-8">
                        //<![CDATA[
                        $(document).ready(function(){
                            $('#description_0').wysiwyg({
                                //maxLength : 10,
                                //formHeight: 500,
                                initialContent: "",
                                rmUnusedControls: true,
                                rmMsWordMarkup: true,
                                controls:{
                                    
                                    
                                    bold: { visible : true },
                                    html: { visible : false },
                                    removeFormat: {visible: true},
                                    createLink : {visible: true},
                                    paragraph : {visible: true},
                                    italic : {visible: true},
                                    underline : {visible: true},
                                    justifyLeft : {visible: true},
                                    justifyCenter : {visible: true},
                                    justifyRight : {visible: true},
                                    justifyFull : {visible: true},
                                    subscript : {visible: true},
                                    superscript : {visible: true},
                                    paragraph : {visible: true}
                                }
                                
                            });
                            
                            $('#description_1').wysiwyg({
                                //maxLength : 10,
                                //formHeight: 500,
                                initialContent: "",
                                rmUnusedControls: true,
                                rmMsWordMarkup: true,
                                controls:{
                                    
                                    
                                    bold: { visible : true },
                                    html: { visible : false },
                                    removeFormat: {visible: true},
                                    createLink : {visible: true},
                                    paragraph : {visible: true},
                                    italic : {visible: true},
                                    underline : {visible: true},
                                    justifyLeft : {visible: true},
                                    justifyCenter : {visible: true},
                                    justifyRight : {visible: true},
                                    justifyFull : {visible: true},
                                    subscript : {visible: true},
                                    superscript : {visible: true},
                                    paragraph : {visible: true}
                                }
                                
                            });

			   
                        });
                        //]]>
                    </script>
        
        <fieldset>
        
            
            <?php for($x = 0;$x<=1;$x++){
                 $pageBoxContent = "";
                 $pageBoxContent = $pageBoxContentObject[$x];
                ?>
         <div id="separator">
                             <?php 
                              if($x == 0){
                                $colHeading = 'Left Box Heading';
                                $colText = 'Left Box Text';
                              }elseif($x == 1){
                                $colHeading = 'Right Box Heading';
                                $colText = 'Right Box Text';
                              }
                              ?>
                              
                             
                           
                            <label for="lcol_title"><?php print($colHeading);?> : </label> 
                           <input type="hidden" id="txtSmallFileName" name="txtSmallFileName[]" value="NONE" />
                            <input name="pageContentId[]" type="hidden" id="pageContentId" tabindex="1" size="30" maxlength="30" <?php if($pageBoxContent){?> value="<?php print($pageBoxContent->id);?>" <?php } ?>/>
                            <input name="contentType[]" type="hidden" id="contentType" tabindex="1" size="30" maxlength="30" value="BOX_CONTENT"/>
                            <input name="contentDisplayOrder[]" type="hidden" id="contentDisplayOrder" tabindex="1" size="30" maxlength="30" value="<?php print($x);?>"/>
                            
                          
                            
                            <input name="pageContentsTitle[]" type="text" id="lcol_title" tabindex="3" style="width:600px;" <?php if($pageBoxContent){?> value="<?php print($pageBoxContent->title);?>" <?php } ?>/>
         </div>




                          <div id="separator">                             
                            <label for="lcol_description"><?php print($colText);?> : </label> 
                            <div style="padding-left: 180px;">
                                <textarea name="pageContents[]" style="width:610px; height: 550px;" id="description_<?php print($x);?>" ><?php if($pageBoxContent){?> <?php print($pageBoxContent->contents);?> <?php } ?></textarea>
                            </div>
                        </div>  
                        
                <div style="clear:both; margin-top:25px;">&nbsp;</div>     
                                
         <?php } ?>
                        
             
        
        
        </fieldset>  
                
                
   </div> 
                    
         <?php }?>           
        
        <!-- eof box content -->
                    
                    
                    
                    
     
      <?php if($pageId == 5){?> 

        <h2>Image Contents</h2>      
        <div>
        
        
        <fieldset>
        
            
            <?php for($x = 0;$x<=1;$x++){
            
                 $homePageColumnsContent = "";
                 $homePageColumnsContent = $testamonials[$x];
                 $contentImage = "";
                 if($homePageColumnsContent){
                    if($homePageColumnsContent->contentMainImage != ''){
                        $contentImage = SITE_BASE_URL.'imgs/'.$homePageColumnsContent->contentMainImage;
                    } else {
                         $contentImage = GLOBAL_ADMIN_CONTENT_PATH."images/no-image-icon.png";
                    }
                 } else {
                     $contentImage = GLOBAL_ADMIN_CONTENT_PATH."images/no-image-icon.png";
                 }
                ?>
         
                <div id="separator">
                
                    <label for="lcol_title">Content Image : </label>
                    <img src="<?php print($contentImage);?>" height="50" width="50" name="btnImg<?php print($x);?>" id="btnImg<?php print($x);?>"/>
                    <input type="hidden" id="txtSmallFileName<?php print($x);?>" name="txtSmallFileName[]" <?php if($homePageColumnsContent){?> value="<?php print($homePageColumnsContent->contentMainImage);?>" <?php } ?> />
                    
                </div>
                
                <div id="separator" style="display: none;">

                            <label for="lcol_title">Content Heading : </label> 
                            <input name="pageContentId[]" type="hidden" id="pageContentId" tabindex="1" size="30" maxlength="30" <?php if($homePageColumnsContent){?> value="<?php print($homePageColumnsContent->id);?>" <?php } ?>/>
                            <input name="contentType[]" type="hidden" id="contentType" tabindex="1" size="30" maxlength="30" value="COLUMN_CONTENT"/>
                            <input name="contentDisplayOrder[]" type="hidden" id="contentDisplayOrder" tabindex="1" size="30" maxlength="30" value="<?php print($x);?>"/>
                            
                          
                            
                            <input name="pageContentsTitle[]" type="text" id="lcol_title" tabindex="3" style="width:600px;" <?php if($homePageColumnsContent){?> value="<?php print($homePageColumnsContent->title);?>" <?php } ?>/>
                        </div>


                         <div id="separator" style="display: none;">


                            <label for="lcol_description">Content Link : </label> 
                            <input name="pageContentsLink[]" type="text" id="pageContentsLink" tabindex="3" style="width:600px;" <?php if($homePageColumnsContent){?> value="<?php print($homePageColumnsContent->contentLink);?>" <?php } ?>/>
                        </div> 


                          <div id="separator" style="display: none;">


                            <label for="lcol_description">Content Text : </label> 
                            <textarea name="pageContents[]" style="width:600px; height:80px;" id="description"><?php if($homePageColumnsContent){?> <?php print($homePageColumnsContent->contents);?> <?php } ?></textarea>
                            
                        </div>  
                        
                        
                        
                <div style="clear:both; margin-top:25px;">&nbsp;</div>     
                                
         <?php } ?>

        
        </fieldset>  
      
   </div>    
                    
         <?php }?>           
                    
                    
                    
                    
</div>
                            
                            
                            
                            
                    <input id="testbutton" class="testbutton" type="submit" value="submit" name="Submit" tabindex="8"/> 
                    <input id="resetbutton" type="reset" tabindex="9" />
                    <input type="hidden" name="txtPageId" id="txtPageId" value="<?php print($pageId);?>" />
                    <input type="hidden" name="txtParentpageId" id="txtParentpageId" value="<?php print($parentpageId);?>" />
                    <input type="hidden" name="txtSecParentpageId" id="txtSecParentpageId" value="<?php print($secParentpageId);?>" />
                    <input type="hidden" name="txtAction" id="txtAction" value="<?php print($action);?>" />
                    <input type="hidden" name="txtPageType" id="txtPageType" value="<?php print($pageType);?>" />
                     <input type="hidden" name="txtListingId" id="txtListingId" value="<?php print($ListingID);?>" />
                      <input type="hidden" name="txtAllowToDelete" id="txtAllowToDelete" value="<?php print($ALLOWDELETE);?>" />
                </p>

                <div id="show"></div>
            </form>
        </div>
        <div class="clear">
        </div> 
    </div>
</div>
<script>
CKEDITOR.replace( 'mainbody' , {
extraPlugins: 'stylesheetparser',
// Stylesheet for the contents.
contentsCss: '<?php print(SITE_BASE_URL."includes/templates/".SITE_TEMPLATE."/");?>contents/css/main.css',
// Do not load the default Styles configuration.
stylesSet: [],
toolbar: [
{items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],
'/',
{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline'] },
	{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl' ] },
	{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
	{ name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
	'/',
	{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
	{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
],
filebrowserBrowseUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/ckfinder.html',
filebrowserImageBrowseUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/ckfinder.html?Type=Images',
//filebrowserFlashBrowseUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/ckfinder.html?Type=Flash',
filebrowserUploadUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
filebrowserImageUploadUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
//filebrowserFlashUploadUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
height: 600
});
</script>