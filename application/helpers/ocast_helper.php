<?php 
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('get_client_ip')) {

    /**
     * Get client IP
     *
     * @return return client IP address
     */
    function get_client_ip() {
        $ipaddress = '';
         if (getenv('HTTP_CLIENT_IP'))
             $ipaddress = getenv('HTTP_CLIENT_IP');
         else if(getenv('HTTP_X_FORWARDED_FOR'))
             $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
         else if(getenv('HTTP_X_FORWARDED'))
             $ipaddress = getenv('HTTP_X_FORWARDED');
         else if(getenv('HTTP_FORWARDED_FOR'))
             $ipaddress = getenv('HTTP_FORWARDED_FOR');
         else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
         else if(getenv('REMOTE_ADDR'))
             $ipaddress = getenv('REMOTE_ADDR');
         else
             $ipaddress = 'UNKNOWN';

         return $ipaddress;
    }
}

if(!function_exists('getDateTime')){
    function getDateTime(){
        return date("Y-m-d H:i:s");
    }
    
}

####ajax uploader
function findexts($filename) {
    $filename = strtolower($filename);
    $exts = preg_split("[/\\.]", $filename);
    $n = count($exts) - 1;
    $exts = $exts[$n];
    return $exts;
}









