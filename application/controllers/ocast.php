<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ocast extends CI_Controller{

	function __construct(){
		parent::__construct();
	}

	public function about(){      
		load_view($this,'ocast/about.tpl.php');
	}

	public function contact(){      
		load_view($this,'ocast/contact.tpl.php');
	}

	public function questions(){      
		load_view($this,'ocast/questions.tpl.php');
	}

	public function terms(){      
		load_view($this,'ocast/terms.tpl.php');
	}
}