<?php
/*
 * @author Lasith Gunawardana <lasith.g@eyepax.com> 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Verifylogin extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('user_model', '', TRUE);
        $this->load->library('security');
        
    }

	function index(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('txtUserName', 'Username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('txtPassword', 'Password', 'trim|required|xss_clean|callback_check_database');

		if($this->form_validation->run() == FALSE){

            redirect('user/login', 'refresh');
            
		}else{      
            
            if($this->input->post('remember') == 1){
                $login_hash = sha1($this->input->post('txtUserName') . $this->input->post('txtPassword') . $this->input->ip_address());

                $login_cookie = array(
                    'name'   => '_lact',
                    'value'  => $login_hash,
                    'expire' => $this->config->item('autologin_cookie_expire')
                );
                
                $this->input->set_cookie($login_cookie); 
                $this->user_model->modify_autolog_key($this->input->post('txtUserName'), $this->input->post('txtPassword'), $this->input->ip_address());
            }
            
            if($this->session->userdata('last_visit')){
                $last_visit = $this->session->userdata('last_visit');
                $this->session->unset_userdata('last_visit');
                redirect($last_visit, 'refresh');
            }
            
			redirect('site/overview', 'refresh');

		}
		
	}
	
	function check_database($password){
		$username = $this->security->xss_clean($this->input->post('txtUserName'));
		$result = $this->user_model->login($username, $this->security->xss_clean($password));
		$tracked = FALSE;
        $access_state = $this->_accessible_state($username);
        
		if($result && $access_state){
			$sess_array = array();
			foreach($result as $row){
				$sess_array = array( // user login
					'UserID'     => $row->ID,
					'UserName'   => $row->UserName,
					'UserTypeID' => $row->UserTypeID,
                    'CompanyID'  => $row->CompanyID,
                    'Fname'      => $row->Fname,
                    'Lname'      => $row->Lname,
				);
                
                $track = array(
                  'LastLogin'   => date('Y-m-d H:i:s'),
                  'LastLoginIP' => $this->input->ip_address()
                );
                
                $tracked = $this->user_model->track_login($track, $row->ID);
				$this->session->set_userdata('logged_in', $sess_array);
                
			}
            
            if($tracked){
                return TRUE;
            }  else {
                
                return FALSE;
            }
			
            
		}else{
            
            if(!$access_state){
                return FALSE;
            }
            
            $this->session->set_flashdata('error', $this->lang->line('login_wrong_user_pass'));
			//$this->form_validation->set_message('check_database', 'Invalid username or password');
			return FALSE;
		}
	}
    
    
    // Check for user signup progress and provide accessibility
    function _accessible_state($username){
        $salt = sha1($username . $this->config->item('encryption_key'));
        
        $user_status = $this->user_model->get_user('AciveStatus, Salt', array('UserName' => $username));
        
        if(!$user_status){
            $this->session->set_flashdata('error', $this->lang->line('login_account_not_found'));
            return FALSE;
        }elseif($user_status->Salt === $salt){
            switch ($user_status->AciveStatus){
                case STATE_ACTIVE:{
                        $this->session->set_flashdata('error', $this->lang->line('login_acc_not_approved'));
                        return FALSE;
                };
                                break;
                case STATE_REVIEW:{
                        return TRUE;
                };
                                break;
                case STATE_APPROVE:{
                        return TRUE;
                };
                                break;
                case STATE_PUBLISH:{
                        return TRUE;
                };
                                break;
                case STATE_PENDING:{
                        $this->session->set_flashdata('error', $this->lang->line('login_acc_not_approved'));
                        return FALSE;
                };
                                break;
            }
        }else{
            $this->session->set_flashdata('error', $this->lang->line('login_acc_not_approved'));
            return FALSE;
        }
    
    }

}