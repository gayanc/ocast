<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class html extends CI_Controller{ 

	 public function __construct() {
        parent::__construct();
    }
	function account(){
		load_view($this,'html/account_info.php');
	}

	function submit(){
		load_view($this,'html/account_info_submit.php');
	}

	function site_case(){
		load_view($this,'html/add_case.php');
	}

	function site(){
		load_view($this,'html/add_site.php');
	}

	function connect(){
		load_view($this,'html/connect.php');
	}

	function connect2(){
		load_view($this,'html/connect2.php');
	}

	function join(){
		load_view($this,'html/join.php');
	}

	function preview(){
		load_view($this,'html/site_preview.php');
	}

	function info(){
		load_view($this,'html/siteinfo.php');
	}

	function thankyou(){
		load_view($this,'html/thank_you.php');
	}
}