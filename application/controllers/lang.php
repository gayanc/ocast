<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Lang extends CI_Controller {

    public function Lang() {
        parent::__construct();
    }

    public function change($lang_code) {

        if ($lang_code == "sv") {
            $lang = "swedish";
        }
        else if ($lang_code == "pl") {
            $lang = "polish";
        }
        else {

            $lang = "english";
        }
        $cookie_locations = array(
          'name' => 'user_lang',
          'value' => $lang,
          'expire' => '604800'
        );
        $this->input->set_cookie($cookie_locations);
        redirect($this->input->get('n'));
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */