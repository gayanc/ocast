<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Site extends CI_Controller{

	function __construct(){
		parent::__construct();
	}

	public function index(){      
		load_view($this,'site/search.tpl.php',$data = NULL, $bypassAuth = true);
	}

	public function join(){
		load_view($this,'join.tpl.php', NULL, TRUE);
	}

	public function language(){
		load_view($this,'user/site_lang.tpl.php');
	}

	public function metadata(){
		load_view($this,'user/site_meta_data.tpl.php');
	}

	public function overview(){
		load_view($this,'user/over_view.tpl.php');
	}

	public function acc_info(){
		load_view($this,'user/account_info.tpl.php');
	}

	public function payment_info(){
		load_view($this,'user/payment_info.tpl.php');
	}

	public function connect(){
		load_view($this,'user/site_connect.tpl.php');
	}

	public function info(){
		load_view($this,'user/site_info.tpl.php');
	}
	
	public function formats(){
		load_view($this,'user/site_formats.tpl.php');
	}
	public function cases(){
		load_view($this,'user/site_cases.tpl.php');
	}

	public function download(){
		load_view($this,'user/site_download.tpl.php');
	}

	public function admin(){
		load_view($this,'user/site_admin.tpl.php');
	}

	public function payment_method(){
		load_view($this,'user/site_payment.tpl.php');
	}

	public function payment_error(){
		load_view($this,'user/site_payment_wrong.tpl.php');
	}

	public function payment_confirm(){
		load_view($this,'user/site_payment_confirm.tpl.php');
	}

	public function payment_success(){
		load_view($this,'user/site_payment_success.tpl.php');
	}

	public function client(){
		load_view($this,'user/client.tpl.php');
	}

	public function search(){
		load_view($this,'site/search.tpl.php');
	}

	public function search_case(){
		load_view($this,'site/search_case.tpl.php');
	}


}