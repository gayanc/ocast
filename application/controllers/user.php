<?php
class User extends CI_Controller {
    
    private $filter_data = null;


    public function __construct(){
		parent::__construct();
        $this->load->model('user_model', '', TRUE);
        $this->load->helper('cookie');
        $this->load->helper('form');
        
        $this->load->model('category_model');
        $this->load->model('country_model');
        $this->load->model('device_model');
        $this->load->model('cookie_model');
        $this->load->model('user_model');
        $this->load->model('Format_Model');
        
        $this->filter_data["category_list"] = $this->category_model->getAll();
        $this->filter_data["country_list"] = $this->country_model->getAll();
        $this->filter_data["device_list"] = $this->device_model->getAll();
        $this->filter_data["type_list"] = $this->Format_Model->getAllFormatTypes();
        
	}

	// public function language(){
	// 	load_view($this,'user/site_lang.tpl.php');
	// }

	public function login(){
        
        if(!$this->session->userdata('logged_in')){
            
            // check for the auto login cookie
            $autolog = get_cookie('_lact');
            
            if($autolog){
                if($this->autolog_check_database($autolog)){
                    redirect('site/overview', 'refresh'); 
                }
            }

			$this->load->view('user/login.tpl.php',  $this->filter_data);
            //load_view($this,'user/login.tpl.php', NULL, TRUE);
            
		}else{
			redirect('site/overview', 'refresh');
		}
		
	}

    public function logout(){
		$this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('last_visit');
		$this->session->sess_destroy();
        delete_cookie('_lact');
		redirect('user/login', 'refresh');
	}
    
	public function reset_password(){
        
        if($this->input->post('txtEmail')){
            
            $result = $this->user_model->get_user('ID,UserTypeID,UserName,CompanyID,Fname,Lname', array('UserName' => $this->input->post('txtEmail')));
            $reset_key = sha1(time().rand());
            
            if($result){
                
                if($this->user_model->update_user(array('PasswordResetCode' => $reset_key, 'ResetRequestDate' => date('Y-m-d H:i:s')), array('UserName' => $this->input->post('txtEmail')))){
                    
                    $config['mailtype'] = "html";
                    $this->email->initialize($config);
                    $subject = $this->config->item('website_name') . " " . lang('reset_password');
                    $bcc = $this->config->item('email_archive');

                    $reset_url = base_url() . "user/new_password/" . $reset_key;

                    $body = "<p><b><u>OCAST Account password reset request</u></b></p>";
                    $body .= "Hi " . $result->Fname . " " . $result->Lname;
                    $body .="<p>We received a request to reset the password linked with your e-mail address.</p>";
                    $body .="<p>If you made this request, please click the link below to reset your password:</p>";
                    $body .= $reset_url;
                    $body .="<p>If you did not make the password reset request, please just ignore this email. Your customer account is safe and has not been changed in anyway.</p>";
                    $body .="<p>Thank you for using OCAST</p>";
                    $body .="<br>" . $this->config->item('website_name');
                    
                    email_notification($subject, $body, $this->input->post('txtEmail'), null, null, $bcc);
                    
                    $this->session->set_flashdata('message', $this->input->post('txtEmail'));
                    
                    redirect('user/sent_password', 'refresh');
                }
            }

        }
        
		$this->load->view('user/reset_password.tpl.php', $this->filter_data);
        
	}

	public function sent_password(){
		$this->load->view('user/sent_password.tpl.php', $this->filter_data);
	}
    
    public function reset_denied(){
		$this->load->view('user/reset_access_denied.tpl.php', $this->filter_data);
	}
    
    public function reset_success(){
		$this->load->view('user/reset_access_success.tpl.php', $this->filter_data);
	}
    
    public function new_password($reset_key = NULL){
       
        if($this->input->post('txtPassword') && $this->input->post('resetCode')){
            
            $result = $this->user_model->get_user('ID,UserTypeID,UserName,CompanyID,Fname,Lname', array('PasswordResetCode' => $this->input->post('resetCode')));
            
            if($result){
                
                $new_password = md5($this->input->post('txtPassword'));
                $this->user_model->update_user(array('PasswordResetCode' => NULL, 'ResetRequestDate' => NULL, 'Password' => $new_password), array('ID' => $result->ID));
                
                redirect('user/reset_success', 'refresh');
                
            }  else {
                var_dump('WERNING: You haven\'t any account in OCAST');exit;
            }
        }elseif(is_null($reset_key)){
            
            redirect('user/reset_denied', 'refresh');
            
        }else{
            $result = $this->user_model->get_user('ID,UserTypeID,UserName,CompanyID,Fname,Lname', array('PasswordResetCode' => $reset_key));
            
            if(!$result){
                redirect('user/reset_denied', 'refresh');
            }
        }
        
        $this->load->view('user/new_password.tpl.php', $this->filter_data);
		
	}
    
    function autolog_check_database($key){

		$result = $this->user_model->verify_autologin($key);
		$tracked = FALSE;
        
		if($result){
			$sess_array = array();
			foreach($result as $row){
				$sess_array = array( // user login
					'UserID'     => $row->ID,
					'UserName'   => $row->UserName,
					'UserTypeID' => $row->UserTypeID,
                    'CompanyID'  => $row->CompanyID,
                    'Fname'      => $row->Fname,
                    'Lname'      => $row->Lname,
				);
                
                $track = array(
                  'LastLogin'   => date('Y-m-d H:i:s'),
                  'LastLoginIP' => $this->input->ip_address()
                );
                
                $tracked = $this->user_model->track_login($track, $row->ID);
				$this->session->set_userdata('logged_in', $sess_array);
                
                if($tracked){
                    // modify LoginKey with autologin
                    $login_hash = sha1($row->UserName . $row->Password . $this->input->ip_address());

                    $login_cookie = array(
                        'name'   => '_lact',
                        'value'  => $login_hash,
                        'expire' => $this->config->item('autologin_cookie_expire')
                    );

                    $this->input->set_cookie($login_cookie); 
                    $this->user_model->modify_autolog_key($row->UserName, $row->Password, $this->input->ip_address());
                
                }
                
			}
            
            if($tracked){

                return TRUE;
                
            }  else {
                
                return FALSE;
                
            }
			
            
		}else{
			return FALSE;
		}
	}
}