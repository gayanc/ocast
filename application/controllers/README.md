Ocast CodeIgnitor Framework Bundle
===============================

Codeigniter Bundle with IonAuth2,RESTful,Base-Model,Twitter Bootstrap and jQuery

It contains:

- Codeigniter (`2.1.3`) 
- ionAuth2 (`#462`) Authentication Lib
- codeigniter-restserver (`#204`) RESTful API
- Base-Model (`#107`) CRUD Base Model
- Twitter Boostrap (`2.3.2`) Frontend Design Framework
- jQuery (`1.10.1`) 
- JQuery-HTML5-placeholder-shim

### Links for additional Info ###

[Codeigniter](http://ellislab.com/codeigniter)

[ionAuth2](https://github.com/benedmunds/CodeIgniter-Ion-Auth)

[codeigniter-restserver](https://github.com/philsturgeon/codeigniter-restserver)

[Base-Model](https://github.com/jamierumbelow/codeigniter-base-model)

[Twitter Bootstrap](http://http://twitter.github.io/bootstrap/)

[jQuery](http://jquery.com/) 

[JQuery-HTML5-placeholder-shim](https://github.com/parndt/jquery-html5-placeholder-shim/) 

© Team R&D - Eyepax IT Consulting Pvt Ltd Colombo SL 2013