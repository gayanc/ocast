<?php

class connectgoogle extends CI_Controller {

    // private static $token;
    function __construct() {
        parent::__construct();
        if (function_exists("set_time_limit") == TRUE AND @ini_get("safe_mode") == 0) {
            @set_time_limit(0);
        }
        $this->load->model('Marketuser_model');
        $this->load->model('Site_model');
        $this->load->model('Analytics_model');
        //$this->load->model('Googleanalytics_model');
    }

    function index() {
        $client = new Google_Client();
        $obj_service = $this->Marketuser_model->create_google_service($client);

        if (isset($_GET['code'])) {       // we received the positive auth callback, get the token and store it in session
            $code = $_GET['code'];
            $client->authenticate($code);
            $access_token = $client->getAccessToken();
            $this->save_token_session($access_token);
            redirect(base_url('signup/connect'), 'location');
        }
        else {
            $this->google_login($client);
        }
    }

    function get($RequestedSiteId = null) {
        
        if ($RequestedSiteId) {
            $SiteList = $this->Site_model->CrongetAllSites($RequestedSiteId);
        }
        $SiteList = $this->Site_model->CrongetAllSites();
        $EndDate = date('Y-m-d', mktime(0, 0, 0, date('n'), date('j') - 1, date('Y')));
        $metrics = 'ga:visitors,ga:visits,ga:pageviews';
        $dimensions = null;
        $sort = null;
        $start_index = null;
        $max_results = null;
        $segment = null;
        $filter = null;
        foreach ($SiteList as $Site) {
            $GoogleSiteID = $Site->GoogleSiteID;
            $LastGoogleUpdate = $Site->LastGoogleUpdate;
            $IsMobile = $Site->IsMobile;
            $ProfileID = $Site->ProfileID;
            $AnalyticsCreatedDate = $Site->addeddate;
            $Isdownloading = $Site->Isdownloading;
            $duration = 0;
            $duration = floor((time() - strtotime($AnalyticsCreatedDate)) / (24 * 3600));
            $token_arr = $this->Site_model->get_token_by_site($Site->SiteID);
            $Token = $token_arr[0]->Token;
            if ($LastGoogleUpdate == "" || $LastGoogleUpdate == "0000-00-00" || $LastGoogleUpdate == "0000-00-00 00:00:00") {
                if ($duration < 365) {
                    $StartDate = $AnalyticsCreatedDate;
                }
                else {
                    $StartDate = date('Y-m-d', mktime(0, 0, 0, date('n'), date('j'), date('Y') - 1));
                }
            }
            else {

                $StartDate = substr($LastGoogleUpdate, 0, 10); // 2013-09-14 12:12:03
            }
            $count = 0;
            while ($EndDate > $StartDate) {
                $Data["UniqueVisits"] = 0;
                $Data["Visits"] = 0;
                $Data["PageViews"] = 0;
                $Data["UniqueVisitsMobile"] = 0;
                $Data["VisitsMobile"] = 0;
                $Data["PageViewsMobile"] = 0;
                
                
                $Data = array();
                $StartDate = DateTime::createFromFormat("Y-m-d", $StartDate);
                $StartDate = date('Y-m-d', mktime(0, 0, 0, $StartDate->format('n'), $StartDate->format('j') + 1, $StartDate->format('Y')));
                $Data["SiteID"] = $Site->SiteID;
                $Data["Date"] = $StartDate;
                $segment = "";
                $GoogleArray = $this->Site_model->ExecuteGoogleQuery($ProfileID, $Token, $GoogleSiteID, $StartDate, $StartDate, $metrics, $dimensions, $sort, $start_index, $max_results, $segment, $filter);
                $count++;
                //  $Token, $GoogleSiteID, $report_start_date, $report_end_date, $metrics, $dimensions = null, $sort = null, $start_index = null, $max_results = null, $segment = null, $filter = null
                if (isset($GoogleArray["ga:visitors"])) {
                    $Data["UniqueVisits"] = $GoogleArray["ga:visitors"];
                }

                if (isset($GoogleArray["ga:visits"])) {
                    $Data["Visits"] = $GoogleArray["ga:visits"];
                }

                if (isset($GoogleArray["ga:pageviews"])) {
                    $Data["PageViews"] = $GoogleArray["ga:pageviews"];
                }
                $segment = "gaid::-11";

                $GoogleArray = $this->Site_model->ExecuteGoogleQuery($ProfileID, $Token, $GoogleSiteID, $StartDate, $StartDate, $metrics, $dimensions, $sort, $start_index, $max_results, $segment, $filter);
                $count++;
                if (isset($GoogleArray["ga:visitors"])) {
                    $Data["UniqueVisitsMobile"] = $GoogleArray["ga:visitors"];
                }

                if (isset($GoogleArray["ga:visits"])) {
                    $Data["VisitsMobile"] = $GoogleArray["ga:visits"];
                }

                if (isset($GoogleArray["ga:pageviews"])) {
                    $Data["PageViewsMobile"] = $GoogleArray["ga:pageviews"];
                }
                
                $Data["UniqueVisits"] = $Data["UniqueVisits"] - $Data["UniqueVisitsMobile"];
                $Data["Visits"] = $Data["Visits"] - $Data["VisitsMobile"] ;
                $Data["PageViews"] = $Data["PageViews"] - $Data["PageViewsMobile"]; 

                $Data["UpdatedTime"] = date('Y-m-d H:i:s', mktime(date('H'), date('i'), date('s'), date('n'), date('j'), date('Y')));

                print_r($Data);
                echo "<br>";
                sleep(1);
                $this->Analytics_model->add_analytics($Data);
                $this->Site_model->update_lastgoogle_date($Site->SiteID, $StartDate);
                if ($count > 3) {
                   // sleep(1);
                    $count = 0;
                }
            }
        }
    }

    function connet_google() {
        $client = new Google_Client();
        $obj_service = $this->Marketuser_model->create_google_service($client);

        if (isset($_GET['code'])) {       // we received the positive auth callback, get the token and store it in session
            $code = $_GET['code'];
            $client->authenticate($code);
            $access_token = $client->getAccessToken();
            $this->savetoken($access_token);
            redirect(base_url('connectgoogle/list_profiles'), 'location');
        }
        else {
            $this->google_login($client);
        }
    }

    function google_login($obj_gclient) {
        $authUrl = $obj_gclient->createAuthUrl();
        redirect($authUrl);
    }

    function save_token_session($access_token) {
        $this->session->set_userdata('access_token', $access_token);
    }

    function get_google_sites() {

        if (!$this->session->userdata('access_token')) {
            $this->connet_google();
        }
        else {
            $client = new Google_Client();
            $accessToken = $this->session->userdata('access_token');
            $obj_service = $this->Marketuser_model->create_google_service($client);
            $this->Marketuser_model->config_google_client($client, $accessToken);
            $account_list = $this->Marketuser_model->get_analytics_accounts($client, $obj_service);            
            print_r($account_list);
            if ($account_list == 0) {
                echo $text_arr['error_msg'] = "You don't have google analytics accounts";
            }
        }

        exit;
    }

}

?>
