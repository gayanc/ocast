<?php

class signup extends CI_Controller {

    public $siteID;

    public function __construct() {
        parent::__construct();
        $this->load->model('Marketuser_model');
        $this->load->model("Site_model");
        $this->load->model('Category_Model');
        $this->load->model("Signup_model");
        $this->load->model("Country_Model");
        $this->load->model("Gprofile_model");
        $this->load->model("Analytics_model");
        $this->load->model("Company_Model");
        $this->load->model("User_model");
        $this->load->model("Demographic_Model");
        $this->load->model("Format_Model");
        $this->load->model("Contact_Model");
        $this->load->model("Quickfac_Model");
        $this->load->model("Testimonial_Model");
        $this->load->model("Case_Model");
        $this->load->model("Cookie_Model");
        $this->load->model('Company_Model');

        $this->load->helper("ocast");
        //print('<pre>');print_r($this->session->userdata);
        $this->siteID = $this->session->userdata('site_id'); //set by session
    }

    /*
     * Call this function once user activated the account and proceeds
     */

    public function updateUserStep() {
        
    }

    public function siteAddContact() {
        // $modcnt = $_POST['modcnt'];
        // $country_extentions = $this->Country_Model->getAll();

        $datarr = array();
        $datarr['modcnt'] = $_POST['modcnt'];
        $datarr['country_extentions'] = $this->Country_Model->getAll();

        $content  = $this->load->view('signup/contact_add.php',$datarr,true);
        $htmlContent = array('innerHTML' => $content );
        $htmlContent =  json_encode($htmlContent);
        $this->output->set_content_type('application/json')
        ->set_output($htmlContent);
    }

    public function siteAddDemographicData() {
        $datarr = array();
        $datarr['modcnt'] = $_POST['modcnt'];

        $content  = $this->load->view('signup/demog_add.php',$datarr,true);
        $htmlContent = array('innerHTML' => $content );
        $htmlContent =  json_encode($htmlContent);
        $this->output->set_content_type('application/json')
        ->set_output($htmlContent);
    }

    public function addDemogAnswer() {
        $modcnt = $_POST['modcnt']; //modcount
        $anscnt = $_POST['anscnt']; //answercount

        $datarr = array();
        $datarr['modcnt'] = $_POST['modcnt'];
        $datarr['anscnt'] = $_POST['anscnt'];

        $content  = $this->load->view('signup/demog_anwr.php',$datarr,true);
        $htmlContent = array('innerHTML' => $content );
        $htmlContent =  json_encode($htmlContent);
        $this->output->set_content_type('application/json')
        ->set_output($htmlContent);
    }

    /**
     * Descrition : add user site and company inforamtion to the temporary table and send confirmation email
     * @author Gayan Chathuranga <gayan.c@eyepax.com>
     */
    function add_site() {
        $CI = get_instance();
        $siteID = $CI->session->userdata('site_id');
        if(isset($siteID ) && $siteID !=''){  //if site id is set
          $user_name = $_POST['acctxt_email'];
          //update account and company inforamtion
          $user_data = array(
            'Fname' => isset($_POST['acctxt_fname']) ? $_POST['acctxt_fname'] : '',
            'Lname' => isset($_POST['acctxt_lname']) ? $_POST['acctxt_lname'] : ''
          );
          $cnd = array('ID'=>$CI->session->userdata['userDetails']['ID']);
          $this->User_model->update_user($cnd,$user_data);
          $company_data = array(
            'Phone' => (isset($_POST['acctxt_tel'])) ? mysql_real_escape_string($_POST['acctxt_tel']) : '',
            'Zip' => (isset($_POST['acctxt_zip'])) ? mysql_real_escape_string($_POST['acctxt_zip']) : '',
            'CountryID' => (isset($_POST['acctxt_country'])) ? mysql_real_escape_string($_POST['acctxt_country']) : '',
            'LastModifiedDate' => getDateTime(),
            'LastModifiedBy' => $this->session->userdata['session_id'],
            'CountryCallingCode' => (isset($_POST['cnt_extension'])) ? mysql_real_escape_string($_POST['cnt_extension']) : '',
          );
          $company_lang_data = array(
            'Name' => (isset($_POST['acctxt_cmpname'])) ? mysql_real_escape_string($_POST['acctxt_cmpname']) : '',
            'Address1' => (isset($_POST['acctxt_addr1'])) ? mysql_real_escape_string($_POST['acctxt_addr1']) : '',
            'Address2' => (isset($_POST['acctxt_addr2'])) ? mysql_real_escape_string($_POST['acctxt_addr2']) : '',
            'City' => (isset($_POST['acctxt_city'])) ? mysql_real_escape_string($_POST['acctxt_city']) : '',
            'LastModifiedDate' => getDateTime(),
            'LastModifiedBy' => $this->session->userdata['session_id']
          );
          $this->Company_Model->updateCompanyInfo($company_data,$company_lang_data);
          //$this->Company_Model->updateCompanyLangInfo($compnay_id,);
          echo 'Done';
        }else{
        $data = array(
            'ActivationCode' => sha1(mt_rand(10000, 99999) . time() . $_POST['acctxt_email']), //random
            'LangCode' => lang('lcode'), //session lang
            'UserName' => (isset($_POST['acctxt_email'])) ? mysql_real_escape_string($_POST['acctxt_email']) : '',
            'Password' => (isset($_POST['acctxt_pwd']) && $_POST['acctxt_pwd'] != '') ? md5($_POST['acctxt_pwd']) : '',
            'Salt' => NULL,
            'Fname' => (isset($_POST['acctxt_fname'])) ? mysql_real_escape_string($_POST['acctxt_fname']) : '',
            'Lname' => (isset($_POST['acctxt_lname'])) ? mysql_real_escape_string($_POST['acctxt_lname']) : '',
            'Company' => (isset($_POST['acctxt_cmpname'])) ? mysql_real_escape_string($_POST['acctxt_cmpname']) : '',
            'PhoneCountryCode' => (isset($_POST['cnt_extension'])) ? mysql_real_escape_string($_POST['cnt_extension']) : '',
            'PhoneNumber' => (isset($_POST['acctxt_tel'])) ? mysql_real_escape_string($_POST['acctxt_tel']) : '',
            'Address1' => (isset($_POST['acctxt_addr1'])) ? mysql_real_escape_string($_POST['acctxt_addr1']) : '',
            'Address2' => (isset($_POST['acctxt_addr2'])) ? mysql_real_escape_string($_POST['acctxt_addr2']) : '',
            'Zip' => (isset($_POST['acctxt_zip'])) ? mysql_real_escape_string($_POST['acctxt_zip']) : '',
            'City' => (isset($_POST['acctxt_city'])) ? mysql_real_escape_string($_POST['acctxt_city']) : '',
            'CountryID' => (isset($_POST['acctxt_country'])) ? mysql_real_escape_string($_POST['acctxt_country']) : '',
            'AciveStatus' => 'pending', //active', 'pending', 'inactive
            'SessionID' => $this->session->userdata['session_id'],
            'RemorteIP' => get_client_ip(),
            'UserAgent' => $_SERVER['HTTP_USER_AGENT'],
            'CreatedDate' => getDateTime(),
            'LastModifiedDate' => getDateTime(),
            'RequestedURL' => (isset($_POST['acctxt_url'])) ? mysql_real_escape_string($_POST['acctxt_url']) : ''
        );
        $this->Signup_model->insertData($data);
        $this->session->set_userdata('requested_URL', $data['RequestedURL']);

            $this->send_activation_mail($data['ActivationCode'], $data['UserName'], $data['Fname'], $data['Lname']);
            echo 'Done';
        }
    }

    /**
     * Descrition : validate each signup process step before proceed
     * @author Gayan Chathuranga <gayan.c@eyepax.com>
     */
    public function validateStep($action) {
        //TODO
    }

    function checkcron() {
        $sitelist = $this->Site_model->GetSitesTochechCronResult();
        $data["analyticts"] = array();
        $data["sitelist"] = array();
        $site_id = "";
        if (sizeof($sitelist) > 0) {
            foreach ($sitelist as $site) {
                $sites[] = array("ID" => $site->ID, "URL" => $site->URL);
            }
            $data["sitelist"] = $sites;
        }

        $site_id = $this->input->post('site_id');
        $data["selectedSite"] = "";

        if ($site_id > 0) {
            $data_arr = $this->Analytics_model->GetDataTochechCronResult($site_id);
            if (sizeof($data_arr) > 0) {
                foreach ($data_arr as $data_row) {
                    $analyticts[] = array('UniqueVisits' => $data_row->UniqueVisits, 'PageViews' => $data_row->PageViews, 'Visits' => $data_row->Visits, 'Date' => $data_row->Date, 'UniqueVisitsMobile' => $data_row->UniqueVisitsMobile, 'PageViewsMobile' => $data_row->PageViewsMobile, 'VisitsMobile' => $data_row->VisitsMobile);
                }

                $data["analyticts"] = $analyticts;
            }

            $data["selectedSite"] = $site_id;
        }

        load_view($this, 'signup/checkcorn', $data);
    }

    function new_account() {
        $data['country_extentions'] = $this->Country_Model->getCallingID();
        $data['company_info'] = $this->Company_Model->getCompanyData($this->siteID);
        $CompanyID = $this->Site_model->getSiteCompanyID($this->siteID);
        $data['useracc_info'] = $this->User_model->getCompanyUserInfo($CompanyID);
//TO DO: check weather the user is returning, already cookie is set or not
        /*
         * If set we display user information and proper message to activate his account
         */
//$this->session->userdata['session_id'];   
        load_view($this, 'signup/add_account_information', $data, TRUE);
    }

    function getTempAccInfo($UserName) {
        $data = array();
        $UserName = urldecode($UserName);
        if ($UserName == "") {
            if (isset($_COOKIE['UserName'])) {
                $UserName = $_COOKIE['UserName'];
            }
        }
        $tmp_account_info = $this->Signup_model->getByUserName($UserName);
        if (sizeof($tmp_account_info) > 0) {
            $data = $tmp_account_info[0];
        }
        echo json_encode($data);
    }

    function site_info() {
        $this->redirectGuest();
        $data['country_extentions'] = $this->Country_Model->getCallingID();
        $data['site_category'] = $this->Category_Model->getAll();         
        $data['site_data'] = $this->Site_model->getSiteData($this->siteID);
        load_view($this, 'signup/site_info.tpl.php', $data, TRUE);
    }

    function get_demo() {
        $this->Demographic_Model->getSiteDemogInfo();
    }

    function add_formats() {
        return true;
    }

    function site_info_submit() {
        //site_id
        $sitecontact = array();
        $quickfacts = array();
        $testimonial = array();
        $demoginfo = array();

        ##### Site Information ####
        $data = array(
          'Logo' => (isset($_POST['sptxt_sitelogo'])) ? mysql_real_escape_string($_POST['sptxt_sitelogo']) : '',
          'AverageAge' => (isset($_POST['slider_avg_age'])) ? mysql_real_escape_string($_POST['slider_avg_age']) : '',
          'MalePercentage' => (isset($_POST['slider_demo_val'])) ? mysql_real_escape_string($_POST['slider_demo_val']) : '',
          'LastModifiedDate' => getDateTime(),
          'LastModifiedBy' => $this->session->userdata['session_id'] //user
        );
        $this->Site_model->updateSiteInfo($this->siteID, $data);
        //update site lang table
        $site_lang_data = array(
          'SiteID' => $this->siteID,
          'LangCode' => lang('lcode'),
          'Description' => $_POST['txt_site_info_desc'],
          'MetaDescription' => '',
          'Meta_Keywords' => '',
          'Audience' => mysql_real_escape_string($_POST['audience_desc']),
          'LastModifiedDate' => getDateTime(),
          'LastModifiedBy' => $this->session->userdata['session_id']
        );
        $this->Site_model->updateSiteLangInfo($this->siteID, $site_lang_data);
        ##### eof Site Information ####
        ##### Site Countries ##########
        $main_country = isset($_POST['sptxt_cntry_main']) ? $_POST['sptxt_cntry_main'] : '';

        $country_array = array(
          'CountryID' => $main_country,
          'SiteID' => $this->siteID,
          'IsDefault' => '1',
          'CreatedDate' => getDateTime(),
          'CreatedBy' => $this->session->userdata['session_id'],
          'LastModifiedDate' => getDateTime(),
          'LastModifiedBy' => $this->session->userdata['session_id']
        );

        $this->Country_Model->deleteSiteCountries('trn_site_country', $this->siteID); //delete countires
        $this->Country_Model->insertSiteCountry('trn_site_country', $country_array);
        $other_countries = isset($_POST['sptxt_cntry_other']) ? $_POST['sptxt_cntry_other'] : array();         //other countries
        if (count($other_countries)) {
            foreach ($other_countries as $value) {
                $country_other_array = array(
                  'CountryID' => $value,
                  'SiteID' => $this->siteID,
                  'IsDefault' => '0',
                  'CreatedDate' => getDateTime(),
                  'CreatedBy' => $this->session->userdata['session_id'],
                  'LastModifiedDate' => getDateTime(),
                  'LastModifiedBy' => $this->session->userdata['session_id']
                );
                $this->Country_Model->insertSiteCountry('trn_site_country', $country_other_array);
            }
        }
        ######## EOF Site Countries ########
        ######### Site Categories #########
        $this->Category_Model->deleteSiteCategories('trn_site_category', $this->siteID); //delete countires

        $site_category = isset($_POST['sptxt_category']) ? $_POST['sptxt_category'] : array();
        if (count($site_category)) {
            foreach ($site_category as $value) {
                $category_data = array(
                  'SiteID' => $this->siteID,
                  'CategoryID' => $value,
                  'CreatedDate' => getDateTime(),
                  'CreatedBy' => $this->session->userdata['session_id'],
                  'LastModifiedDate' => getDateTime(),
                  'LastModifiedBy' => $this->session->userdata['session_id']
                );

                $this->Category_Model->insertSiteCategory('trn_site_category', $category_data);
            }
        }
        ######### EOF Site Categories #########
        ###### Site Contact Data ##########
        $sitecontact = $_POST['sptxt_contact'];
        //TODO : if data exist delete and insert
        $this->Contact_Model->deleteSiteContacts('trn_site_contact', 'trn_site_contact_lang', $this->siteID); //Delete categories
        $fname = '';
        foreach ($sitecontact as $key => $value) {
            $fname = isset($value['fname']) ? mysql_real_escape_string($value['fname']) : '';
            $default = ($value['default'] == 'on') ? 1 : 0;

            $sirname = isset($value['sirname']) ? mysql_real_escape_string($value['sirname']) : '';
            $title = isset($value['title']) ? mysql_real_escape_string($value['title']) : '';
            $email = isset($value['email']) ? mysql_real_escape_string($value['email']) : '';
            $excode = isset($value['excode']) ? mysql_real_escape_string($value['excode']) : '';
            $tel = isset($value['tel']) ? mysql_real_escape_string($value['tel']) : '';
            $image = isset($value['image']) ? mysql_real_escape_string($value['image']) : '';
            if ($email != '' || $tel != '') {
                $site_contact_data = array(
                  'SiteID' => $this->siteID,
                  'Fname' => $fname,
                  'IsDefault' => $default,
                  'Lname' => $sirname,
                  'Email' => $email,
                  'ConcatNumber' => $tel,
                  'ProfilePic' => $image,
                  'ContactCountryCode ' => $excode,
                  'CreatedDate' => getDateTime(),
                  'CreatedBy' => $this->session->userdata['session_id'],
                  'LastModifiedDate' => getDateTime(),
                  'LastModifiedBy' => $this->session->userdata['session_id']
                );
                $SiteContactID = $this->Contact_Model->insertMasterContactData('trn_site_contact', $site_contact_data);
                $contact_lang_data = array(
                  'SiteContactID' => $SiteContactID,
                  'LangCode' => lang('lcode'),
                  'Designation' => $title,
                  'CreatedDate' => getDateTime(),
                  'CreatedBy' => $this->session->userdata['session_id'],
                  'LastModifiedDate' => getDateTime(),
                  'LastModifiedBy' => $this->session->userdata['session_id']
                );
                //TODO : if data exist delete and insert
                $this->Contact_Model->insertContactLangData('trn_site_contact_lang', $contact_lang_data);
            }
        }
        ###### EOF Site Contact Data ##########
        ###### Site Quickfacts Data ##########
        $quickfacts = $_POST['quickfacts'];
        $qfacts_data = array(
          'SiteID' => $this->siteID,
          'LastUpdated' => getDateTime(),
          'CreatedDate' => getDateTime(),
          'CreatedBy' => $this->session->userdata['session_id'],
          'LastModifiedDate' => getDateTime(),
          'LastModifiedBy' => $this->session->userdata['session_id']
        );
        $this->Quickfac_Model->deleteSiteQfacts('trn_quick_fact', 'trn_quick_fact_lang', $this->siteID); //Delete facts
        $QuickFactID = $this->Quickfac_Model->insertMasterQFactsData('trn_quick_fact', $qfacts_data);
        foreach ($quickfacts as $key => $value) {
            if (isset($value['text']) && $value['text'] != '') {
                $qfacts_lang_data = array(
                  'QuickFactID' => $QuickFactID,
                  'Text' => mysql_real_escape_string($value['text']),
                  'LangCode' => lang('lcode'),
                  'CreatedDate' => getDateTime(),
                  'CreatedBy' => $this->session->userdata['session_id'],
                  'LastModifiedDate' => getDateTime(),
                  'LastModifiedBy' => $this->session->userdata['session_id']
                );
                //TODO : if data exist delete and insert
                $this->Quickfac_Model->insertQFactsLangData('trn_quick_fact_lang', $qfacts_lang_data);
            }
        }
        ###### EOF Site Quickfacts Data ##########
        ###### Site Testimonial Data ##########
        $testimonial = $_POST['testimonial'];
        $testimonial_data = array(
          'SiteID' => $this->siteID,
          'UpdateDate' => getDateTime(),
          'CreatedDate' => getDateTime(),
          'CreatedBy' => $this->session->userdata['session_id'],
          'LastModifiedDate' => getDateTime(),
          'LastModifiedBy' => $this->session->userdata['session_id']
        );
        $this->Testimonial_Model->deleteSiteTestimonialData('trn_testimonial', 'trn_testimonial_lang', $this->siteID); //Delete facts
        $TestimonialID = $this->Testimonial_Model->insertMasterTestimonialData('trn_testimonial', $testimonial_data);
        foreach ($testimonial as $key => $value) {
            if ($value['quote'] != '' || $value['said'] != '') {
                $testimonial_lang_data = array(
                  'TestimonialID' => $TestimonialID,
                  'Quote' => $value['quote'],
                  'ByWhom' => $value['said'],
                  'LangCode' => lang('lcode'),
                  'CreatedDate' => getDateTime(),
                  'CreatedBy' => $this->session->userdata['session_id'],
                  'LastModifiedDate' => getDateTime(),
                  'LastModifiedBy' => $this->session->userdata['session_id']
                );
                $this->Testimonial_Model->insertTestimonialLangData('trn_testimonial_lang', $testimonial_lang_data);
            }
        }
        ###### EOF Site Testimonial Data ##########
        ###### Demograpich Data ##########
        $demoginfo = $_POST['demoginfo'];
        $this->Demographic_Model->deleteSiteDemogData('trn_demographic_header', 'trn_demographic_header_lang', 'trn_demographic_detail', 'trn_demographic_detail_lang', $this->siteID); //Delete demographic data
        foreach ($demoginfo as $key => $head) {
            /* write demographic heads
             * tables : trn_demograpic_header trn_demograpic_header_lang
             */
            if ($head['heading'] != '') {

                $header_master_data = array(
                  'SiteID' => $this->siteID,
                  'CreatedDate' => getDateTime(),
                  'CreatedBy' => $this->session->userdata['session_id'],
                  'LastUpdated' => getDateTime(),
                  'LastModifiedBy' => $this->session->userdata['session_id']
                );
                $headerID = $this->Demographic_Model->insertDemogHeader('trn_demographic_header', $header_master_data);
                $header_lang_data = array(
                  'DemographicHeaderID' => $headerID,
                  'HeadLine' => mysql_real_escape_string($head['heading']),
                  'LangCode' => lang('lcode'),
                  'CreatedDate' => getDateTime(),
                  'CreatedBy' => $this->session->userdata['session_id'],
                  'LastModifiedDate' => getDateTime(),
                  'LastModifiedBy' => $this->session->userdata['session_id']
                );
                $this->Demographic_Model->insertDemogHeaderLang('trn_demographic_header_lang', $header_lang_data);
                /* write demographic details
                 * tables : trn_demographic_detail trn_demographic_detail_lang
                 */
                $demoHeadDetail = $head['head'];
                foreach ($demoHeadDetail as $key => $detail) {
                    if ($detail['answer'] != '') {
                        $header_points_data = array(
                          'HeaderID' => $headerID,
                          'Points' => $detail['point'],
                          'CreatedDate' => getDateTime(),
                          'CreatedBy' => $this->session->userdata['session_id'],
                          'LastModifiedDate' => getDateTime(),
                          'LastModifiedBy' => $this->session->userdata['session_id']
                        );
                        $demographicDetailID = $this->Demographic_Model->insertDemogHeadPoint('trn_demographic_detail', $header_points_data);
                        $answers_to_points_data = array(
                          'DemographicDetailID' => $demographicDetailID,
                          'Answer' => mysql_real_escape_string($detail['answer']),
                          'LangCode' => lang('lcode'),
                          'CreatedDate' => getDateTime(),
                          'CreatedBy' => $this->session->userdata['session_id'],
                          'LastModifiedDate' => getDateTime(),
                          'LastModifiedBy' => $this->session->userdata['session_id']
                        );
                        $this->Demographic_Model->insertDemogPointAnswers('trn_demographic_detail_lang', $answers_to_points_data);
                    }
                }
            }
        }
###### Eof Demograpich Data ##########
        echo 'data inserted';
    }

    function connected() {
        //$this->redirectGuest(); // need to check this step validation
        $site_name = "";
        $GoogleSiteList = "";

        if (isset($_POST["site_name"])) {
            $site_name = $_POST["site_name"];
        }

        if (isset($_POST["GoogleSiteList"])) {
            $GoogleSiteList = $_POST["GoogleSiteList"];
        }

        if ($GoogleSiteList == "" || $GoogleSiteList == "") {
             $data["message"] = lang('Pls_select_website');
             load_view($this, 'signup/signup_connect', $data);
             return 0;
        }
        //$user_id = $_POST['user_id'];
        //TODO : site id

        $site_list = $this->session->userdata('google_site_list');
        if ($site_list != null) {
            foreach ($site_list as $gsite) {
                if ($gsite["GoogleSiteID"] == $GoogleSiteList) {
                    $profiledata["Token"] = $this->session->userdata('access_token');
                    $profiledata["CompanyId"] = $this->session->userdata('CompanyId');
                    $google_profile_id = $this->Gprofile_model->addnew($profiledata);
                    $gsite["ProfileID"] = $google_profile_id;
                    $gsite["SiteName"] = mysql_real_escape_string($site_name);
                    $gsite['CreatedDate'] = getDateTime();
                    $gsite['CreatedBy'] = $this->session->userdata['session_id'];
                    $gsite['LastModifiedDate'] = getDateTime();
                    $gsite['LastModifiedBy'] = $this->session->userdata['session_id'];

                    $this->Site_model->save_google_site($gsite); //write to set_user_site and trn_user
                    $message = "Successfully connected; <a href=''><i>" . $gsite["SiteName"] . "</i></a>";
                    $message .= "<ul>" . $SitesNameList . "</ul>";
                    $data["message"] = $message;
                    // load signup/signup_connected if sucess and display message 
                    echo"success:" . $message;
                    return;
                }
            }
        }


        echo "error: Please select your website";
    }

    function connect() {

        //$auth = isset($_GET['auth']) ? $_GET['auth'] : 0;  && $auth == 'user'
        // if(!isset($this->session->userdata('site_id'))){
        //       redirect('signup/new_account', 'refresh');
        // }
        // print_r($this->session->all_userdata());exit;
        $isempty = 1;         
        $CompanyId = $this->session->userdata('company_id'); 
        if($CompanyId =="")
        {
            redirect('signup/new_account', 'refresh');
        }
        $Sitelist = $this->Signup_model->IsAddedSite($CompanyId);


        if (sizeof($Sitelist) > 0) {

            foreach ($Sitelist as $site) {
                $data['SiteName'] = $site['SiteName'];
                $data['URL'] = $site['URL'];
                $data["isempty"] = $isempty;
                $data["message"] = lang('Site_already_added');
                load_view($this, 'signup/signup_connected', $data);
                return 0;
            }
        }
        $account_list = $this->Site_model->get_google_sites();
        $requested_URL = $this->session->userdata('requested_URL');
        $CompanyID = $this->session->userdata('company_id');
        $FlagSiteFound = 0;
        $SitesNameList = "";
        $message = "";


        $site_list = array();
        if (($account_list)) {
            $isempty = 0;
            foreach ($account_list as $account) {

                //array('GoogleSiteID'=>$prof_id,'SiteName'=>$site_name,'CreatedDate'=>substr($created, 0, 10));  
                $GoogleSiteID = $account["GoogleSiteID"];
                $SiteName = $account["SiteName"];
                $AnalyticsCreatedDate = $account["CreatedDate"];
                $siteData["AnalyticsCreatedDate"] = $AnalyticsCreatedDate;
                $siteData["GoogleSiteID"] = $GoogleSiteID;
                $siteData["URL"] = $SiteName;
                $siteData["CompanyID"] = $CompanyID;
                $site_list[] = $siteData;
                $SitesNameList.="<li>" . $SiteName . "</li>";
            }


            $this->session->set_userdata('google_site_list', $site_list);
        }

        $data["isempty"] = $isempty;
        $data["message"] = lang('No_sites_in_google');
        load_view($this, 'signup/signup_connected', $data);
    }

    /**
     * @author Gayan Chathuranga <gayan.c@eyepax.com>
     * Function : addProductsAndFormats
     * @param   $sid - site id
     * @param   $lang - langauge code - 'en' Default
     * @tables   trn_demographic_header(trnDH),trn_demographic_header_lang(trnDHL) 
     * @tables   trn_demographic_detail(trnDD), trn_demographic_detail_lang(trnDDL)
     * @return  category Array
     */
    public function addProductsAndFormats() {

        load_view($this, 'signup/product_formats.tpl.php');
    }

    public function proceed() {
        return true;
    }

    function product_formats() {
        $this->redirectGuest();
        $data['format_data'] = $this->Format_Model->getSiteAllFormats($this->siteID);
        $data['formatTypes'] = $this->Format_Model->getAllFormatTypes();
        $data['priceTypes'] = $this->Format_Model->getAllFormatPriceTypes();
        load_view($this, 'signup/products_formats.tpl.php', $data);
    }

    public function addFormat() {
        $datarr = array();
        $datarr['modcnt'] = $_POST['modcnt'];
        $datarr['priceTypes'] = $this->Format_Model->getAllFormatPriceTypes();
        $datarr['formatTypes'] = $this->Format_Model->getAllFormatTypes();

        $content  = $this->load->view('signup/add_format.php',$datarr,true);
        $htmlContent = array('innerHTML' => $content );
        $htmlContent =  json_encode($htmlContent);
        $this->output->set_content_type('application/json')
        ->set_output($htmlContent);
    }

    public function editProductsAndFormats() {
        //print_r($_POST);
        $formatTypes = $this->Format_Model->getAllFormatTypes();
        $priceTypes = $this->Format_Model->getAllFormatPriceTypes();
        $data = array();

        $modcnt = $_POST['modcnt'];

        if ($_POST['mode'] == 'edit') {
            $nodeData = $_POST['product'][$modcnt];
            $insertID = $_POST['fid'];
            //print_r($_POST);
            $data = array(
              'screen' => isset($nodeData['screen']) ? $nodeData['screen'] : '',
              'format_type' => isset($nodeData['format_type']) ? $nodeData['format_type'] : '',
              'format' => isset($nodeData['format']) ? $nodeData['format'] : '',
              'price_type' => isset($nodeData['price_type']) ? $nodeData['price_type'] : '',
              'price' => $nodeData['price'],
            );

            $fType = $this->Format_Model->getFormatTypeByID($data['format_type']);
            $fType = isset($fType[0]['Title']) ? $fType[0]['Title'] : 'Type';
            $pType = $this->Format_Model->getPriceTypeByID($data['price_type']);
            $pType = isset($pType[0]['Text']) ? $pType[0]['Text'] : 'Price type'; //format Price Type

            $nodeHtml = '<div>
            <form id="formatNode' . $modcnt . '">
                                <div class="col-lg-12 thumbnilimg">';

            if (isset($data['screen']) && $data['screen'] != '') {
                $nodeHtml .='<img src="' . base_url('uploads/product/' . $data['screen']) . '" width="150px" height="100px" name="imgScreenName' . $modcnt . '" id="imgScreen' . $modcnt . '"/>';
            }
            else {
                $nodeHtml .='<img src="' . base_url('assets/images/add_screen.png') . '" width="150px" height="100px" name="imgScreenName' . $modcnt . '" id="imgScreen' . $modcnt . '"/>';
            }

            $nodeHtml .='<input type="hidden" id="productScreen' . $modcnt . '" name="product[' . $modcnt . '][screen]" value="' . $data['screen'] . '"/>
                                </div>
                                <div class="col-lg-12 " style="background:#f5f5f5">
                                    <div class="clearfix"></div>
                                    <div class="martp10" id="prdctype' . $modcnt . '">
                                          <div class="btn-group selbx100 selctboxstyle selbx100sml">
                                              <div data-toggle="dropdown" class="covr">
                                                  <div class="txt">' . $fType . '</div> 
                                                  <div class="arow">
                                                      <span class="caret"></span>
                                                      <div class="clearfix"></div>
                                                  </div>
                                                  <div class="clearfix"></div>
                                              </div>
                                              <ul id ="prdt_type" class="dropdown-menu">';

            foreach ($formatTypes as $key => $value) {
                $nodeHtml .='<li><a href="#" id="' . $value['FormatID'] . '" accesskey="' . $modcnt . '">' . $value['Title'] . '</a></li>';
            }


            $nodeHtml .= '</ul></div>
                                          <input type="hidden" name="product[' . $modcnt . '][format_type]" id="format_type_id" value="' . $data['format_type'] . '" />
                                      </div>
                                    <div class="martp10">
                                        <input type="hidden" name="product[' . $modcnt . '][format]" id="formatID" class="form-control h825" value="' . $data['format'] . '">
                                    </div>
                                    <div class="martp10" id="pricetype' . $modcnt . '">
                                        <div class="btn-group selbx100 selctboxstyle selbx100sml">
                                            <div data-toggle="dropdown" class="covr">
                                                <div class="txt">' . $pType . '</div> 
                                                <div class="arow">
                                                    <span class="caret"></span>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <ul id ="price_type" class="dropdown-menu">';
            foreach ($priceTypes as $key => $value) {
                $nodeHtml .='<li><a href="#" id="' . $value['PriceTypeID'] . '" accesskey="' . $modcnt . '">' . $value['Text'] . '</a></li>';
            }
            $nodeHtml .= '</ul>
                                        </div>
                                        <input type="text" name="product[' . $modcnt . '][price_type]" id="price_type_id" value="' . $data['price_type'] . '" />
                                    </div>
                                    <div class="martp10">
                                        <input type="text" name="product[' . $modcnt . '][price]" id="priceID" class="form-control h825" placeholder="Price" value="' . $nodeData['price'] . '">
                                    </div>
                                    <div class="martp10 col-lg-12 padno" style="margin-bottom:10px">
                                        <div class=" pull-left fntsmll">
                                            <a href="#" accessKey="' . $modcnt . '" class="cancelFormat">'.lang('stp_4_cancel').'</a> 
                                        </div>
                                        <div class=" pull-right">
                                            <button accesskey="' . $modcnt . '" type="button" class="btn btn-small btn-primary saveFormat" >'.lang('stp_4_saveformat').'</button>
                                        </div><div class="clearfix"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <input type="hidden" name="mode" value="save"/>
                                    <input type="hidden" name="modcnt" value="' . $modcnt . '"/>
                                    <input type="hidden" name="fid" value="' . $insertID . '" />
                                </div></form></div>';
            $htmlContent = array("innerHTML" => $nodeHtml);
        }
        if ($_POST['mode'] == 'save') {
            $nodeData = isset($_POST['product'][$modcnt]) ? $_POST['product'][$modcnt] : array();
            $fid = isset($_POST['fid']) ? $_POST['fid'] : '';
            unset($data);
            $data = array(
              'SiteID' => $this->siteID,
              'TypeID' => isset($nodeData['format_type']) ? $nodeData['format_type'] : "",
              'Format' => isset($nodeData['format']) ? mysql_real_escape_string($nodeData['format']) : "",
              'PriceTypeID' => isset($nodeData['price_type']) ? $nodeData['price_type'] : "",
              'Image' => isset($nodeData['screen']) ? $nodeData['screen'] : NULL,
              'Price' => isset($nodeData['price']) ? mysql_real_escape_string($nodeData['price']) : "",
              'CreatedDate' => getDateTime(),
              'CreatedBy' => $this->session->userdata['session_id'],
              'LastModifiedDate' => isset($_POST['fid']) ? getDateTime() : '',
              'LastModifiedBy' => $this->session->userdata['session_id']
            );
            //if format exist remove then insert
            if (count($this->Format_Model->getSiteFormat($fid, $this->siteID))) {
                $this->Format_Model->deleteSiteFormat('trn_format', $fid);
                $insertID = $this->Format_Model->insertFormatData('trn_format', $data);
            }
            else {
                $insertID = $this->Format_Model->insertFormatData('trn_format', $data);
            }
            //if format type or price type is set we get the text value

            $fType = $this->Format_Model->getFormatTypeByID($data['TypeID']);
            $fType = (isset($fType[0]['Title']) && $fType[0]['Title'] != '') ? $fType[0]['Title'] : ''; //format Type Text
            $pType = $this->Format_Model->getPriceTypeByID($data['PriceTypeID']);
            $pType = (isset($pType[0]['Text']) && $pType[0]['Text'] != '') ? $pType[0]['Text'] : ''; //format Price Type
            //write data to table trn_format
            $nodeHtml = '<form id="formatNode' . $modcnt . '">
                              <div class="col-lg-12 thumbnilimg thumbvord">';
            if (isset($data['Image']) && $data['Image'] != '') {
                $nodeHtml .='<img src="' . base_url('uploads/product/' . $data['Image']) . '" width="150px" height="100px" name="imgScreenName' . $modcnt . '" id="imgScreen' . $modcnt . '"/>';
            }
            else {
                $nodeHtml .='<img src="' . base_url('assets/images/add_screen.png') . '" width="150px" height="100px" name="imgScreenName' . $modcnt . '" id="imgScreen' . $modcnt . '"/>';
            }
            $nodeHtml .='<input type="hidden" id="productScreen' . $modcnt . '" name="product[' . $modcnt . '][screen]" value="' . $data['Image'] . '"/>
                                </div>
                                <div style="border:1px solid #dbdbdb" class="col-lg-12">
                                    <div class="clearfix"></div>
                                    <div class="martp10 checkbox padno">
                                        <p class="marginno colorthumb">' . $fType . '</p>
                                        <h6 class="marginno"><i>Format:</i> <span class="colr">' . $data['Format'] . '</span></h6>
                                        <h6 class="marginno"><i>Pricetype:</i> <span class="colr">' . $pType . '</span></h6>
                                        <h6 class="marginno"><i>Price:</i> <span class="colr">' . $data['Price'] . ' SEK</span></h6>
                                        <div style="margin-bottom:10px" class="martp10 col-lg-12 padno">
                                            <div class=" pull-left fntsmll">
                                                <a href="#" accesskey="' . $modcnt . '" class="edtFormat">Edit</a> <span style="color:#e1e1e1">|</span> <a href="" data-fid="' . $insertID . '" accesskey="' . $modcnt . '" class="removeFormat">'.lang('stp_4_removeformat').'</a> 
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>                             
                                    <div class="clearfix"></div>
                                     <input type="hidden" name="product[' . $modcnt . '][format_type]" value="' . $data['TypeID'] . '" />
                                     <input type="hidden" name="product[' . $modcnt . '][format]" class="form-control h825" value="' . $data['Format'] . '">
                                     <input type="hidden" name="product[' . $modcnt . '][price_type]"  value="' . $data['PriceTypeID'] . '"/>
                                     <input type="hidden" name="product[' . $modcnt . '][price]"  class="form-control h825" value="' . $data['Price'] . '">
                                    <input type="hidden" name="mode" value="edit"/>
                                    <input type="hidden" name="modcnt" value="' . $modcnt . '"/>
                                    <input type="hidden" name="fid" value="' . $insertID . '" />
                                </div>
                                <div class="clearfix"></div>
                                </form>
                                </div>
                                ';
            $htmlContent = array("innerHTML" => $nodeHtml);
        }
        if ($_POST['mode'] == 'remove') {
            $nodeHtml = '';
            $fid = $_POST['fid'];
            $this->Format_Model->deleteSiteFormat('trn_format', $fid);
            //delete data from table trn_format
            $htmlContent = array("innerHTML" => $nodeHtml);
        }
        print json_encode($htmlContent);
    }

    public function add_cases() {
        $this->redirectGuest();
        $data['site_cases'] = $this->Case_Model->getSiteCases($this->siteID);
        load_view($this, 'signup/signup_cases.tpl.php', $data);
    }

    public function addNewCase() {
        $modcnt = $_POST['modcnt'];
        $htmlContent = array("innerHTML" => '
                      <div class="col-lg-12 padno formmar">
                        <div class="col-lg-12 padno padleftno formmar">
                            <div class="form-group">
                              <label>Title</label>
                              <input type="text" placeholder="Case title" name="txtcases[' . $modcnt . '][title]" id="case_title_mod_' . $modcnt . '" class="form-control" value="">
                            </div>
                            <div class="form-group">
                              <label>Description</label>
                              <textarea class="form-control" rows="4" name="txtcases[' . $modcnt . '][desc]" id="case_desc_mod_' . $modcnt . '"></textarea>
                            </div>
                        </div>
                      </div>
                        <div class="col-lg-12 padno">
                          <button class="btn btn-primary pull-left margnrightbtn" id="caseImg' . $modcnt . '">ADD IMAGE</button>
                          <input type="hidden" name="txtcases[' . $modcnt . '][img]" id="case_img_mod_' . $modcnt . '" />
                          <h6 class="fntsmll">Images are displayed between the title and description.</h6>
                        </div>');
        print json_encode($htmlContent);
    }

    public function case_submit() {


        $this->Case_Model->deleteSiteCases('trn_case', 'trn_case_lang', $this->siteID); //Delete case
        $case_data = (isset($_POST['txtcases']) && count($_POST['txtcases'])) ? $_POST['txtcases'] : array();
        if (count($case_data)) {
            foreach ($case_data as $key => $value) {
                if ($value['title'] != '' || $value['img'] != '') {
                    $cs_data = array(
                      'SiteID' => $this->siteID,
                      'Image' => mysql_real_escape_string($value['img']),
                      'SortingOrder' => 0,
                      'CreatedBy' => $this->session->userdata['session_id'],
                      'CreatedDate' => getDateTime(),
                      'LastUpdated' => getDateTime(),
                      'LastModifiedBy' => $this->session->userdata['session_id']
                    );
                    $CaseID = $this->Case_Model->insertMasterCaseData('trn_case', $cs_data);
                    //case lang data
                    $cs_lang_data = array(
                      'CaseID' => $CaseID,
                      'Title' => mysql_real_escape_string($value['title']),
                      'Description' => mysql_real_escape_string($value['desc']),
                      'LangCode' => lang('lcode'),
                      'CreatedDate' => getDateTime(),
                      'CreatedBy' => $this->session->userdata['session_id'],
                      'LastModifiedDate' => getDateTime(),
                      'LastModifiedBy' => $this->session->userdata['session_id']
                    );
                    $this->Case_Model->insertMasterCaseData('trn_case_lang', $cs_lang_data);
                }
            }
            echo 'true';
        }
    }

    function preview_site() {
        $this->redirectGuest();
        $data['country_extentions'] = $this->Country_Model->getCallingID();
        $data['site_category'] = $this->Category_Model->getAll();
        $data['site_data'] = $this->Site_model->getSiteData($this->siteID);
        load_view($this, 'signup/preview.tpl.php', $data);
    }

    function uploadFile() {
        $upload_path = '';
        $gennerateFileName = strtotime(date("Y-m-d H:i:s"));
        if (isset($_FILES['screen'])) {
            $module = 'screen';
            $upload_path = PRODUCT_IMG_UPLOAD_URL;
            $theFileExts = findexts($_FILES['screen']['name']);
        }
        elseif (isset($_FILES['case'])) {
            $module = 'case';
            $upload_path = CASE_IMG_UPLOAD_URL;
            $theFileExts = findexts($_FILES['case']['name']);
        }
        elseif (isset($_FILES['contact'])) {
            $module = 'contact';
            $upload_path = CONTACT_IMG_UPLOAD_URL;
            $theFileExts = findexts($_FILES['contact']['name']);
        }
        elseif (isset($_FILES['logo'])) {
            $module = 'logo';
            $upload_path = LOGO_IMG_UPLOAD_URL;
            $theFileExts = findexts($_FILES['logo']['name']);
        }
        $generatedFilename = $gennerateFileName . "." . $theFileExts;
        $file = $upload_path . $generatedFilename;

        sleep(1);
        if (move_uploaded_file($_FILES[$module]['tmp_name'], $file)) {
            echo $generatedFilename;
        }
        else {
            echo "error";
        }
        exit;
//TODO:croping image
//http://www.codeforest.net/upload-crop-and-resize-images-with-php
    }

    /**
     * Descrition :  
     * @author  
     */
    function userActivation($activation_code) {
        try {

            $query = $this->Signup_model->getUserByAcivationcode($activation_code);
            if (!sizeof($query)) {
                $data["message"] = lang('Acct_code_not_found');
                $data["isempty"] = "1";
                load_view($this, 'signup/signup_connected', $data);
                return;
            }
            $result = $query[0];
            $AciveStatus = $result["AciveStatus"];
            if ($AciveStatus == "active") {
                $this->session->set_userdata('user_id', $result["user_id"]);
                $this->session->set_userdata('company_id', $result["CompanyID"]);
                $this->session->set_userdata('requested_URL', '');
                $data["URL"] = '';
                $data["message"] = lang('user_activated');
                load_view($this, 'signup/signup_connect', $data);
                return;
            }

########## Add Company data ################
            $data_company = array();
            $data_company["Phone"] = $result["PhoneNumber"];
            $data_company["zip"] = $result["Zip"];
            $data_company["CountryID"] = $result["CountryID"];
            $data_company["CountryCallingCode"] = $result["PhoneCountryCode"];
            $data_company["CreatedDate"] = date('Y-m-d H:i:s');
            $data_company["LastModifiedDate"] = date('Y-m-d H:i:s');


            $CompanyID = $this->Company_Model->addCompnay($data_company);

            $update_arr["CompanyID"] = $CompanyID;


########## Add Company Lang data ################ 
            $data_company_lang = array();
            $data_company_lang["CompanyID"] = $CompanyID;
            $data_company_lang["LangCode"] = $result["LangCode"];
            $data_company_lang["Name"] = $result["Company"];
            $data_company_lang["Address1"] = $result["Address1"];
            $data_company_lang["Address2"] = $result["Address2"];
            $data_company_lang["CreatedDate"] = date('Y-m-d H:i:s');
            $data_company_lang["LastModifiedDate"] = date('Y-m-d H:i:s');


            $companyLangID = $this->Company_Model->addCompnayLang($data_company_lang);
            $update_arr["companyLangID"] = $companyLangID;

########## Add user data ################ 
            $data_user = array();
            $data_user["UserName"] = $result["UserName"];
            $data_user["Password"] = $result["Password"];
            $data_user["Salt"] = sha1($result["UserName"] . $this->config->item('encryption_key'));
            $data_user["Fname"] = $result["Fname"];
            $data_user["Lname"] = $result["Lname"];
            $data_user["AciveStatus"] = "active";
            $data_user["UserTypeID"] = "1";
            $data_user["CompanyID"] = $CompanyID;
            $data_user["CreatedDate"] = date('Y-m-d H:i:s');
            $data_user["LastModifiedDate"] = date('Y-m-d H:i:s');

            $user_id = $this->User_model->addUser($data_user);
            $update_arr["UserID"] = $user_id;


//add user next step and create cookie
// $this->Site_model->insertSignUpStep($user_id,3);
// $this->load->library('usercookie');
// $objCookie = new $this->usercookie();
// $objCookie->setEmail($data_user["UserName"]);
// $objCookie->setUserCookie();
########## Add Company Temp data ################
            $data_company_tmp = array();
            $data_company_tmp["Phone"] = $result["PhoneNumber"];
            $data_company_tmp["zip"] = $result["Zip"];
            $data_company_tmp["CountryID"] = $result["CountryID"];
            $data_company_tmp["CountryCallingCode"] = $result["PhoneCountryCode"];
            $data_company_tmp["CreatedDate"] = date('Y-m-d H:i:s');
            $data_company_tmp["LastModifiedDate"] = date('Y-m-d H:i:s');
            $data_company_tmp["TransactionID"] = $CompanyID;

            $tmp_CompanyID = $this->Company_Model->addTmpCompany($data_company_tmp);
            $update_arr["CompanyTmpID"] = $tmp_CompanyID;


########## Add Company Temp Lang data ################
            $data_company_tmp_lang = array();
            $data_company_tmp_lang["CompanyID"] = $tmp_CompanyID;
            $data_company_tmp_lang["LangCode"] = $result["LangCode"];
            $data_company_tmp_lang["Name"] = $result["Company"];
            $data_company_tmp_lang["Address1"] = $result["Address1"];
            $data_company_tmp_lang["Address2"] = $result["Address2"];
            $data_company_tmp_lang["CreatedDate"] = date('Y-m-d H:i:s');
            $data_company_tmp_lang["LastModifiedDate"] = date('Y-m-d H:i:s');
            $data_company_tmp_lang["Status"] = "1";
            $data_company_tmp_lang["TransactionID"] = $companyLangID;
            $TmpCompanyLangID = $this->Company_Model->addTmpCompanyLang($data_company_tmp_lang);

            $update_arr["TmpCompanyLangID"] = $TmpCompanyLangID;

            $this->session->set_userdata('user_id', $user_id);
            $this->session->set_userdata('company_id', $CompanyID);

            $this->session->set_userdata('requested_URL', '');
            $this->Signup_model->UpdateTmpAccountInfo($activation_code);
            $data["URL"] = '';
            $data["message"] = lang('user_activation_success');
            load_view($this, 'signup/signup_connect', $data);
            return;
        } catch (Exception $e) {
            echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
    }

    function send_activation_mail($activation_code, $contact_email, $first_name, $last_name) {
        $config['mailtype'] = "html";
        $this->email->initialize($config);
        $subject = $this->config->item('website_name') . " " . lang('activation');
        $body = "";
        $bcc = $this->config->item('email_archive');

        $activation_url = base_url() . "signup/userActivation/" . $activation_code;
        $activatio_msg = "Hi " . $first_name . " " . $last_name;
        $activatio_msg .= "<p><b><u>" . $this->config->item('website_name') . " user activation and confirmation details</u></b></p>";
        $activatio_msg .="<p>Welcome to <a href=" . $this->config->item('website_url') . ">" . $this->config->item('website_name') . "</a> Your registration was successful but your account still needs to be";
        $activatio_msg .="activated by you. ";
        $activatio_msg .="Please read all of the following important information. ";
        $activatio_msg .="<p>You has requested to register with " . $this->config->item('website_name') . " with this email (" . $contact_email . "), ";
        $activatio_msg .= "If you tried for register with us please keep this information safe.";
        $activatio_msg .= "To activation of the account please follow the instructions; ";
        $activatio_msg .="<p>To activate your membership,";
        $activatio_msg .="<p>click here:";
        $activatio_msg .="<p><a href=" . $activation_url . ">Register me as a member of " . $this->config->item('website_name') . "</a></p>";
        $activatio_msg .="<p>if above given link does not perform its task, please copy below link and paste in your browser window (in the address bar)";
        $activatio_msg .="<br><a href=" . $activation_url . ">" . $activation_url . "</a>";
        $activatio_msg .="<p>Thank you for registering with us";
        $activatio_msg .="<br>" . $this->config->item('website_name');
//email_notification( $subject, $body, $contact_email, $from=null, $from_name=null, $bcc); 
        email_notification($subject, $activatio_msg, $contact_email, $from = null, $from_name = null, $bcc);
    }

    /**
     * Check user email is exists in the sign up process
     */
    function validateEmail() {
        $valid = 'true';
        $email = mysql_real_escape_string($_POST['acctxt_email']);
        $tmp_account_info = $this->Signup_model->getByUserName($email);
        if (sizeof($tmp_account_info) > 0) {
            $valid = 'false';
        }
        echo $valid;
    }

    function rmv() {
        $user = mysql_real_escape_string($_GET['user']);
        $this->Signup_model->removeUser($user);
        echo 'Madam, user removed!!!';
        exit();
    }

    function redirectGuest() {
        $CI = get_instance();
        if ($CI->router->fetch_class() == 'signup' && !$this->Cookie_Model->validateUser()) {
            redirect('signup/new_account', 'refresh');
        }
    }

    function sendcnt_form() {
        $config['mailtype'] = "html";
        $this->email->initialize($config);
        $subject = $this->config->item('website_name') . " " . lang('contact_form');
        $body = "";
        $bcc = $this->config->item('email_archive');

        $email = isset($_POST['cnt_email']) ? $_POST['email'] : '';
        $msgHTML = isset($_POST['cnt_msg']) ? $_POST['cnt_msg'] : '';
        $msgHTML .= '<p> User Email :' . $email;
        email_contact($subject, $msgHTML, $from = null, $from_name = null, $bcc);
    }

}

?>
