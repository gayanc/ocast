<?php

class search extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model("Country_Model");
        $this->load->model("category_model");
        $this->load->model("demographic_model");
        $this->load->model("device_model");
        $this->load->model("type_model");
        $this->load->model("search_model");
    }

    function find() {
        load_view($this, 'front/searach');
        //$Language_arr = Country_ModelgetAll( $this->session->userdata('lang'));
        // if($Language_arr) 
    }

    function test() {
        $result = $this->search_model->detault_search();
        print_r($result);
    }

    function get_default_search($data = null) {
        $result = $this->search_model->main_search($data);
       // $result = $this->search_model->detault_search($data); 
        
        $out_put = array();
        if (sizeof($result) > 0) {
            foreach ($result as $row) {
                $SiteID = $row["SiteID"];
                $URL = $row["URL"];
                $Logo = $row["Logo"];
                $PublishedDate = $row["PublishedDate"];
                $SiteName = $row["SiteName"];
                $CompanyName = $row["CompanyName"];
                $UniqueVisits = $row["UniqueVisits"];
                $PageViews = $row["PageViews"];
                $Visits = $row["Visits"];
                $back_date = date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') - 10, date('Y')));
                $is_new = 0;
                if ($PublishedDate > $back_date) {

                    $is_new = 1;
                }

                if (!$Logo) {
                    $Logo = DEFAUL_COMPANY_LOGO;
                }

                $p_v = @round($PageViews / $Visits, 2);
                $u_v = @round($UniqueVisits / $Visits, 2);
                $percentage = "100%";
                $company = array();
                $company["logo"] = $Logo;
                $company["name"] = $CompanyName;
                $company["is_new"] = $is_new;
                $company["sitename"] = $SiteName;

                $data = array('company' => $company,
                  'unique_visits' => $UniqueVisits,
                  'visites' => $Visits,
                  'pageviews' => $PageViews,
                  'p_v' => $p_v, 'u_v' => $u_v,
                  'percentage' => $percentage, 'site_id' => $SiteID);
                $out_put[] = $data;
            }
        }

        return $out_put;
    }

    function search_result() {
        
    }

    function create_table() {
        $data[] = '<div class="container result">
        <!-- Container area -->
           <div class="col-lg-12 ">
           <div class="col-lg-5 pull-right padno">
               <div class="btn-group catbox" id="catbox_month">
                  <div data-toggle="dropdown">
                      <div class="">month <i class="icon-caret-down"></i></div>                            
                      <div class="clearfix"></div>
                  </div>
                   <ul style="min-width:100%;overflow:scroll;overflow-x:hidden;height:300px" class="dropdown-menu">
                        <li><a idval="1" href="#">January</a></li>
                        <li><a idval="2" href="#">February</a></li>
                        <li><a idval="3" href="#">march</a></li>
                        <li><a idval="4" href="#">april</a></li>
                        <li><a idval="5" href="#">may</a></li>
                        <li><a idval="6" href="#">june</a></li>
                        <li><a idval="7" href="#">july</a></li>
                        <li><a idval="8" href="#">august</a></li>
                        <li><a idval="9" href="#">september</a></li>
                        <li><a idval="10" href="#">october</a></li>
                        <li><a idval="11" href="#">november</a></li>
                        <li><a idval="12" href="#">december</a></li>
                  </ul>
              </div>

                  <div class="btn-group catbox" id="catbox_week">
                  <div data-toggle="dropdown">
                      <div class="">week <i class="icon-caret-down"></i></div>                            
                      <div class="clearfix"></div>
                  </div>
                   <ul style="min-width:100%;overflow:scroll;overflow-x:hidden;height:300px" class="dropdown-menu">
                        <li><a idval="1" href="#">1</a></li>
                        <li><a idval="2" href="#">2</a></li>
                        <li><a idval="3" href="#">3</a></li>
                        <li><a idval="4" href="#">4</a></li>
                        <li><a idval="5" href="#">5</a></li>
                        <li><a idval="6" href="#">6</a></li>
                        <li><a idval="7" href="#">7</a></li>
                        <li><a idval="8" href="#">8</a></li>
                        <li><a idval="9" href="#">9</a></li>
                        <li><a idval="10" href="#">10</a></li>
                        <li><a idval="11" href="#">11</a></li>
                        <li><a idval="12" href="#">12</a></li>
                  </ul>
              </div>
                <div class="btn-group catbox" id="catbox_day">
                  <div data-toggle="dropdown">
                      <div class="">day <i class="icon-caret-down"></i></div>                            
                      <div class="clearfix"></div>
                  </div>
                   <ul style="min-width:100%;overflow:scroll;overflow-x:hidden;height:300px" class="dropdown-menu">
                        <li><a idval="1" href="#">1</a></li>
                        <li><a idval="2" href="#">2</a></li>
                        <li><a idval="3" href="#">3</a></li>
                        <li><a idval="4" href="#">4</a></li>
                        <li><a idval="5" href="#">5</a></li>
                        <li><a idval="6" href="#">6</a></li>
                        <li><a idval="7" href="#">7</a></li>
                        <li><a idval="8" href="#">8</a></li>
                        <li><a idval="9" href="#">9</a></li>
                        <li><a idval="10" href="#">10</a></li>
                        <li><a idval="11" href="#">11</a></li>
                        <li><a idval="12" href="#">12</a></li>
                        <li><a idval="13" href="#">13</a></li>
                        <li><a idval="14" href="#">14</a></li>
                        <li><a idval="15" href="#">15</a></li>
                        <li><a idval="16" href="#">16</a></li>
                        <li><a idval="17" href="#">17</a></li>
                        <li><a idval="18" href="#">18</a></li>
                        <li><a idval="19" href="#">19</a></li>
                        <li><a idval="20" href="#">20</a></li>
                        <li><a idval="21" href="#">21</a></li>
                        <li><a idval="22" href="#">22</a></li>
                        <li><a idval="23" href="#">23</a></li>
                        <li><a idval="24" href="#">24</a></li>
                        <li><a idval="25" href="#">25</a></li>
                        <li><a idval="26" href="#">26</a></li>
                        <li><a idval="27" href="#">27</a></li>
                        <li><a idval="28" href="#">28</a></li>
                        <li><a idval="29" href="#">29</a></li>
                        <li><a idval="30" href="#">30</a></li>
                        <li><a idval="31" href="#">31</a></li>
                  </ul>
              </div>
            </div>
           </div>
           <div class="clearfix"></div>
          <div class="col-lg-12 ">
                    <div class="reponsivetbl martp10">
                      <table class="tablesorter">
                        <thead> 
                          <tr>
                            <th keyid="1"  class="header">Comany name</th>
                            <th keyid="2" class="header">Unique</th>
                            <th keyid="3"  class="header">Visits</th>
                            <th keyid="4" class="header">Page Views</th>
                            <th keyid="5" class="header">P/V</th>
                            <th keyid="6" class="header">U/V</th>
                            <th keyid="7" class="header">%</th>
                            <th keyid="8"  ><i class="icon-heart"></i></th>
                            <th keyid="9" ><img src="' . base_url('assets/images/icnsarow_03.png') . '"></th>
                          </tr>
                        </thead>
                        <tbody id="tbody_result">
                        
                    
                        </tbody>
                      </table>
                    </div>
          </div>

          <!-- Container area end-->
        <div class="clearfix"></div>
      </div> <div id="mo"></div>'; 
        
       // $data["data"] = $this->get_default_search();
        echo json_encode($data);
    }

    function get() {
//        $country_list = "";
//        $category_list = "";
//        $gender_list = "";
//        $device_list = "";
//        $type_list = "";
//        $age_from = "";
//        $age_to = "";
//        $sortingkey = "";
//        $rowCount = "";
//        $sortingOrder = "";
//
//        $country_list = (isset($_POST["country_list"]) == TRUE) ? $_POST["country_list"] : '';
//        $category_list = (isset($_POST["category_list"]) == TRUE) ? $_POST["category_list"] : '';
//        $device_list = (isset($_POST["device_list"]) == TRUE) ? $_POST["device_list"] : '';
//        $type_list = (isset($_POST["type_list"]) == TRUE) ? $_POST["type_list"] : '';
//        $age_from = (isset($_POST["age_from"]) == TRUE) ? $_POST["age_from"] : '';
//        $age_to = (isset($_POST["age_to"]) == TRUE) ? $_POST["age_to"] : '';
//        $rowCount = (isset($_POST["rowCount"]) == TRUE) ? $_POST["rowCount"] : '';
//        $sortingOrder = (isset($_POST["sortingOrder"]) == TRUE) ? $_POST["sortingOrder"] : '';

        $sortingkey = "";
        $gender_list = "";
        $sortingkey = (isset($_POST["sortingkey"]) == TRUE) ? $_POST["sortingkey"] : '';
        $gender_list = (isset($_POST["gender_list"]) == TRUE) ? $_POST["gender_list"] : '';

        if (isset($_POST["sortingkey"])) {

            if ($sortingkey == 1) {
                $_POST["sortingkey"] = 'trn_site.SiteName';
            }
            else if ($sortingkey == 2) {
                $_POST["sortingkey"] = 'UniqueVisits';
            }
            else if ($sortingkey == 3) {
                $_POST["sortingkey"] = 'Visits';
            }
            else if ($sortingkey == 4) {
                $_POST["sortingkey"] = 'PageViews';
            }
            else if ($sortingkey == 5) {
                $_POST["sortingkey"] = 'PageViews';
            }
            else if ($sortingkey == 6) {
                $_POST["sortingkey"] = 'UniqueVisits';
            }
            else if ($sortingkey == 7) {
                $_POST["sortingkey"] = 'UniqueVisits';
            }
        } 
        if ((isset($_POST["gender_list"]) == TRUE)) {
            if (in_array("male", $gender_list) && in_array("female", $gender_list)) {
                $_POST["gender_list"] = "MalePercentage<=60 && MalePercentage =>50 ";
            }
            else if (in_array("female", $gender_list)) {
                $_POST["gender_list"] = "MalePercentage<40";
            }
            else {
                $_POST["gender_list"] = "MalePercentage>60";
            }
        }


  $data = $this->get_default_search($_POST);  
   echo json_encode($data); 
   return 0;

        $logo = "logotab.jpg";
        $name = "Swecklockers";
        $is_new = "1";
        $company = array();
        $company["logo"] = $logo;
        $company["name"] = $name;
        $company["is_new"] = $is_new;
        //$row = array('company')

        $unique_visits = "120";
        $visites = "550";
        $pageviews = "1500";
        $p_v = 100;
        $u_v = 120;
        $percentage = 10 %
        $favorit = "100";
        $summery = "yes";



         $data = array('company' => $company, 'unique_visits' => $unique_visits, 'visites' => $visites, 'pageviews' => $pageviews, 'p_v' => $p_v, 'u_v' => $u_v, 'percentage' => $percentage, 'site_id' => 1);
         $out_put[] = $data;
         
        $data = array('company' => $company, 'unique_visits' => $unique_visits, 'visites' => $visites, 'pageviews' => $pageviews, 'p_v' => $p_v, 'u_v' => $u_v, 'percentage' => $percentage, 'site_id' => 1);
         $out_put[] = $data;  
        echo json_encode($out_put);
        
    }

    function get_analytics_by_week() {

        $site_list = $_POST["site_list"];

        foreach ($site_list as $site_id) {
            
        }
        $unique_visits = "120";
        $visites = "550";
        $pageviews = "1500";
        $p_v = 100;
        $u_v = 120;
        $percentage = '10 %';
        $site_id = 1;
        $data = array('unique_visits' => $unique_visits, 'visites' => $visites, 'pageviews' => $pageviews, 'p_v' => $p_v, 'u_v' => $u_v, 'percentage' => $percentage, 'site_id' => $site_id);
        $out_put[] = $data;

        $unique_visits = "1000";
        $visites = "2000";
        $pageviews = "3500";
        $p_v = 5;
        $u_v = 10;
        $percentage = '20 %';
        $site_id = 2;
        $data = array('unique_visits' => $unique_visits, 'visites' => $visites, 'pageviews' => $pageviews, 'p_v' => $p_v, 'u_v' => $u_v, 'percentage' => $percentage, 'site_id' => $site_id);
        $out_put[] = $data;

        $unique_visits = "2000";
        $visites = "3000";
        $pageviews = "4500";
        $p_v = 15;
        $u_v = 25;
        $percentage = '30 %';
        $site_id = 3;
        $data = array('unique_visits' => $unique_visits, 'visites' => $visites, 'pageviews' => $pageviews, 'p_v' => $p_v, 'u_v' => $u_v, 'percentage' => $percentage, 'site_id' => $site_id);
        $out_put[] = $data;
        echo json_encode($out_put);
    }

}

?>