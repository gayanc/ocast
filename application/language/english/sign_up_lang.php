<?php 

$lang['lcode'] = "en";
$lang['join_ocast'] = 'Join Ocast';
$lang['acc_information'] = 'Account information';


//process steps
$lang['stp_acc_info'] = 'Account info';
$lang['stp_connect'] = 'Connect';
$lang['stp_site_info'] = 'Site information';
$lang['stp_product_format'] = 'Formats & products';
$lang['stp_case'] = 'Case';
$lang['stp_preview'] = 'Preview & submit';
$lang['stp_skip_msg1'] = 'I\'\ll do this later! ';
$lang['stp_skip_msg2'] = 'Skip this step for now';


//step 01
$lang['stp_1_desc'] = ' Congrats! You are one step closer to having your site being Ocasted.Sit ämet consectetur adipisicing elit sed. Do eiusmod tempör incididunt. Ut laböre et. Dolore mägna äliquä ligulå nisl ullamcörper sem curåbitur. ';
$lang['stp_1_acc_info'] = 'Account information ';
$lang['stp_1_tip_acc_info'] = 'Lörem ipsum dolör. Sit ämet consectetur adipisicing elit sed. Do eiusmod tempör incididunt. Ut laböre et. Dolore mägna äliquä ligulå nisl ullamcörper sem curåbitur.äliquä ligulå nisl ullamcörper sem curåbitur.äliquä ligulå nisl ullamcörper sem curåbitur.';
$lang['stp_1_activation_success'] = 'Thank you! Now check your email to  complete the registration';
$lang['stp_1_company'] = 'Company';
$lang['stp_1_phnumber'] = 'Phone number';
$lang['stp_1_addr1'] = 'Address 1';
$lang['stp_1_addr2'] = 'Address 2';
$lang['stp_1_email_hint1'] = 'Email';
$lang['stp_1_email_hint2'] = ' (Verification will be sent to this email)';
$lang['stp_1_zip'] = 'Zip';
$lang['stp_1_city'] = 'City';
$lang['stp_1_country'] = 'Country';
$lang['stp_1_termncon'] = 'Terms and conditions';
$lang['stp_1_iagree'] = 'I agree the';
$lang['stp_1_sdbar_title'] = 'lorem information'; 
$lang['stp_1_sdbar_desc1'] = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis'; 
$lang['stp_1_sdbar_desc2'] = 'parturient montes, nascetur';
$lang['stp_1_sdbar_desc3'] = 'ridiculus mus. Donec quam felis, ultricies nec.';
$lang['stp_1_first_name'] = 'First name';
$lang['stp_1_last_name'] = 'Last name';
$lang['stp_1_password'] = 'Choose password';
$lang['stp_1_cpassword'] = 'Confirm password';
$lang['stp_1_company_info'] = 'Company information ';

//step 02 google connect
$lang['stp_2_connect_yrsite'] = 'CONNECT YOUR SITE';
$lang['stp_2_connect'] = 'Connect';
$lang['stp_2_connect_site'] = 'CONNECT YOUR SITE';
$lang['stp_2_connect_ttip'] = 'Lörem ipsum dolör. Sit ämet consectetur adipisicing elit sed. Do eiusmod tempör incididunt. Ut laböre et. Dolore mägna äliquä ligulå nisl ullamcörper sem curåbitur.äliquä ligulå nisl ullamcörper sem curåbitur.äliquä ligulå nisl ullamcörper sem curåbitur.';
$lang['stp_2_connect_ganalytic'] = 'connect to google analytics';
$lang['stp_2_google_term1'] = 'i agree Lörem ipsum dolör. Sit ämet consectetur adipisicing elit sed. Do eiusmod tempör incididunt. Ut laböre et. Dolore mägna äliquä ligulå nisl ullamcörper.'; 
$lang['stp_2_google_term2'] = 'Terms Conditions';
$lang['stp_2_site_name'] = 'Site name';
$lang['stp_2_connect_yrsite_ttip'] = 'Lörem ipsum dolör. Sit ämet consectetur adipisicing elit sed. Do eiusmod tempör incididunt. Ut laböre et. Dolore mägna äliquä ligulå nisl ullamcörper sem curåbitur.äliquä ligulå nisl ullamcörper sem curåbitur.äliquä ligulå nisl ullamcörper sem curåbitur';
$lang['stp_2_gooogle_agree'] = 'i agree Lörem ipsum dolör. Sit ämet consectetur adipisicing elit sed. Do eiusmod tempör incididunt. Ut laböre et. Dolore mägna äliquä ligulå nisl ullamcörper.'; 
$lang['stp_2_gooogle_agree2'] = 'Terms Conditions';

//step 03 - site information
$lang['stp_3_site_info'] = 'Site information';
$lang['stp_3_logo_type'] = 'Logotype';
$lang['stp_3_logo_hint'] = 'Upload your logo to appear in the listing and on your personal page. ';
$lang['stp_3_add_photo'] = 'ADD PHOTO';
$lang['stp_3_pcrop_hint'] = 'Images will be automatically cropped to fit 90x70 pixels';
$lang['stp_3_description'] = 'Add description';
$lang['stp_3_description_tip'] = 'Description may consist of maximum 1 000 characters. It will appear in your listing as well as on your personal page. Only the first 100 characters or so will be visible in your listing.';
$lang['stp_3_cntct_person'] = 'contact persons';
$lang['stp_3_first_name'] = 'First name'; 
$lang['stp_3_sirname'] = 'Surname';
$lang['stp_3_title'] = 'Title';
$lang['stp_3_email'] = 'E-mail';
$lang['stp_3_tel_no'] = 'Telephone';
$lang['stp_3_photo_hint'] = 'Photos are displayed on your contact page.';
$lang['stp_3_add_contact'] = 'ADD ANOTHER CONTACT PERSON';
$lang['stp_3_choose_cnt'] = 'choose countries';
$lang['stp_3_cntry_desc'] = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.'; 
$lang['stp_3_main_cntry'] = 'Main Country';
$lang['stp_3_admore_cnty'] = 'Add more countries';
$lang['stp_3_selected'] = 'SELECTED';
$lang['stp_3_chose_cat'] = 'choose categories';
$lang['stp_3_logo_hint'] = 'Add categories';
$lang['stp_3_demograpics'] = 'demographics';
$lang['stp_3_demog_desc'] = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.';
$lang['stp_3_avg_age'] = 'Select average age';
$lang['stp_3_yrs'] = 'years';
$lang['stp_3_set_demog'] = 'Set demographics ';
$lang['stp_3_women'] = 'Women';
$lang['stp_3_about_audience'] = 'about the audience';
$lang['stp_3_audience_desc'] = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.';
$lang['stp_3_quick_fact'] = 'quick facts';
$lang['stp_3_quick_fact_hint'] = 'will appear as bullet points. Maximum 3 facts / 50 letters each.';
$lang['stp_3_testimonial'] = 'Testimonials';
$lang['stp_3_testimonial_desc'] = 'Testimonials will appear as bullet points. Maximum 3 quotes / 50 letters each.';
$lang['stp_3_quote'] = 'Quote';
$lang['stp_3_who_said'] = 'Who said this?';
$lang['stp_3_add_demoginfo'] = 'add demographic info';
$lang['stp_3_demog_hint'] = 'Lorem ipsum dolor sit amet lipsum dolores.';
$lang['stp_3_answer'] = 'Answer';
$lang['stp_3_add_demog_answr'] = 'ADD ANOTHER ANSWER (MAX 5)';
$lang['stp_3_headline'] = 'Headline';
$lang['stp_3_add_module'] = 'ADD ANOTHER MODULE';
$lang['stp_3_use_same_contact'] ='Use same as account contact';
$lang['stp_3_quick_factholder'] = 'E.g. We are the biggest newspaper in Norway';
$lang['stp_3_demog_eg'] = 'E.g. Education';

//step 04 - product formats
$lang['stp_4_add_yrsite'] = 'Add your site';
$lang['stp_4_upload_formats'] = 'Upload FORMATS & PRODUCTS';
$lang['stp_4_prdctformat_desc'] = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque';
$lang['stp_4_upload_err'] = 'Error Uploading!!! Only JPG, PNG or GIF files are allowed';
$lang['stp_4_addformat'] = 'ADD FORMAT';
$lang['stp_4_cancel'] = 'Cancel';
$lang['stp_4_saveformat'] = 'SAVE FORMAT';
$lang['stp_4_removeformat'] = 'Remove';

//step 05 - cases
$lang['stp_5_add_yrsite'] = 'Add your site';
$lang['stp_5_add_case'] =  'Add case';
$lang['stp_5_add_case_desc'] =  'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque'; 
$lang['stp_5_add_image'] =  'ADD IMAGE';
$lang['stp_5_add_img_desc'] =  'Images are displayed between the title and description.';
$lang['stp_5_add_another_case'] = 'ADD ANOTHER CASE';
$lang['stp_5_go_back'] = 'GO BACK';
$lang['stp_5_case_title_eg'] =  'Case title';

//step 06 - preview submit
$lang['stp_6_add_yrsite'] = 'Add your site';
$lang['stp_6_new_tool_tip'] = 'Lörem ipsum dolör. Sit ämet consectetur sem curåbitur.';
$lang['stp_6_preview_desc'] = 'Here is a preview of your page';
$lang['stp_6_submit'] = 'SUBMIT';
$lang['stp_6_gobk_edit'] = 'GO BACK AND EDIT';
$lang['stp_6_categories'] = 'CATEGORIES';
$lang['stp_6_add_2_favo'] = 'Add to favourites';
$lang['stp_6_site_toltip'] = 'Lörem ipsum dolör. Sit ämet consectetur adipisicing elit sed. Do eiusmod tempör incididunt. Ut laböre et. Dolore mägna äliquä ligulå nisl ullamcörper sem curåbitur.';
$lang['stp_6_contct_sale'] = 'CONTACT SALES';
$lang['stp_6_case'] = 'CASE';
$lang['stp_6_mobile'] = 'MOBILE';
$lang['stp_6_desktop'] = 'DESKTOP';
$lang['stp_6_stats'] = 'Statistics';
$lang['stp_6_chose_time'] = 'Choose time period';
$lang['stp_6_unique'] = 'Unique';
$lang['stp_6_visit'] = 'Visits';
$lang['stp_6_pviews'] = 'Page Views';
$lang['stp_6_pv'] = 'P/V';
$lang['stp_6_uv'] = 'U/V';
$lang['stp_6_site_toltip'] = 'Lörem ipsum dolör.';
$lang['stp_6_sml_desc'] = 'Nike Oz';
$lang['stp_6_unique_browser'] = 'Unique browsers ';
$lang['stp_6_age'] = 'AGE';
$lang['stp_6_gender'] = 'GENDER';
$lang['stp_6_women'] = 'WOMEN';
$lang['stp_6_men'] = 'MEN';
$lang['stp_6_avgage'] = 'AVERAGE AGE';
$lang['stp_6_abt_audience'] = 'ABOUT THE AUDIENE';
$lang['stp_6_abt_quickfact'] = 'QUICK FACTS';
$lang['stp_6_case'] = 'CASE';
$lang['stp_6_formats_prdct'] = 'formats &amp; products';
$lang['stp_6_format'] = 'Format';
$lang['stp_6_pricetype'] = 'Pricetype';
$lang['stp_6_price'] = 'Price';
$lang['stp_6_allcases'] =  'All cases';
$lang['stp_6_contact'] =  'Contact';
$lang['stp_6_people'] = 'People';
$lang['stp_6_more'] = 'more';

$lang['stp_6_cotact_form'] = 'contact form';
$lang['stp_6_email_sucess'] = 'Email Sent Successfully';
$lang['stp_6_your_email'] = 'Your E-mail';
$lang['stp_6_question'] = 'Questions';
$lang['stp_6_send'] = 'SEND';




$lang['save_next'] = 'SAVE & NEXT';
$lang['go_back'] = 'GO BACK';










?>