<?php

$lang['lcode'] = "en";
$lang['user_activated'] = "You account already activated. Please proceed to connect google.";
$lang['user_activation_success'] = "Congratulations, <br> Your registration has been activated successfully";
$lang["activation"] = "Activation";
$lang["Admin"] = "Admin";
$lang["reset_password"] = "Account password reset request";
$lang["Account_information"] = "Account information";
$lang["No_sites_in_google"] = "There are no analytict sites in your google account.";
$lang["Acct_code_not_found"] = "The activation code not found";
$lang['Site_already_added'] = "You have already added the site";
$lang['Pls_select_website'] = "Please select your website";


// Login
$lang['login_account_not_found'] = 'We couldn\'t found any account related to your e-mail..!';
$lang['login_wrong_user_pass'] = 'The username or password is incorrect';
$lang['login_acc_not_approved'] = 'Your account still not approved..!';
$lang['login_acc_not_activated'] = 'Your account still not activated..!';
$lang['login_error'] = 'Error!';
$lang['login_success'] = 'Success!';

$lang['login_retreve_login'] = 'Retrieve Login Information';
$lang['login_new_password'] = 'Enter your new login details.';

$lang['retreve_new_password'] = 'Enter you email address and we will send you  an email with a link to reset your password.';
$lang['retreve_denied'] = 'Your password reset URL expired or you don\'t have permission to reset password. Click below link to return password reset request page.';
$lang['retreve_success'] = 'Your account has been successfully updated. Click below link to login.';
$lang['retreve_mail_sent'] = ' An email has been sent to:';

//contact form
$lang['contact_form'] = 'An email has been sent to:';


?>
   