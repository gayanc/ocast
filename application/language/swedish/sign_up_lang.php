<?php 

$lang['lcode'] = "sv";
$lang['join_ocast'] = '############';
$lang['acc_information'] = '#############';


//process steps
$lang['stp_acc_info'] = '######';
$lang['stp_connect'] = '#######';
$lang['stp_site_info'] = '#######';
$lang['stp_product_format'] = '#######';
$lang['stp_case'] = '######';
$lang['stp_preview'] = '#######';
$lang['stp_skip_msg1'] = '#########';
$lang['stp_skip_msg2'] = '###########';


//step 01
$lang['stp_1_desc'] = ' ####################';
$lang['stp_1_acc_info'] = '############';
$lang['stp_1_tip_acc_info'] = '#################';
$lang['stp_1_company'] = '#########';
$lang['stp_1_phnumber'] = '######';
$lang['stp_1_addr1'] = '##########';
$lang['stp_1_addr2'] = '###########';
$lang['stp_1_email_hint1'] = '########';
$lang['stp_1_email_hint2'] = '###########';
$lang['stp_1_zip'] = '##########';
$lang['stp_1_city'] = '#########';
$lang['stp_1_country'] = '##########';
$lang['stp_1_termncon'] = '############';
$lang['stp_1_iagree'] = '##########';
$lang['stp_1_sdbar_title'] = '##########'; 
$lang['stp_1_sdbar_desc1'] = '###############'; 
$lang['stp_1_sdbar_desc2'] = '#######';
$lang['stp_1_sdbar_desc3'] = '############';
$lang['stp_1_first_name'] = '############';
$lang['stp_1_last_name'] = '#############';
$lang['stp_1_password'] = '########';
$lang['stp_1_cpassword'] = '###########';
$lang['stp_1_company_info'] = '#######';

//step 02 google connect
$lang['stp_2_connect_yrsite'] = '#######';
$lang['stp_2_connect'] = '#########';
$lang['stp_2_connect_site'] = '####';
$lang['stp_2_connect_ttip'] = '############';
$lang['stp_2_connect_ganalytic'] = '##########';
$lang['stp_2_google_term1'] = '#############'; 
$lang['stp_2_google_term2'] = '##########';
$lang['stp_2_site_name'] = '###########';
$lang['stp_2_connect_yrsite_ttip'] = '#########';
$lang['stp_2_gooogle_agree'] = '##############'; 
$lang['stp_2_gooogle_agree2'] = '#########';

//step 03 - site information
$lang['stp_3_site_info'] = '#####';
$lang['stp_3_logo_type'] = '##########';
$lang['stp_3_logo_hint'] = '#########';
$lang['stp_3_add_photo'] = '##########';
$lang['stp_3_pcrop_hint'] = '###########';
$lang['stp_3_description'] = '#################';
$lang['stp_3_description_tip'] = '################';
$lang['stp_3_cntct_person'] = '#######';
$lang['stp_3_first_name'] = '############'; 
$lang['stp_3_sirname'] = '############';
$lang['stp_3_title'] = '##############';
$lang['stp_3_email'] = '#######';
$lang['stp_3_tel_no'] = '###########';
$lang['stp_3_photo_hint'] = '##############';
$lang['stp_3_add_contact'] = '############';
$lang['stp_3_choose_cnt'] = '#############';
$lang['stp_3_cntry_desc'] = '###############'; 
$lang['stp_3_main_cntry'] = '##########';
$lang['stp_3_admore_cnty'] = '#############';
$lang['stp_3_selected'] = '###############';
$lang['stp_3_chose_cat'] = '##############';
$lang['stp_3_logo_hint'] = '##########';
$lang['stp_3_demograpics'] = '###################';
$lang['stp_3_demog_desc'] = '################';
$lang['stp_3_avg_age'] = '###########';
$lang['stp_3_yrs'] = '############';
$lang['stp_3_set_demog'] = '#########';
$lang['stp_3_women'] = '#########';
$lang['stp_3_about_audience'] = '#########';
$lang['stp_3_audience_desc'] = '#########';
$lang['stp_3_quick_fact'] = '########';
$lang['stp_3_quick_fact_hint'] = '#######';
$lang['stp_3_testimonial'] = '########';
$lang['stp_3_testimonial_desc'] = '###########';
$lang['stp_3_quote'] = '########';
$lang['stp_3_who_said'] = '#######';
$lang['stp_3_add_demoginfo'] = '#########';
$lang['stp_3_demog_hint'] = '#####';
$lang['stp_3_answer'] = '##########';
$lang['stp_3_add_demog_answr'] = '#######';
$lang['stp_3_headline'] = '########';
$lang['stp_3_add_module'] = '######';
$lang['stp_3_use_same_contact'] ='##############';
$lang['stp_3_quick_factholder'] = '###################';
$lang['stp_3_demog_eg'] = '##########';

//step 04 - product formats
$lang['stp_4_add_yrsite'] = '#######';
$lang['stp_4_upload_formats'] = '#######';
$lang['stp_4_prdctformat_desc'] = '###########';
$lang['stp_4_upload_err'] = '#####';
$lang['stp_4_addformat'] = '########';
$lang['stp_4_cancel'] = '#######';
$lang['stp_4_saveformat'] = '#######';
$lang['stp_4_removeformat'] = '#######';

//step 05 - cases
$lang['stp_5_add_yrsite'] = '####';
$lang['stp_5_add_case'] =  '######';
$lang['stp_5_add_case_desc'] =  '#############'; 
$lang['stp_5_add_image'] =  '#####';
$lang['stp_5_add_img_desc'] =  '#############';
$lang['stp_5_add_another_case'] = '#########';
$lang['stp_5_go_back'] = '###########';
$lang['stp_5_case_title_eg'] =  '#########';


//step 06 - preview submit
$lang['stp_6_add_yrsite'] = '##########';
$lang['stp_6_new_tool_tip'] = '###########';
$lang['stp_6_preview_desc'] = '######';
$lang['stp_6_submit'] = '#############';
$lang['stp_6_gobk_edit'] = '########';
$lang['stp_6_categories'] = '######';
$lang['stp_6_add_2_favo'] = '##########';
$lang['stp_6_site_toltip'] = '###########';
$lang['stp_6_contct_sale'] = '##########';
$lang['stp_6_case'] = '#############';
$lang['stp_6_mobile'] = '###########';
$lang['stp_6_desktop'] = '##########';
$lang['stp_6_stats'] = '###############';
$lang['stp_6_chose_time'] = '#############';
$lang['stp_6_unique'] = '#############';
$lang['stp_6_visit'] = '#########';
$lang['stp_6_pviews'] = '############';
$lang['stp_6_pv'] = '############';
$lang['stp_6_uv'] = '#########';
$lang['stp_6_site_toltip'] = '########';
$lang['stp_6_sml_desc'] = '############';
$lang['stp_6_unique_browser'] = '############# ';
$lang['stp_6_age'] = '############';
$lang['stp_6_gender'] = '############';
$lang['stp_6_women'] = '#############';
$lang['stp_6_men'] = '#####';
$lang['stp_6_avgage'] = '##############';
$lang['stp_6_abt_audience'] = '########';
$lang['stp_6_abt_quickfact'] = '#############';
$lang['stp_6_case'] = '#####';
$lang['stp_6_formats_prdct'] = '#########';
$lang['stp_6_format'] = '#########';
$lang['stp_6_pricetype'] = '###########';
$lang['stp_6_price'] = '########';
$lang['stp_6_allcases'] =  '###########';
$lang['stp_6_contact'] =  '#################';
$lang['stp_6_people'] = '########';
$lang['stp_6_more'] = '#####';


$lang['stp_6_cotact_form'] = '###########';
$lang['stp_6_email_sucess'] = '###########';
$lang['stp_6_your_email'] = '#############';
$lang['stp_6_question'] = '############';
$lang['stp_6_send'] = '###########';


$lang['save_next'] = '##########';
$lang['go_back'] = '##########';


?>