<div id="outer-wrap">
<div id="inner-wrap">

<div class="topheaderbar">
    <div class="container" style="margin-top:0">
     <div class="col-lg-12" style="position:inherit">
        <header id="top" role="banner">
            <div class="block">
                <h1 class="block-title"><a href="<?php echo base_url();?>"><img src="<?php echo base_url('assets/images/logo.png')?>"></a></h1>
                <a class="nav-btn" id="nav-open-btn" href="#nav">Book Navigation</a>
                <div class="searchapnd"></div>
            </div>
        </header>

        <nav id="nav" role="navigation">
            <div class="block">
                <h2 class="block-title navtitl"><img src="<?php echo base_url('assets/images/logo.png')?>"></h2>
                    <div class="appn">
                    <ul class="nav navbar-nav" id="filterapnd">
                      <li class="filtrlink" ><a href="#">Filter <div><i class="icon-caret-right"></i></div></a></li>
                    </ul>
                    </div>
                    <ul class="nav navbar-nav favritmar">
                      <li><a href="#">Favorites  <i  class="icon-heart icnhrt"></i> </a></li>
                    </ul>
                     <div class="col-lg-3 em_div">
                       <form class="navbar-form custmserch" action="">
                          <input type="text" class="form-control borderradius navvinputheight" placeholder="Search">
                      </form>
                     </div>
                     <ul class="nav navbar-nav pull-left">
                        <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-angle-down icnangldwn"></i> Language </a>
                          <ul class="dropdown-menu" id="dropnav">
                            <li><a href="#">Sweden</a>
                            </li>
                            <li><a href="#">USA</a>
                            </li>
                            <li><a href="#">UK</a>
                            </li>
                            <li><a href="#">Srilanka</a>
                            </li>
                        </ul>
                      </li>
                      <li  class="abt_txt"><a href="#">About</a></li>
                    </ul>
                    <?php 
                    $CI = get_instance();
                    //echo $CI->router->fetch_method(); exit();
                    if($CI->router->fetch_class() == 'site' && $CI->router->fetch_method() !='index' && $CI->router->fetch_method() !='join' && $CI->router->fetch_method() !='client' && $CI->router->fetch_method() !='search' && $CI->router->fetch_method() !='search_case' ):?>    
                <ul class="nav navbar-nav pull-right myaccnt">
                  <li class="dropdown config">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">my account <i class="icon-cog" style="font-size:14px"></i> </a>
                      <ul class="dropdown-menu" id="dropnav">
                        <li><a href="#">Settings</a>
                        </li>
                        <li><a href="#">Downloads</a>
                        </li>
                        <li><a href="#">Log out</a>
                        </li>
                    </ul>
                  </li>
               </ul>
                     <?php //if($this->session->userdata('logged_in')): ?>
                        <!-- commented for html displaying purpsose-->
                           <!--  <ul class="nav navbar-nav pull-right">
                              <li class="dropdown config">
                                <a data-toggle="dropdown" class="dropdown-toggle" href="<?php echo base_url('site/overview'); ?>">my account <i style="font-size:14px" class="icon-cog"></i> </a>
                                  <ul id="dropnav" class="dropdown-menu">
                                    <li><a href="#">Settings</a>
                                    </li>
                                    <li><a href="#">Downloads</a>
                                    </li>
                                    <li><a href="<?php echo base_url('user/logout'); ?>">Log out</a>
                                    </li>
                                </ul>
                              </li>
                           </ul> -->
                     <?php else: ?>
                        <ul class="nav navbar-nav pull-right">
                             <li class="signuptxtmar"><a href="<?php echo base_url('site/join'); ?>">Sign up</a></li>
                             <li><button type="button" class="btn btn-small btn-primary" onClick="window.location = '<?php echo base_url('user/login'); ?>'"><i class="icon-lock icnlck"></i> SIGN IN</button></li>
                        </ul>
                   <?php endif ?>
                <a class="close-btn" id="nav-close-btn" href="#top">Return to Content</a>
            </div>
        </nav>
        <div class="clearfix"></div>

     </div>
    </div>
    <div class="clearfix"></div>
</div>


    <div id="main" role="main">
        <div id="filtertpbx"></div>
         <!--Filter box-->
        <div id="filters" style="display: none;">
              <div id="filbox" >
                <div class="container">
                 <div class="col-lg-12">
                  <div class="pull-left" style="margin-right:10px">ACTIVE FILTERS</div>
                  <div id="fil1"></div>
                  <div id="fil2"></div>
                  <div id="fil3"></div>
                  <div id="fil4"></div>
                  <div id="fil5"></div>
                  <div id="fil6"></div>
                
                
                 <div class=" pull-right" id="clrall">CLEAR ALL<i class="icon-remove-sign pull-right signremv"></i></div>
                </div>
                </div>
              </div>
              <div id="selctbxs">
                <div class="container">
                  <form method="post" name="InputCacheCheck" action="about:blank">
            <div class="input-group input-group">
                <div class="btn-group selctboxstyle">
                    <div data-toggle="dropdown" class="covr">
                      <div class="txt filcountry"> Country</div> 
                        <div class="arow">
                          <span class="caret"></span>
                          <div class="clearfix"></div>
                        </div>

                      <div class="clearfix"></div>
                    </div>
                    <ul id="countrybox" class="dropdown-menu">
                        <?php foreach ($country_list as $country) { ?>   
                            <li><a id="<?= $country['CountryID'] ?>" href="#"><?= $country["CountryName"] ?></a></li>  
                        <?php } ?>
                        
                    </ul>
                </div>
                <div class="btn-group selctboxstyle">
                    <div data-toggle="dropdown" class="covr">
                      <div class="txt filcategory"> Category</div> 
                        <div class="arow">
                          <span class="caret"></span>
                          <div class="clearfix"></div>

                        </div>
                      <div class="clearfix"></div>
                    </div>
                    <ul class="dropdown-menu" id="categorybox">
                      <?php foreach ($category_list as $category) { ?>   
                            <li><a id= "<?= $category['ID'] ?>" href="#"><?= $category["Description"] ?></a></li>
                      <?php } ?>
               
                    </ul>
                </div>
                <div class="btn-group selctboxstyle">
                    <div data-toggle="dropdown" class="covr">
                      <div class="txt filage"> Age</div> 
                        <div class="arow">
                          <span class="caret"></span>
                          <div class="clearfix"></div>
                        </div>
                      <div class="clearfix"></div>
                    </div>
                    <ul id="agebox" class="dropdown-menu">
                        <li>
                            <div class="filttit">Choose age span</div>  
                             <div class="ageinner">
                             <div class="rangewid">
                                 <span class="amount" id="amount-start" style="margin-right:10px">0</span>
                                <div id="slider-range" ></div>                               
                                  <span class="amount" id="amount-end" style="margin-left:10px">0</span>  
                                   <div class="clearfix"></div>                             
                                <div class="agebtns"><a href="" class="canclbtnfil">Cancel</a> <button id="set-age" class="btn btn-primary btn-xs" type="button">SET FILTER</button></div>
                                 <div class="clearfix"></div>
                             </div>
                          </div>  
                        </li>
                       
                    </ul>
                </div>

                 <div class="btn-group selctboxstyle">
                    <div data-toggle="dropdown" class="covr">
                      <div class="txt filgender"> Gender</div> 
                        <div class="arow">
                          <span class="caret"></span>
                          <div class="clearfix"></div>
                        </div>
                      <div class="clearfix"></div>
                    </div>
                    <ul id="genderbox" class="dropdown-menu">
                        <li><a id="male" href="#">Male</a>
                        </li>
                        <li><a id="female" href="#">Female</a>
                        </li>
                    </ul>
                </div>
                   <div class="btn-group selctboxstyle">
                    <div data-toggle="dropdown" class="covr">
                      <div class="txt fildevice"> Device</div> 
                        <div class="arow">
                          <span class="caret"></span>
                          <div class="clearfix"></div>
                        </div>
                      <div class="clearfix"></div>

                    </div>
                    <ul id="devicebox" class="dropdown-menu">
                        <?php foreach($device_list as $device) { ?>
                        <li><a id="<?=$device["DeviceID"] ?>" href="#"><?=$device["DeviceName"] ?></a></li>
                        <?php } ?>
                        
                    </ul>
                </div>
                 <div class="btn-group selctboxstyle">
                    <div data-toggle="dropdown" class="covr">
                      <div class="txt filtype"> Type</div> 
                        <div class="arow">
                          <span class="caret"></span>
                          <div class="clearfix"></div>
                        </div>
                      <div class="clearfix"></div>
                    </div>
                    <ul id="typbox" class="dropdown-menu">
                       <?php foreach($type_list as $type) { ?> 
                        <li><a id="<?=$type["FormatID"] ?>" href="#"><?=$type["Title"] ?></a></li>
                        <?php } ?>
                      
                        </li>
                    </ul>
                </div>
                <a href="#" data-toggle="tooltip" data-title="LÃ¶rem ipsum dolÃ¶r. Sit Ã¤met consectetur adipisicing elit sed. Do eiusmod tempÃ¶r incididunt. Ut labÃ¶re et. Dolore mÃ¤gna Ã¤liquÃ¤ ligulÃ¥ nisl ullamcÃ¶rper sem curÃ¥bitur." data-placement="left" class="icninfo" id="popovr"><i class="icon-info"></i></a>
               
            </div>
        </form>
                </div>
              </div>
        </div>
        <!--Filter box end-->
