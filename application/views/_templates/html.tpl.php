<?php
/**
 * @file
 * Default theme implementation to display the basic html structure of a single CI page
 */
  session_start();
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Google Analytics | Buyers Audience</title>
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <!-- ocast theme styles-->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/step3.css') ?>">
        <!-- Le styles -->
        <link href="<?php echo base_url('assets/css/bootstrap.css') ?>" rel="stylesheet">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="../assets/js/html5shiv.js"></script>
        <![endif]-->
        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url('assets/ico/apple-touch-icon-144-precomposed.png') ?>">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url('assets/ico/apple-touch-icon-114-precomposed.png') ?>">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url('assets/ico/apple-touch-icon-72-precomposed.png') ?>">
        <link rel="apple-touch-icon-precomposed" href="<?php echo base_url('assets/ico/apple-touch-icon-57-precomposed.png') ?>">
        <link rel="shortcut icon" href="<?php echo base_url('assets/ico/favicon.png') ?>">
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
       <!--  <link rel="stylesheet" href="<?php //echo base_url('assets/css/normalize.css') ?>">
        <link rel="stylesheet" href="<?php //echo base_url('assets/css/main.css') ?>">-->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/reset.css') ?>"> 
        <!-- ocast theme styles-->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/stylesheet.css') ?>" media="screen">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/style_sort.css') ?>" media="screen"> 
        <link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.css') ?>" media="screen">
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
        <link rel="stylesheet" href="<?php //echo base_url('assets/css/lightbox.css') ?>" media="screen"/>
        <link rel="stylesheet" href="<?php //echo base_url('assets/css/custom_drop.css')    ?>" media="screen"/>  
        <!-- chosen-->
        <link rel="stylesheet" href="<?php //echo base_url('assets/css/chosen.css')?>">
        
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script type="text/javascript">
            //constants goes here
            var base_url = "<?php echo base_url();?>";
            var char_limit = "<?php echo CHAR_LIMIT;?>";
            var audesc_limit = "<?php echo AUDI_CHAR_LIMIT;?>";
            var logo_path = "<?php echo COMPANY_LOGO_PATH;?>";
            var MaxDemogAnswer = "<?php echo DEMO_ANSWER;?>";
            

            //set user cookie data
            var f_name = "<?php if(isset($userDetails['Fname'])) echo $userDetails['Fname'];?>";
            var l_name = "<?php if(isset($userDetails['Lname'])) echo $userDetails['Lname'];?>";
            var e_mail = "<?php if(isset($userDetails['UserName'])) echo $userDetails['UserName'];?>";
            var p_code = "<?php if(isset($userDetails['PhoneCountryCode'])) echo $userDetails['PhoneCountryCode'];?>";
            var c_number = "<?php if(isset($userDetails['PhoneNumber'])) echo $userDetails['PhoneNumber'];?>";
        </script>       

    </head>