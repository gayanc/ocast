<div class="container">
      <!-- Container area -->
        <div class="col-lg-12 col8top">
        
         <h1 class="h1mar">Add your site</h1>
         </div>

          <div class="col-lg-8 ">
          <div class="border-tp"></div>
            
             <div class="col-lg-12" id="stepids"> 
                <div class="col-lg-1 linecircl marg active2"><i class="icon-ok"></i></div>
                <div class="col-lg-1 linebrd active"></div>
                <div class="col-lg-1 linecircl active2"><i class="icon-ok"></i></div>
                <div class="col-lg-1 linebrd active"></div>
                <div class="col-lg-1 linecircl active2"><i class="icon-ok"></i></div>
                <div class="col-lg-1 linebrd active"></div>
                <div class="col-lg-1 linecircl active2"><i class="icon-ok"></i></div>
                <div class="col-lg-1 linebrd active"></div>
                <div class="col-lg-1 linecircl active">5</div>
                <div class="col-lg-1 linebrd"></div>
                <div class="col-lg-1 linecircl">6</div>
                <div class="clearfix"></div>
                <div class="col-lg-1 stptxt info active">Account info</div>
                <div class="col-lg-1 txtspc"></div>
                <div class="col-lg-1 stptxt connect active">Connect</div>
                <div class="col-lg-1 txtspc"></div>
                <div class="col-lg-1 stptxt site active">Site information</div>
                <div class="col-lg-1 txtspc"></div>
                <div class="col-lg-1 stptxt formats active">Formats & products</div>
                <div class="col-lg-1 txtspc"></div>
                <div class="col-lg-1 stptxt case active">Case</div>
                <div class="col-lg-1 txtspc"></div>
                <div class="col-lg-1 stptxt submit">Preview & submit</div>
             </div>
              <div class="clearfix"></div>
              <div class="col-lg-12 padno formmar">
                  <div class="alert alert-warning" style="text-align:center">
                    I'll do this later! <a href=""><i>Skip this step for now</i>  <i class="icon-angle-right" style="font-size:11px"></i></a>
                  </div>
                  <div class="col-lg-12 padno">
                    <div class="panel panel-default">
                      <div class="panel-heading">Add case</div>
                      <div class="panel-body">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque 
                     </div>
                      <div class="col-lg-12 padno formmar">
                        <div class="col-lg-12 padno padleftno formmar">
                            <div class="form-group">
                              <label>Title</label>
                              <input type="text" placeholder="Case title" id="Inpucstit" class="form-control">
                            </div>
                            <div class="form-group">
                              <label>Description</label>
                              <textarea class="form-control" rows="4"></textarea>
                            </div>
                        </div>
                      </div>
                        <div class="col-lg-12 padno">
                          <button class="btn btn-primary pull-left margnrightbtn smallmobilbtn">ADD IMAGE</button>
                          <h6 class="fntsmll">Images are displayed between the title and description.</h6>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 padno formmar2">
                             <button class="btn ashbtn margnrightbtn btn-block"><span style=" font-size: 17px;">+</span> ADD ANOTHER CASE</button>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                    <div class="col-lg-12 padno martp10">
                      <button class="btn btn-primary pull-right ">SAVE &amp; NEXT</button>
                      <button class="btn btn-primary pull-right margnrightbtn">GO BACK</button>
                   </div>
                     <div class="clearfix"></div>  
              </div>
              <div class="clearfix"></div>              
          </div>
           </div>
