<div class="container">

      <!-- Container area -->
        <div class="col-lg-12 col8top">
        
         <h1 class="h1mar">Add your site</h1>
         </div>

          <div class="col-lg-8 ">
          <div class="border-tp"></div>
            
             <div class="col-lg-12" id="stepids"> 
                <div class="col-lg-1 linecircl marg active2"><i class="icon-ok"></i></div>
                <div class="col-lg-1 linebrd active"></div>
                <div class="col-lg-1 linecircl active2"><i class="icon-ok"></i></div>
                <div class="col-lg-1 linebrd active"></div>
                <div class="col-lg-1 linecircl active2"><i class="icon-ok"></i></div>
                <div class="col-lg-1 linebrd active"></div>
                <div class="col-lg-1 linecircl active">4</div>
                <div class="col-lg-1 linebrd"></div>
                <div class="col-lg-1 linecircl">5</div>
                <div class="col-lg-1 linebrd"></div>
                <div class="col-lg-1 linecircl">6</div>
                <div class="clearfix"></div>
                <div class="col-lg-1 stptxt info active">Account info</div>
                <div class="col-lg-1 txtspc"></div>
                <div class="col-lg-1 stptxt connect active">Connect</div>
                <div class="col-lg-1 txtspc"></div>
                <div class="col-lg-1 stptxt site active">Site information</div>
                <div class="col-lg-1 txtspc"></div>
                <div class="col-lg-1 stptxt formats active">Formats & products</div>
                <div class="col-lg-1 txtspc"></div>
                <div class="col-lg-1 stptxt case">Case</div>
                <div class="col-lg-1 txtspc"></div>
                <div class="col-lg-1 stptxt submit">Preview & submit</div>
             </div>
              <div class="clearfix"></div>
              <div class="col-lg-12 padno formmar">
                  <div class="alert alert-warning" style="text-align:center">
                    I'll do this later! <a href=""><i>Skip this step for now</i>  <i class="icon-angle-right" style="font-size:11px"></i></a>
                  </div>
                  <div class="col-lg-12 padno">
                    <div class="panel panel-default">
                      <div class="panel-heading">Upload FORMATS & PRODUCTS</div>
                      <div class="panel-body">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque 
                     </div>
                      <div class="col-lg-12 padno formts">
                       <div class="adformat">
                        <div class="col-lg-4 padleftno formmar formt">
                            <div class="col-lg-12 thumbnilimg"></div>
                            <div class="col-lg-12 padno martp10">
                               <button class="btn ashbtn margnrightbtn btn-block"><span style=" font-size: 17px;">+</span> ADD FORMAT</button>
                            </div>
                        </div>
                        </div>
                        <div class="addscreen">
                        <div class="col-lg-4 midbxclz formmar screenadd">
                            <div class="col-lg-12 thumbnilimg">
                                <div class=" fntsmll addscrnshtxtxt">
                                    <a href=""><span class="plstxt">+</span> Add screenshot</a> 
                                  </div>
                            </div>
                             <div class="col-lg-12 " style="background:#f5f5f5">
                             <div class="clearfix"></div>
                                <div class="martp10">
                                 <div class="btn-group selbx100 selctboxstyle selbx100sml">
                                  <div data-toggle="dropdown" class="covr">
                                    <div class="txt"> Type</div> 
                                      <div class="arow">
                                        <span class="caret"></span>
                                        <div class="clearfix"></div>
                                      </div>
                                    <div class="clearfix"></div>
                                  </div>
                                  <ul class="dropdown-menu">
                                      <li><a href="#">Type</a></li>
                                  </ul>
                                  </div>
                                </div>
                                <div class="martp10">
                                    <input type="text" id="Inputthm" class="form-control h825" placeholder="Format">
                                </div>
                                <div class="martp10">
                                  <div class="btn-group selbx100 selctboxstyle selbx100sml">
                                  <div data-toggle="dropdown" class="covr">
                                    <div class="txt"> Priceype</div> 
                                      <div class="arow">
                                        <span class="caret"></span>
                                        <div class="clearfix"></div>
                                      </div>
                                    <div class="clearfix"></div>
                                  </div>
                                  <ul class="dropdown-menu">
                                      <li><a href="#">Priceype</a></li>
                                  </ul>
                                  </div>
                                </div>
                                
                                 <div class="martp10">
                                    <input type="text" id="Inputsek" class="form-control h825" placeholder="Price">
                                </div>
                                <div class="martp10 col-lg-12 padno" style="margin-bottom:10px">
                                  <div class=" pull-left fntsmll canclbnmar">
                                    <a href="">Cancel</a> 
                                  </div>
                                  <div class=" pull-right">
                                    <button type="button" class="btn btn-small btn-primary smlbtn"> SAVE FORMAT</button>
                                  </div><div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                             </div>

                        </div>
                        </div>
                        <div class="col-lg-4 padrighttno  formmar ">
                            <div class="col-lg-12 thumbnilimg thumbvord" ></div>
                             <div style="border:1px solid #dbdbdb" class="col-lg-12">
                             <div class="clearfix"></div>
                                <div class="martp10 checkbox padno">
                                  <p class="marginno colorthumb">BANNER</p>
                                  <h6 class="marginno"><i>Format:</i> <span class="colr">1250 x 360</span></h6>
                                  <h6 class="marginno"><i>Pricetype:</i> <span class="colr">Request Price</span></h6>
                                  <h6 class="marginno"><i>Price:</i> <span class="colr">9 020 SEK</span></h6>
                                  <div style="margin-bottom:10px" class="martp10 col-lg-12 padno">
                                    <div class=" pull-left fntsmll">
                                      <a href="">Edit</a> <span style="color:#e1e1e1">|</span> <a href="">Remove</a> 
                                    </div>
                                    <div class="clearfix"></div>
                                  </div>
                                  <div class="clearfix"></div>
                                </div>                             
                                  <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                             </div>
                             <div class="adscreenapnd"></div>
                              <div class="adformatapnd"></div>

                        </div>
                        <div class="clearfix"></div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                    <div class="col-lg-12 padno">
                      <button class="btn btn-primary pull-right ">SAVE &amp; NEXT</button>
                      <button class="btn btn-primary pull-right margnrightbtn">GO BACK</button>
                   </div>
                     <div class="clearfix"></div>  
              </div>
              <div class="clearfix"></div>              
          </div>
           </div>
