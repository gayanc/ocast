<div class="container">
      <!-- Container area -->
        <div class="col-lg-12 col8top">
        
         <h1 class="h1mar">Thank you!</h1>
         </div>

          <div class="col-lg-8 ">
          <div class="border-tp "></div>
            
            <div id="stepids" class="col-lg-12"> 
                <div class="col-lg-1 linecircl marg active2"><i class="icon-ok"></i></div>
                <div class="col-lg-1 linebrd active"></div>
                <div class="col-lg-1 linecircl active2"><i class="icon-ok"></i></div>
                <div class="col-lg-1 linebrd active"></div>
                <div class="col-lg-1 linecircl active2"><i class="icon-ok"></i></div>
                <div class="col-lg-1 linebrd active"></div>
                <div class="col-lg-1 linecircl active2"><i class="icon-ok"></i></div>
                <div class="col-lg-1 linebrd active"></div>
                <div class="col-lg-1 linecircl active2"><i class="icon-ok"></i></div>
                <div class="col-lg-1 linebrd active"></div>
                <div class="col-lg-1 linecircl active2"><i class="icon-ok"></i></div>
                <div class="clearfix"></div>
                <div class="col-lg-1 stptxt info active">Account info</div>
                <div class="col-lg-1 txtspc"></div>
                <div class="col-lg-1 stptxt connect active">Connect</div>
                <div class="col-lg-1 txtspc"></div>
                <div class="col-lg-1 stptxt site active">Site information</div>
                <div class="col-lg-1 txtspc"></div>
                <div class="col-lg-1 stptxt formats active">Formats & products</div>
                <div class="col-lg-1 txtspc"></div>
                <div class="col-lg-1 stptxt case active">Case</div>
                <div class="col-lg-1 txtspc"></div>
                <div class="col-lg-1 stptxt submit active">Preview & submit</div>
             </div>
              <div class="clearfix"></div>
             
              <div class="border-tp formmar"></div>
              <div class="col-lg-12 padno formmar">
                  <div>
                    <p class="semibold fontsizerm"> Your site is now submitted and Ocast will notify through email once approved.</p>
                 </div>
                  <div class="col-lg-12 padno">
                    <button class="btn btn-primary pull-right margnrightbtn marginno martp10">GO TO START</button>
                  </div>
       
              </div>
              <div class="clearfix"></div>
               
          </div>


       <!-- Container area end-->
        <div class="clearfix"></div>
      </div>