<div class="container">

      <!-- Container area -->
        <div class="col-lg-12 col8top">
        
         <h1 class="h1mar">Site information</h1>
         </div>

          <div class="col-lg-8 ">
          <div class="border-tp"></div>
            
             <div class="col-lg-12" id="stepids"> 
                <div class="col-lg-1 linecircl marg active2"><i class="icon-ok"></i></div>
                <div class="col-lg-1 linebrd active"></div>
                <div class="col-lg-1 linecircl active2"><i class="icon-ok"></i></div>
                <div class="col-lg-1 linebrd active"></div>
                <div class="col-lg-1 linecircl active">3</div>
                <div class="col-lg-1 linebrd"></div>
                <div class="col-lg-1 linecircl">4</div>
                <div class="col-lg-1 linebrd"></div>
                <div class="col-lg-1 linecircl">5</div>
                <div class="col-lg-1 linebrd"></div>
                <div class="col-lg-1 linecircl">6</div>
                <div class="clearfix"></div>
                 <div class="col-lg-1 stptxt info active">Account info</div>
                <div class="col-lg-1 txtspc"></div>
                <div class="col-lg-1 stptxt connect active">Connect</div>
                <div class="col-lg-1 txtspc"></div>
                <div class="col-lg-1 stptxt site active">Site information</div>
                <div class="col-lg-1 txtspc"></div>
                <div class="col-lg-1 stptxt formats">Formats & products</div>
                <div class="col-lg-1 txtspc"></div>
                <div class="col-lg-1 stptxt case">Case</div>
                <div class="col-lg-1 txtspc"></div>
                <div class="col-lg-1 stptxt submit">Preview & submit</div>
             </div>
              <div class="clearfix"></div>
              <div class="col-lg-12 padno formmar">
                  <div class="alert alert-warning" style="text-align:center">
                    I'll do this later! <a href=""><i>Skip this step for now</i>  <i class="icon-angle-right" style="font-size:11px"></i></a>
                  </div>
                  <div class="col-lg-12 padno">
                    <div class="panel panel-default">
                      <div class="panel-heading">Logotype</div>
                      <div class="panel-body">
                        Upload your logo to appear in the listing and on your personal page. 
                        <div class="col-lg-12 padno formmar">
                          <button class="btn btn-primary pull-left margnrightbtn smallmobilbtn">SELECT IMAGE</button>
                          <h6 class="fntsmll">
                            Images will be automatically cropped to fit 90x70 pixels
                          </h6>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                     <div class="clearfix"></div>  
                     <div class="panel panel-default">
                      <div class="panel-heading">Add description</div>
                      <div class="panel-body">
                        Description may consist of maximum 1 000 characters. It will appear in your listing as well as on your personal page. Only the first 100 characters or so will be visible in your listing.
                        <div class="col-lg-12 padno">
                          <div class="pull-right fontsizerm">
                            1000
                          </div>
                            <div class="clearfix"></div>
                          <textarea class="form-control" rows="4"></textarea>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                        <div class="clearfix"></div>  
                     <div class="panel panel-default">
                      <div class="panel-heading">contact persons 
                        <a href="#" data-toggle="tooltip" data-title="Lörem ipsum dolör. Sit ämet consectetur adipisicing elit sed. Do eiusmod tempör incididunt. Ut laböre et. Dolore mägna äliquä ligulå nisl ullamcörper sem curåbitur." data-placement="right" class="icninfo blu" id="popovr"><i class="icon-info "></i></a>
                      </div>
                      <div class="panel-body">
                        <form role="form">
                        <div class="col-lg-6 padno padleftno">
                            <div class="form-group">
                              <label>First name</label>
                              <input type="text" id="Inputnme" class="form-control">
                            </div>
                        </div>
                         <div class="col-lg-6 padrighttno">
                            <div class="form-group">
                              <label>Surname</label>
                              <h6 class="fntsmll pull-right padno marginno chkmob"><input type="checkbox" class="chkboxmarg"> Use same as account contact</h6>
                              <input type="text" id="Inputsname" class="form-control">  
                            </div>
                          </div>
                        <div class="col-lg-12 padno padleftno">
                            <div class="form-group">
                              <label>Title</label>
                              <input type="text" id="Inputit" class="form-control">
                            </div>
                        </div>
                         <div class="col-lg-6 padno padleftno">
                              <div class="form-group">
                                <label>E-mail</label>
                                <input type="text" class="form-control" id="Inputmail">
                              </div>
                          </div>
                          <div class="col-lg-6 padrighttno">
                            <div class="form-group">
                              <div class="col-lg-5 padno padleftno">
                                <label>Telephone</label>
                                <div class="btn-group selbx100 selctboxstyle">
                                  <div class="covr" data-toggle="dropdown">
                                    <div class="txt"> </div> 
                                      <div class="arow">
                                        <span class="caret"></span>
                                        <div class="clearfix"></div>
                                      </div>
                                    <div class="clearfix"></div>
                                  </div>
                                  <ul class="dropdown-menu">
                                      <li><a href="#">Sve (+43)</a></li>
                                      <li><a href="#">Lk (+94)</a></li>
                                  </ul>
                               </div>
                              </div>
                              <div class="col-lg-7 padrighttno">
                                <label>&nbsp;</label>
                                <input type="text" class="form-control" id="Inputcity">
                              </div>
                              <div class="clearfix"></div>
                            </div>
                          </div>
                          <div class="col-lg-12 padno formmar">
                            <button class="btn btn-primary pull-left margnrightbtn smallmobilbtn">ADD PHOTO</button>
                            <h6 class="fntsmll">
                              Photos are displayed on your contact page.
                            </h6>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 padno formmar">
                            <button class="btn ashbtn margnrightbtn btn-block"><span style=" font-size: 17px;">+</span> ADD ANOTHER CONTACT PERSON</button>
                        </div>
                       </form>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="panel panel-default">
                      <div class="panel-heading">choose countries</div>
                      <div class="panel-body">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. 
                        <div class="col-lg-12 padno formmar">
                          <div class="col-lg-6 padno padleftno">
                            <div class="btn-group selbx100 selctboxstyle">
                                  <div class="covr" data-toggle="dropdown">
                                    <div class="txt">Main Country</div> 
                                      <div class="arow">
                                        <span class="caret"></span>
                                        <div class="clearfix"></div>
                                      </div>
                                    <div class="clearfix"></div>
                                  </div>
                                  <ul class="dropdown-menu">
                                      <li><a href="#">Main Country</a></li>
                                  </ul>
                               </div>
                          </div>
                          <div class="col-lg-6 padrighttno marginselbox1">
                              <div class="btn-group selbx100 selctboxstyle">
                                  <div class="covr" data-toggle="dropdown">
                                    <div class="txt">Add more countries</div> 
                                      <div class="arow">
                                        <span class="caret"></span>
                                        <div class="clearfix"></div>
                                      </div>
                                    <div class="clearfix"></div>
                                  </div>
                                  <ul class="dropdown-menu">
                                      <li><a href="#">Add more countries</a></li>
                                  </ul>
                               </div>
                          </div>
                        </div>
                        <div class="col-lg-12 padno formmar slectd">
                          <div style="margin-right:10px" class="pull-left fntsmll sansbold">SELECTED</div>
                          <div id="sel1 " class="maincntry"><p>USA <i id="close1" class="icon-remove"></i></p></div>
                          <div id="sel2"><p>India <i id="close1" class="icon-remove"></i></p></div>                  
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                     <div class="panel panel-default">
                      <div class="panel-heading">choose categories</div>
                      <div class="panel-body">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. 
                        <div class="col-lg-12 padno formmar">
                          <div class="col-lg-6 padno padleftno">
                              <div class="btn-group selbx100 selctboxstyle">
                                  <div class="covr" data-toggle="dropdown">
                                    <div class="txt">Add categories</div> 
                                      <div class="arow">
                                        <span class="caret"></span>
                                        <div class="clearfix"></div>
                                      </div>
                                    <div class="clearfix"></div>
                                  </div>
                                  <ul class="dropdown-menu">
                                      <li><a href="#">Add categories</a></li>
                                  </ul>
                               </div>
                          </div>
                        </div>
                        <div class="col-lg-12 padno formmar slectd">
                          <div style="margin-right:10px" class="pull-left fntsmll sansbold">SELECTED</div>
                          <div id="sel1"><p>Tech <i id="close1" class="icon-remove"></i></p></div>
                          <div id="sel2"><p>Sports <i id="close1" class="icon-remove"></i></p></div>                  
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="panel panel-default">
                      <div class="panel-heading">demographics</div>
                      <div class="panel-body">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.   
                        <div class="col-lg-12 padno formmar sliderrange">
                            <div class="col-lg-4 padno agage">Select average age</div>
                            <div class="col-lg-6 slde"><div class="slider ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" aria-disabled="false"><a href="#" class="ui-slider-handle ui-state-default ui-corner-all" style="left: 0%;"></a></div></div>
                            <div class="col-lg-2 padno yrs avgyears"><label><span style="color:#4f4f4f">50</span> years</label></div>                
                        </div>
                         <div class="clearfix"></div>
                        <div class="col-lg-12 padno formmar sliderrange">
                            <div class="col-lg-4 padno demoset">Set demographics </div>

                            <div class="col-lg-3 padno manper"><label class="marginno" style="margin:0">Men <span style="color:#4f4f4f">50%</span></label></div>
                            <div style="position: relative; top: -2px;" class="col-lg-6  slde2"><div aria-disabled="false" class="slider ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"><a style="left: 70%;" class="ui-slider-handle ui-state-default ui-corner-all" href="#"></a></div></div>
                            <div class="col-lg-2 padno womnper"><label>Women <span style="color:#4f4f4f">50%</span></label></div>                
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="panel panel-default">
                      <div class="panel-heading">about the audience</div>
                      <div class="panel-body">
                         Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.  
                        <div class="col-lg-12 padno">
                          <div class="pull-right fontsizerm">
                            500
                          </div>
                            <div class="clearfix"></div>
                          <textarea rows="4" class="form-control"></textarea>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                    <div class="clearix"></div>
                    <div class="panel panel-default">
                      <div class="panel-heading">quick facts</div>
                      <div class="panel-body">
                         Quick facts will appear as bullet points. Maximum 3 facts / 50 letters each.
                        <div class="col-lg-12 padno padleftno formmar">
                            <div class="form-group">
                              <label>Fact</label>
                              <input type="text" class="form-control" id="Inpufact" placeholder="E.g. We are the biggest newspaper in Norway">
                            </div>
                            <div class="form-group">
                              <label>Fact</label>
                              <input type="text" class="form-control" id="Inpufact1">
                            </div>
                            <div class="form-group">
                              <label>Fact</label>
                              <input type="text" class="form-control" id="Inpufact2">
                            </div>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="panel panel-default">
                      <div class="panel-heading">Testimonials</div>
                      <div class="panel-body">
                         Testimonials will appear as bullet points. Maximum 3 quotes / 50 letters each.
                        <div class="col-lg-6 padno padleftno formmar">
                            <div class="form-group">
                              <label>Quote</label>
                              <input type="text" class="form-control" id="Inpuqt" >
                            </div>
                         </div>
                         <div class="col-lg-6  padrighttno formmar marbom1">
                            <div class="form-group">
                              <label>Who said this?</label>
                              <input type="text" class="form-control" id="Inpuqt1">
                            </div>
                          </div>
                           <div class="col-lg-6 padno padleftno formmar">
                            <div class="form-group">
                              <label>Quote</label>
                              <input type="text" class="form-control" id="Inpuqt2">
                            </div>
                         </div>
                          <div class="col-lg-6  padrighttno formmar marbom1">
                            <div class="form-group">
                              <label>Who said this?</label>
                              <input type="text" class="form-control" id="Inpuqt1">
                            </div>
                          </div>
                            <div class="col-lg-6 padno padleftno formmar">
                            <div class="form-group">
                              <label>Quote</label>
                              <input type="text" class="form-control" id="Inpuqt2">
                            </div>
                         </div>
                          <div class="col-lg-6  padrighttno formmar ">
                            <div class="form-group">
                              <label>Who said this?</label>
                              <input type="text" class="form-control" id="Inpuqt1">
                            </div>
                          </div>
                      
                      </div>
                      <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                       <div class="panel panel-default">
                      <div class="panel-heading">add demographic info</div>
                      <div class="panel-body">
                         Lorem ipsum dolor sit amet lipsum dolores. 
                       <div class="col-lg-12 padno padleftno formmar">
                             <div class="form-group">
                              <label>Headline</label>
                              <input type="text" placeholder="E.g. Education" id="Inpuhdlin" class="form-control">
                            </div>
                       </div>
                        <div class="col-lg-6 padno padleftno ">
                            <div class="form-group">
                              <label>Answer 1</label>
                              <input type="text" id="Inpuans1" class="form-control">
                            </div>
                        </div>
                          <div class="col-lg-6  padrighttno formmar" style="position: relative; top: 0px;">
                          <div class="col-lg-12 sliderrange padno martp10">
                         
                          <div class="col-lg-10 padrighttno slde_answ"><div class="slider ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" aria-disabled="false"><a href="#" class="ui-slider-handle ui-state-default ui-corner-all" style="left: 0%;"></a></div></div>
                          <div class="col-lg-2 padno yrs_answ" style="text-align:right"><label><span style="color:#4f4f4f">50%</span></label></div>                 
                        </div>
                      </div>
                         <div class="clearfix"></div>
                          <div class="col-lg-6 padno padleftno ">
                            <div class="form-group">
                              <label>Answer 2</label>
                              <input type="text" id="Inpuqans2" class="form-control">
                            </div>
                        </div>
                       
                      <div class="col-lg-6  padrighttno formmar" style="position: relative; top: 4px;">
                          <div class="col-lg-12 sliderrange padno martp10">
                         
                          <div class="col-lg-10 padrighttno slde_answ"><div class="slider ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" aria-disabled="false"><a href="#" class="ui-slider-handle ui-state-default ui-corner-all" style="left: 0%;"></a></div></div>
                          <div class="col-lg-2 padno yrs_answ" style="text-align:right"><label><span style="color:#4f4f4f">50%</span></label></div>                 
                        </div>
                      </div>
                       <div class="clearfix"></div>
                       <div class="col-lg-12 padno formmar">
                        <button class="btn btn-primary margnrightbtn btn-block"><i class="icon-plus"></i> ADD ANOTHER ANSWER (MAX 5)</button>
                      </div>
                      <div class="col-lg-12 padno formmar">
                        <button class="btn ashbtn margnrightbtn btn-block"><span style=" font-size: 17px;">+</span> ADD ANOTHER MODULE</button>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <div class="col-lg-12 padno formmar">
                    <button class="btn btn-primary pull-right ">SAVE &amp; NEXT</button>
                    <button class="btn btn-primary pull-right margnrightbtn">GO BACK</button>
                  </div>

              </div>
              <div class="clearfix"></div>              
          </div>
           </div>
       <!-- Container area end-->
        <div class="clearfix"></div>

      </div>