<div class="container">

      <!-- Container area -->
        <div class="col-lg-12 col8top">
        
         <h1 class="h1mar">Join Ocast - Account information</h1>
         </div>

          <div class="col-lg-8 ">
          <div class="border-tp"></div>
            
             <div class="col-lg-12" id="stepids"> 
                <div class="col-lg-1 linecircl marg active">1</div>
                <div class="col-lg-1 linebrd"></div>
                <div class="col-lg-1 linecircl">2</div>
                <div class="col-lg-1 linebrd"></div>
                <div class="col-lg-1 linecircl">3</div>
                <div class="col-lg-1 linebrd"></div>
                <div class="col-lg-1 linecircl">4</div>
                <div class="col-lg-1 linebrd"></div>
                <div class="col-lg-1 linecircl">5</div>
                <div class="col-lg-1 linebrd"></div>
                <div class="col-lg-1 linecircl">6</div>
                <div class="clearfix"></div>
                <div class="col-lg-1 stptxt info active">Account info</div>
                <div class="col-lg-1 txtspc"></div>
                <div class="col-lg-1 stptxt connect">Connect</div>
                <div class="col-lg-1 txtspc"></div>
                <div class="col-lg-1 stptxt site">Site information</div>
                <div class="col-lg-1 txtspc"></div>
                <div class="col-lg-1 stptxt formats">Formats & products</div>
                <div class="col-lg-1 txtspc"></div>
                <div class="col-lg-1 stptxt case">Case</div>
                <div class="col-lg-1 txtspc"></div>
                <div class="col-lg-1 stptxt submit">Preview & submit</div>
             </div>
             <div class="clearfix"></div>
             <h6>
                <p class="col-lg-12 padno">
                Non phäretrå risus sapien tempor pellentesque. Fringilla årcu enim. Sem söllicitudin pörtå åliquåm proin, odio ät tellus. Vulputate praesent libero ultricies, <a href="">Justo dignissim</a> sem ämet et. </p>
              </h6>
              <div class="clearfix"></div>
              <h5 class="titl formmar">Account information 
                <a href="#" data-toggle="tooltip" data-title="Lörem ipsum dolör. Sit ämet consectetur adipisicing elit sed. Do eiusmod tempör incididunt. Ut laböre et. Dolore mägna äliquä ligulå nisl ullamcörper sem curåbitur.äliquä ligulå nisl ullamcörper sem curåbitur.äliquä ligulå nisl ullamcörper sem curåbitur." data-placement="right" class="icninfo" id="popovr"><i class="icon-info"></i></a>
              </h5>
              <div class="border-tp"></div>
              <div class="col-lg-12 padno formmar">
               <form role="form">
                <div class="col-lg-12 padno padleftno">
                    <div class="col-lg-6 padno padleftno">
                      <div class="form-group">
                        <label>What site do you represent?</label>
                        <input type="text" class="form-control" id="Inputsite">
                      </div>
                    </div>
                    <div class="col-lg-6 padrighttno">
                       <div class="form-group">
                        <label>Email <span class="semibold">(Verification will be sent to this email)</span></label>
                        <input type="email" class="form-control" id="Inputemail">
                      </div>
                    </div>

                    <div class="col-lg-6 padno padleftno">
                      <div class="form-group">
                        <label>First name</label>
                        <input type="text" class="form-control" id="Inputfname">  
                      </div>
                    </div>
                    <div class="col-lg-6 padrighttno">
                      <div class="form-group">
                        <label >Last name</label>
                        <input type="text" class="form-control" id="Inputlname">
                      </div>
                    </div>

                     <div class="col-lg-6 padno padleftno">
                       <div class="form-group">
                        <label>Choose password</label>
                        <input type="password" class="form-control" id="Inputchooseassword">
                      </div>
                    </div>
                    <div class="col-lg-6 padrighttno">
                      <div class="form-group">
                        <label>Confirm password</label>
                        <input type="password" class="form-control" id="InputConfirmpassword">
                      </div>
                    </div>
                </div>
   
               </form>
              </div>
              <div class="clearfix"></div>
                <h5 class="titl formmar">Company information 
                <a href="#" data-toggle="tooltip" data-title="Lörem ipsum dolör. Sit ämet consectetur adipisicing elit sed. Do eiusmod tempör incididunt. Ut laböre et. Dolore mägna äliquä ligulå nisl ullamcörper sem curåbitur.äliquä ligulå nisl ullamcörper sem curåbitur.äliquä ligulå nisl ullamcörper sem curåbitur." data-placement="right" class="icninfo" id="popovr"><i class="icon-info"></i></a>
              </h5>
              <div class="border-tp"></div>
              <div class="col-lg-12 padno formmar">
               <form role="form">
                <div class="col-lg-12 padno padleftno">
                    <div class="col-lg-6 padno padleftno">
                      <div class="form-group">
                        <label >Company</label>
                        <input type="text" class="form-control" id="Company">
                      </div>
                    </div>
                    <div class="col-lg-6 padrighttno">
                      <div class="form-group">
                        <div class="col-lg-5 padno padleftno">
                          <label >Phone number</label>
                          <div class="btn-group selbx100 selctboxstyle">
                                    <div class="covr" data-toggle="dropdown">
                                      <div class="txt"> </div> 
                                        <div class="arow">
                                          <span class="caret"></span>
                                          <div class="clearfix"></div>
                                        </div>
                                      <div class="clearfix"></div>
                                    </div>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Sve (+43)</a></li>
                                    </ul>
                           </div>
                        </div>
                        <div class="col-lg-7 padrighttno">
                          <label >&nbsp;</label>
                          <input type="text" id="Inputcity" class="form-control">
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>

                    <div class="col-lg-6 padno padleftno">
                      <div class="form-group">
                        <label >Address 1</label>
                        <input type="text" class="form-control" id="Inputadres1">  
                      </div>
                    </div>
                     <div class="col-lg-6 padrighttno">
                        <div class="form-group">
                          <label>Address 2</label>
                          <input type="text" class="form-control" id="Inputaddrs2">
                        </div>
                      </div>
                       <div class="col-lg-6 padno padleftno">
                         <div class="form-group">
                          <div class="col-lg-5 padno padleftno">
                            <label >Zip</label>
                            <input type="text" id="Inputzip" class="form-control">
                          </div>
                          <div class="col-lg-7 padrighttno citybox">
                            <label >City</label>
                            <input type="text" id="Inputcity" class="form-control">
                          </div>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                      <div class="col-lg-6 padrighttno">
                      <div class="form-group">
                        <label>Country</label>
                        <input type="text" class="form-control" id="InputCountry">
                      </div>
                    </div>

                </div>

               </form>
               <div class="col-lg-12 padno"><div class="checkbox">
                  <label>
                    <input type="checkbox" class="chkboxmar"> <h6 class="marginno">I agree the <a href="">Terms and conditions</a></h6>
                  </label>
                </div>
              </div>
               <div class="col-lg-12 padno formmar"><button class="btn btn-primary pull-right">SAVE & NEXT</button></div>
            </div>
          </div>

          <div class="col-lg-4 rightsidsm" >
          <div class="border-tp"></div>
          <h5 class="titl">lorem information</h5>
           <div class="box_line">
              <p>
                 Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis <a href="">parturient montes, nascetur</a> ridiculus mus. Donec quam felis, ultricies nec.
              </p>
              
          </div>

        </div>
       <!-- Container area end-->
        <div class="clearfix"></div>

      </div>