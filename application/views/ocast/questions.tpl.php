<div class="container">

      <!-- Container area -->
        <div class="col-lg-12 col8top">
        
        
         <h1 class="h1mar">FAQ</h1>
         </div>

          <div id="faq" class="col-lg-8 ">
            <div class="border-tp"></div>
              
                  <h6>
                  <p class="col-lg-12 padno">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel congue metus. Ut ac mattis nibh. Sed sagittis facilisis diam nec tempor. Duis luctus tincidunt tellus, ut aliquam risus pulvinar ut. Aliquam erat volutpat. Ut nisl nunc, eleifend sed aliquet in, <a href="">blandit at</a> mi. Etiam auct.</p>
                      <div class="clearfix"></div>
                  </h6>
                  <div class="col-lg-7 padno">
                    <h6><p>What is your question about?</p></h6>
                  </div>
                  <div class="col-lg-6 padno padleftno">
                              <div class="btn-group selbx100 selctboxstyle">
                                  <div data-toggle="dropdown" class="covr">
                                    <div class="txt">What is your question about?</div> 
                                      <div class="arow">
                                        <span class="caret"></span>
                                        <div class="clearfix"></div>
                                      </div>
                                    <div class="clearfix"></div>
                                  </div>
                                  <ul class="dropdown-menu">
                                      <li><a href="#">Membership</a></li>
                                      <li><a href="#">Payment</a></li>
                                      <li><a href="#">My account</a></li>
                                  </ul>
                               </div>
                          </div>
                <div class="clearfix"></div>
            <div class="accordion formmar" id="monogram-acc">
              <h5 class="titl formmar" style="border-top:0">Membership</h5>
               
                  <div class="accordion-group ">
                      <div class="accordion-heading">
                          <div class="accordion-toggle" data-toggle="collapse" data-parent="#monogram-acc" href="#register"> <i class="icon-caret-right caretri" ></i> <a href=""> How do I register? </a></div>
                      </div>
                      <div class="accordion-body collapse" id="register"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel congue metus. Ut ac mattis nibh. Sed sagittis facilisis diam nec tempor. Duis luctus tincidunt tellus, ut aliquam risus pulvinar ut. Aliquam erat volutpat. Ut nisl nunc, eleifend sed aliquet in<br/><br/></div>
                  </div>
                  <div class="accordion-group ">
                      <div class="accordion-heading">
                          <div class="accordion-toggle" data-toggle="collapse" data-parent="#monogram-acc" href="#cost"> <i class="icon-caret-right caretri" ></i>   <a href="">How much does it cost? </a></div>
                      </div>
                      <div class="accordion-body collapse" id="cost">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel congue metus. Ut ac mattis nibh. Sed sagittis facilisis diam nec tempor. Duis luctus tincidunt tellus, ut aliquam risus pulvinar ut. Aliquam erat volutpat. Ut nisl nunc, eleifend sed aliquet in<br/><br/></div>
                  </div>
                  <div class="accordion-group ">
                      <div class="accordion-heading">
                          <div class="accordion-toggle" data-toggle="collapse" data-parent="#monogram-acc" href="#paypal"> <i class="icon-caret-right caretri" ></i>   <a href="">Can I pay using paypal?  </a></div>
                      </div>
                      <div class="accordion-body collapse" id="paypal"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel congue metus. Ut ac mattis nibh. Sed sagittis facilisis diam nec tempor. Duis luctus tincidunt tellus, ut aliquam risus pulvinar ut. Aliquam erat volutpat. Ut nisl nunc, eleifend sed aliquet in<br/><br/></div>
                  </div>
                  <div class="accordion-group ">
                      <div class="accordion-heading">
                          <div class="accordion-toggle" data-toggle="collapse" data-parent="#monogram-acc" href="#multiple"><i class="icon-caret-right caretri" ></i>  <a href="">  Managing multiple sites?  </a></div>
                      </div>
                      <div class="accordion-body collapse" id="multiple">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel congue metus. Ut ac mattis nibh. Sed sagittis facilisis diam nec tempor. Duis luctus tincidunt tellus, ut aliquam risus pulvinar ut. Aliquam erat volutpat. Ut nisl nunc, eleifend sed aliquet in<br/><br/></div>
                  </div>
                  <div class="accordion-group ">
                      <div class="accordion-heading">
                          <div class="accordion-toggle" data-toggle="collapse" data-parent="#monogram-acc" href="#mobile"> <i class="icon-caret-right caretri" ></i>   <a href="">How do I add a mobile site?  </a></div>
                      </div>
                      <div class="accordion-body collapse" id="mobile"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel congue metus. Ut ac mattis nibh. Sed sagittis facilisis diam nec tempor. Duis luctus tincidunt tellus, ut aliquam risus pulvinar ut. Aliquam erat volutpat. Ut nisl nunc, eleifend sed aliquet in<br/><br/></div>
                  </div>
              <h5 class="titl formmar" style="border-top:0">Payment</h5>
               
                  <div class="accordion-group ">
                      <div class="accordion-heading">
                          <div class="accordion-toggle" data-toggle="collapse" data-parent="#monogram-acc" href="#p1"> <i class="icon-caret-right caretri" ></i>  <a href="" >How do I register? </a></div>
                      </div>
                      <div class="accordion-body collapse" id="p1"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel congue metus. Ut ac mattis nibh. Sed sagittis facilisis diam nec tempor. Duis luctus tincidunt tellus, ut aliquam risus pulvinar ut. Aliquam erat volutpat. Ut nisl nunc, eleifend sed aliquet in<br/><br/></div>
                  </div>
                  <div class="accordion-group ">
                      <div class="accordion-heading">
                          <div class="accordion-toggle" data-toggle="collapse" data-parent="#monogram-acc" href="#p2"> <i class="icon-caret-right caretri" ></i>   <a href="">How much does it cost? </a></div>
                      </div>
                      <div class="accordion-body collapse" id="p2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel congue metus. Ut ac mattis nibh. Sed sagittis facilisis diam nec tempor. Duis luctus tincidunt tellus, ut aliquam risus pulvinar ut. Aliquam erat volutpat. Ut nisl nunc, eleifend sed aliquet in<br/><br/></div>
                  </div>
                  <div class="accordion-group ">
                      <div class="accordion-heading">
                          <div class="accordion-toggle" data-toggle="collapse" data-parent="#monogram-acc" href="#p3"><i class="icon-caret-right caretri" ></i>   <a href=""> Can I pay using paypal?  </a></div>
                      </div>
                      <div class="accordion-body collapse" id="p3"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel congue metus. Ut ac mattis nibh. Sed sagittis facilisis diam nec tempor. Duis luctus tincidunt tellus, ut aliquam risus pulvinar ut. Aliquam erat volutpat. Ut nisl nunc, eleifend sed aliquet in<br/><br/></div>
                  </div>
                  <div class="accordion-group ">
                      <div class="accordion-heading">
                          <div class="accordion-toggle" data-toggle="collapse" data-parent="#monogram-acc" href="#p4"> <i class="icon-caret-right caretri" ></i>   <a href="">Managing multiple sites?  </a></div>
                      </div>
                      <div class="accordion-body collapse" id="p4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel congue metus. Ut ac mattis nibh. Sed sagittis facilisis diam nec tempor. Duis luctus tincidunt tellus, ut aliquam risus pulvinar ut. Aliquam erat volutpat. Ut nisl nunc, eleifend sed aliquet in<br/><br/></div>
                  </div>
                  <div class="accordion-group ">
                      <div class="accordion-heading">
                          <div class="accordion-toggle" data-toggle="collapse" data-parent="#monogram-acc" href="#p5"> <i class="icon-caret-right caretri" ></i>  <a  href="">How do I add a mobile site?  </a></div>
                      </div>
                      <div class="accordion-body collapse" id="p5"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel congue metus. Ut ac mattis nibh. Sed sagittis facilisis diam nec tempor. Duis luctus tincidunt tellus, ut aliquam risus pulvinar ut. Aliquam erat volutpat. Ut nisl nunc, eleifend sed aliquet in<br/><br/></div>
                  </div>
              <h5 class="titl formmar" style="border-top:0">My account</h5>
               
                  <div class="accordion-group">
                      <div class="accordion-heading">
                          <div class="accordion-toggle" data-toggle="collapse" data-parent="#monogram-acc" href="#ac1"><i class="icon-caret-right caretri" ></i>  <a href="" >  How do I register? </a></div>
                      </div>
                      <div class="accordion-body collapse" id="ac1"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel congue metus. Ut ac mattis nibh. Sed sagittis facilisis diam nec tempor. Duis luctus tincidunt tellus, ut aliquam risus pulvinar ut. Aliquam erat volutpat. Ut nisl nunc, eleifend sed aliquet in<br/><br/></div>
                  </div>
                  <div class="accordion-group ">
                      <div class="accordion-heading">
                          <div class="accordion-toggle" data-toggle="collapse" data-parent="#monogram-acc" href="#ac2"><i class="icon-caret-right caretri" ></i>   <a href=""> How much does it cost? </a></div>
                      </div>
                      <div class="accordion-body collapse" id="ac2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel congue metus. Ut ac mattis nibh. Sed sagittis facilisis diam nec tempor. Duis luctus tincidunt tellus, ut aliquam risus pulvinar ut. Aliquam erat volutpat. Ut nisl nunc, eleifend sed aliquet in<br/><br/></div>
                  </div>
                  <div class="accordion-group ">
                      <div class="accordion-heading">
                          <div class="accordion-toggle" data-toggle="collapse" data-parent="#monogram-acc" href="#ac3"><i class="icon-caret-right caretri" ></i>   <a href="">  Can I pay using paypal?  </a></div>
                      </div>
                      <div class="accordion-body collapse" id="ac3"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel congue metus. Ut ac mattis nibh. Sed sagittis facilisis diam nec tempor. Duis luctus tincidunt tellus, ut aliquam risus pulvinar ut. Aliquam erat volutpat. Ut nisl nunc, eleifend sed aliquet in<br/><br/></div>
                  </div>
             
              
          </div>
  
          </div>
          <div class="col-lg-4 titmarr">
          <div class="border-tp"></div>
          <h5 class="titl">Can’t find what you’re looking for?</h5>
           <div class="box_line padbotfaq" style="border-bottom:0">
              <p style="border-bottom:0">
                If you’re having difficulties finding what you are looking for, then <a href="">contact us</a> and we’ll try our best to answers your questions.
              </p>
          </div>
          <div class="border-tp" style=" margin-top: 8px;"></div>
          <h5 class="titl">Sign up</h5>
           <div class="box_line" >
              <p style="border-bottom:0">
                Sign up today. It’s simple, fun, and it doesn’t cost as much as you might think.
              </p>
              <button class="btn btn-primary  pull-left" name="submit" type="submit">YES, SIGN ME UP!</button>
          </div>

        </div>
       <!-- Container area end-->
        <div class="clearfix"></div>

      </div>