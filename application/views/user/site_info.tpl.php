<div class="container">

      <!-- Container area -->
        <div class="col-lg-12 col8top">
        
          <h1 class="h1mar">Site information</h1>
        </div>
          <div class="col-lg-8 " id="backend">
          <div class="border-tp"></div>
                  <h6>
                      <p class="col-lg-12 padno">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel congue metus. Ut ac mattis nibh. Sed sagittis facilisis diam nec tempor. Duis luctus tincidunt tellus, ut aliquam risus pulvinar ut. Aliquam erat volutpat. Ut nisl nunc, eleifend sed aliquet in, mi. Etiam auct.</p>
                  </h6>
                  <div class="col-lg-5 padleftno">
                              <div class="btn-group selbx100 selctboxstyle">
                                  <div class="covr" data-toggle="dropdown">
                                    <div class="txt">Select new language</div> 
                                      <div class="arow">
                                        <span class="caret"></span>
                                        <div class="clearfix"></div>
                                      </div>
                                    <div class="clearfix"></div>
                                  </div>
                                  <ul class="dropdown-menu">
                                      <li><a href="#">Select new language</a></li>
                                  </ul>
                               </div>
                  </div>
                  <div class="col-lg-4 padno"><button class="btn btn-primary pull-left smallmobilbtn">ADD</button></div>
                  <div class="clearfix"></div>
                  <div class="col-lg-12 padno formmar">
                  <p class="semibold marginno" >Edit language version: <a href="">SE</a> | <a href="">EN</a> | <a href="">DE</a> | <a href="">FR</a></p>
                  </div>
              <div class="clearfix"></div>
              <div class="col-lg-12 padno formmar" id="ovrwritbacken">
                    <div class="panel panel-default" >
                      <div class="panel-heading">Logotype</div>
                      <div class="panel-body">
                        Upload your logo to appear in the listing and on your personal page. 
                        <div class="col-lg-12 padno formmar">
                          <button class="btn btn-primary pull-left margnrightbtn smallmobilbtn">SELECT IMAGE</button>
                          <h6 class="fntsmll">
                            Images will be automatically cropped to fit 90x70 pixels
                          </h6>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                     <div class="clearfix"></div>  
                     <div class="panel panel-default">
                      <div class="panel-heading">Add description</div>
                      <div class="panel-body">
                        Description may consist of maximum 1 000 characters. It will appear in your listing as well as on your personal page. Only the first 100 characters or so will be visible in your listing.
                        <div class="col-lg-12 padno">
                          <div class="pull-right fontsizerm">
                            1000
                          </div>
                            <div class="clearfix"></div>
                          <textarea rows="4" class="form-control"></textarea>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                        <div class="clearfix"></div>  
                     <div class="panel panel-default">
                      <div class="panel-heading">contact persons 
                        <a id="popovr" class="icninfo blu" data-placement="right" data-title="Lörem ipsum dolör. Sit ämet consectetur adipisicing elit sed. Do eiusmod tempör incididunt. Ut laböre et. Dolore mägna äliquä ligulå nisl ullamcörper sem curåbitur." data-toggle="tooltip" href="#" data-original-title="" title=""><i class="icon-info "></i></a>
                      </div>
                      <div class="panel-body">
                        <form role="form">
                        <div class="col-lg-6 padno padleftno">
                            <div class="form-group">
                              <label>First name</label>
                              <input type="text" class="form-control" id="Inputnme">
                            </div>
                        </div>
                         <div class="col-lg-6 padrighttno">
                            <div class="form-group">
                              <label>Surname</label>
                              <h6 class="fntsmll pull-right padno marginno chkmob"><input type="checkbox" class="chkboxmarg"> Use same as account contact</h6>
                              <input type="text" class="form-control" id="Inputsname">  
                            </div>
                          </div>
                        <div class="col-lg-12 padno padleftno">
                            <div class="form-group">
                              <label>Title</label>
                              <input type="text" class="form-control" id="Inputit">
                            </div>
                        </div>
                         <div class="col-lg-6 padno padleftno">
                              <div class="form-group">
                                <label>E-mail</label>
                                <input type="text" id="Inputmail" class="form-control">
                              </div>
                          </div>
                          <div class="col-lg-6 padrighttno">
                            <div class="form-group">
                              <div class="col-lg-5 padno padleftno">
                                <label>Telephone</label>
                                 <div class="btn-group selbx100 selctboxstyle">
                                  <div class="covr" data-toggle="dropdown">
                                    <div class="txt"> </div> 
                                      <div class="arow">
                                        <span class="caret"></span>
                                        <div class="clearfix"></div>
                                      </div>
                                    <div class="clearfix"></div>
                                  </div>
                                  <ul class="dropdown-menu">
                                      <li><a href="#">Sve (+43)</a></li>
                                      <li><a href="#">Lk (+94)</a></li>
                                  </ul>
                               </div>
                              </div>
                              <div class="col-lg-7 padrighttno">
                                <label>&nbsp;</label>
                                <input type="text" id="Inputcity" class="form-control">
                              </div>
                              <div class="clearfix"></div>
                            </div>
                          </div>
                          <div class="col-lg-12 padno formmar">
                            <button class="btn btn-primary pull-left margnrightbtn smallmobilbtn">ADD PHOTO</button>
                            <h6 class="fntsmll">
                              Photos are displayed on your contact page.
                            </h6>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 padno formmar">
                            <button class="btn ashbtn margnrightbtn btn-block"><span style=" font-size: 17px;">+</span> ADD ANOTHER CONTACT PERSON</button>
                        </div>
                       </form>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="panel panel-default">
                      <div class="panel-heading">choose countries</div>
                      <div class="panel-body">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. 
                        <div class="col-lg-12 padno formmar">
                          <div class="col-lg-6 padno padleftno">
                             <div class="btn-group selbx100 selctboxstyle">
                                  <div class="covr" data-toggle="dropdown">
                                    <div class="txt">Main Country</div> 
                                      <div class="arow">
                                        <span class="caret"></span>
                                        <div class="clearfix"></div>
                                      </div>
                                    <div class="clearfix"></div>
                                  </div>
                                  <ul class="dropdown-menu">
                                      <li><a href="#">Main Country</a></li>
                                  </ul>
                               </div>
                          </div>
                          <div class="col-lg-6 padrighttno marginselbox1">
                              <div class="btn-group selbx100 selctboxstyle">
                                  <div class="covr" data-toggle="dropdown">
                                    <div class="txt">Add more countries</div> 
                                      <div class="arow">
                                        <span class="caret"></span>
                                        <div class="clearfix"></div>
                                      </div>
                                    <div class="clearfix"></div>
                                  </div>
                                  <ul class="dropdown-menu">
                                      <li><a href="#">Add more countries</a></li>
                                  </ul>
                               </div>
                          </div>
                        </div>
                        <div class="col-lg-12 padno formmar slectd">
                          <div style="margin-right:10px" class="pull-left fntsmll sansbold">SELECTED</div>
                          <div id="sel1 " class="maincntry"><p>USA <i id="close1" class="icon-remove"></i></p></div>
                          <div id="sel2"><p>India <i id="close1" class="icon-remove"></i></p></div>                  
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                     <div class="panel panel-default">
                      <div class="panel-heading">choose categories</div>
                      <div class="panel-body">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. 
                        <div class="col-lg-12 padno formmar">
                          <div class="col-lg-6 padno padleftno">
                             <div class="btn-group selbx100 selctboxstyle">
                                  <div class="covr" data-toggle="dropdown">
                                    <div class="txt">Add categories</div> 
                                      <div class="arow">
                                        <span class="caret"></span>
                                        <div class="clearfix"></div>
                                      </div>
                                    <div class="clearfix"></div>
                                  </div>
                                  <ul class="dropdown-menu">
                                      <li><a href="#">Add categories</a></li>
                                  </ul>
                               </div>
                          </div>
                        </div>
                        <div class="col-lg-12 padno formmar slectd">
                          <div class="pull-left fntsmll sansbold" style="margin-right:10px">SELECTED</div>
                          <div id="sel1"><p>Tech <i class="icon-remove" id="close1"></i></p></div>
                          <div id="sel2"><p>Sports <i class="icon-remove" id="close1"></i></p></div>                  
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="panel panel-default">
                      <div class="panel-heading">demographics</div>
                      <div class="panel-body">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.   
                        <div class="col-lg-12 padno formmar sliderrange">
                          <div class="col-lg-4 padno">Select average age</div>
                          <div class="col-lg-6 slde "><div class="slider ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" aria-disabled="false"><a href="#" class="ui-slider-handle ui-state-default ui-corner-all" style="left: 0%;"></a></div></div>
                          <div class="col-lg-2 padno avgyears"><label><span style="color:#4f4f4f">50</span> years</label></div>                
                        </div>
                        <div class="col-lg-12 padno formmar sliderrange">
                          <div class="col-lg-4 padno demoset">Set demographics </div>

                          <div class="col-lg-3 padno manper"><label style="margin:0" class="marginno">Men <span style="color:#4f4f4f">50%</span></label></div>
                          <div class="col-lg-6  slde2" style="position: relative; top: -2px;" ><div class="slider ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" aria-disabled="false"><a href="#" class="ui-slider-handle ui-state-default ui-corner-all" style="left: 0%;"></a></div></div>
                          <div class="col-lg-2 padno womnper"><label>Women <span style="color:#4f4f4f">50%</span></label></div>                
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="panel panel-default">
                      <div class="panel-heading">about the audience</div>
                      <div class="panel-body">
                         Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.  
                        <div class="col-lg-12 padno">
                          <div class="pull-right fontsizerm">
                            500
                          </div>
                            <div class="clearfix"></div>
                          <textarea class="form-control" rows="4"></textarea>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                    <div class="clearix"></div>
                    <div class="panel panel-default">
                      <div class="panel-heading">quick facts</div>
                      <div class="panel-body">
                         Quick facts will appear as bullet points. Maximum 3 facts / 50 letters each.
                        <div class="col-lg-12 padno padleftno formmar">
                            <div class="form-group">
                              <label>Fact</label>
                              <input type="text" placeholder="E.g. We are the biggest newspaper in Norway" id="Inpufact" class="form-control">
                            </div>
                            <div class="form-group">
                              <label>Fact</label>
                              <input type="text" id="Inpufact1" class="form-control">
                            </div>
                            <div class="form-group">
                              <label>Fact</label>
                              <input type="text" id="Inpufact2" class="form-control">
                            </div>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="panel panel-default">
                      <div class="panel-heading">Testimonials</div>
                       <div class="panel-body">
                         Testimonials will appear as bullet points. Maximum 3 quotes / 50 letters each.
                        <div class="col-lg-6 padno padleftno formmar">
                            <div class="form-group">
                              <label>Quote</label>
                              <input type="text" class="form-control" id="Inpuqt" >
                            </div>
                         </div>
                         <div class="col-lg-6  padrighttno formmar marbom1">
                            <div class="form-group">
                              <label>Who said this?</label>
                              <input type="text" class="form-control" id="Inpuqt1">
                            </div>
                          </div>
                           <div class="col-lg-6 padno padleftno formmar">
                            <div class="form-group">
                              <label>Quote</label>
                              <input type="text" class="form-control" id="Inpuqt2">
                            </div>
                         </div>
                          <div class="col-lg-6  padrighttno formmar marbom1">
                            <div class="form-group">
                              <label>Who said this?</label>
                              <input type="text" class="form-control" id="Inpuqt1">
                            </div>
                          </div>
                            <div class="col-lg-6 padno padleftno formmar">
                            <div class="form-group">
                              <label>Quote</label>
                              <input type="text" class="form-control" id="Inpuqt2">
                            </div>
                         </div>
                          <div class="col-lg-6  padrighttno formmar ">
                            <div class="form-group">
                              <label>Who said this?</label>
                              <input type="text" class="form-control" id="Inpuqt1">
                            </div>
                          </div>
                      
                      </div>
                      <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                       <div class="panel panel-default">
                      <div class="panel-heading">add demographic info</div>
                      <div class="panel-body">
                         Lorem ipsum dolor sit amet lipsum dolores. 
                       <div class="col-lg-12 padno padleftno formmar">
                             <div class="form-group">
                              <label>Headline</label>
                              <input type="text" placeholder="E.g. Education" id="Inpuhdlin" class="form-control">
                            </div>
                       </div>
                        <div class="col-lg-6 padno padleftno ">
                            <div class="form-group">
                              <label>Answer 1</label>
                              <input type="text" id="Inpuans1" class="form-control">
                            </div>
                        </div>
                          <div class="col-lg-6  padrighttno formmar" style="position: relative; top: 0px;">
                            <div class="col-lg-12 sliderrange padno martp10">
                           
                            <div class="col-lg-10 padrighttno slde_answ"><div class="slider ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" aria-disabled="false"><a href="#" class="ui-slider-handle ui-state-default ui-corner-all" style="left: 0%;"></a></div></div>
                            <div class="col-lg-2 padno yrs_answ" style="text-align:right"><label><span style="color:#4f4f4f">50%</span></label></div>                 
                          </div>
                         </div>
                         <div class="clearfix"></div>
                          <div class="col-lg-6 padno padleftno ">
                            <div class="form-group">
                              <label>Answer 2</label>
                              <input type="text" id="Inpuqans2" class="form-control">
                            </div>
                        </div>
                       
                      <div class="col-lg-6  padrighttno formmar" style="position: relative; top: 4px;">
                          <div class="col-lg-12 sliderrange padno martp10">
                         
                          <div class="col-lg-10 padrighttno slde_answ"><div class="slider ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" aria-disabled="false"><a href="#" class="ui-slider-handle ui-state-default ui-corner-all" style="left: 0%;"></a></div></div>
                          <div class="col-lg-2 padno yrs_answ" style="text-align:right"><label><span style="color:#4f4f4f">50%</span></label></div>                 
                        </div>
                      </div>
                       <div class="clearfix"></div>
                       <div class="col-lg-12 padno formmar">
                        <button class="btn btn-primary margnrightbtn btn-block"><i class="icon-plus"></i> ADD ANOTHER ANSWER (MAX 5)</button>
                      </div>
                      <div class="col-lg-12 padno formmar">
                        <button class="btn ashbtn margnrightbtn btn-block"><span style=" font-size: 17px;">+</span> ADD ANOTHER MODULE</button>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <div class="col-lg-12 padno formmar" style="margin-top:15px">
                    <button class="btn btn-primary pull-left btn_black">REMOVE LANGUAGE</button>
                    <button class="btn btn-primary pull-right ">SAVE &amp; NEXT</button>
                    <button class="btn btn-primary pull-right margnrightbtn cancelbtn">CANCEL</button>
                  </div>

              </div>
               
          </div>

          <?php include_once( 'sidebar.tpl.php'); ?>
       <!-- Container area end-->
        <div class="clearfix"></div>

      </div>