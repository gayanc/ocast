<div class="container">

      <!-- Container area -->
        <div class="col-lg-12 col8top">
        
          <h1 class="h1mar">Admin users</h1>
        </div>
          <div class="col-lg-8 " id="backend">
          <div class="border-tp"></div>
                  <h6>
                      <p class="col-lg-12 padno">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel congue metus. Ut ac mattis nibh. Sed sagittis facilisis diam nec tempor. Duis luctus tincidunt tellus, ut aliquam risus pulvinar ut. Aliquam erat volutpat. Ut nisl nunc, eleifend sed aliquet in, mi. Etiam auct.</p>
                  </h6>
              <div class="clearfix"></div>
              <h5 class="titl">add new admin user</h5>
              <div class="border-tp"></div>
              <div class="col-lg-12 padno formmar">    
                <form role="form">
                        <div class="col-lg-6 padno padleftno">
                            <div class="form-group">
                              <label>First name</label>
                              <input type="text" class="form-control" id="Inputnme">
                            </div>
                        </div>
                         <div class="col-lg-6 padrighttno">
                            <div class="form-group">
                              <label>Surname</label>
                              <input type="text" class="form-control" id="Inputsname">  
                            </div>
                          </div>
                        <div class="col-lg-12 padno padleftno">
                            <div class="form-group">
                               <label>E-mail</label>
                               <input type="text" id="Inputmail" class="form-control">
                            </div>
                        </div>
                         <div class="col-lg-6 padno padleftno">
                              <div class="form-group">
                                <label>Password</label>
                                <input type="password" id="Inputpass1" class="form-control">
                              </div>
                          </div>
                           <div class="col-lg-6 padrighttno">
                            <div class="form-group">
                              <label>Confirm password</label>
                              <input type="password" class="form-control" id="Inputpass1">  
                            </div>
                          </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 padno formmar">
                            <button class="btn ashbtn margnrightbtn btn-block" style="margin-bottom:15px"><span style=" font-size: 17px;">+</span> ADD ANOTHER ADMIN USER</button>
                        </div>
                       </form>
              </div>
              <div class="clearfix"></div>
              <div class="col-lg-12 padno formmar">   
                   <h5 class="titl">active admin users</h5>
                   <div class="border-tp"></div>
                   <table class="adminuser table fontsizerm ">
                    <tbody>
                      <tr  class="tableborbtm">
                        <td class="comnmedsiz">Peter Abrahamsson</td>
                        <td class="semibold font11"><a href="mailto:peter.abrahamsson@lorem.se">peter.abrahamsson@lorem.se</a></td>
                        <td class="pull-right"><div class="tagdiv inactive">Super Admin</div></td>
                        <td><div class="tagdiv">User</div></td>
                        <td><h6 class="fntsmll marginno"> <a href="">Edit</a> |  <a href="">Remove</a></h6></td>
                      </tr>
                       <tr class="tableborbtm">
                        <td class="comnmedsiz">Emil Karlsson</td>
                        <td class="semibold font11"><a href="mailto:peter.abrahamsson@lorem.se">emil.arlsson@lorem.se</a></td>
                        <td class="pull-right"><div class="tagdiv inactive">Super Admin</div></td>
                        <td><div class="tagdiv">User</div></td>
                        <td><h6 class="fntsmll marginno"> <a href="">Edit</a> |  <a href="">Remove</a></h6></td>
                      </tr>
                       <tr class="tableborbtm">
                        <td class="comnmedsiz">David Leinar</td>
                        <td class="semibold font11"><a href="mailto:peter.abrahamsson@lorem.se">david.leinar@lorem.se</a></td>
                        <td class="pull-right"><div class="tagdiv inactive">Super Admin</div></td>
                        <td><div class="tagdiv">User</div></td>
                        <td><h6 class="fntsmll marginno"> <a href="">Edit</a> |  <a href="">Remove</a></h6></td>
                      </tr>
                    </tbody>
                  </table>
              </div>
              <div class="col-lg-12 padno formmar2 ">                   
                    <button class="btn btn-primary pull-right ">SAVE</button>
                    <button class="btn btn-primary pull-right margnrightbtn cancelbtn">CANCEL</button>
              </div>
       

              <div class="clearfix"></div>
               
          </div>
          <?php include_once( 'sidebar.tpl.php'); ?>
       <!-- Container area end-->
        <div class="clearfix"></div>

      </div>