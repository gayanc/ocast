<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>OCAST</title>

    <!--[if (gt IE 8) | (IEMobile)]><!-->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/step3.css'); ?>">
    <link href="<?php echo base_url('assets/css/bootstrap.css'); ?>" rel="stylesheet" media="screen">
    <link href="<?php echo base_url('assets/css/stylesheet.css'); ?>" rel="stylesheet" media="screen">
    <link href="<?php echo base_url('assets/css/font-awesome.css'); ?>" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    <!--<![endif]-->
    <!--[if (lt IE 9) & (!IEMobile)]>
    <link rel="stylesheet" href="css/ie.css">
    <![endif]-->
    <style type="text/css">

/*      .em_div{width: 424px;}*/

      @media screen and (max-width: 991px) {  
/*        .em_div{width: 266px;}*/

      }
    </style>
</head>
<body style="background:#eee">




    <?php include 'application/views/_templates/header.tpl.php'; ?>

    <div id="main" role="main" style="background:#eee;border: none;padding-top: 150px;">

        <div id="filtertpbx"></div>

        <div class="container" id="loginscr">
          <h5 class="titl" style="text-transform:none;border:0"><span class="lin"> | </span> Login</h5>
          <div class=" well col-lg-12 wellpad">
             <?php  if($this->session->flashdata('error')):  ?>
                <div class="fntsmll erroemsg"><i class="icon-exclamation-sign icn"></i> <?php echo $this->session->flashdata('error');  ?></div>
             <?php endif  ?>
             
            <?php echo form_open('verifylogin'); ?>

              <div class="form-group">
                  <label>E-mail</label>
                  <input type="email" class="form-control" id="Inputemail" name="txtUserName">
              </div>
           <div class="form-group">
                  <label>Password</label>
                  <input type="password" class="form-control" id="Inputpasswrd" name="txtPassword">
              </div>
                  <label class="checkbox">
                    <input type="checkbox" name="remember" value="1" > <span class="fntsmll">Remember Me</span>
                  </label>
            <button type="submit" name="submit" class="btn btn-primary  pull-right">SIGN IN</button>
            <?php echo form_close(); ?> 
          </div>
          <span class="fntsmll pull-left"> <a href=""><i class="icon-angle-left"></i> Back to loremipsum.se</a> </span>
          <span class="fntsmll pull-right"> <a href="<?php echo base_url('user/reset_password'); ?>">Forgot Password?</a> <span style="font-size:10px;color: #DEDEDE;"> | </span>  <a href="<?php echo base_url('site/join'); ?>">Sign up</a> </span>
        </div>
    </div>



</div>
<!--/#inner-wrap-->
</div>

<!--/#outer-wrap-->
    <script src="<?php echo base_url('assets/js/modernizr.js'); ?>"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script src="<?php echo base_url('assets/js/main.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/custom.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
   

  


</body>
</html>