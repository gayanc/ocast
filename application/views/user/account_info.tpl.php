<div class="container">

      <!-- Container area -->
        <div class="col-lg-12 col8top">
        
          <h1 class="h1mar">Account Information</h1>
        </div>
          <div class="col-lg-8 " id="backend">
          <div class="border-tp"></div>
                  <h6>
                      <p class="col-lg-12 padno">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel congue metus. Ut ac mattis nibh. Sed sagittis facilisis diam nec tempor. Duis luctus tincidunt tellus, ut aliquam risus pulvinar ut. Aliquam erat volutpat. Ut nisl nunc, eleifend sed aliquet in, <a href="">blandit at</a> mi. Etiam auct.</p>
                  </h6>
              <div class="clearfix"></div>
              <h5 class="titl formmar">account information</h5>
              <div class="border-tp"></div>
              <div class="col-lg-12 padno formmar">           
                  <div class="col-lg-12 padno ">
                     <form role="form">
                      <div class="col-lg-12 padno padleftno">
                          <div class="col-lg-6 padno padleftno ">
                            <div class="form-group">
                              <label>What site do you represent?</label>
                              <input type="text" id="Inputsite" class="form-control">
                            </div>
                          </div>
                          <div class="col-lg-6 padrighttno">
                            <div class="form-group">
                              <label>Email</label>
                              <input type="email" id="Inputemail" class="form-control">
                            </div>
                          </div>
                          <div class="col-lg-6 padno padleftno ">
                            <div class="form-group">
                              <label>First name</label>
                              <input type="text" id="Inputfname" class="form-control">  
                            </div>
                          </div>
                          <div class="col-lg-6 padrighttno">
                            <div class="form-group">
                              <label>Last name</label>
                              <input type="text" id="Inputlname" class="form-control">
                            </div>
                          </div>
                          <div class="col-lg-6 padno padleftno ">
                             <div class="form-group">
                              <label>Password</label>
                              <input type="password" id="Inputchooseassword" class="form-control">
                            </div>
                          </div>
                          <div class="col-lg-6 padrighttno">
                            <div class="form-group">
                              <label>Confirm password</label>
                              <input type="password" id="InputConfirmpassword" class="form-control">
                            </div>
                          </div>


                      </div>
 
                     </form>
                  </div> 
              </div>
              <div class="col-lg-12 padno formmar2">
                    <button class="btn btn-primary pull-right ">SAVE &amp; NEXT</button>
                    <button class="btn btn-primary pull-right margnrightbtn">SAVE</button>
                    <button class="btn btn-primary pull-right margnrightbtn cancelbtn">CANCEL</button>
              </div>

              <div class="clearfix"></div>
               
          </div>
          <?php include_once( 'sidebar.tpl.php'); ?>
       <!-- Container area end-->
        <div class="clearfix"></div>

      </div>