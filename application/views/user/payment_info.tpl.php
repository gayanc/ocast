<div class="container">

      <!-- Container area -->
        <div class="col-lg-12 col8top">
        
          <h1 class="h1mar">Company & payment information</h1>
        </div>
          <div class="col-lg-8 " id="backend">
          <div class="border-tp"></div>
                  <h6>
                      <p class="col-lg-12 padno">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel congue metus. Ut ac mattis nibh. Sed sagittis facilisis diam nec tempor. Duis luctus tincidunt tellus, ut aliquam risus pulvinar ut. Aliquam erat volutpat. Ut nisl nunc, eleifend sed aliquet in, <a href="">blandit at</a> mi. Etiam auct.</p>
                  </h6>
              <div class="clearfix"></div>
              <h5 class="titl formmar">company information</h5>
              <div class="border-tp"></div>
                  <div class="col-lg-12 padno formmar">
                   <form role="form">
                    <div class="col-lg-12 padno padleftno">
                    <div class="col-lg-6 padno padleftno">
                      <div class="form-group">
                        <label >Company</label>
                        <input type="text" class="form-control" id="Company">
                      </div>
                    </div>
                    <div class="col-lg-6 padrighttno">
                      <div class="form-group">
                        <div class="col-lg-5 padno padleftno">
                          <label >Phone number</label>
                          <div class="btn-group selbx100 selctboxstyle">
                                    <div class="covr" data-toggle="dropdown">
                                      <div class="txt"> </div> 
                                        <div class="arow">
                                          <span class="caret"></span>
                                          <div class="clearfix"></div>
                                        </div>
                                      <div class="clearfix"></div>
                                    </div>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Sve (+43)</a></li>
                                    </ul>
                           </div>
                        </div>
                        <div class="col-lg-7 padrighttno ">
                          <label >&nbsp;</label>
                          <input type="text" id="Inputcity" class="form-control">
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>

                    <div class="col-lg-6 padno padleftno">
                      <div class="form-group">
                        <label >Address 1</label>
                        <input type="text" class="form-control" id="Inputadres1">  
                      </div>
                    </div>
                     <div class="col-lg-6 padrighttno">
                        <div class="form-group">
                          <label>Address 2</label>
                          <input type="text" class="form-control" id="Inputaddrs2">
                        </div>
                      </div>
                       <div class="col-lg-6 padno padleftno">
                         <div class="form-group">
                          <div class="col-lg-5 padno padleftno">
                            <label >Zip</label>
                            <input type="text" id="Inputzip" class="form-control">
                          </div>
                          <div class="col-lg-7 padrighttno citybox">
                            <label >City</label>
                            <input type="text" id="Inputcity" class="form-control">
                          </div>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                      <div class="col-lg-6 padrighttno">
                      <div class="form-group">
                        <label>Country</label>
                        <input type="text" class="form-control" id="InputCountry">
                      </div>
                    </div>

                </div>
                        
                   </form>
                   <div class="clearfix"></div>
                      <h5 class="titl formmar">payment information</h5>
                      <div class="border-tp"></div>
                      <div class="comnmedsiz formmar">Registered credit card</div>
                      <h6 class="marginno martp10 visatxt">Visa **** **** ****98 45</h6>
                      <div class="fntsmll"><a href="">Edit</a> | <a href="">Remove</a> </div>
                </div>
                <div class="col-lg-12 padno formmar">   
                   <h5 class="titl">invoices</h5>
                   <div class="border-tp"></div>
                   <table class="adminuser table fontsizerm ">
                    <tbody>
                      <tr class="tableborbtm">
                        <td class="comnmedsiz">01/09/13 – 30/09/13</td>
                        <td class="semibold font11">Amount: 995 SEK  </td>
                        <td class="semibold font11">Invoice No: SE-0000-0010</td>
                        <td><h6 class="fntsmll marginno"> <a href="">View invoice</a></h6></td>
                      </tr>
                       <tr class="tableborbtm">
                        <td class="comnmedsiz">01/01/13 – 30/09/13</td>
                        <td class="semibold font11">Amount: 995 SEK  </td>
                        <td class="semibold font11">Invoice No: SE-0000-0000</td>
                        <td><h6 class="fntsmll marginno"> <a href="">View invoice</a></h6></td>
                      </tr>
                       <tr  class="tableborbtm">
                        <td class="comnmedsiz">01/06/13 – 30/06/13</td>
                        <td class="semibold font11">Amount: 995 SEK  </td>
                        <td class="semibold font11">Invoice No: SE-00100-0010</td>
                        <td><h6 class="fntsmll marginno"> <a href="">View invoice</a></h6></td>
                      </tr>
                    </tbody>
                  </table>
              </div>
              <div class="col-lg-12 padno ">
                  <button class="btn ashbtn margnrightbtn btn-block"><span style=" font-size: 17px;">+</span> DISPLAY MORE INVOICES</button>
               </div>
              <div class="col-lg-12 padno formmar2 marbtnchnge">
              <button class="btn btn-primary pull-left btn_black">CANCEL SUBSCRIPTION</button>
                    <button class="btn btn-primary pull-right ">SAVE &amp; NEXT</button>
                    <button class="btn btn-primary pull-right margnrightbtn">SAVE</button>
                    <button class="btn btn-primary pull-right margnrightbtn cancelbtn">CANCEL</button>
              </div>

              <div class="clearfix"></div>
               
          </div>
          <?php include_once( 'sidebar.tpl.php'); ?>
       <!-- Container area end-->
        <div class="clearfix"></div>
      </div>