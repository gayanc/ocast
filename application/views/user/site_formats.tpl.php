<div class="container">

      <!-- Container area -->
        <div class="col-lg-12 col8top">
        
          <h1 class="h1mar">Formats & products</h1>
        </div>
          <div class="col-lg-8 " id="backend">
          <div class="border-tp"></div>
                  <h6>
                      <p class="col-lg-12 padno">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel congue metus. Ut ac mattis nibh. Sed sagittis facilisis diam nec tempor. Duis luctus tincidunt tellus, ut aliquam risus pulvinar ut. Aliquam erat volutpat. Ut nisl nunc, eleifend sed aliquet in, <a href="">blandit at</a> mi. Etiam auct.</p>
                  </h6>
              <div class="clearfix"></div>
                  <div class="col-lg-12 padno formts">
                    <div class="col-lg-4 padleftno formmar">
                        <div class="col-lg-12 thumbnilimg thumbvord"></div>
                          <div class="col-lg-12" style="border:1px solid #dbdbdb">
                             <div class="clearfix"></div>
                              <div class="martp10 checkbox padno">
                                  <p class="marginno colorthumb">BANNER</p>
                                  <h6 class="marginno"><i>Format:</i> <span class="colr">1250 x 360</span></h6>
                                  <h6 class="marginno"><i>Pricetype:</i> <span class="colr">Request Price</span></h6>
                                  <h6 class="marginno"><i>Price:</i> <span class="colr">9 020 SEK</span></h6>
                                  <div class="martp10 col-lg-12 padno" style="margin-bottom:10px">
                                      <div class=" pull-left fntsmll">
                                        <a href="">Edit</a> <span style="color:#e1e1e1">|</span> <a href="">Remove</a> 
                                      </div>
                                      <div class="clearfix"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>                             
                                  <div class="clearfix"></div>
                            </div>
                                <div class="clearfix"></div>
                      </div>
                       <div class="col-lg-4 midbxclz formmar">
                        <div class="col-lg-12 thumbnilimg thumbvord"></div>
                          <div class="col-lg-12" style="border:1px solid #dbdbdb">
                             <div class="clearfix"></div>
                              <div class="martp10 checkbox padno">
                                  <p class="marginno colorthumb">BANNER</p>
                                  <h6 class="marginno"><i>Format:</i> <span class="colr">1250 x 360</span></h6>
                                  <h6 class="marginno"><i>Pricetype:</i> <span class="colr">Request Price</span></h6>
                                  <h6 class="marginno"><i>Price:</i> <span class="colr">9 020 SEK</span></h6>
                                  <div class="martp10 col-lg-12 padno" style="margin-bottom:10px">
                                      <div class=" pull-left fntsmll">
                                        <a href="">Edit</a> <span style="color:#e1e1e1">|</span> <a href="">Remove</a> 
                                      </div>
                                      <div class="clearfix"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>                             
                                  <div class="clearfix"></div>
                            </div>
                                <div class="clearfix"></div>
                      </div>
                       <div class="col-lg-4 padrighttno formmar">
                        <div class="col-lg-12 thumbnilimg thumbvord"></div>
                          <div class="col-lg-12" style="border:1px solid #dbdbdb">
                             <div class="clearfix"></div>
                              <div class="martp10 checkbox padno">
                                  <p class="marginno colorthumb">BANNER</p>
                                  <h6 class="marginno"><i>Format:</i> <span class="colr">1250 x 360</span></h6>
                                  <h6 class="marginno"><i>Pricetype:</i> <span class="colr">Request Price</span></h6>
                                  <h6 class="marginno"><i>Price:</i> <span class="colr">9 020 SEK</span></h6>
                                  <div class="martp10 col-lg-12 padno" style="margin-bottom:10px">
                                      <div class=" pull-left fntsmll">
                                        <a href="">Edit</a> <span style="color:#e1e1e1">|</span> <a href="">Remove</a> 
                                      </div>
                                      <div class="clearfix"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>                             
                                  <div class="clearfix"></div>
                            </div>
                                <div class="clearfix"></div>
                      </div>
                  </div>
                  <div class="col-lg-12 padno formts">
                    <div class="adformat">
                        <div class="col-lg-4  formmar padleftno formt">
                            <div class="col-lg-12 thumbnilimg"></div>
                            <div class="col-lg-12 padno martp10">
                               <button class="btn ashbtn margnrightbtn btn-block"><span style=" font-size: 17px;">+</span> ADD FORMAT</button>
                            </div>
                        </div>
                      </div>
                      <div class="addscreen">
                        <div class="col-lg-4 midbxclz formmar screenadd">
                            <div class="col-lg-12 thumbnilimg">
                                <div class=" fntsmll addscrnshtxtxt">
                                    <a href=""><span class="plstxt">+</span> Add screenshot</a> 
                                  </div>
                            </div>
                             <div style="background:#f5f5f5" class="col-lg-12">
                             <div class="clearfix"></div>
                                <div class="martp10">
                                  <div class="btn-group selbx100 selctboxstyle selbx100sml">
                                  <div data-toggle="dropdown" class="covr">
                                    <div class="txt"> Type</div> 
                                      <div class="arow">
                                        <span class="caret"></span>
                                        <div class="clearfix"></div>
                                      </div>
                                    <div class="clearfix"></div>
                                  </div>
                                  <ul class="dropdown-menu">
                                      <li><a href="#">Type</a></li>
                                  </ul>
                                  </div>
                                </div>
                                <div class="martp10">
                                    <input type="text"  placeholder="Format" class="form-control h825" id="Inputthm">
                                </div>
                                <div class="martp10">
                                  <div class="btn-group selbx100 selctboxstyle selbx100sml">
                                  <div data-toggle="dropdown" class="covr">
                                    <div class="txt"> Priceype</div> 
                                      <div class="arow">
                                        <span class="caret"></span>
                                        <div class="clearfix"></div>
                                      </div>
                                    <div class="clearfix"></div>
                                  </div>
                                  <ul class="dropdown-menu">
                                      <li><a href="#">Priceype</a></li>
                                  </ul>
                                  </div>
                                </div>
                                 <div class="martp10">
                                    <input type="text" placeholder="Price" class="form-control h825" id="Inputsek">
                                </div>
                                <div style="margin-bottom:10px" class="martp10 col-lg-12 padno">
                                  <div class=" pull-left fntsmll  canclbnmar">
                                    <a href="">Cancel</a> 
                                  </div>
                                  <div class=" pull-right">
                                    <button class="btn btn-small btn-primary smlbtn" type="button"> SAVE FORMAT</button>
                                  </div><div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                             </div>

                        </div>
                        </div>
                        <div class="col-lg-4 padrighttno formmar">
                            <div class="col-lg-12 thumbnilimg thumbvord"></div>
                             <div class="col-lg-12" style="border:1px solid #dbdbdb">
                             <div class="clearfix"></div>
                                <div class="martp10 checkbox padno">
                                  <p class="marginno colorthumb">BANNER</p>
                                  <h6 class="marginno"><i>Format:</i> <span class="colr">1250 x 360</span></h6>
                                  <h6 class="marginno"><i>Pricetype:</i> <span class="colr">Request Price</span></h6>
                                  <h6 class="marginno"><i>Price:</i> <span class="colr">9 020 SEK</span></h6>
                                  <div class="martp10 col-lg-12 padno" style="margin-bottom:10px">
                                    <div class=" pull-left fntsmll">
                                      <a href="">Edit</a> <span style="color:#e1e1e1">|</span> <a href="">Remove</a> 
                                    </div>
                                    <div class="clearfix"></div>
                                  </div>
                                  <div class="clearfix"></div>
                                </div>                             
                                  <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                             </div>
                              <div class="adscreenapnd"></div>
                             <div class="adformatapnd"></div>

                        </div>
              <div class="col-lg-12 padno formmar2 marbtnchnge">
                    <button class="btn btn-primary pull-right ">SAVE &amp; NEXT</button>
                    <button class="btn btn-primary pull-right margnrightbtn">SAVE</button>
                    <button class="btn btn-primary pull-right margnrightbtn cancelbtn">CANCEL</button>
              </div>

              <div class="clearfix"></div>
               
          </div>
          <?php include_once( 'sidebar.tpl.php'); ?>
       <!-- Container area end-->
        <div class="clearfix"></div>

      </div>