  <body style="background-color: rgb(238, 238, 238); background-position: initial initial; background-repeat: initial initial;">
  <div style="background:#eee">
        <div id="filtertpbx"></div>

        <div class="container" id="loginscr">
          <h5 class="titl" style="text-transform:none;border:0"><span class="lin"> | </span> Login</h5>
          <div class=" well col-lg-12 wellpad">
             <?php  if($this->session->flashdata('error')):  ?>
                <div class="fntsmll erroemsg"><i class="icon-exclamation-sign icn"></i> <?php echo $this->session->flashdata('error');  ?></div>
             <?php endif  ?>
             
            <?php echo form_open('verifylogin'); ?>

              <div class="form-group">
                  <label>E-mail</label>
                  <input type="email" class="form-control" id="Inputemail" name="txtUserName">
              </div>
           <div class="form-group">
                  <label>Password</label>
                  <input type="password" class="form-control" id="Inputpasswrd" name="txtPassword">
              </div>
                  <label class="checkbox">
                    <input type="checkbox" name="remember" value="1" > <span class="fntsmll">Remember Me</span>
                  </label>
            <button type="submit" name="submit" class="btn btn-primary  pull-right">SIGN IN</button>
            <?php echo form_close(); ?> 
          </div>
          <span class="fntsmll pull-left"> <a href=""><i class="icon-angle-left"></i> Back to loremipsum.se</a> </span>
          <span class="fntsmll pull-right"> <a href="">Forgot Password?</a> <span style="font-size:10px;color: #DEDEDE;"> | </span>  <a href="">Sign up</a> </span>
        </div>
    </div>
    </body>