<div class="container">

      <!-- Container area -->
        <div class="col-lg-12 col8top">
        
         <h1 class="h1mar">Overview</h1>
         </div>

          <div class="col-lg-8 " id="backend">
          <div class="border-tp"></div>
             <div class="row">
               <div class="col-lg-12 formmar padno">
                  <div class="col-lg-2 padno imgcontactmob" style="width:90px"><img src="<?php echo base_url('assets/images/ovrvwlogo.gif')?>"></div>
                  <div class="col-lg-10 padno "><h1 class="marginno bl sansbold h1mob">Livingladolcevita.org </h1></div>
               </div>
              </div>

              <div class="clearfix"></div>
              <h5 class="titl formmar2">Ocast updates
              </h5>

              <div class="border-tp"></div>
              <div class="col-lg-12 padno formmar">
                   <img class="img-responsive" src="<?php echo base_url('assets/images/blankimg.jpg')?>">
                    <h5 class="h5bldclr nrm"> 
                      Sed molestie adipiscing neque et sodales
                   </h5>             
                   <h6>
                    <p class="col-lg-12 padno">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel congue metus. Ut ac mattis nibh. Sed sagittis facilisis diam nec tempor. Duis luctus tincidunt tellus, ut aliquam risus pulvinar ut. Aliquam erat volutpat. Ut nisl nunc, eleifend sed aliquet in, blandit at mi. Etiam auct... <a href="">Read more</a></p>
                  </h6>
              </div>
               <div class="col-lg-12 padno formmar ovrvw">
                <h5 class="titl formmar">Your latest updates </h5>
                  <div class="panel panel-default active">
                    <div class="panel-body">
                      <div class="col-lg-3  lu1 sansbold">Contact person</div>
                      <div class="col-lg-6 adtxt padno  lu2">Latest update: <a href="">You haven’t added any contact persons</a></div>
                      <div class="col-lg-3 adtxt lu3" style="text-align:right"> <a href="">Add now</a></div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="panel panel-default active">
                    <div class="panel-body">
                      <div class="col-lg-3 lu1 sansbold">Contact person</div>
                      <div class="col-lg-6 adtxt padno lu2">Latest update: <a href="">You haven’t added any contact persons</a></div>
                      <div class="col-lg-3 adtxt lu3" style="text-align:right"> <a href="">Add now</a></div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="panel panel-default active">
                    <div class="panel-body">
                      <div class="col-lg-3 lu1 sansbold">Contact person</div>
                      <div class="col-lg-6 adtxt padno lu2">Latest update: <a href="">You haven’t added any contact persons</a></div>
                      <div class="col-lg-3 adtxt lu3" style="text-align:right"> <a href="">Add now</a></div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                   <div class="panel panel-default ">
                    <div class="panel-body">
                      <div class="col-lg-3 lu1 sansbold">Demographics</div>
                      <div class="col-lg-4 adtxt padno lu2">Latest update: <a href="">Changed age ratio</a></div>
                      <div class="col-lg-3 adtxt lu2">17 June 2013, 11:46 AM</div>
                      <div class="col-lg-2 adtxt lu3" style="text-align:right"> <a href="">Update now</a></div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="panel panel-default ">
                    <div class="panel-body">
                      <div class="col-lg-3 lu1 sansbold">Demographics</div>
                      <div class="col-lg-4 adtxt padno lu2">Latest update: <a href="">Changed age ratio</a></div>
                      <div class="col-lg-3 adtxt lu2">17 June 2013, 11:46 AM</div>
                      <div class="col-lg-2 adtxt lu3" style="text-align:right"> <a href="">Update now</a></div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
               </div>
              <div class="clearfix"></div>
               
          </div>
         <?php include_once( 'sidebar.tpl.php'); ?>
       <!-- Container area end-->
        <div class="clearfix"></div>

      </div>