<?php
$CI = get_instance();

$site_name = isset($site_data['master_data'][0]['SiteName']) ? $site_data['master_data'][0]['SiteName']: '' ;
$logo = isset($site_data['master_data'][0]['Logo']) ? $site_data['master_data'][0]['Logo']: '' ;
$id = isset($site_data['master_data'][0]['ID']) ? $site_data['master_data'][0]['ID']: '' ;
$average_age = isset($site_data['master_data'][0]['AverageAge']) ? $site_data['master_data'][0]['AverageAge'] : '';
$mail_percent = isset($site_data['master_data'][0]['MalePercentage']) ? $site_data['master_data'][0]['MalePercentage'] : '';
$description = isset($site_data['master_data'][0]['Description']) ? $site_data['master_data'][0]['Description'] : '';
$audience_desc = isset($site_data['master_data'][0]['Audience']) ? $site_data['master_data'][0]['Audience'] : '';
$sldrAgeval = isset($site_data['master_data'][0]['AverageAge']) ? $site_data['master_data'][0]['AverageAge'] : 50;
$sldrMnval = isset($site_data['master_data'][0]['MalePercentage']) ? $site_data['master_data'][0]['MalePercentage'] : 50;

$site_email = $CI->session->userdata['userDetails']['UserName'];

//company data
$company_data = isset($site_data['company_data']) ? $site_data['company_data'] : array();
$cp_name = isset($company_data[0]['Name']) ? $company_data[0]['Name'] : '';
$cp_addr1 = isset($company_data[0]['Address1']) ? $company_data[0]['Address1'] : '';
$cp_addr2 = isset($company_data[0]['Address2']) ? $company_data[0]['Address2'] : '';
$cp_city = isset($company_data[0]['City']) ? $company_data[0]['City'] : '';
$cp_cnt_code = isset($company_data[0]['CountryCallingCode']) ? $company_data[0]['CountryCallingCode'] : '';
$cp_zip = isset($company_data[0]['Zip']) ? $company_data[0]['Zip'] : '';
$cp_phone = isset($company_data[0]['Phone']) ? $company_data[0]['Phone'] : '';
$cp_cntry = isset($company_data[0]['CountryID']) ? $company_data[0]['CountryID'] : '';


$CI->load->model('Country_Model');
$cp_cntry = $CI->Country_Model->getCountryLangName($cp_cntry);
$cp_cntry = isset($cp_cntry[0]['CountryName']) ? $cp_cntry[0]['CountryName'] : '';

//Desktop data
$contactData = isset($site_data['contact_data']) ? $site_data['contact_data'] : array();
$noOfContacts = (count($contactData)) ? count($contactData)+1 : 2; //module counter for js
$contryData = isset($site_data['country_data']) ? $site_data['country_data'] : array();
$categoryData = isset($site_data['category_data']) ? $site_data['category_data'] : array();
$qfactData = isset($site_data['qfact_data']) ? $site_data['qfact_data'] : array(); 
$testm_data = isset($site_data['testm_data']) ? $site_data['testm_data'] : array();
$demog_data  = isset($site_data['demog_data']) ? $site_data['demog_data'] : array();
$case_data  = isset($site_data['case_data']) ? $site_data['case_data'] : array();
$format_data = isset($site_data['format_data']) ? $site_data['format_data'] : array();
?>
<div class="container">
    <!-- Container area -->
    <div class="col-lg-12 col8top">
        <h1 class="h1mar"><?php echo lang('stp_6_add_yrsite');?></h1>
    </div>
    <div class="col-lg-8 ">
        <div class="border-tp"></div>
        <?php include_once('wizard_step.tpl.php');?>
        <div class="clearfix"></div>   
    </div>
      <?php if(isset($userDetails['AciveStatus']) && $userDetails['AciveStatus'] == 'pending'){?>
              <?php include('account_message.tpl.php');?>
      <?php }else if(isset($userDetails['AciveStatus']) && $userDetails['AciveStatus'] == 'active'){?>
    <div class="col-lg-12 ">
        <div class="col-lg-6 padno">
            <h4 class="pull-left sansbold" style="color:#6d6d6d"><?php echo lang('stp_6_preview_desc');?> </h4>
            <div class="clearfix"></div> 
        </div>
        <div class="col-lg-6 padno">
            <button class="btn btn-primary pull-right "><?php echo lang('stp_6_submit');?></button>
            <button class="btn btn-primary pull-right margnrightbtn" onclick="javascript:window.location = ('add_cases')"><?php echo lang('stp_6_gobk_edit');?></button>
            <div class="clearfix"></div> 
        </div>
        <div class="clearfix"></div>  
    </div>
    <?php }?>
    <!-- Container area end-->
    <div class="clearfix"></div>




</div>
<div class="col-lg-12  formmar" style="border-top: 1px solid #E2E2E2;">
    <div class="container" style="margin-top:0">
        <div class="col-lg-12 catgr">
            <div class="col-lg-8 padno  slectd" style="padding-top:17px">
                <div class="pull-left fntsmll sansbold categorymob " style="margin-right:10px;color:#BFBFBF"><?php echo lang('stp_6_categories');?></div>
                <?php 

                foreach ($categoryData as $key => $category) {?> 
                <div><p><?php echo $category[0]["Description"]; ?></p></div>
                <?php } ?>   
                <div class="clearfix"></div>                
            </div>
            <div class="col-lg-4 padno  slectd">
                <div class="pull-right addfav"><a href="#"><?php echo lang('stp_6_add_2_favo');?>  <i class="icon-heart icnhrt"></i> </a></div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="jumbotron">
    <div class="container">
        <div class="col-lg-12">
            <h1 class="pull-left"><?php echo $site_name; ?><a id="popovr" class="icninfo icnnew" data-placement="top" data-title="<?php echo lang('stp_6_site_toltip');?>" data-toggle="tooltip" href="#" data-original-title="" title="" style="top: -5px;left: -2px;display:block"></a></h1>
            <div class="clearfix"></div>
            <p class="col-lg-8 padno"><?php echo substr($description,0,200).'...';?></p>
            <div class="clearfix"></div>
            <p><a href=""><?php echo lang('stp_6_more');?> <i class="icon-caret-down"></i></a></p>
        </div>
    </div>
</div>
<div class="container" id="adsit">
    <div class="col-lg-12">
        <ul id="myTab" class="nav nav-tabs"  >
            <li class="contsales"><a href="#contact" data-toggle="tab"><?php echo lang('stp_6_contct_sale');?> <i style="font-size: 14px;margin-left: 5px;" class="icon-envelope tabmob"></i></a></li>
            <li class=""><a href="#case" data-toggle="tab"><?php echo lang('stp_6_case');?></a></li>
            <li class="active"><a href="#mobile" data-toggle="tab"><?php echo lang('stp_6_mobile');?></a></li>
            <li class=""><a href="#desktop" data-toggle="tab"><?php echo lang('stp_6_desktop');?></a></li>
        </ul>
    </div>
    <div id="myTabContent" class="tab-content">
        <?php require_once('desktop_view.tpl.php'); ?>
        <?php require_once('mobile_view.tpl.php'); ?>
        <div class="tab-pane fade" id="case">
            <?php if(count($case_data)){
                foreach ($case_data as $key => $value) { 
                    if($key<1){ //show the latest modified first case
                                $title = $value['Title'];
                                $description = $value['Description'];
                                $image = (isset($value['Image']) && $value['Image'] !='') ? $value['Image'] : 'blankimg.jpg';
                    ?>
                   <div class="col-lg-8  caseright">
                        <h4 class="h5bldclr nrm formmar"><?=$title?></h4>  
                        <img src="<?php echo base_url('uploads/case/'.$image)?>" class="img-responsive">
                        <h6>
                            <p class=""><?=$description?></p>
                        </h6>
                    </div>
                
            <?php }}}?>
            
            <div class="col-lg-4 rightsidsm">
                <div class="border-tp"></div>
                <h5 class="titl"><?php echo lang('stp_6_allcases');?></h5>
                <div class="box_line">
                    <?php if(count($case_data)){ 
                            foreach ($case_data  as $key => $value) {
                                $title = $value['Title'];
                                $description = $value['Description'];
                                $image = (isset($value['Image']) && $value['Image'] !='') ? $value['Image'] : 'demothum2.jpg';
                            ?>
                            <div class="col-lg-12 padno ">
                                <img alt="Responsive image" class="img-responsive" src="<?php echo base_url('uploads/case/'.$image);?>">
                                <div class="clearfix"></div>
                                <label class="marginno martp10 headtit1"><?=$title?></label>
                                <h6 class="fntsmll marginno"><i><?=$description?></i></h6>
                            </div>
                    <?php }} ?>
                </div>

            </div>
        </div>
        <div class="tab-pane fade" id="contact">
            <div class="container">
                <div class="col-lg-12 col8top">

                    <h1 class="h1mar"><?php echo lang('stp_6_contact');?></h1>
                </div>

                <div class="col-lg-8 ">
                    <div class="border-tp"></div> 

                    <h6 class="formmar2 marginno" ><label class="marginno"><?=$cp_name?>.</label>
                        <p class="marginno"><?=$cp_addr1?>,<br>
                            <?=$cp_addr2?><br>
                            <?=$cp_city?>,
                            <?=$cp_cntry?></p>
                        <p><a href="mailto:<?=$site_email?>"><?=$site_email?></a></p>
                    </h6>
                    <div id="contabrderbtm" class="page-header padno">
                        <h5  class="titl"><?php echo lang('stp_6_people');?></h5>
                    </div>
                    <?php 
                    if(count($contactData)){ 
                        foreach ($contactData as $key => $value) { 
                            $fname = $value['Fname'];
                            $lname = $value['Lname'];
                            $email = $value['Email'];
                            $concatNumber = $value['ConcatNumber'];
                            $profilePic = ($value['ProfilePic'] !='') ? $value['ProfilePic'] : 'Ocast_Contact_03.jpg';
                            $countryCode = $value['ContactCountryCode'];

                            $CI = get_instance();
                            $CI->load->model('Country_Model');
                            $countryCode = $CI->Country_Model->getCountryCallingIDByCode($countryCode);
                            $countryCode = isset($countryCode[0]['CallingCode']) ? '+'.$countryCode[0]['CallingCode'] : '';

                            $designation    = $value['Designation'];
                    ?>
                            <div class="panel panel-default imgcontactbx">
                                <div class="panel-body">
                                    <div class="col-lg-2 nospace imgcontactmob">
                                        <img src="<?php echo base_url('uploads/contact/'.$profilePic)?>" width="90px" height="90px">
                                    </div>
                                    <div class="col-lg-9 nospace">
                                        <div class="nam sansbold"><?=strtoupper($fname)?>&nbsp;<?=strtoupper($lname)?></div>
                                        <h6 class="designat"><?=$designation?></h6>
                                        <?php if($email){?> <h6><a href="mailto:<?=$email?>"> <i class="icon-envelope icnico"></i> <?=$email?></a></h6><?php }?> 
                                        <?php if($concatNumber){?><h6><i class="icon-comments icnico"></i> <?=$countryCode?> &nbsp;<?=$concatNumber?></h6>  <?php }?>                      
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                    <?php }}?>
                    
                </div>
                <?php include_once('contact_from.tpl.php') ;?>
                <div class="clearfix"></div>
            </div>
        </div>

    </div>
</div>
