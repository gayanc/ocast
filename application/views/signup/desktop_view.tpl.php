<div class="tab-pane fade" id="desktop">

            <div class="col-lg-12 formmar ">
                <div class="col-lg-5 padno bordtp">
                    <h5 class="titl"><?php echo lang('stp_6_stats');?></h5>
                    <div class="borderbtm"></div>
                </div>
                <div class="col-lg-7 padno">
                    <div class="col-lg-4 martp10 pull-left datecs padno">
                        <div style="color:#bfbfbf;text-align:right" class="fontsizerm semibold"><?php echo lang('stp_6_chose_time');?>
                            <a style="margin-top:0;display:block" title="" data-original-title="" href="#" data-toggle="tooltip" data-title="<?php echo lang('stp_6_new_tool_tip');?>" data-placement="bottom" class="icninfo " id="popovr"><i class="icon-info"></i></a>
                        </div>
                    </div>
                    <div class="padno martp10 pull-right" style="margin-left:10px">
                        <div class=" caltxt"><a href="#">4th March 2013 </a></div>
                    </div>
                    <div class="padno martp10 pull-right" style="margin-right:10px">
                        <div class=" caltxt"><a href="#">4th March 2013 </a></div>
                    </div>


                </div>
                <div class="clearfix"></div>
                <div class="reponsivetbl">
                    <table >
                        <thead>
                            <tr>
                                <th ><?=$cp_name?></th>
                                <th><?php echo lang('stp_6_unique');?></th>
                                <th class=""><?php echo lang('stp_6_visit');?></th>
                                <th><?php echo lang('stp_6_pviews');?></th>
                                <th class=""><?php echo lang('stp_6_pv');?></th>
                                <th class=""><?php echo lang('stp_6_uv');?></th>
                                <th class="">%</th>
                                <th class="hidetr"><i class="icon-heart"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="tdstyle"><div style="float:left"><img src="<?php echo base_url('assets/images/logotab.jpg')?>"></div>
                                    <div class="tdtitlbt"><h5 class="marginno txtcenttbl"><?=$cp_name?>
                                            <a title="" data-original-title="" href="#" data-toggle="tooltip" data-title="<?php echo lang('stp_6_site_toltip');?>" data-placement="top" class="icninfo icnnew asty" id="popovr" style="display:block"></a>
                                        </h5>
                                        <h6 class="fntsmll marginno"><i><?php echo lang('stp_6_sml_desc');?></i></h6></div>
                                </td>
                                <td >999999</td>
                                <td>1 300</td>
                                <td class="">92 132</td>
                                <td>8.8</td>
                                <td class="">4.76</td>
                                <td class="">+100</td>
                                <td class="hidetr"><i class="icon-heart"></i></td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-lg-12 padno formmar chartmob">
                <div class="col-lg-3 ">
                    <div class="btn-group selbx100 selctboxstyle">
                        <div class="covr" data-toggle="dropdown">
                            <div class="txt">Unique browsers </div> 
                            <div class="arow">
                                <span class="caret"></span>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <ul class="dropdown-menu">
                            <li><a href="#">Unique browsers</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-12  ">
                    <img src="<?php echo base_url('assets/images/samplechart.jpg')?>" class="img-responsive">
                </div> 
            </div>
            <div class="col-lg-12 padno formmar ">
                <div class="col-lg-4 ">
                    <div class="border-tp"></div>
                    <h5 class="titl ageico"><?php echo lang('stp_6_age');?> &amp; <?php echo lang('stp_6_gender');?></h5>
                    <div class="box_line sansbold">
                        <div class="col-lg-6 gndrbx" style="border:0">
                            <h6 class="fontsizerm" style="margin-bottom:0"><?php echo lang('stp_6_women');?> </h6>
                            <i class="icon-female gndrtxt"></i><span class="gntdrtxt2"> <?php echo 100-round($mail_percent);?>%</span> 
                        </div>
                        <div class="col-lg-6 gndrbx">
                            <h6 class="fontsizerm" style="margin-bottom:0"><?php echo lang('stp_6_men');?></h6>
                            <i class="icon-male gndrtxt"></i><span class="gntdrtxt2"> <?php echo round($mail_percent); ?>%</span>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 avgagetxt"> <?php echo lang('stp_6_avgage');?> <span class="avgagecir"><?php echo round($sldrAgeval);?></span>
                        </div>
                        <div class="clearfix"></div>                      
                    </div>
                </div>
                <div class="col-lg-8 audincetxt">
                    <div class="border-tp"></div>
                    <h5 class="titl"><?php echo lang('stp_6_abt_audience');?></h5>
                    <div class="box_line">
                        <h6 class="marginno"><p style="border-bottom:0"><?php echo $audience_desc;?></p></h6>
                    </div>
                </div>

                <div class="clearfix"></div>

                <?php 
                if(count($qfactData)){ echo count($qfactData);?>
                <div class="col-lg-4 ">
                    <div class="border-tp"></div>
                    <h5 class="titl"><?php echo lang('stp_6_abt_quickfact');?></h5>
                    <div class="box_line">
                        <ul class="boxli">
                             <?php foreach ($qfactData as $key => $value){
                            $text = isset($value['Text']) ? $value['Text'] : '';
                            if($text){
                            ?>
                           <li> <span><?php echo $text; ?></span></li>                              
                            <?php }}?>
                        </ul>
                    </div>
                </div>
                <?php }?>

                <?php if(count($testm_data)){ ?>
                <div class="col-lg-4 testimonial">
                    <?php                     
                        foreach ($testm_data as $key => $value) { if($key<1){ //display only one record?>
                    <h5 style="text-transform:none;min-height: 127px;border:0" class="titl"><?=$value['Quote']?></h5>
                    <h6 class="fntsmll"><i>- <?=$value['ByWhom']?></i></h6>
                   <?php } } ?>
                </div>
                <?php }?>

                <?php if(count($demog_data)){
                    foreach ($demog_data as $key => $value) { 
                        $headline = $value->HeaderLine; ?>
                    <div class="col-lg-4 ">
                    <div class="border-tp"></div>
                    <h5 class="titl"><?=strtoupper($headline)?></h5>
                    <div class="box_line ">
                        <?php foreach ($value->HeaderData as $key => $hvalue) {
                             $answer = $hvalue['Answer'];
                             $point = $hvalue['points'];
                        ?>
                        <div class="padno boxspac relate">
                            <div class="col-lg-10 padno">
                                <div class="alert alert-info fontsizerm" style="width:<?=$point?>%"><?=$answer?></div>
                            </div>
                            <div class="col-lg-2 perctxt"><?=$point?>% </div>   
                            <div class="clearfix"></div>                
                        </div>
                        <?php }?>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <?php }  }?>

                <?php if(count($case_data)){ ?>
                <div class="col-lg-12 ">
                    <div class="border-tp"></div>
                    <h5 class="titl"><?php echo lang('stp_6_case');?></h5>
                    <div class="box_line ">
                            <?php
                            foreach ($case_data  as $key => $value) {
                                $title = $value['Title'];
                                $description = $value['Description'];
                                $image = (isset($value['Image']) && $value['Image'] !='') ? $value['Image'] : 'demothum2.jpg';
                            ?>
                            <div class="col-lg-4 padleftno  ">
                            <img alt="Responsive image" class="img-responsive" src="<?php echo base_url('uploads/case/'.$image);?>">
                            <div class="clearfix"></div>
                            <label class="marginno martp10 headtit1"><?=$description?></label>
                            <h6 class="fntsmll marginno"><i><?=$title?></i></h6>
                        </div>
                        <?php } ?>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <?php } ?>

                <?php if (count($format_data)) { ?>
                <div class="col-lg-12 ">
                    <div class="border-tp"></div>
                    <h5 class="titl"><?php echo lang('stp_6_formats_prdct');?></h5>
                    <div class="box_line padno">
                        <?php 
                            foreach ($format_data as $key => $value) {  
                                $title = isset($value->Type[0]['Title']) ? $value->Type[0]['Title'] : '';
                                $format = $value->Format;
                                $pricetype = isset($value->priceType[0]['Text']) ? $value->priceType[0]['Text'] : '';
                                $price = $value->price;
                                $image = ($value->image !='') ? $value->image : FORMAT_DEFAUL_IMG;
                        ?>
                                     <div class="col-lg-4 padleftno formmar">
                                        <div class="col-lg-12 thumbnilimg thumbvord padno"><img src="<?php echo base_url('uploads/product/'.$image)?>" class="img-responsive"></div>
                                        <div class="col-lg-12  ftmtsprdctbx">
                                            <div class="clearfix"></div>
                                            <div class="martp10 checkbox padno">
                                                <label class="marginno martp10 headtit1"><?=$title?></label>
                                                <h6 class="marginno"><i><?php echo lang('stp_6_format');?>:</i> <span class="colr"><?=$format?></span></h6>
                                                <h6 class="marginno"><i><?php echo lang('stp_6_pricetype');?>:</i> <span class="colr"><?=$pricetype?></span></h6>
                                                <h6 class="marginno"><i><?php echo lang('stp_6_price');?>:</i> <span class="colr"><?=$price?>SEK</span></h6>

                                                <div class="clearfix"></div>
                                            </div>                             
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                        <?php } ?>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <?php } ?>
            </div>

        </div>