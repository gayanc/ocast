<div class="container">

    <!-- Container area -->
    <div class="col-lg-12 col8top">

        <h1 class="h1mar"><?php echo lang('stp_2_connect'); ?></h1>
    </div>

    <div class="col-lg-8 ">
        <div class="border-tp"></div>

        <?php include_once('wizard_step.tpl.php');?>
        <div class="clearfix"></div>
        <h5 class="titl"><?php echo lang('stp_2_connect_yrsite'); ?>
            <a href="#" data-toggle="tooltip" data-title="<?php echo lang('stp_2_connect_yrsite_ttip'); ?> ." data-placement="left" class="icninfo" id="popovr"><i class="icon-info"></i></a>
        </h5>
        <div class="border-tp"></div>
        <form id="site_connected">
            <div class="col-lg-12 padno formmar"> 

                <?php
                if (!isset($isempty)) {
                    $isempty = 1;
                }
                if (!isset($message)) {
                    $message = "";
                }
                
               if (!isset($URL)) {
                    $URL = "";
                }
                
                if (!isset($SiteName)) {
                    $SiteName = "";
                }
                
                if ($isempty == 0) {
                    $site_list = $this->session->userdata('google_site_list');
                    ?>
                    <div class="form-group">
                        <label><?php echo lang('stp_2_site_name'); ?></label>
                        <input id="acctxt_fnameID" class="form-control" type="text" tabindex="3" name="site_name">


                        <?php foreach ($site_list as $gsite) { ?>

                            <label>  <input type="radio" name="GoogleSiteList" value="<?= $gsite["GoogleSiteID"] ?>" id="GoogleSiteList_0">  <?= $gsite["URL"] ?> </label>
                            <br>

                        <?php }
                        ?>
                    </div>
                    <?php
                }
                else {
                    echo "<div class='alert alert-warning'>" . $message . " <a href='".prep_url($URL)."'>$SiteName </a></div>";
                    
                     
                }
                ?>    
                <div class="col-lg-12 padno"> 
                    <button class="btn btn-primary pull-right " ><?php echo lang('save_next'); ?></button> 

                    <button class="btn btn-primary pull-right margnrightbtn" onclick="javascript:window.location = ('new_account')"><?php echo lang('go_back'); ?></button>
                </div>
                <div class="col-lg-12 padno margin_tp">
                    <img src="<?php echo base_url('assets/images/galogo.png'); ?>">
                    <h6 class="fntsmll">
                        *<?php echo lang('stp_2_gooogle_agree1'); ?><a href=""><?php echo lang('stp_2_gooogle_agree2'); ?></a>
                    </h6>
                </div>
            </div>
            <div class="clearfix"></div>
        </form>

    </div>

     <?php include_once('sidebar.tpl.php');?>
    <!-- Container area end-->
    <div class="clearfix"></div>
    <script>
        $(document).ready(function() {
            // validate site_info on keyup and submit
            $("#site_connected").validate({
                rules: {
                    site_name : {
                        required : true
                    }
                },
                submitHandler: function() {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url('signup/connected'); ?>",
                        data: $("#site_connected").serialize(), // serializes the form's elements. http://api.jquery.com/serialize/
                        success: function(Res) {
                            window.location.replace('<?php echo base_url('signup/site_info'); ?>');
                        },
                        error: function(Res) {
                                
                        }
                    });
                    return false;

                },
                errorPlacement: function(error, element) { //element.prop("placeholder", error.text()); overide the error placement
                }
            });
        });


    </script>