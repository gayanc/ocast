<div class="col-lg-4 ">
                    <div class="border-tp"></div>
                    <h5 class="titl"><?php echo lang('stp_6_cotact_form');?></h5>
                    <div class="box_line">
                        <p class="text-success prv_cntfrm" style="display:none;"><?php echo lang('stp_6_email_sucess');?></p>
                        <form role="form" id="prvcont_form">
                            <div class="form-group">
                                <label class="nam"><?php echo lang('stp_6_your_email');?></label>
                                <input type="email" name="cnt_email" id="cnt_emailID" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="nam"><?php echo lang('stp_6_question');?></label>
                                <textarea name="cnt_msg" class="form-control"></textarea>
                            </div>
                            <button class="btn btn-primary pull-right "><?php echo lang('stp_6_send');?></button>
                        </form>
                    </div>
</div>
<script>
$(document).ready(function() {
 $("#prvcont_form").validate({
            rules: {
                cnt_email:{
                    required : true,
                    email: true
                },
                cnt_msg : {
                    required: true
                }
            },
            submitHandler: function() {                
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('signup/sendcnt_form'); ?>",
                    data: $("#prvcont_form").serialize(), // serializes the form's elements. http://api.jquery.com/serialize/
                    success: function(Res) {
                      $("#prvcont_form")[0].reset();
                      $("#prvcont_form").fadeOut('slow');  
                      $('.prv_cntfrm').css('display','block');
                    },
                    error: function(Res) {
                       
                    }
                });
                return false;
            },
            errorPlacement: function(error, element) { //element.prop("placeholder", error.text()); overide the error 
            
            }
        }); 
}); 
</script>