<?php 

//print('<pre>');print_r($site_data);
$id = isset($site_data['master_data'][0]['ID']) ? $site_data['master_data'][0]['ID'] : '' ;
$average_age = isset($site_data['master_data'][0]['AverageAge']) ? $site_data['master_data'][0]['AverageAge'] : '';
$mail_percent = isset($site_data['master_data'][0]['MalePercentage']) ? $site_data['master_data'][0]['MalePercentage'] : '';
$description = isset($site_data['master_data'][0]['Description']) ? $site_data['master_data'][0]['Description'] : '';
$audience_desc = isset($site_data['master_data'][0]['Audience']) ? $site_data['master_data'][0]['Audience'] : '';
$sldrAgeval = isset($site_data['master_data'][0]['AverageAge']) ? $site_data['master_data'][0]['AverageAge'] : 50;
$sldrMnval = isset($site_data['master_data'][0]['MalePercentage']) ? $site_data['master_data'][0]['MalePercentage'] : 50;


$contactData = isset($site_data['contact_data']) ? $site_data['contact_data'] : array();
//print('<pre>');print_r($contactData);
$noOfContacts = (count($contactData)) ? count($contactData)+1 : 2; //module counter for js

$contryData = isset($site_data['country_data']) ? $site_data['country_data'] : array();
$categoryData = isset($site_data['category_data']) ? $site_data['category_data'] : array();
$qfactData = isset($site_data['qfact_data']) ? $site_data['qfact_data'] : array(); 
$testm_data = isset($site_data['testm_data']) ? $site_data['testm_data'] : array();
$demog_data  = isset($site_data['demog_data']) ? $site_data['demog_data'] : array();
//print('<pre>');print_r($demog_data);exit;

$noOfDemog = count($demog_data);
$mod_count = 0;
foreach ($demog_data as $key => $value) {
  $mod_count = count($value->HeaderData);
}
$demog_data_json = json_encode($demog_data);
?>
<div class="container">
        <!-- Container area -->
<div class="container">

      <!-- Container area -->
        <div class="col-lg-12 col8top">        
         <h1 class="h1mar"><?php echo lang('stp_3_site_info');?>/<h1>
         </div>
          <div class="col-lg-8 ">
          <div class="border-tp"></div>            
             <?php include_once('wizard_step.tpl.php');?>
              <div class="clearfix"></div>
              <?php
              if(isset($userDetails['AciveStatus']) && $userDetails['AciveStatus'] == 'pending'){?>
                      <?php include('account_message.tpl.php');?>
              <?php }else if(isset($userDetails['AciveStatus']) && $userDetails['AciveStatus'] == 'active'){ ?>
              <form id="site_info">
                <div class="col-lg-12 padno formmar">
                    <div class="alert alert-warning" style="text-align:center">
                      <?php echo lang('stp_skip_msg1'); ?><a href="<?php echo base_url('signup/product_formats'); ?>"><i><?php echo lang('stp_skip_msg2'); ?></i>  <i class="icon-angle-right" style="font-size:11px"></i></a>
                    </div>
                    <div class="col-lg-12 padno">
                      <div class="panel panel-default">
                        <div class="panel-heading"><?php echo lang('stp_3_logo_type');?></div>
                        <div class="panel-body">
                          <?php echo lang('stp_3_logo_hint');?>
                          <div class="col-lg-12 padno formmar">
                            <button class="btn btn-primary pull-left margnrightbtn" id="imgSiteLogo"><?php echo lang('stp_3_add_photo');?></button>
                                  <input type="hidden" name="sptxt_sitelogo" id="sptxt_sitelogoID" />
                            <h6 class="fntsmll">
                              <?php echo lang('stp_3_pcrop_hint');?>
                            </h6>
                          </div>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                       <div class="clearfix"></div>  
                       <div class="panel panel-default">
                        <div class="panel-heading"><?php echo lang('stp_3_description');?></div>
                        <div class="panel-body">
                          <?php echo lang('stp_3_description_tip');?>
                          <div class="col-lg-12 padno">
                            <div class="pull-right fontsizerm">
                                      <?php echo CHAR_LIMIT;?>
                            </div>
                              <div class="clearfix"></div>
                            <textarea class="form-control" id="site_info_desc" name="txt_site_info_desc" rows="4"><?php echo $description; ?></textarea>
                          </div>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                          <div class="clearfix"></div>  
                       <div class="panel panel-default">
                        <div class="panel-heading"><?php echo lang('stp_3_cntct_person');?>
                          <a href="#" data-toggle="tooltip" data-title="Lörem ipsum dolör. Sit ämet consectetur adipisicing elit sed. Do eiusmod tempör incididunt. Ut laböre et. Dolore mägna äliquä ligulå nisl ullamcörper sem curåbitur." data-placement="right" class="icninfo blu" id="popovr"><i class="icon-info "></i></a>
                        </div>
                        <div class="panel-body">
                          <!-- ##################### CONTACT DATA ################-->
                          <form role="form">                          
                          <?php 
                          if(count($contactData)){
                            $i =1;
                            foreach ($contactData as $key => $value) { 
                              //var_dump($value);exit();
                              $contact_number = isset($value['ConcatNumber']) ?  $value['ConcatNumber'] : '';
                              $fname = isset($value['Fname']) ? $value['Fname'] : '';
                              $sname = isset($value['Lname']) ? $value['Lname'] : '';
                              $is_deflt = ($value['IsDefault'] == 0) ? 1 : 0; 
                              $designation = isset($value['Designation'])? $value['Designation'] : '';
                              $email = isset($value['Email']) ? $value['Email'] : '';
                              $cntryCode = isset($value['ContactCountryCode']) ? $value['ContactCountryCode'] : '';
                              $prof_pic = isset($value['ProfilePic']) ?  $value['ProfilePic'] : ''; 
                              if($email){
                          ?>
                          <div class="col-lg-6 padno padleftno">
                                  <div class="form-group">
                                      <label><?php echo lang('stp_3_first_name');?></label>
                                      <input type="text" name="sptxt_contact[<?php echo $i;?>][fname]" id="sptxt_cnfname1ID" class="form-control" value="<?php echo $fname;?>">
                                  </div>
                              </div>
                              <div class="col-lg-6 padrighttno">
                                  <div class="form-group">
                                      <label><?php echo lang('stp_3_sirname');?></label>
                                       <h6 class="fntsmll pull-right padno marginno"><input type="checkbox" class="acchk_duplc" name="sptxt_contact[<?php echo $i;?>][default]" id="acchk_duplcID1" <?php if($is_deflt) {?> checked <?php }?> accessKey="<?=$i?>"> <?php echo lang('stp_3_use_same_contact');?></h6>
                                      <input type="text" name="sptxt_contact[<?php echo $i;?>][sirname]" id="sptxt_cnsirname1ID" value="<?php echo $sname; ?>" class="form-control">  
                                  </div>
                              </div>
                              <div class="col-lg-12 padno padleftno">
                                  <div class="form-group">
                                      <label><?php echo lang('stp_3_title');?></label>
                                      <input type="text" name="sptxt_contact[<?php echo $i;?>][title]" id="sptxt_cntitle1ID" value="<?php echo $designation;?>" class="form-control" value="">
                                  </div>
                              </div>
                              <div class="col-lg-6 padno padleftno">
                                  <div class="form-group">
                                      <label><?php echo lang('stp_3_email');?></label>
                                      <input type="email" name="sptxt_contact[<?php echo $i;?>][email]" id="sptxt_cnemail1ID"  value="<?php echo $email;?>" class="form-control">
                                  </div>
                              </div>
                              <div class="col-lg-6 padrighttno">
                                  <div class="form-group">
                                      <div class="col-lg-5 padno padleftno">
                                          <label><?php echo lang('stp_3_tel_no');?></label>
                                          <div id="sinfo_country_extension<?php echo $i;?>" class="btn-group selbx100 selctboxstyle">
                                            <div class="covr" data-toggle="dropdown">
                                              <div class="txt"><?php echo $value['ContactCountryCode'];?></div> 
                                                <div class="arow">
                                                  <span class="caret"></span>
                                                  <div class="clearfix"></div>
                                                </div>
                                              <div class="clearfix"></div>
                                            </div>
                                            <ul id="sinfo_country_Ext" class="dropdown-menu">
                                               <?Php 
                                              foreach ($country_extentions as $key => $value) { ?>
                                                    <li><a href="#" accesskey="<?php echo $i;?>"  id="<?php echo $value['CallingCode']; ?>"><?php echo $value['ISO'].'&nbsp;(+'.$value['CallingCode'].')';?></a></li>
                                              <?php }?>
                                            </ul>
                                         </div>
                                        <input type="hidden"  name="sptxt_contact[<?php echo $i;?>][excode]" id="sptxt_excode<?php echo $i;?>ID" class="form-control" value="<?php echo $cntryCode;?>">
                                      </div>
                                      <div class="col-lg-7 padrighttno">
                                          <label>&nbsp;</label> <?php //echo $value['ConcatNumber'];?>
                                          <input type="tel" name="sptxt_contact[<?php echo $i;?>][tel]" id="sptxt_cntel<?php echo $i;?>ID" class="form-control" value="<?php echo $contact_number; ?>">
                                      </div>
                                      <div class="clearfix"></div>
                                  </div>
                              </div>
                              <div class="col-lg-12 padno formmar">
                                  <button class="btn btn-primary pull-left margnrightbtn" id="imgContact<?php echo $i;?>"><?php echo lang('stp_3_add_photo');?></button>
                                  <input type="hidden" name="sptxt_contact[<?php echo $i;?>][image]" id="contact_imageID<?php echo $i;?>" value="<?php echo $prof_pic;?>"  />
                                  <h6 class="fntsmll">
                                      <?php echo lang('stp_3_photo_hint');?>
                                  </h6>
                              </div>
                              <div class="clearfix"></div>  
                          <div class="clearfix"></div>
                          <?php $i++; }} } else{?>
                          <div class="col-lg-6 padno padleftno">
                                  <div class="form-group">
                                      <label><?php echo lang('stp_3_first_name');?></label>
                                      <input type="text" name="sptxt_contact[1][fname]" id="sptxt_cnfname1ID" class="form-control">
                                  </div>
                              </div>
                              <div class="col-lg-6 padrighttno">
                                  <div class="form-group">
                                      <label><?php echo lang('stp_3_sirname');?></label>
                                       <h6 class="fntsmll pull-right padno marginno"><input type="checkbox" class="acchk_duplc" name="sptxt_contact[1][default]" id="acchk_duplcID1" accessKey="1"> <?php echo lang('stp_3_use_same_contact');?></h6>
                                      <input type="text" name="sptxt_contact[1][sirname]" id="sptxt_cnsirname1ID" class="form-control">  
                                  </div>
                              </div>
                              <div class="col-lg-12 padno padleftno">
                                  <div class="form-group">
                                      <label><?php echo lang('stp_3_title');?></label>
                                      <input type="text" name="sptxt_contact[1][title]" id="sptxt_cntitle1ID" class="form-control" value="">
                                  </div>
                              </div>
                              <div class="col-lg-6 padno padleftno">
                                  <div class="form-group">
                                      <label><?php echo lang('stp_3_email');?></label>
                                      <input type="email" name="sptxt_contact[1][email]" id="sptxt_cnemail1ID"  class="form-control" id="Inputmail">
                                  </div>
                              </div>
                              <div class="col-lg-6 padrighttno">
                                  <div class="form-group">
                                      <div class="col-lg-5 padno padleftno">
                                          <label><?php echo lang('stp_3_tel_no');?></label>
                                          <div id="sinfo_country_extension1" class="btn-group selbx100 selctboxstyle">
                                            <div class="covr" data-toggle="dropdown">
                                              <div class="txt"> </div> 
                                                <div class="arow">
                                                  <span class="caret"></span>
                                                  <div class="clearfix"></div>
                                                </div>
                                              <div class="clearfix"></div>
                                            </div>
                                            <ul id="sinfo_country_Ext" class="dropdown-menu">
                                               <?Php 
                                              foreach ($country_extentions as $key => $value) { ?>
                                                    <li><a href="#" accesskey="1"  id="<?php echo $value['CallingCode']; ?>"><?php echo $value['ISO'].'&nbsp;(+'.$value['CallingCode'].')';?></a></li>
                                              <?php }?>
                                            </ul>
                                         </div>
                                        <input type="hidden"  name="sptxt_contact[1][excode]" id="sptxt_excode1ID" class="form-control">
                                      </div>
                                      <div class="col-lg-7 padrighttno">
                                          <label>&nbsp;</label>
                                          <input type="tel" name="sptxt_contact[1][tel]" id="sptxt_cntel1ID" class="form-control" id="Inputcity">
                                      </div>
                                      <div class="clearfix"></div>
                                  </div>
                              </div>
                              <div class="col-lg-12 padno formmar">
                                  <button class="btn btn-primary pull-left margnrightbtn" id="imgContact1"><?php echo lang('stp_3_add_photo');?></button>
                                  <input type="hidden" name="sptxt_contact[1][image]" id="contact_imageID1" />
                                  <h6 class="fntsmll">
                                      <?php echo lang('stp_3_photo_hint');?>
                                  </h6>
                              </div>`
                              <div class="clearfix"></div>  
                          <div class="clearfix"></div>
                          <?php }?>
                          <!-- ##################### CONTACT DATA ################-->
                            <div class="apdcntdiv"></div><!-- append this new contact info-->
                          <div class="col-lg-12 padno formmar">
                              <button class="btn ashbtn margnrightbtn btn-block addcontact"><span style=" font-size: 17px;">+</span><?php echo lang('stp_3_add_contact');?></button>
                          </div>
                         </form>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="panel panel-default">
                        <div class="panel-heading"><?php echo lang('stp_3_choose_cnt');?></div>
                        <div class="panel-body">
                          <?php echo lang('stp_3_cntry_desc');?> 
                          <div class="col-lg-12 padno formmar">                          
                            <div class="col-lg-6 padno padleftno">
                              <div class="btn-group selbx100 selctboxstyle">
                                    <div class="covr" data-toggle="dropdown">
                                      <div class="txt"><?php echo lang('stp_3_main_cntry');?></div> 
                                        <div class="arow">
                                          <span class="caret"></span>
                                          <div class="clearfix"></div>
                                        </div>
                                      <div class="clearfix"></div>
                                    </div>
                                    <ul id="sit_info_mcountry" class="dropdown-menu">
                                        <?php 
                                        foreach ($country_list as $key => $value) { ?>
                                              <li><a href="#" id="<?php echo $value['ID']; ?>"><?php echo $value['CountryName'];?></a></li>
                                        <?php }?>
                                    </ul>
                                 </div>
                                 <input type="hidden"  name="sptxt_cntry_main" id="sptxt_mcountryID" class="form-control">

                            </div>
                            <div class="col-lg-6 padrighttno marginselbox1">
                                <div class="btn-group selbx100 selctboxstyle">
                                    <div class="covr" data-toggle="dropdown">
                                      <div class="txt"><?php echo lang('stp_3_admore_cnty');?></div> 
                                        <div class="arow">
                                          <span class="caret"></span>
                                          <div class="clearfix"></div>
                                        </div>
                                      <div class="clearfix"></div>
                                    </div>
                                    <ul id="sit_info_othrcountry" class="dropdown-menu">
                                       <?php 
                                        foreach ($country_list as $key => $value) { ?>
                                              <li><a href="#" id="<?php echo $value['ID']; ?>"><?php echo $value['CountryName'];?></a></li>
                                        <?php }?>
                                    </ul>
                                 </div>
                                  <div id="apndInput"></div>
                            </div>
                          </div>
                          <div class="col-lg-12 padno formmar slectd">
                            <div style="margin-right:10px" class="pull-left fntsmll sansbold"><?php echo lang('stp_3_selected');?></div>
                            <div id="mcntryapnd" class="maincntry"></div>
                            <div id="otrcntryapnd">
                              <?php 
                              if(count($contryData)){
                              foreach ($contryData  as $key => $cvalue) {
                                $indexID = isset($cvalue[0]['ID']) ? $cvalue[0]['ID'] : '';
                                $cntryName = isset($cvalue[0]['CountryName']) ? $cvalue[0]['CountryName'] : '';
                                $default = isset($cvalue[0]['IsDefault']) ? $cvalue[0]['IsDefault'] : '';
                                $cls = '';
                                ?>
                                <?php if($default) { $name='name="sptxt_cntry_main"'; $cls="class='maincntry'"; } else { $name='name="sptxt_cntry_other[]"'; }?>
                                  <div <?=$cls?>> <p data-index="<?php echo $indexID; ?>" class="close2"><?php echo $cntryName ; ?><input type="hidden" <?=$name?> value= "<?php echo $indexID; ?>" /><i class="icon-remove"></i></p> </div>
                              <?php }}?>    
                            </div>                                       
                          </div>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                      <div class="clearfix"></div>
                       <div class="panel panel-default">
                        <div class="panel-heading"><?php echo lang('stp_3_chose_cat');?></div>
                        <div class="panel-body">
                          <?php echo lang('stp_3_logo_hint');?> 
                          <div class="col-lg-12 padno formmar">
                            <div class="col-lg-6 padno padleftno">
                                <div class="btn-group selbx100 selctboxstyle">
                                    <div class="covr" data-toggle="dropdown">
                                      <div class="txt"><?php echo lang('stp_3_logo_hint');?></div> 
                                        <div class="arow">
                                          <span class="caret"></span>
                                          <div class="clearfix"></div>
                                        </div>
                                      <div class="clearfix"></div>
                                    </div>
                                    <ul id="sit_info_catbox" class="dropdown-menu">
                                        <?php foreach ($category_list as $category) { ?>   
                                              <li><a id= "<?php echo $category['ID']; ?>" href="#"><?php echo $category["Description"]; ?></a></li>
                                        <?php } ?>
                                    </ul>
                                 </div>
                            </div>
                            <div id="apndCatgoryInput"></div>
                          </div>                        
                          <div class="col-lg-12 padno formmar slectd">
                            <div style="margin-right:10px" class="pull-left fntsmll sansbold"><?php echo lang('stp_3_selected');?></div>
                            <div id="catapnd">
                                <?php foreach ($categoryData as $key => $value) {?>
                                 <p data-key="<?php echo $value[0]['CategoryID']; ?>" data-index="<?php echo $value[0]['CategoryID']; ?>" class="close2">
                                  <input type="hidden" name="sptxt_category[]" value= "<?php echo $value[0]['CategoryID']; ?>" />
                                  <?php echo $value[0]['Description']; ?><i class="icon-remove"></i></p>
                                <?php }?>
                            </div>                  
                          </div>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="panel panel-default">
                        <div class="panel-heading"><?php echo lang('stp_3_demograpics');?></div>
                        <div class="panel-body">
                          <?php echo lang('stp_3_demog_desc');?>   
                          <div class="col-lg-12 padno formmar sliderrange">
                              <div class="col-lg-4 padno agage"><?php echo lang('stp_3_avg_age');?></div>
                              <div class="col-lg-6 slde"><div class="slider_avg_age ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" aria-disabled="false"><a href="#" class="ui-slider-handle ui-state-default ui-corner-all" style="left: 0%;"></a></div></div>
                              <div class="col-lg-2 padno yrs"><label><span style="color:#4f4f4f" class="data_age"></span> <?php echo lang('stp_3_yrs');?></label></div>    
                               <input type="hidden" id="slider_avg_ageID" name="slider_avg_age">            
                          </div>
                           <div class="clearfix"></div>
                          <div class="col-lg-12 padno formmar sliderrange">
                              <div class="col-lg-4 padno demoset"><?php echo lang('stp_3_set_demog');?></div>

                              <div class="col-lg-3 padno manper"><label class="marginno" style="margin:0" ><?php echo lang('stp_3_men');?> <span style="color:#4f4f4f" class="data_demo_men"></span></label></div>
                              <div style="position: relative; top: -2px;" class="col-lg-6  slde2"><div class="slider_demo_val ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" aria-disabled="false" ><a style="left: 70%;" class="ui-slider-handle ui-state-default ui-corner-all" href="#"></a></div></div>
                              <div class="col-lg-2 padno womnper"><label><?php echo lang('stp_3_women');?> <span style="color:#4f4f4f" class="data_demo_women"></span></label></div>
                              <input type="hidden" id="slider_demo_valID" name="slider_demo_val">                
                          </div>
                        </div>
                        <div class="clearfix


                        "></div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="panel panel-default">
                        <div class="panel-heading"><?php echo lang('stp_3_about_audience');?></div>
                        <div class="panel-body">
                           <?php echo lang('stp_3_audience_desc');?>  
                          <div class="col-lg-12 padno">
                            <div class="pull-right audilength">
                              <?php echo AUDI_CHAR_LIMIT;?>
                            </div>
                              <div class="clearfix"></div>
                            <textarea rows="4" class="form-control" name="audience_desc" id="audience_char_limitID"><?php echo $audience_desc; ?></textarea>
                          </div>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                      <div class="clearix"></div>
                      <div class="panel panel-default">
                        <div class="panel-heading"><?php echo lang('stp_3_quick_fact');?></div>
                        <div class="panel-body">
                           <?php echo lang('stp_3_quick_fact_hint');?>
                          <div class="col-lg-12 padno padleftno formmar">
                             <!-- #### Quick fact data ### -->
                             <?php
                              $j = 0; 
                              for ($i=1; $i <= NO_OF_QFACTS; $i++) { 
                                $text = isset($qfactData[$j]['Text']) ? $qfactData[$j]['Text'] : '';?>
                                <div class="form-group">                             
                                  <label>Fact</label>
                                  <input type="text" class="form-control" name="quickfacts[<?=$i?>][text]" id="quickfacts_<?=$i?>" placeholder="<?php echo lang('stp_3_quick_factholder');?>" value="<?php echo $text; ?>">
                                </div>
                              <?php $j++;} ?>
                              <!-- #### EOF Quick fact data ### -->
                          </div>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="panel panel-default">
                        <div class="panel-heading"><?php echo lang('stp_3_testimonial');?></div>
                        <div class="panel-body">
                          <!-- ###### Testimonial Data -->
                           <?php echo lang('stp_3_testimonial_desc');?>
                           <?php
                           $j= 0; 
                           for ($i=1; $i <= NO_OF_TMONIALS ; $i++) {
                            $quote = isset($testm_data[$j]['Quote']) ? $testm_data[$j]['Quote'] : '';
                            $bywhom = isset($testm_data[$j]['ByWhom']) ? $testm_data[$j]['ByWhom'] : '';
                           //foreach ($testm_data as $key => $value){
                            ?>
                            <div class="col-lg-6 padno padleftno formmar">
                              <div class="form-group">
                                <label><?php echo lang('stp_3_quote');?></label>
                                <input type="text" class="form-control" name="testimonial[<?=$i?>][quote]" id="testimonial_quote_<?=$i?>" value="<?php echo $quote; ?>">
                              </div>
                           </div>
                           <div class="col-lg-6  padrighttno formmar marbom1">
                              <div class="form-group">
                                <label><?php echo lang('stp_3_who_said');?></label>
                                <input type="text" class="form-control" name="testimonial[<?=$i?>][said]" id="testimonial_said_<?=$i?>" value="<?php echo $bywhom; ?>">
                              </div>
                           </div>
                           <?php $j++;} ?>   
                           <!-- ###### EOF Testimonial Data -->                  
                        </div>
                        <div class="clearfix"></div>

                      </div>
                      <div class="clearfix"></div>
                         <div class="panel panel-default">
                        <div class="panel-heading"><?php echo lang('stp_3_add_demoginfo');?></div>
                        <div class="panel-body">
                           <?php echo lang('stp_3_demog_hint');?>
                            <?php if(count($demog_data)){
                            $j = 1; 
                            foreach ($demog_data as $key => $value) { 
                              $headline = $value->HeaderLine;
                            ?>
                               <div class="col-lg-12 padno padleftno formmar">
                                     <div class="form-group">
                                      <label>Headline</label>
                                      <input type="text" name="demoginfo[<?=$j?>][heading]" id="demoghead_mod_<?=$j?>_id_<?=$j?>" value="<?php echo $headline; ?>" class="form-control">
                                    </div>
                               </div>
                              <?php 
                              $i =1;
                              foreach($value->HeaderData as $hdata){
                                  $answer = $hdata['Answer']; 
                                  $point =  floor($hdata['points']);
                                ?>
                                  <div class="col-lg-6 padno padleftno ">
                                        <div class="form-group">
                                          <label><?php echo lang('stp_3_answer');?> <?=$i?></label>
                                          <input type="text" name="demoginfo[<?=$j?>][head][<?=$i?>][answer]" id="demoganswer_mod_<?=$j?>_id_<?=$i?>" value="<?php echo $answer; ?>" class="form-control">
                                        </div>
                                  </div>
                                  <div class="col-lg-6  padrighttno formmar" style="position: relative; top: 0px;">
                                      <div class="col-lg-12 sliderrange padno martp10">
                                     
                                      <div class="col-lg-10 padrighttno slde_answ"><div class="slider_mod_<?=$j?>_id_<?=$i?>" data-axis="pointval_mod_<?=$j?>_id_<?=$i?>" data-hid="demoganspoint_mod_<?=$j?>_id_<?=$i?>" aria-disabled="false"></div></div>
                                      <div class="col-lg-2 padno yrs_answ" style="text-align:right"><label><span style="color:#4f4f4f" class="pointval_mod_<?=$j?>_id_<?=$i?>"></span></label></div>
                                      <input type="hidden" name="demoginfo[<?=$j?>][head][<?=$i?>][point]" id="demoganspoint_mod_<?=$j?>_id_<?=$i?>" class="form-control" value="<?php echo $point; ?>">                 
                                    </div>
                                  </div>
                                  <div class="clearfix"></div>
                              <?php $i++;}?>
                              <div class="apddemoganwrMod<?=$j?>"></div> <!-- append demog answer-->
                               <div class="col-lg-12 padno formmar">
                                <button class="btn btn-primary margnrightbtn btn-block addDanswer<?=$j?>" id="<?=$j?>"><span style=" font-size: 17px;">+</span> <?php echo lang('stp_3_add_demog_answr');?></button>
                              </div>
                        <?php $j++;}}else{?>
                            <div class="col-lg-12 padno padleftno formmar">
                                   <div class="form-group">
                                    <label><?php echo lang('stp_3_headline');?></label>
                                    <input type="text" placeholder="<?php echo lang('stp_3_demog_eg');?>" name="demoginfo[1][heading]" id="demoghead_mod_1_id_1" class="form-control">
                                  </div>
                             </div>
                            <?php for($i=1; $i<3; $i++):?>
                                <div class="col-lg-6 padno padleftno ">
                                      <div class="form-group">
                                        <label><?php echo lang('stp_3_answer');?> <?=$i?></label>
                                        <input type="text" name="demoginfo[1][head][<?=$i?>][answer]" id="demoganswer_mod_1_id_<?=$i?>" class="form-control">
                                      </div>
                                </div>
                                <div class="col-lg-6  padrighttno formmar" style="position: relative; top: 0px;">
                                    <div class="col-lg-12 sliderrange padno martp10">
                                   
                                    <div class="col-lg-10 padrighttno slde_answ"><div class="slider_mod_1_id_<?=$i?>" data-axis="<?=$i?>" data-id="demoganspoint_mod_1_id_<?=$i?>" aria-disabled="false"><a href="#" style="left: 0%;"></a></div></div>
                                    <div class="col-lg-2 padno yrs_answ" style="text-align:right"><label><span style="color:#4f4f4f" class="pointval_mod_1_id_<?=$i?>"></span></label></div>
                                    <input type="hidden" name="demoginfo[1][head][<?=$i?>][point]" id="demoganspoint_mod_1_id_<?=$i?>" class="form-control">                 
                                  </div>
                                </div>
                                <div class="clearfix"></div>
                            <?php endfor;?>
                            <div class="apddemoganwrMod1"></div> <!-- append demog answer-->
                            <div class="col-lg-12 padno formmar">
                              <button class="btn btn-primary margnrightbtn btn-block addFirstanswer" id="1"><span style=" font-size: 17px;">+</span><?php echo lang('stp_3_add_demog_answr');?></button>
                            </div>
                        <?php }?>
                        <?php for($i=2; $i<6; $i++ ): ?>
                            <div class="apddemogdiv<?=$i?>"></div><!-- appending demog info-->
                        <?php endfor; ?>    
                        <div class="col-lg-12 padno formmar">
                          <button class="btn ashbtn margnrightbtn btn-block adddemog"><span style=" font-size: 17px;">+</span> <?php echo lang('stp_3_add_module');?></button>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                    <div class="col-lg-12 padno formmar">
                      <button class="btn btn-primary pull-right "><?php echo lang('save_next');?></button>
                      <button class="btn btn-primary pull-right margnrightbtn" onclick="javascript:window.location = ('new_account')"><?php echo lang('go_back');?></button>
                    </div>
                </div>
                <div>
                <div class="clearfix"></div>              
                </div>
                </div>
              </form>
              <?php }?>
          <!-- Container area end-->
        <div class="clearfix"></div>
      </div>
    </div>

<script>

//###################Textarea Count Limit##################################
  
  function limiting(obj, limit,cls) {
    var cnt = $("."+cls);
    var txt = $(obj).val(); 
    var len = txt.length;
    
    // check if the current length is over the limit
    if(len > limit){
       $(obj).val(txt.substr(0,limit));
       //$(cnt).html(len-1);
       $(cnt).html(limit);
       obj.addClass("error");
     } 
     else { 
       $(cnt).html(limit-len);
     }
     // check if user has less than [n] chars left
     if(limit-len <= 20) {
        $(cnt).addClass("error");
     }
  }

$(function() {
limiting($("#site_info_desc"), char_limit,'fontsizerm');
    $('#site_info_desc').keyup(function(){
    limiting($(this), char_limit,'fontsizerm'); //char_limit -> constant
});

limiting("#audience_char_limitID", audesc_limit,'audilength');
$('#audience_char_limitID').keyup(function(){
    limiting($(this), audesc_limit,'audilength');
  });
});
//###################EOF Textarea Count Limit##################################

contactDuplicate();  
function contactDuplicate(){
    $(".acchk_duplc").on('click',function(){
        var modCnt =$(this).attr('accessKey');
        if($('#acchk_duplcID'+modCnt).is(':checked')){
          $('#sptxt_cnfname'+modCnt+'ID').val(f_name);
          $('#sptxt_cnsirname'+modCnt+'ID').val(l_name);
          $('#sptxt_cnemail'+modCnt+'ID').val(e_mail);
          $('#sinfo_country_extension'+modCnt+' .txt').html(p_code);
          $('#sptxt_cntel'+modCnt+'ID').val(c_number);
        }else{
          //var modCnt =$(this).attr('accessKey');
          $('#sptxt_cnfname'+modCnt+'ID').val('');
          $('#sptxt_cnsirname'+modCnt+'ID').val('');
          $('#sptxt_cnemail'+modCnt+'ID').val('');
          $('#sptxt_cntel'+modCnt+'ID').val('');
        }
    });
}
$(document).ready(function() {
  var imgUpload=$('#imgSiteLogo');
    new AjaxUpload(imgUpload, {
        action: "<?php echo base_url('signup/uploadFile'); ?>",
        name: 'logo',
        onSubmit: function(file, ext){
            if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
                $('.scrfmterr').fadeIn('slow');
                return false;
            }else{
                var prg_image = "<?php echo base_url('assets/images/loading.gif')?>";
                $('#imgScreen').attr("src",prg_image);

                $('.scrfmterr').fadeOut('slow');
            }
        },
        onComplete: function(file, response){
            if(response!="error"){
                 $('.scrfmterr').fadeOut('slow');
                 var new_image = "<?php echo base_url('uploads/logo'); ?>"+'/'+response;
                 console.log(new_image);
                 //$('#imgScreen'+scrcnt).attr("src",new_image);//here we don't set the image
                 $("#sptxt_sitelogoID").attr("value", response);
            } else{ 
                $('.scrfmterr').html("Error, when upload the file"); 
                $('.scrfmterr').fadeIn('slow');   
            }
        }
    });         
});
//demographic defaults
$(function() {
  var avgVal = 50;
  var maxVal = 100;

  var sldrAgeval = <?php echo $sldrAgeval; ?>;
  var avgVal = (sldrAgeval) ? sldrAgeval : avgVal;

  $( ".slider_avg_age").slider({
      range: "min",
      value: avgVal,
      min: 0,
      max: maxVal,
      slide: function( event, ui ) {
       $('#slider_avg_ageID').val(ui.value);
       $('.data_age').html(ui.value);
      }
    });

  $('#slider_avg_ageID').val(avgVal);
  $('.data_age').html(avgVal);


  var sldrMnval = <?php echo $sldrMnval; ?>;
  var avgVal = (sldrMnval) ? sldrMnval : avgVal;
  //men/women
  $( ".slider_demo_val").slider({
      range: "min",
      value: avgVal,
      min: 0,
      max: maxVal,
      slide: function( event, ui ) {       
       $('#slider_demo_valID').val(ui.value);
       $('.data_demo_women').html(ui.value+' %');
       $('.data_demo_men').html(maxVal-ui.value+' %');
      }
    });
  $('#slider_demo_valID').val(avgVal);
  $('.data_demo_women').html(avgVal+' %');
  $('.data_demo_men').html(maxVal-avgVal+' %');
});

$(document).ready(function() {
        // validate site_info on keyup and submit
        $("#site_info").validate({
            rules: {
                
            },
            submitHandler: function() {                
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('signup/site_info_submit'); ?>",
                    data: $("#site_info").serialize(), // serializes the form's elements. http://api.jquery.com/serialize/
                    success: function(Res) {
                      window.location.replace('<?php echo base_url('signup/product_formats'); ?>');
                    },
                    error: function(Res) {
                       
                    }
                });
                return false;
            },
            errorPlacement: function(error, element) { //element.prop("placeholder", error.text()); overide the error 
                    }
        }); 
        addUploader(1);   //First Contact Image
});

//################### CONTACT DATA ######################
//contact image uploader
scrcnt = 2;
function addUploader(scrcnt){
    var imgUpload=$('#imgContact'+scrcnt);
    new AjaxUpload(imgUpload, {
        action: "<?php echo base_url('signup/uploadFile'); ?>",
        name: 'contact',
        onSubmit: function(file, ext){
            if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
                $('.scrfmterr').fadeIn('slow');
                return false;
            }else{
                var prg_image = "<?php echo base_url('assets/images/loading.gif')?>";
                $('#imgScreen'+scrcnt).attr("src",prg_image);

                $('.scrfmterr').fadeOut('slow');
            }
        },
        onComplete: function(file, response){
            if(response!="error"){
                 $('.scrfmterr').fadeOut('slow');
                 var new_image = "<?php echo base_url('uploads/product'); ?>"+'/'+response;
                 console.log(new_image);
                 //$('#imgScreen'+scrcnt).attr("src",new_image);//here we don't set the image
                console.log(scrcnt);
                 $("#contact_imageID"+scrcnt).attr("value", response);
            } else{ 
                $('.scrfmterr').html("Error, when upload the file"); 
                $('.scrfmterr').fadeIn('slow');   
            }
        }
    });                
}

//addcontact
var modcnt = <?php echo $noOfContacts; ?>;
var isData = <?php echo $noOfContacts; ?>;
$(document).ready(function() { //edit mode
  if(isData){
    for (var i =1; i < modcnt; i++) {
      addUploader(i);
      console.log('imageUploader',i);
    };
  }
});

$(document).on('click','.addcontact',function(){
    $.ajax({
            url: '<?php echo base_url('signup/siteAddContact'); ?>',
            type: 'POST',
            dataType: 'json',
            data: {
             content: '',
             mode: 'add',
             modcnt: modcnt
            },
            success:function (data) {                    
                    $('.apdcntdiv').append(data.innerHTML);
                    addUploader(modcnt);
                    contactDuplicate();
                     modcnt++;
            }
        });
       
        return false;
});

//################### EOF CONTACT DATA ######################


//################## Demographic Data #######################
$(function() {
     
  var modItem = <?php echo $noOfDemog;?>;
  var ans = <?php echo $mod_count;?>;
  var demog_data_json = <?php echo $demog_data_json;?>;

  var modItem =1;
  if(demog_data_json.length){  
          $.each(demog_data_json, function(key, val) { 

                var ans = val.HeaderData.length; 
                addDemoAnswer(modItem,ans); 

                var dataItem = 1;
                $.each(val.HeaderData, function(mkey, mval) {

                    //console.log('.slider_mod_',modItem,'_id_',dataItem );
                    var defVal = Math.round(mval.points);
                    var defVal = (mval.points) ? Math.round(mval.points) : 50;
                    console.log(defVal);
                    $( ".slider_mod_"+modItem+"_id_"+dataItem).slider({
                        range: "min",
                        min: 0,
                        max: 100,
                        value: defVal, 
                        slide: function( event, ui ) {
                          var axis = "." + $(this).data("axis");
                          var hid  = $(this).data('hid');
                          $('#'+hid).val(ui.value);
                          $(axis).text(ui.value + "%");
                        }
                    });

                    $('.pointval_mod_'+modItem+'_id_'+dataItem).html(defVal+'%');
                    $('#demoganspoint_mod_'+modItem+'_id_'+dataItem).val(defVal);
                    dataItem++;
                });
              modItem++;   
            });
  }else{
    $(function() {
      for(var i=1; i<3; i++){
        $( ".slider_mod_1_id_"+i ).slider({
          range: "min",
          value: 50,
          min: 0,
          max: 100,
          slide: function( event, ui ) {
           var axis = $(this).data("axis");
           $('#demoganspoint_mod_1_id_'+axis).val(ui.value);
           $('.pointval_mod_1_id_'+axis).html(ui.value+' %');
          }
        });

        $('.pointval_mod_1_id_'+i).html(defVal+'%');
        $('#demoganspoint_mod_1_id_'+i).val(defVal);
      }
    });
  } 

});
//add demographic
var modDemogcnt = <?php echo $noOfDemog; ?>;
var modDemogcnt = (modDemogcnt) ? modDemogcnt : 2;
$(document).on('click','.adddemog',function(){
  
    $.ajax({
            url: '<?php echo base_url('signup/siteAddDemographicData'); ?>',
            type: 'POST',
            dataType: 'json',
            data: {
             content: '',
             mode: 'add',
             modcnt: modDemogcnt
            },
            success:function (data) {                    
                    $('.apddemogdiv'+modDemogcnt).append(data.innerHTML);
                    bindModDefaultSliders(modDemogcnt); //2 sliders init
                    addDemoAnswer(modDemogcnt);
                    modDemogcnt++;
            }
        });
       
        return false;
});

//addDanswer
//#### works with the first demographic module
var modDemoanswr = 3;
$(document).on('click','.addFirstanswer',function(){
    var modID = this.id;
    if(modDemoanswr <= MaxDemogAnswer){ //demog_answer defined in constats
        $.ajax({
            url: '<?php echo base_url('signup/addDemogAnswer'); ?>',
            type: 'POST',
            dataType: 'json',
            data: {
             content: '',
             mode: 'add',
             modcnt: modID,
             anscnt: modDemoanswr
            },
            success:function (data) {              
                    $('.apddemoganwrMod'+modID).append(data.innerHTML); //apddemoganwrMod1
                    bindModAnsSlider(modID,modDemoanswr); //for addtional each slider  
                    modDemoanswr++;
            }
        });
    }
        return false;
});
//for rest of addtional binds
function addDemoAnswer(mid,ans){ //for the rest of additional modules
  var modDemoanswr = (typeof(ans)==='undefined') ? 3 : ans+1;
  $(document).on('click','.addDanswer'+mid,function(){
    var modID = this.id;
    if(modDemoanswr <= MaxDemogAnswer){ //demog_answer defined in constats
        $.ajax({
            url: '<?php echo base_url('signup/addDemogAnswer'); ?>',
            type: 'POST',
            dataType: 'json',
            data: {
             content: '',
             mode: 'add',
             modcnt: modID,
             anscnt: modDemoanswr
            },
            success:function (data) {                               
                    $('.apddemoganwrMod'+modID).append(data.innerHTML); //apddemoganwrMod1
                    bindModAnsSlider(modID,modDemoanswr); //for addtional each slider  
                    modDemoanswr++;
            }
        });
    }
    return false;
  });
}

//add iniital module sliders 
//http://jqueryui.com/slider/#range
 var defVal = 50;
 //dynamically binding module sliders
 function bindModAnsSlider(mid,pid){
  $( ".slider_mod_"+mid+"_id_"+pid ).slider({
      range: "min",
      value: defVal,
      min: 0,
      max: 100,
      slide: function( event, ui ) {
        var axis = $(this).data("axis"); //console.log(axis);
       $('#demoganspoint_mod_'+mid+'_id_'+axis).val(ui.value);
       $('.pointval_mod_'+mid+'_id_'+axis).html(ui.value+' %');
      }
    });
  $('.pointval_mod_'+mid+'_id_'+pid).html(defVal+'%');
  $('#demoganspoint_mod_'+mid+'_id_'+pid).val(defVal);
 }
 function bindModDefaultSliders(mid){
  for(var i=1; i<3; i++){
    $( ".slider_mod_"+mid+"_id_"+i).slider({
      range: "min",
      value: defVal,
      min: 0,
      max: 100,
      slide: function( event, ui ) {
        var axis = $(this).data("axis"); 
        $('#demoganspoint_mod_'+mid+'_id_'+axis).val(ui.value);
        $('.pointval_mod_'+mid+'_id_'+axis).html(ui.value+' %');
      }
    });
    $('.pointval_mod_'+mid+'_id_'+i).html(defVal+'%');
    $('#demoganspoint_mod_'+mid+'_id_'+i).val(defVal);
  }    
 }
</script>