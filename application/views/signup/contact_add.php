<div class="col-lg-6 padno padleftno">
                                <div class="form-group">
                                    <label>First name</label>
                                    <input type="text" name="sptxt_contact[<?=$modcnt?>][fname]" id="sptxt_cnfname<?=$modcnt?>ID" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-6 padrighttno">
                                <div class="form-group">
                                    <label>Surname</label>
                                    <h6 class="fntsmll pull-right padno marginno"><input type="checkbox" class="acchk_duplc" name="sptxt_contact[<?=$modcnt?>][default]" id="acchk_duplcID<?=$modcnt?>"  accessKey="<?=$modcnt?>"> Use same as account contact</h6>
                                    <input type="text" name="sptxt_contact[<?=$modcnt?>][sirname]" id="sptxt_cnsirname<?=$modcnt?>ID" class="form-control">  
                                </div>
                            </div>
                            <div class="col-lg-12 padno padleftno">
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" name="sptxt_contact[<?=$modcnt?>][title]" id="sptxt_cntitle<?=$modcnt?>ID" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-6 padno padleftno">
                                <div class="form-group">
                                    <label>E-mail</label>
                                    <input type="email" name="sptxt_contact[<?=$modcnt?>][email]" id="sptxt_cnemail<?=$modcnt?>ID"  class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-6 padrighttno">
                                <div class="form-group">
                                    <div class="col-lg-5 padno padleftno">
                                        <label>Telephone</label>
                                        <div id="sinfo_country_extension<?=$modcnt?>"  class="btn-group selbx100 selctboxstyle">
                                          <div class="covr" data-toggle="dropdown">
                                            <div class="txt"> </div> 
                                              <div class="arow">
                                                <span class="caret"></span>
                                                <div class="clearfix"></div>
                                              </div>
                                            <div class="clearfix"></div>
                                          </div>
                                          <ul id="sinfo_country_Ext" class="dropdown-menu">
                                               <?php  foreach ($country_extentions as $key => $value) { ?>
                                                    <li><a href="#" accesskey="<?=$modcnt?>" id="$value['CallingCode']?>"><?=$value['ISO']?>+<?=$value['CallingCode']?></a></li>
                                               <?php } ?>
                                          </ul>
                                       </div>
                                      <input type="hidden"  name="sptxt_contact[<?=$modcnt?>][excode]" id="sptxt_excode<?=$modcnt?>ID" class="form-control">
                                    </div>
                                    <div class="col-lg-7 padrighttno">
                                        <label>&nbsp;</label>
                                        <input type="tel" name="sptxt_contact[<?=$modcnt?>][tel]" id="sptxt_cntel<?=$modcnt?>ID" class="form-control" id="Inputcity">
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-lg-12 padno formmar">
                                <button class="btn btn-primary pull-left margnrightbtn" id="imgContact<?=$modcnt?>">ADD PHOTO</button>
                                <input type="hidden" name="sptxt_contact[<?=$modcnt?>][image]" id="sptxt_cnimage<?=$modcnt?>ID" id="contact_image_<?=$modcnt?>ID" />
                                <h6 class="fntsmll">
                                    Photos are displayed on your contact page.
                                </h6>
                            </div>
                            <div class="clearfix"></div>