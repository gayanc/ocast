<?php 
$CI = get_instance();
$CI->load->model('country_model');
$siteID = $this->session->userdata('site_id');
$siteID = ($siteID !='') ? $siteID : '0'; //pass to the js
$mrr_company_id = isset($CI->session->userdata['userDetails']['CompanyID']) ? $CI->session->userdata['userDetails']['CompanyID'] : '';
$siteID = isset($siteID) ? $siteID : '';
//user information

$email = isset($useracc_info[0]->UserName) ? $useracc_info[0]->UserName : '';
$fname = isset($useracc_info[0]->Fname) ? $useracc_info[0]->Fname : '';
$lname = isset($useracc_info[0]->Lname) ? $useracc_info[0]->Lname : '';
//company information
$phone = isset($company_info[0]['Phone']) ? $company_info[0]['Phone'] : '';
$zip = isset($company_info[0]['Zip']) ? $company_info[0]['Zip'] : '';
$callingCode = isset($company_info[0]['CountryCallingCode']) ? $company_info[0]['CountryCallingCode'] : '';
$countryID = isset($company_info[0]['CountryID']) ? $company_info[0]['CountryID'] : '';
$country = $CI->Country_Model->getCountryLangName($countryID);
$country = isset($country[0]['CountryName']) ? $country[0]['CountryName'] : '';
$city = isset($company_info[0]['City']) ? $company_info[0]['City'] : '';
$name = isset($company_info[0]['Name']) ? $company_info[0]['Name'] : '';
$address1 = isset($company_info[0]['Address1']) ? $company_info[0]['Address1'] : '';
$address2 = isset($company_info[0]['Address2']) ? $company_info[0]['Address2'] : '';
?>
<div class="container">
      <!-- Container area -->
        <div class="col-lg-12 col8top">        
         <h1 class="h1mar"><?php echo lang('join_ocast');?> - <?php echo lang('acc_information');?></h1>
         </div>
          <div class="col-lg-8 ">
          <div class="border-tp"></div>            
             <?php include_once('wizard_step.tpl.php');?>
             <div class="clearfix"></div>
             <h6>
               <p class="col-lg-12 padno"><?php echo lang('stp_1_desc');?></p>
              </h6>
              <div class="clearfix"></div>
              <h5 class="titl formmar"><?php echo lang('stp_1_acc_info');?>
                <a href="#" data-toggle="tooltip" data-title="<?php echo lang('stp_1_tip_acc_info');?>" data-placement="right" class="icninfo" id="popovr"><i class="icon-info"></i></a>
              </h5>
              <div class="border-tp"></div>
              <div class="col-lg-12 padno formmar">
                <div class="alert alert-warning accmsg" style="display:none;">
                    <?php echo lang('stp_1_activation_success'); ?>
                </div>
              </div>                           
              <?php if(isset($userDetails['AciveStatus']) && $userDetails['AciveStatus'] == 'pending'){?>
                   <?php include('account_message.tpl.php');?>
              <?php }else{?>
              <form id="acc_info">   
               <div class="col-lg-12 padno formmar">
                <div class="col-lg-12 padno padleftno">
                    <div class="col-lg-6 padno padleftno">
                      <div class="form-group">
                        <label><?php echo lang('stp_1_email_hint1');?> <span class="semibold"><?php echo lang('stp_1_email_hint2');?></span></label>
                        <input type="email" name="acctxt_email" tabindex="2" id="acctxt_emailID" class="form-control" <?php if($siteID){?> readonly <?php }?> value="<?=$email?>">
                      </div>
                    </div>
                    <div class="col-lg-6 padrighttno">
                       <div class="form-group">
                        <label><?php echo lang('stp_1_first_name');?></label>
                         <input type="text" name="acctxt_fname" tabindex="3" id="acctxt_fnameID" class="form-control" value="<?=$fname?>">
                      </div>
                    </div>

                    <div class="col-lg-6 padno padleftno">
                      <div class="form-group">
                        <label ><?php echo lang('stp_1_last_name');?></label>
                        <input type="text" name="acctxt_lname" tabindex="4" id="acctxt_lnameID" class="form-control" value="<?=$lname?>">
                      </div>
                    </div>
                    <div class="col-lg-6 padrighttno">
                      <div class="form-group">
                        <label><?php echo lang('stp_1_password');?></label>
                        <input type="password" name="acctxt_pwd" tabindex="5" id="acctxt_pwdID" class="form-control" <?php if($siteID){?> readonly <?php }?>>
                      </div>
                    </div>

                     <div class="col-lg-6 padno padleftno">
                       <div class="form-group">
                        <label><?php echo lang('stp_1_cpassword');?></label>
                        <input  type="password" name="acctxt_cpwd" tabindex="6" id="acctxt_cpwdID" class="form-control" <?php if($siteID){?> readonly <?php }?>>
                      </div>
                    </div>
                    <div class="col-lg-6 padrighttno">
                      
                    </div>
                </div>
              </div>
              <div class="clearfix"></div>
              <div id="cominfo">
                <h5 class="titl formmar"><?php echo lang('stp_1_company_info');?>
                <a href="#" data-toggle="tooltip" data-title="Lörem ipsum dolör. Sit ämet consectetur adipisicing elit sed. Do eiusmod tempör incididunt. Ut laböre et. Dolore mägna äliquä ligulå nisl ullamcörper sem curåbitur.äliquä ligulå nisl ullamcörper sem curåbitur.äliquä ligulå nisl ullamcörper sem curåbitur." data-placement="right" class="icninfo" id="popovr"><i class="icon-info"></i></a>
              </h5>
              <div class="border-tp"></div>
              <div class="col-lg-12 padno formmar">
                <div class="col-lg-12 padno padleftno">
                    <div class="col-lg-6 padno padleftno">
                      <div class="form-group">
                        <label ><?php echo lang('stp_1_company'); ?></label>
                        <input  type="text" name="acctxt_cmpname" id="acctxt_cmpnameID" tabindex="7" class="form-control" value="<?=$name?>">
                      </div>
                    </div>
                    <div class="col-lg-6 padrighttno">
                      <div class="form-group">
                        <div class="col-lg-5 padno padleftno">
                          <label ><?php echo lang('stp_1_phnumber'); ?></label>
                          <div id="country_extension" class="btn-group selbx100 selctboxstyle">
                                    <div class="covr" data-toggle="dropdown">
                                      <div class="txt"><?=$callingCode?></div> 
                                        <div class="arow">
                                          <span class="caret"></span>
                                          <div class="clearfix"></div>
                                        </div>
                                      <div class="clearfix"></div>
                                    </div>
                                    <ul id="country_Ext" class="dropdown-menu" >
                                      <?Php 
                                      foreach ($country_extentions as $key => $value) { ?>
                                            <li><a href="#" id="<?php echo $value['CallingCode']; ?>"><?php echo $value['ISO'].'&nbsp;(+'.$value['CallingCode'].')';?></a></li>
                                      <?php }?>
                                        
                                    </ul>
                                    <input type="hidden" id="cnt_extension_id" name="cnt_extension" value="<?=$callingCode?>">
                           </div>
                        </div>
                        <div class="col-lg-7 padrighttno">
                          <label >&nbsp;</label>
                          <input type="tel"  tabindex="8" maxlength="10" name="acctxt_tel" id="acctxt_telID" class="form-control" value="<?=$phone?>">
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>

                    <div class="col-lg-6 padno padleftno">
                      <div class="form-group">
                        <label ><?php echo lang('stp_1_addr1'); ?></label>
                        <input type="text" name="acctxt_addr1" id="acctxt_addr1ID" tabindex="9" class="form-control" id="Inputadres1" value="<?=$address1?>">
                      </div>
                    </div>
                     <div class="col-lg-6 padrighttno">
                        <div class="form-group">
                          <label><?php echo lang('stp_1_addr2'); ?></label>
                          <input type="text"   name="acctxt_addr2" id="acctxt_addr2ID" tabindex="10" class="form-control" value="<?=$address2?>">
                        </div>
                      </div>
                       <div class="col-lg-6 padno padleftno">
                         <div class="form-group">
                          <div class="col-lg-5 padno padleftno">
                            <label ><?php echo lang('stp_1_zip'); ?></label>
                            <input type="text" name="acctxt_zip" tabindex="11"  id="acctxt_zipID" class="form-control" value="<?=$zip?>">
                          </div>
                          <div class="col-lg-7 padrighttno citybox">
                            <label ><?php echo lang('stp_1_city'); ?></label>
                            <input type="text" name="acctxt_city" tabindex="12" id="acctxt_cityID" class="form-control" value="<?=$city?>">
                          </div>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                      <div class="col-lg-6 padrighttno">
                      <div class="form-group">
                        <label><?php echo lang('stp_1_country'); ?></label>
                        <input type="text" name="acctxt_country" id="acctxt_countryID" tabindex="13" class="form-control" value="<?=$country?>">
                      </div>
                    </div>

                </div>
               <div class="col-lg-12 padno"><div class="checkbox">
                <?php if(!$siteID){?>
                  <label>
                    <input name="acchk_terms" id="acchk_termsID" tabindex="14" type="checkbox" value="1" class="chkboxmar">
                    <h6 class="marginno"><?php echo lang('stp_1_iagree'); ?><a href=""><?php echo lang('stp_1_termncon'); ?></a></h6>
                    <p class="text-danger trms"></p>
                  </label>
                  <?php }?>
                  
                </div>
                  </div>
               <div class="col-lg-12 padno">
                        <button class="btn btn-primary pull-right" tabindex="15" id="sp_svxt_stp1"><?php echo lang('save_next');?></button>
                </div>
                  </div>
                </div><!-- of companyinfo-->
                <input type="hidden" name="site_id" value="<?=$siteID?>" /> 
               </form> <!-- acc_info form ends-->
               <?php }?>
          </div>
          <?php include_once('sidebar.tpl.php');?>
       <!-- Container area end-->
        <div class="clearfix"></div>

      </div>
    <script type="text/javascript">
    $(document).ready(function() {

          siteID = <?php echo $siteID; ?>; 
          var  siteID = (siteID) ? siteID : '';
          
        if(siteID){
           $("#acc_info").validate({
                onkeyup: false,
                rules: {
                    acctxt_cmpname: {
                      required: true
                    },
                    acctxt_fname: {
                      required: true
                    },
                    acctxt_lname: {
                      required: true
                    },
                },
                submitHandler: function() {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url('signup/add_site'); ?>",
                       // dataType: 'json',
                        data: $("#acc_info").serialize(), // serializes the form's elements. http://api.jquery.com/serialize/
                        beforeSend: function(Res){                
                            $('#acc_info, #cominfo').css('opacity', '0.5');
                        },
                        success: function(Res) {
                                window.location.replace('<?php echo base_url('signup/site_info'); ?>');
                        },
                        error: function(Res) {
                       
                        }
                    });
                    return false;

                },
                errorPlacement: function(error, element) { 
                    //element.prop("placeholder", error.text()); //TODO: if nessesary overide the error placement
                    if(element.attr('id')=='acchk_termsID'){
                          $('.trms').html(error.text());
                    }else{
                        element.val('');
                     element.attr("placeholder",error.text());
                    }
                       
                }
                
            });
            }else{


                  $("#acc_info").validate({
                  onkeyup: false,
                  rules: {
                      
                            acctxt_pwd: {
                                required: true
                            },
                            acctxt_cpwd: {
                                equalTo: "#acctxt_pwdID"
                            },
                            acctxt_email: {
                                required: true,
                                email: true,
                                remote: {
                                    url: "<?php echo base_url('signup/validateEmail'); ?>",
                                    type: "post"
                                },
                                success: function(exist)
                                {
                                    //TO DO Display account information for the existing users
                                }
                            },
                      acctxt_cmpname: {
                        required: true
                      },
                      acctxt_pwd: {
                        required: true
                      },
                      acctxt_fname: {
                        required: true
                      },
                      acctxt_lname: {
                        required: true
                      },
                      acchk_terms: {
                        required: true
                      }
                  },
                  messages: {
                      acctxt_email: {
                          email : "Valid email required",
                          remote: "Email already registered with us"
                      }
                  },
                  submitHandler: function() {
                      $.ajax({
                          type: "POST",
                          url: "<?php echo base_url('signup/add_site'); ?>",
                          dataType: 'json',
                          data: $("#acc_info").serialize(), // serializes the form's elements. http://api.jquery.com/serialize/
                          beforeSend: function(Res){                
                              $('#acc_info, #cominfo').css('opacity', '0.5');
                          },
                          success: function(Res) {
                              console.log(Res);
                              $("#acc_info, #cominfo").fadeOut( "slow", function() {
                                  $(".accmsg").css("display", "block");
                                    $.getJSON('<?php echo base_url(); ?>usercookie/setUserCookie',{email: function(){return $('#acctxt_emailID').val();}})
                                    .done(function(data){ });
                              });
                          },
                          error: function(Res) {
                         
                          }
                      });
                      return false;

                  },
                  errorPlacement: function(error, element) { 
                      //element.prop("placeholder", error.text()); //TODO: if nessesary overide the error placement
                      if(element.attr('id')=='acchk_termsID'){
                            $('.trms').html(error.text());
                      }else{
                          element.val('');
                       element.attr("placeholder",error.text());
                      }
                         
                  }
                  
              });

            }
    
        });


    </script>
