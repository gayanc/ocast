<div class="col-lg-4 clsformat">
                                          <form id="formatNode<?=$modcnt?>">
                                          <div id="modformatcnt<?=$modcnt?>" class="adFrmtcls">
                                          <div class="col-lg-12 thumbnilimg">
                                            <img src="<?=base_url('assets/images/add_screen.png')?>" width="150px" height="100px" name="imgScreenName<?=$modcnt?>" id="imgScreen<?=$modcnt?>"/>
                                            <input type="hidden" id="productScreen<?=$modcnt?>" name="product[<?=$modcnt?>][screen]"/>
                                </div>
                                <div class="col-lg-12 " style="background:#f5f5f5">
                                    <div class="clearfix"></div>
                                      <div class="martp10" id="prdctype<?=$modcnt?>">
                                          <div class="btn-group selbx100 selctboxstyle selbx100sml">
                                              <div data-toggle="dropdown" class="covr">
                                                  <div class="txt"> Type</div> 
                                                  <div class="arow">
                                                      <span class="caret"></span>
                                                      <div class="clearfix"></div>
                                                  </div>
                                                  <div class="clearfix"></div>
                                              </div>
                                              <ul id ="prdt_type" class="dropdown-menu">
                                                  <?php foreach ($formatTypes as $key => $value) {?>
                                                      <li><a href="#" id="<?=$value['FormatID']?>" accesskey="<?=$modcnt?>"><?=$value['Title']?></a></li>
                                                  <?php } ?>
                                              </ul>
                                          </div>
                                          <input type="hidden" name="product[<?=$modcnt?>][format_type]" id="format_type_id"/>
                                      </div>
                                    <div class="martp10">
                                        <input type="text" id="formatID" name="product[<?=$modcnt?>][format]" class="form-control h825" placeholder="Format">
                                    </div>

                                    <div class="martp10" id="pricetype<?=$modcnt?>">
                                        <div class="btn-group selbx100 selctboxstyle selbx100sml">
                                            <div data-toggle="dropdown" class="covr">
                                                <div class="txt"> Pricetype</div> 
                                                <div class="arow">
                                                    <span class="caret"></span>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <ul id ="price_type" class="dropdown-menu">
                                              <?php foreach ($priceTypes as $key => $value) { ?>
                                                 <li><a href="#" id="<?=$value['PriceTypeID']?>" accesskey="<?=$modcnt?>"><?=$value['Text']?></a></li>
                                              <?php } ?>
                                            </ul>
                                        </div>
                                        <input type="hidden" name="product[<?=$modcnt?>][price_type]" id="price_type_id" />
                                    </div>
                                    <div class="martp10">
                                        <input type="text" id="priceID" name="product[<?=$modcnt?>][price]" class="form-control h825" placeholder="Price">
                                    </div>
                                    <div class="martp10 col-lg-12 padno" style="margin-bottom:10px">
                                        <div class=" pull-left fntsmll">
                                            <a href="#" accessKey="<?=$modcnt?>" class="cancelFormat<?=$modcnt?>"><?=lang('stp_4_cancel')?></a> 
                                        </div>
                                        <div class=" pull-right">
                                            <button type="button" accessKey="<?=$modcnt?>" class="btn btn-small btn-primary saveFormat" ><?=lang('stp_4_saveformat')?></button>
                                        </div><div class="clearfix"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <input type="hidden" name="mode" value="save"/>
                                    <input type="hidden" name="modcnt" value="<?=$modcnt?>"/>
                                    <input type="hidden" name="fid"/>
                                </div></div></form></div>