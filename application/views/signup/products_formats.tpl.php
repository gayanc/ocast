<?php 
$format_data = isset($format_data) ? $format_data : array();
$fmtcnt = count($format_data) ? count($format_data) : 1;
?>
<div class="container">
    <!-- Container area -->
    <div class="container">

        <!-- Container area -->
        <div class="col-lg-12 col8top">

            <h1 class="h1mar"><?php echo lang('stp_4_add_yrsite');?></h1>
        </div>

        <div class="col-lg-8 ">
            <div class="border-tp"></div>

             <?php include_once('wizard_step.tpl.php');?>
            <div class="clearfix"></div>
                <?php if(isset($userDetails['AciveStatus']) && $userDetails['AciveStatus'] == 'pending'){?>
                      <?php include('account_message.tpl.php');?>
                <?php }else if(isset($userDetails['AciveStatus']) && $userDetails['AciveStatus'] == 'active'){?>
<!--             <form id="prct_formt"> -->
            <div class="col-lg-12 padno formmar">
                <div class="alert alert-warning" style="text-align:center">
                     <?php echo lang('stp_skip_msg1'); ?><a href="<?php echo base_url('signup/add_cases'); ?>"><i><?php echo lang('stp_skip_msg2'); ?></i>  <i class="icon-angle-right" style="font-size:11px"></i></a>
                </div>
                <div class="col-lg-12 padno">
                    
                    <div class="panel panel-default">
                        <div class="panel-heading"><?php echo lang('stp_4_upload_formats');?></div>
                        <div class="panel-body">
                            <?php echo lang('stp_4_prdctformat_desc');?> 
                        </div>
                        <div class="alert alert-danger scrfmterr" style="display:none;">
                               <?php echo lang('stp_4_upload_err');?>
                        </div>
                        <div class="col-lg-12 apndiv">
                            <div class="col-lg-4 padno formts">
                             
                            <div class="col-lg-12 padleftno formmar">
                                <div class="col-lg-12 thumbnilimg"></div>
                                <div class="col-lg-12 padno martp10">
                                    <button class="btn ashbtn margnrightbtn btn-block apndivcls" onclick="return false;"><span style=" font-size: 17px;">+</span> <?php echo lang('stp_4_addformat');?></button>
                                </div>
                            </div>

                            </div>
                            <?php 
                            if(count($format_data)){
                                $k =1;
                                foreach ($format_data as $key => $value) {
                                    $type = isset($value->Type[0]['Title']) ? $value->Type[0]['Title'] : '';
                                    $typeId = isset($value->Type[0]['ID']) ? $value->Type[0]['ID'] : '';
                                    $format = $value->Format;
                                    $priceId = isset($value->priceType[0]['ID']) ? $value->priceType[0]['ID'] : '';
                                    $pricetype = isset($value->priceType[0]['Text']) ? $value->priceType[0]['Text'] : '';
                                    $price = $value->price;
                                    $image = $value->image;
                                    $fid = $value->ID;
                                ?>
                            <div class="col-lg-4 midbxclz formmar">
                                <div id="modformatcnt<?=$k?>" class="adFrmtcls">
                                <form id="formatNode<?=$k?>" name="frm">
                                      <div class="col-lg-12 thumbnilimg">                                        
                                            <?php if(isset($image) && $image !=''){ ?>
                                                <img src="<?=base_url('uploads/product/'.$image)?>" width="150px" height="100px" name="imgScreenName' . $modcnt . '" id="imgScreen<?=$k?>"/>
                                            <?php }else{ ?>                  
                                                <img src="<?=base_url('assets/images/add_screen.png')?>" width="150px" height="100px" name="imgScreenName' . $modcnt . '" id="imgScreen<?=$k?>"/>
                                            <?php } ?>
                                            <input type="hidden" id="productScreen<?=$k?>" name="product[<?=$k?>][screen]" value="<?=$image?>"/>
                                     </div>
                                     <div class="col-lg-12 " style="background:#f5f5f5">
                                    <div class="clearfix"></div>
                                    <div class="martp10" id="prdctype<?=$k?>">
                                          <div class="btn-group selbx100 selctboxstyle selbx100sml">
                                              <div data-toggle="dropdown" class="covr">
                                                  <div class="txt"><?=$type?></div> 
                                                  <div class="arow">
                                                      <span class="caret"></span>
                                                      <div class="clearfix"></div>
                                                  </div>
                                                  <div class="clearfix"></div>
                                              </div>
                                              <ul id ="prdt_type" class="dropdown-menu">
                                            <?php foreach ($formatTypes as $key => $value){?>
                                                    <li><a href="#" id="<?php echo $value['FormatID'];?>" accesskey="<?=$k?>"><?=$value['Title']?></a></li>
                                            <?php } ?>
                                          </ul></div>
                                          <input type="hidden" name="product[<?=$k?>][format_type]" id="format_type_id" value="<?=$typeId?>" />
                                    </div>
                                    <div class="martp10">
                                        <input type="text" name="product[<?=$k?>][format]" id="formatID" class="form-control h825" value="<?=$format?>">
                                    </div>
                                    <div class="martp10" id="pricetype<?=$k?>">
                                        <div class="btn-group selbx100 selctboxstyle selbx100sml">
                                            <div data-toggle="dropdown" class="covr">
                                                <div class="txt"><?=$pricetype?></div> 
                                                <div class="arow">
                                                    <span class="caret"></span>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <ul id ="price_type" class="dropdown-menu">
                                             <?php foreach ($priceTypes as $key => $value){ ?>
                                                    <li><a href="#" id="<?=$value['PriceTypeID']?>" accesskey="<?=$k?>"><?=$value['Text']?></a></li>
                                            <?php } ?>
                                          </ul>
                                        </div>
                                        <input type="hidden" name="product[<?=$k?>][price_type]" id="price_type_id" value="<?=$priceId?>" />
                                    </div>
                                    <div class="martp10">
                                        <input type="text" name="product[<?=$k?>][price]" id="priceID" class="form-control h825" placeholder="Price" value="<?=$price?>"/>
                                    </div>
                                    <div class="martp10 col-lg-12 padno" style="margin-bottom:10px">
                                        <div class=" pull-left fntsmll">
                                            <a href="#" accessKey="<?=$k?>" class="cancelFormat"><?php echo lang('stp_4_cancel');?></a> 
                                        </div>
                                        <div class=" pull-right">
                                            <button accesskey="<?=$k?>" type="button" class="btn btn-small btn-primary saveFormat" ><?php echo lang('stp_4_saveformat');?></button>
                                        </div><div class="clearfix"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <input type="hidden" name="mode" value="save"/>
                                    <input type="hidden" name="modcnt" value="<?=$k?>"/>
                                    <input type="hidden" name="fid" value="<?=$fid?>" />
                                     </div>
                                 </div>
                                    </form>
                                 </div>
                            <?php $k++; ?>
                            <?php } ?>
                            
                            <?php  } ?>

                        </div><!-- append area-->
                        <div class="clearfix"></div>
                    </div>
                
                    <div class="clearfix"></div>
                </div>
                <div class="col-lg-12 padno">
                    <button class="btn btn-primary pull-right" onclick="javascript:window.location = ('add_cases')">SAVE &amp; NEXT</button>
                    <button class="btn btn-primary pull-right margnrightbtn" onclick="javascript:window.location = ('site_info')" >GO BACK</button>
                </div>
                <div class="clearfix"></div>  
            </div>
<!--             </form> -->
                <?php }?>
            <div class="clearfix"></div>              
        </div>
    </div>
    <!-- Container area end-->
    <div class="clearfix"></div>
</div>
<script>

var frm = $('#prct_formt');
    frm.submit(function (ev) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('signup/add_formats'); ?>",
            data: '',
            success: function (data) {
                window.location.replace('<?php echo base_url('signup/add_cases'); ?>');
            }
        });
        ev.preventDefault();
    });

var modcnt = <?php echo $fmtcnt; ?>;
var isData = <?php echo $fmtcnt; ?>;
$(function() {

    if (isData) {
        for (var i = 1; i <= modcnt; i++) {
            bindUploader(i);
        };
    }

});
$(document).on('click','.apndivcls',function(){
    modcnt++;
    $.ajax({
            url: '<?php echo base_url('signup/addFormat'); ?>',
            type: 'POST',
            dataType: 'json',
            data: {
             content: '',
             mode: 'add',
             modcnt: modcnt
            },
            success:function (data) {                    
                    $('.apndiv').append(data.innerHTML);
                    bindUploader(modcnt);
                    modcnt++;
            }
        });
       
        return false;
});

$(document).on('click', '.removeFormat', function(){
    var modcnt = $(this).get(0).accessKey;
    var fid = $(this).data('fid');

    var divHtmlContent = $('#saveStat').html();
        $.ajax({
            url: '<?php echo base_url('signup/editProductsAndFormats'); ?>',
            type: 'POST',
            dataType: 'json',
             data: {
              fid: fid,
              mode: 'remove',
              modcnt: modcnt
             },
            success:function (data) {
                    $('#modformatcnt'+modcnt).empty();
                   //TODO : remove data
            }
        });
        return false;
});

    $(document).on('click', '.saveFormat', function(){
    var modcnt = $(this).get(0).accessKey;
    console.log(modcnt);
        $.ajax({
            url: '<?php echo base_url('signup/editProductsAndFormats'); ?>',
            type: 'POST',
            dataType: 'json',
            data: $('#formatNode'+modcnt).serialize(),
            success:function (data) {
                    $('#modformatcnt'+modcnt).html(data.innerHTML);
            }
        });
    });

$(document).on('click', '.cancelFormat', function(){
    var modcnt = $(this).get(0).accessKey;
    var divHtmlContent = $('#saveStat').html();
        $.ajax({
            url: '<?php echo base_url('signup/editProductsAndFormats'); ?>',
            type: 'POST',
            dataType: 'json',
            data: {
             content: divHtmlContent,
             mode: 'save',
             nodeData: $('#formatNode'+modcnt).serialize(),
             modcnt: modcnt
            },
            success:function (data) {
                    //$('#modformatcnt'+modcnt).html(data.innerHTML);
            }
        });
});
$(document).on('click', '.edtFormat', function(){
    var modcnt = $(this).get(0).accessKey;
    var divHtmlContent = $('#saveStat').html();
    console.log($('#formatNode'+modcnt).serialize());
        $.ajax({
            url: '<?php echo base_url('signup/editProductsAndFormats'); ?>',
            type: 'POST',
            dataType: 'json',
            data: $('#formatNode'+modcnt).serialize(),
            success:function (data) {
                    //$('#modformatcnt'+modcnt).empty();
                    $('#modformatcnt'+modcnt).html(data.innerHTML);
                    bindUploader(modcnt);
            }
        });
});
</script>
<script>
    // $(document).ready(function() {
    //     // validate site_info on keyup and submit
    //     $("#prct_formt").validate({
    //         rules: function(){
    //           return true;
    //         },
    //         submitHandler: function() {
    //             $.ajax({
    //                 type: "POST",
    //                 url: "<?php echo base_url('signup/proceed'); ?>",
    //                 data: $("#prct_formt").serialize(), // serializes the form's elements. http://api.jquery.com/serialize/
    //                 beforeSend: function(Res){                
    //                 },
    //                 success: function(Res) {
    //                      window.location.replace('<?php echo base_url('signup/add_cases'); ?>');
    //                 },
    //                 error: function(Res) {
                       
    //                 }
    //             });
    //             return false;

    //         },
    //         errorPlacement: function(error, element) { //element.prop("placeholder", error.text()); TODO: if nessesary overide the error placement
    //                 }
    //     });
    // });


 function bindUploader(scrcnt){
    var imgUpload=$('#imgScreen'+scrcnt);
    new AjaxUpload(imgUpload, {
        action: "<?php echo base_url('signup/uploadFile'); ?>",
        name: 'screen',
        onSubmit: function(file, ext){
            if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
                $('.scrfmterr').fadeIn('slow');
                return false;
            }else{
                var prg_image = "<?php echo base_url('assets/images/loading.gif')?>";
                $('#imgScreen'+scrcnt).attr("src",prg_image);

                $('.scrfmterr').fadeOut('slow');
            }
        },
        onComplete: function(file, response){
            if(response!="error"){
                 $('.scrfmterr').fadeOut('slow');
                 var new_image = "<?php echo base_url('uploads/product'); ?>"+'/'+response;
                 console.log(new_image);
                 $('#imgScreen'+scrcnt).attr("src",new_image);
                 $("#productScreen"+scrcnt).attr("value", response);
            } else{ 
                $('.scrfmterr').html("Error, when upload the file"); 
                $('.scrfmterr').fadeIn('slow');   
            }
        }
    });                
}

function addChoosenDrop(){
    var config = {
  '.chosen-select'           : {},
  '.chosen-select-deselect'  : {allow_single_deselect:true},
  '.chosen-select-no-single' : {disable_search_threshold:10},
  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
  '.chosen-select-width'     : {width:"95%"}
}
for (var selector in config) {
  $(selector).chosen(config[selector]);
}
}


</script>