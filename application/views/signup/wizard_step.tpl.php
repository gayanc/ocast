<?php 
$CI = get_instance();
$method = $CI->router->fetch_method();
?>
<div class="col-lg-12" id="stepids"> 
    <a href="<?php echo base_url('signup/new_account')?>"><div class="col-lg-1 linecircl marg <?php if($method =='new_account'){?> active <?php }?>">1</div></a>
    <div class="col-lg-1 linebrd"></div>
    <a href="<?php echo base_url('signup/connect')?>"><div class="col-lg-1 linecircl <?php if($method =='connect' || $method =='connected' ){?>active <?php }?>">2</div></a>
    <div class="col-lg-1 linebrd"></div>
    <a href="<?php echo base_url('signup/site_info')?>"><div class="col-lg-1 linecircl <?php if($method =='site_info'){?> active <?php }?>">3</div></a>
    <div class="col-lg-1 linebrd"></div>
    <a href="<?php echo base_url('signup/product_formats')?>"><div class="col-lg-1 linecircl <?php if($method =='product_formats'){?>active <?php }?>">4</div></a>
    <div class="col-lg-1 linebrd"></div>
    <a href="<?php echo base_url('signup/add_cases')?>"><div class="col-lg-1 linecircl <?php if($method == 'add_cases'){?>active <?php }?>">5</div></a>
    <div class="col-lg-1 linebrd"></div>
    <a href="<?php echo base_url('signup/preview_site')?>"><div class="col-lg-1 linecircl <?php if($method =='preview_site'){?>active <?php }?>">6</div></a>
    <div class="clearfix"></div>
    <a href="<?php echo base_url('signup/new_account')?>"><div class="col-lg-1 stptxt info <?php if($method =='new_account'){?> active <?php }?>"><?php echo lang('stp_acc_info');?></div></a>
    <div class="col-lg-1 txtspc"></div>
    <a href="<?php echo base_url('signup/connect')?>"><div class="col-lg-1 stptxt connect <?php if($method =='connect' || $method =='connected' ){?>active <?php }?>"><?php echo lang('stp_connect');?></div></a>
    <div class="col-lg-1 txtspc"></div>
    <a href="<?php echo base_url('signup/site_info')?>"><div class="col-lg-1 stptxt site <?php if($method =='site_info'){?> active <?php }?>"><?php echo lang('stp_site_info');?></div></a>
    <div class="col-lg-1 txtspc"></div>
    <a href="<?php echo base_url('signup/product_formats')?>"><div class="col-lg-1 stptxt formats <?php if($method =='product_formats'){?>active <?php }?>"><?php echo lang('stp_product_format');?></div></a>
    <div class="col-lg-1 txtspc"></div>
    <a href="<?php echo base_url('signup/add_cases')?>"><div class="col-lg-1 stptxt case <?php if($method == 'add_cases'){?>active <?php }?>"><?php echo lang('stp_case');?></div></a>
    <div class="col-lg-1 txtspc"></div>
    <a href="<?php echo base_url('signup/preview_site')?>"><div class="col-lg-1 stptxt submit <?php if($method =='preview_site'){?>active <?php }?>"><?php echo lang('stp_preview');?></div></a>
 </div>