<?php 
$nof_cases = (count($site_cases)>1) ? count($site_cases) : 1 

?>
<div class="container">

      <!-- Container area -->
        <div class="col-lg-12 col8top">
        
         <h1 class="h1mar"><?php echo lang('stp_5_add_yrsite');?></h1>
         </div>

          <div class="col-lg-8 ">
          <div class="border-tp"></div>            
              <?php include_once('wizard_step.tpl.php');?>
              <div class="clearfix"></div>
              <form id="signup_cases">
                    <?php if(isset($userDetails['AciveStatus']) && $userDetails['AciveStatus'] == 'pending'){?>
                      <?php include('account_message.tpl.php');?>

                    <?php }else if(isset($userDetails['AciveStatus']) && $userDetails['AciveStatus'] == 'active'){?>
                                <div class="col-lg-12 padno formmar">
                                    <div class="alert alert-warning" style="text-align:center">
                                       <?php echo lang('stp_skip_msg1'); ?><a href="<?php echo base_url('signup/preview_site'); ?>"><i><?php echo lang('stp_skip_msg2'); ?></i>  <i class="icon-angle-right" style="font-size:11px"></i></a>
                                    </div>
                                    <div class="col-lg-12 padno">
                                      <div class="panel panel-default">
                                        <div class="panel-heading"><?php echo lang('stp_5_add_case'); ?></div>
                                        <div class="panel-body"><?php echo lang('stp_5_add_case_desc'); ?>
                                         
                                       </div>
                                       <?php
                                       if(count($site_cases)){
                                        $i = 1;
                                       foreach ($site_cases as $key => $value) { 
                                          $title = $value['Title'];
                                          $description = $value['Description'];
                                          $image = $value['Image'];
                                        ?>
                                        <div class="col-lg-12 padno formmar">
                                          <div class="col-lg-12 padno padleftno formmar">
                                              <div class="form-group">
                                                <label>Title</label>
                                                <input type="text" name="txtcases[<?=$i?>][title]" id="case_title_mod_<?=$i?>" placeholder="<?php echo lang('stp_5_case_title_eg'); ?>" value="<?=$title?>"  class="form-control">
                                              </div>
                                              <div class="form-group">
                                                <label>Description</label>
                                                <textarea class="form-control" rows="4" name="txtcases[<?=$i?>][desc]" id="case_desc_mod_<?=$i?>" ><?=$description?></textarea>
                                              </div>
                                          </div>
                                        </div>
                                          <div class="col-lg-12 padno">
                                            <button class="btn btn-primary pull-left margnrightbtn" id="caseImg<?=$i?>" onclick="return false;"><?php echo lang('stp_5_add_image'); ?></button>
                                             <input type="hidden" name="txtcases[<?=$i?>][img]" id="case_img_mod_<?=$i?>" value="<?=$image?>"/>
                                            <h6 class="fntsmll"><?php echo lang('stp_5_add_img_desc'); ?></h6>
                                          </div>
                                          <div class="clearfix"></div>
                                          <?php $i++;}}else{?>
                                          <div class="col-lg-12 padno formmar">
                                          <div class="col-lg-12 padno padleftno formmar">
                                              <div class="form-group">
                                                <label>Title</label>
                                                <input type="text" name="txtcases[1][title]" id="case_title_mod_1" placeholder="<?php echo lang('stp_5_case_title_eg'); ?>"  class="form-control">
                                              </div>
                                              <div class="form-group">
                                                <label>Description</label>
                                                <textarea class="form-control" rows="4" name="txtcases[1][desc]" id="case_desc_mod_1" ></textarea>
                                              </div>
                                          </div>
                                        </div>
                                          <div class="col-lg-12 padno">
                                            <button class="btn btn-primary pull-left margnrightbtn" id="caseImg1" onclick="return false;"><?php echo lang('stp_5_add_image'); ?></button>
                                             <input type="hidden" name="txtcases[1][img]" id="case_img_mod_1" />
                                            <h6 class="fntsmll"><?php echo lang('stp_5_add_img_desc'); ?></h6>
                                          </div>
                                          <div class="clearfix"></div>
                                          <?php }?>

                                            <div class="case_apnd"></div><!-- case append div-->
                                          <div class="col-lg-12 padno formmar2">
                                               <button class="btn ashbtn margnrightbtn btn-block modcase"><span style=" font-size: 17px;">+</span> <?php echo lang('stp_5_add_another_case'); ?> </button>
                                          </div>
                                          <div class="clearfix"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                      </div>
                                      <div class="col-lg-12 padno martp10">
                                        <button class="btn btn-primary pull-right "><?php echo lang('save_next'); ?></button>
                                        <button class="btn btn-primary pull-right margnrightbtn"  onclick="javascript:window.location = ('product_formats')"><?php echo lang('stp_5_go_back'); ?></button>
                                     </div>
                                       <div class="clearfix"></div>  
                                </div>
                              </form>
                    <?php }?>
              <div class="clearfix"></div>              
          </div>
           </div>
<script type="text/javascript">
var modcnt = <?php echo $nof_cases; ?>; 

$(document).on('click','.modcase',function(){
  modcnt++;
    $.ajax({
            url: '<?php echo base_url('signup/addNewCase'); ?>',
            type: 'POST',
            dataType: 'json',
            data: {
             content: '',
             mode: 'add',
             modcnt: modcnt
            },
            success:function (data) {                    
                    $('.case_apnd').append(data.innerHTML);
                    addUploader(modcnt);
                    modcnt++;
            }
        });       
        return false;
});
$(document).ready(function() {
        // validate site_info on keyup and submit
        console.log(modcnt);
        for (var i = 1; i <= modcnt; i++) {
          addUploader(i);
        }

        $("#signup_cases").validate({
            submitHandler: function() {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('signup/case_submit'); ?>",
                    data: $("#signup_cases").serialize(), // serializes the form's elements. http://api.jquery.com/serialize/
                    beforeSend: function(Res){                
                    },
                    success: function(Res) {
                         window.location.replace('<?php echo base_url('signup/preview_site'); ?>');
                    },
                    error: function(Res) {
                       
                    }
                });
                return false;

            },
            errorPlacement: function(error, element) { //element.prop("placeholder", error.text()); TODO: if nessesary overide the error placement
                    }
        });
        //addUploader(1); //First Case Image       
    });

function addUploader(scrcnt){
    var imgUpload=$('#caseImg'+scrcnt);
    new AjaxUpload(imgUpload, {
        action: "<?php echo base_url('signup/uploadFile'); ?>",
        name: 'case',
        onSubmit: function(file, ext){
            if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
                $('.scrfmterr').fadeIn('slow');
                return false;
            }else{
                //var prg_image = "<?php echo base_url('assets/images/loading.gif')?>";
                //$('#imgScreen'+scrcnt).attr("src",prg_image);
               // $('.scrfmterr').fadeOut('slow');
            }
        },
        onComplete: function(file, response){
            if(response!="error"){
                 $('.scrfmterr').fadeOut('slow');
                 var new_image = "<?php echo base_url('uploads/case'); ?>"+'/'+response;
                 console.log(new_image);
                 //$('#imgScreen'+scrcnt).attr("src",new_image);
                 $("#case_img_mod_"+scrcnt).attr("value", response);
            } else{ 
                $('.scrfmterr').html("Error, when upload the file"); 
                $('.scrfmterr').fadeIn('slow');   
            }
        }
    });                
}
</script>