<?php

/**
 * History Model
 * @author : Gayan Chathuranga <gayan.c@eyepax.com> 
 */
class History_Model extends CI_Model {

    public $ID;
    public $ModuleID;
    private $UserID;
    private $SiteID;
    public $Operation;
    public $Description;

    private $CreatedDate;
    private $LastModifiedDate;
    private $LastModifiedBy;
    private $CreatedBy;


    public function __construct() {
        parent::__construct();
    }

    public function __get($property){
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
          $this->$property = $value;
        }
    }
    /**
     * Function : getHistoryList()
     * Get site module history list
     * @param   $site- siteID
     * @param   $user - user ID    
     * @table   trn_User_History (trnUH),sys_Module
     * @return  History List
     */
    public function getModuleHistoryList() {
        try {
            $this->db->select('*');
            $this->db->from('trn_User_History trnUH');
            $this->db->join('sys_Module', 'trnUH.ModuleID = sys_Module.ID', 'left');
            $this->db->where('trnUH.UserID', $this->UserID);
            $this->db->where('trnUH.SiteID', $this->SiteID);
           	$query = $this->db->get();
        	return $query->result_array();
        } catch (Exception $e) {
           echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
        }
    }
    
    /**
     * Function : updateModuleHistory()
     * Update module history list for site
     * @param   $apndQ- Append Query //userID and siteID
     * @param   $data   
     * @table   trn_User_History (trnUH)
     * @return  bool
     */
    public function updateModuleHistory($apndQ,$data) {
        try {
            $this->db->where($apndQ);
            return $this->db->update('trn_User_History', $data);
        } catch (Exception $e) {
           echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
        }
    }


    /**
     * Function : writeUserRegistrationHistory()
     * write registration process history
     * @param   $data- data array
     * @table   sys_Registration_History (sysRH)
     * @return  bool
     */
    public function writeUserRegistrationHistory($data) {
        try {
            return $this->db->insert('sys_Registration_History',$data);
        } catch (Exception $e) {
           echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
        }
    }

    
    
    
    
    
    
    
    
    
    


}

?>
