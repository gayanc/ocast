<?php 
/**
 * Format Types Model
 * @author : Gayan Chathuranga <gayan.c@eyepax.com> 
 */
class Format_Model extends CI_Model{

   
    
	public function __construct(){
		parent::__construct();
        $lang = lang('lcode');
	}

    function getSiteFormat($id,$sid){
            try {
                $data = array('*');
                $this->db->select($data);
                $this->db->from('trn_format trnFMT');
                $this->db->where('trnFMT.ID', $id);
                $this->db->where('trnFMT.SiteID', $sid);
                $this->db->order_by('trnFMT.ID', 'ASC');
                $query = $this->db->get();
                return $query->result_array();
            } catch (Exception $e) {
                 echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
            }
 
    }

     function getSiteAllFormats($sid){
        try {
            
            $format_arr = array();
            $data = array('*');
            $this->db->select($data);
            $this->db->from('trn_format trnFMT');
            $this->db->where('trnFMT.SiteID', $sid);
            $this->db->order_by('trnFMT.ID', 'ASC');
            $query = $this->db->get();
            $format_data = $query->result_array(); 

            foreach ($format_data as $key => $value) {
                $objSite = new stdClass();
                $objSite->Type = $this->getFormatTypeByID($value['TypeID']);
                $objSite->Format = $value['Format'];
                $objSite->priceType = $this->getPriceTypeByID($value['PriceTypeID']);
                $objSite->price = $value['Price'];
                $objSite->image = $value['Image'];
                $objSite->ID = $value['ID'];
                array_push($format_arr,$objSite);
            }
            return $format_arr;
        } catch (Exception $e) {
             echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
           
    }
	/**
	 * Get All Format Types
	 * @tables   set_format(setFMT),set_format_Lang(setFMTL) 
	 * @return  Format Types Array
	 */
	public function getAllFormatTypes(){
		try {
            $data = array(
                'setFMTL.FormatID',
                'setFMTL.Title'
            );
            $this->db->distinct();
            $this->db->select($data);
            $this->db->from('set_format setFMT, set_format_lang setFMTL');
            $this->db->join('set_format_lang', 'setFMT.ID = setFMTL.FormatID');
            $this->db->where('setFMTL.LangCode', lang('lcode'));
            $this->db->order_by('setFMTL.Title', 'ASC');
            $query = $this->db->get(); 
            return $query->result_array();
        } catch (Exception $exc) {

            echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
	}

    public function getFormatTypeByID($id){
        try {
            $data = array(
                'Title',
                'ID'
            );
            $this->db->select($data);
            $this->db->from('set_format_lang setFMTL');
            $this->db->where('setFMTL.FormatID', $id);
            $this->db->where('setFMTL.LangCode', lang('lcode'));
            $query = $this->db->get();
            return $query->result_array();
            
        } catch (Exception $e) {
             echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
    }

    /**
     * Get All Format Price Types
     * @tables   set_price_type(setPRT),set_price_type_lang(setPRTL) 
     * @return  Format Price Types Array
     */
    public function getAllFormatPriceTypes(){
        try {
            $data = array(
                'setPRTL.PriceTypeID',
                'setPRTL.Text'
            );
            $this->db->distinct();
            $this->db->select($data);
            $this->db->from('set_price_type setPRT, set_price_type_lang setPRTL');
            $this->db->join('set_price_type_lang', 'setPRT.ID = setPRTL.PriceTypeID');
            $this->db->where('setPRTL.LangCode', lang('lcode'));
            $this->db->order_by('setPRTL.Text', 'ASC');
            $query = $this->db->get();
            return $query->result_array();
        } catch (Exception $exc) {

            echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
    }

    public function getPriceTypeByID($id){
        try {

            $data = array(
                'Text',
                'ID'
            );
            $this->db->select($data);
            $this->db->from('set_price_type_lang setPRTL');
            $this->db->where('setPRTL.PriceTypeID', $id);
            $this->db->where('setPRTL.LangCode', lang('lcode'));
            $query = $this->db->get();
            return $query->result_array();
            
        } catch (Exception $e) {
             echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
    }

    public function insertFormatData($table, $data){
        try {

            $this->db->trans_start();
            $this->db->insert($table, $data);
            $insert_id = $this->db->insert_id();
            $this->db->trans_complete();
            return  $insert_id;
        } catch (Exception $e) {
            echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
        }
    }

    public function deleteSiteFormat($table,$id){
        try {
            //$this->db->delete('trn_format', array('ID' => $id));
            $sql = "DELETE FROM  $table WHERE ID = $id";
            $this->db->query($sql);
            
        } catch (Exception $e) {
            echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
        }
    }

    public function updateSiteFormat($tbl, $id, $data){
        try {

            $this->db->update($tbl,$data);
            $this->db->where('ID',$id);
            return  $id;
        } catch (Exception $e) {
            echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
        }
    }
    
}