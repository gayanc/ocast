<?php

class User_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function addUser($data) {
        try {
            $this->db->insert('set_user', $data);
            return $this->db->insert_id();
        } catch (Exception $e) {
            echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
    }
    
    function login($username,$password){
        
        // verification salt
        
        $salt = sha1($username . $this->config->item('encryption_key'));        
		$this->db->select('ID,UserTypeID,UserName,Password,CompanyID,Fname,Lname');
		$this->db->from('set_user');
		$this->db->where(array('UserName' => $username, 'Password' => md5($password), 'Salt' => $salt));
		$query = $this->db->get(); 
		
		if($query->num_rows() == 1){
			return $query->result();
		}else{
			return false;
		}
	}
    
    function track_login($data, $id){
        try {
            $this->db->update('set_user', $data);
            $this->db->where('id', $id);
            return TRUE;
        } catch (Exception $e) {
            echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
    }
    
    function verify_autologin($key){
        try {
            $this->db->select('ID,UserTypeID,UserName,Password,CompanyID,Fname,Lname');
            $this->db->from('set_user');
            $this->db->where('LoginKey', $key);
            $query = $this->db->get(); 
		
            if($query->num_rows() == 1){
                return $query->result();
            }else{
                return false;
            }
        } catch (Exception $e) {
            echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
    }
    
    function modify_autolog_key($username, $password, $ip){
        try {
            $login_hash = sha1($username . $ip);
            $this->db->update('set_user', array('LoginKey' => $login_hash));
            $this->db->where(array('UserName' => $username, 'Password' => md5($password)));
            return TRUE;
        } catch (Exception $e) {
            echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
    }
    
    function check_login_ip($ip){
        try {
            
            $this->db->select('LastLoginIP');
            $this->db->from('set_user');
            $this->db->where('LastLoginIP', $ip);
            $query = $this->db->get(); 
		
            if($query->num_rows() == 1){
                return TRUE;
            }else{
                return FALSE;
            }
            
        } catch (Exception $e) {
            return FALSE;
        }
    }
    
    function get_user($select, $condition){
        try {
            $this->db->select($select);
            $this->db->from('set_user');
            $this->db->where($condition);
            $query = $this->db->get(); 
		
            if($query->num_rows() == 1){
                return $query->row();
            }else{
                return FALSE;
            }
            
        } catch (Exception $e) {
            return FALSE;
        }
    }
    
    function update_user($update, $condition){
        try {
            $this->db->where($condition);
            $this->db->update('set_user', $update);

            return TRUE;

        } catch (Exception $e) {
            echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
    }

    function getCompanyUserInfo($cID){
        try {
             $this->db->select('ID,UserTypeID,UserName,Password,Fname,Lname');
             $this->db->from('set_user');
                $this->db->where(array('CompanyID' => $cID));
                $query = $this->db->get();                 
                if($query->num_rows() == 1){
                    return $query->result();
                }
        } catch (Exception $e) {
            
        }
    }


}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
