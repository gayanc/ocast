<?php

/**
 * Category Model
 * @author : Gayan Chathuranga <gayan.c@eyepax.com> 
 */
class Category_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * Get all category list
     * @param   $lang - default is set to english
     * @table   set_Category(setCAT),set_Category_Lang(setCATL) 
     * @return  category Array
     */
    public function getAll() {
        try {
            $data = array(
                'setCAT.ID',
                'setCATL.Description'
            );
            $this->db->select($data);
            $this->db->from('set_category setCAT');
            $this->db->join('set_category_lang setCATL', 'setCAT.ID = setCATL.CategoryID');
            $this->db->where('setCATL.LangCode',  lang( 'lcode' ));
            $this->db->order_by('setCATL.Description', 'ASC');
            $query = $this->db->get();  
            return $query->result_array();
        } catch (Exception $exc) {
            echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
    }

    function insertSiteCategory($table, $data){
        try {
            return  $this->db->insert($table, $data);
        } catch (Exception $e) {
            echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
        }
    }

    public function getCatLangData($ID){
        try {
            $data = array('*');
            $this->db->select($data);
            $this->db->from('set_category_lang sysCATL');
            $this->db->where('sysCATL.CategoryID',  $ID);
            $this->db->where('sysCATL.LangCode', lang( 'lcode' ));
            $query = $this->db->get();  
            return $query->result_array();           
        } catch (Exception $e) {
            echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();   
        }
    }

    public function getSiteCategory($siteID){
        try {
                    $objSite = new stdClass();
                    $objSite->categories = array();
                    $data = array('*');
                    $this->db->select($data);
                    $this->db->from('trn_site_category tansSCAT');
                    $this->db->where('tansSCAT.SiteID',  $siteID);
                    $query = $this->db->get();  
                    $cat_data = $query->result_array();
                    foreach ($cat_data as $key => $value) {
                        $cat_lang_data = $this->getCatLangData($value['CategoryID']);
                        array_push($objSite->categories, $cat_lang_data);
                    }
                    return $objSite->categories;
        } catch (Exception $e) {            
            echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
        }
    }

    function deleteSiteCategories($table,$id){
         try {
            $this->db->delete($table, array('SiteID' => $id));
        } catch (Exception $e) {
            echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
        }
    }

}

?>
