<?php
/**
 * Qucik Facts Model
 * @author : Gayan Chathuranga <gayan.c@eyepax.com> 
 */
class Testimonial_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    /**
     * Get Degmographic Info
     * @param   $cid - company id
     * @param   $lang - langauge code - 'en' Default
     * @tables   trn_company(trnCMP),trn_company_lang(trnCMPL) 
     * @return  company Array
     */
    public function insertMasterTestimonialData($table, $data){
        try {
            $this->db->trans_start();
            $this->db->insert($table, $data);
            $insert_id = $this->db->insert_id();
            $this->db->trans_complete();
            return  $insert_id;
        } catch (Exception $e) {
            echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
        }
    }

    public function insertTestimonialLangData($table, $data){
        try {
            return  $this->db->insert($table, $data);
        } catch (Exception $e) {
            echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
        }
    }

    public function getSiteTestimonial($siteID){
       try {
                    $data = array('*');
                    $this->db->select($data);
                    $this->db->from('trn_testimonial tansTM');
                    $this->db->join('trn_testimonial_lang tansTML', 'tansTM.ID = tansTML.TestimonialID');
                    $this->db->where('tansTM.SiteID',  $siteID);
                    $this->db->where('tansTML.LangCode',  lang( 'lcode' ));
                    $query = $this->db->get();  
                    return $query->result_array();
        } catch (Exception $e) {            
            echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
        }
    }

    function getParentID($siteID){
        try {
                    $data = array('ID');
                    $this->db->select($data);
                    $this->db->from('trn_testimonial');
                    $this->db->where('SiteID',  $siteID);
                    $query = $this->db->get();  
                    $parentID = $query->result_array();
                    return isset($parentID[0]['ID']) ? $parentID[0]['ID'] : '';
        } catch (Exception $e) {            
            echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
        }    
    }

    function deleteSiteTestimonialData($table,$lngtbl,$id){        
        try {
            $parentID = $this->getParentID($id);
            $this->db->delete($table, array('SiteID' => $id));
            $this->db->delete($lngtbl, array('TestimonialID' => $parentID));
        } catch (Exception $e) {
            echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
        }
    }
}
?>