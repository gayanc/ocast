<?php

/**
 * Cookie Model
 * @author : Gayan Chathuranga <gayan.c@eyepax.com> 
 */
class Cookie_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function getCookieValue(){
        $CI =& get_instance();
        return base64_decode($CI->input->cookie('returnUID'));
    }

    
            
    /**
     * Check the current user is a returning instance
     * @param   $lang - default is set to english
     * @table   set_Category(setCAT),set_Category_Lang(setCATL) 
     * @return  category Array
     */
    public function isReturningUser(){
        $CI =& get_instance();
        $CI->load->model('signup_model');
        $tmp_account_info = array();
        $conroller = $CI->router->fetch_class();
        $methoD = $CI->router->fetch_method();
        $scriptUri = $conroller.'/'.$methoD;
        $returning = false;

       // $email = isset($this->getCookieValue('returnUID')) ? $this->getCookieValue('returnUID') : NULL;
        if($this->getCookieValue('returnUID')) $tmp_account_info = $CI->signup_model->getTempUserByName($this->getCookieValue('returnUID'));
        if (sizeof($tmp_account_info) > 0) {
            if($tmp_account_info[0]['AciveStatus'] =='active'){
                //get user details => table set_user

                $account_info = $CI->signup_model->getAccountInfo($this->getCookieValue('returnUID'));
                $this->session->set_userdata('userDetails', $account_info[0]);
                $this->session->set_userdata('site_id', $account_info[0]['SiteID']);
                $this->session->set_userdata('authentication', true);
                $this->session->set_userdata('previousStep', '');
                $this->session->set_userdata('nextStep', '');
            }else{
                $this->session->set_userdata('authentication', false);
                $this->session->set_userdata('userDetails', $tmp_account_info[0]);
            }
           $returning = true;
        }
        return $returning;
    }


    function validateUser(){
        $valid = false;
        $CI =& get_instance();
        $CI->load->model('signup_model');
        $tmp_account_info = $CI->signup_model->getTempUserByName($this->getCookieValue('returnUID'));
        
        $status = isset($tmp_account_info[0]['AciveStatus']) ? $tmp_account_info[0]['AciveStatus'] : '';
        if (sizeof($tmp_account_info) && $status === 'active') {
            $valid = true;
        }
        // echo sizeof($tmp_account_info);
        // echo ($tmp_account_info[0]['AciveStatus']); exit();
        return $valid;
    }


}

?>
