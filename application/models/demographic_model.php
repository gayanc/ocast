<?php 
/**
 * Demographic Model
 * @author : Gayan Chathuranga <gayan.c@eyepax.com> 
 */

class Demographic_Model extends CI_Model{

	public $headerId;
	public $points;

	function __construct() {
	    parent::__construct();
	}

	 /**
	 * Get Degmographic Info
	 * @param   $sid - site id
	 * @param   $lang - langauge code - 'en' Default
	 * @tables   trn_demographic_header(trnDH),trn_demographic_header_lang(trnDHL) 
	 * @tables   trn_demographic_detail(trnDD), trn_demographic_detail_lang(trnDDL)
	 * @return  category Array
	 */
	public function getDemographicInfo($sid=10,$lang='en'){

		try {
			$data   =   array(
                'trnDD.points', 
                'trnDHL.HeadLine',
                'trnDDL.Answer'
			);

            $this->db->select($data);
            $this->db->from('trn_demographic_header trnDH, trn_demographic_detail trnDD, trn_demographic_header_lang trnDHL, trn_demographic_detail_lang trnDDL');
            $this->db->join('trn_demographic_header_lang', 'trnDH.ID = trnDHL.DemographicHeaderID');
            $this->db->join('trn_demographic_detail', 'trnDH.ID = trnDD.HeaderID');            
            $this->db->where('trnDH.SiteId', $sid);
            $this->db->where('trnDH.SiteId', $sid);
            $this->db->where('trnDHL.LangCode', $lang);
            //$this->db->where('trnDDL.LangCode', $lang);
            $this->db->order_by('trnDH.ID', 'ASC');
           	$query = $this->db->get();
        	return $query->result_array();
        	
        } catch (Exception $e) {
            echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
        }
	}

	/**
	 * Inser Degmographic Header
	 * @param   $data
	 * @param   $lang - langauge code - 'en' Default
	 * @tables   trn_demographic_header(trnDH)
	 * @return  category Array
	 */
	public function insertDemogHeader($table,$data){
		try {
			$this->db->trans_start();
		    $this->db->insert($table, $data);
		    $insert_id = $this->db->insert_id();
		    $this->db->trans_complete();
		    return  $insert_id;
		} catch (Exception $e) {
			echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
		}
	}


	public function insertDemogHeaderLang($table,$data){
		try {
		    return  $this->db->insert($table, $data);
		} catch (Exception $e) {
			echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
		}
	}

	public function insertDemogHeadPoint($table, $data){
		try {
		    $this->db->trans_start();
		    $this->db->insert($table, $data);
		    $insert_id = $this->db->insert_id();
		    $this->db->trans_complete();
		    return  $insert_id;
		} catch (Exception $e) {
			echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
		}
	}

	public function insertDemogPointAnswers($table, $data){
		try {
		    return  $this->db->insert($table, $data);
		} catch (Exception $e) {
			echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
		}
	}

	public function getSiteDemogHeaderInfo($siteID){
		try {
			$data   =   array(
				'trnDH.ID',
                'trnDHL.HeadLine',
			);
            $this->db->select($data);
            $this->db->from('trn_demographic_header trnDH');
            $this->db->join('trn_demographic_header_lang trnDHL', 'trnDH.ID = trnDHL.DemographicHeaderID');          
            $this->db->where('trnDH.SiteID', $siteID);
            $this->db->where('trnDHL.LangCode', lang( 'lcode' ));
            //$this->db->where('trnDDL.LangCode', $lang);
            $this->db->order_by('trnDH.ID', 'ASC');
           	$query = $this->db->get();
        	return $query->result_array();
        	
        } catch (Exception $e) {
            echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
        }
	}
	public function getSiteDemogInfo($siteID=1){
		$demo_head = array();
		
		 try {
		 	$demogHeaderData = $this->getSiteDemogHeaderInfo($siteID);
		 	//print('<pre>');print_r($demogHeaderData); exit;
		 	foreach ($demogHeaderData as $key => $value) {	
		 	$objDemoData = new StdClass();	 			
		 			$objDemoData->HeaderLine = $value['HeadLine'];
		 			$data   =   array(
		                'trnDD.points', 
		                'trnDDL.Answer'
					);
					$this->db->select($data);
		            $this->db->from('trn_demographic_detail trnDD');
		            $this->db->join('trn_demographic_detail_lang trnDDL', 'trnDD.ID = trnDDL.DemographicDetailID','right');            
		            $this->db->where('trnDD.HeaderID', $value['ID']);
		            $this->db->where('trnDDL.LangCode', lang( 'lcode' ));
		           	$query = $this->db->get();
		        	$objDemoData->HeaderData = $query->result_array();
		        	array_push($demo_head, $objDemoData);
		 	}
          //print('<pre>');print_r($demo_head);
		 	return $demo_head;
        	
        } catch (Exception $e) {
            echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
        }
                    
	}

	function getParentHeadID($siteID){
        try {
                    $data = array('ID');
                    $this->db->select($data);
                    $this->db->from('trn_demographic_header');
                    $this->db->where('SiteID',  $siteID);
                    $query = $this->db->get();
                    $parentID = $query->result_array();
                    return isset($parentID) ? $parentID : array();
        } catch (Exception $e) {            
            echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
        }    
    }

    function getParentDetailID($headerID){
        try {
                    $data = array('ID');
                    $this->db->select($data);
                    $this->db->from('trn_demographic_detail');
                    $this->db->where('HeaderID',  $headerID);
                    $query = $this->db->get();
                    $parentID = $query->result_array();
                    return isset($parentID) ? $parentID : array();
        } catch (Exception $e) {            
            echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
        }    
    }

    function deleteSiteDemogData($hdtable,$hdlangtable,$dtltabl,$dtlangtabl,$sid){        
        try {
            $head_data = $this->getParentHeadID($sid);
            	$this->db->delete($hdtable, array('SiteID' => $sid));
            	foreach ($head_data as $value) {
            		$headID = $value['ID'];
            		$this->db->delete($hdlangtable, array('DemographicHeaderID' => $headID));
            		$detail_data = $this->getParentDetailID($headID);		            	
		            	$this->db->delete($dtltabl, array('HeaderID' => $headID));
		            foreach ($detail_data  as $value) {            	
		            	$this->db->delete($dtlangtabl, array('DemographicDetailID' => $value['ID']));
		            } 	
            	}
             
        } catch (Exception $e) {
            echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
        }
    }

}