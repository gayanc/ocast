<?php 
/**
 * Price Types Model
 * @author : Gayan Chathuranga <gayan.c@eyepax.com> 
 */
class Price_Type_Model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}

	/**
	 * Get All Price Types
	 * @tables   set_Price_Type(setPRT), set_Price_Type_Lang(setPRTL)
	 * @return  Format Types Array
	 */
	public function getAll(){
		try {
            $data = array(
                'setPRT.*',
                'setPRTL.*'
            );
            $this->db->select($data);
            $this->db->from('set_Format_Type setFMT, set_Price_Type_Lang setPRTL');
            $this->db->join('set_Price_Type_Lang', 'setPRT.ID = setPRTL.PriceTypeID');
            $this->db->where('setPRTL.LangCode', $lang);
            $this->db->order_by('setPRTL.text', 'ASC');
            $query = $this->db->get();
            return $query->result_array();
        } catch (Exception $exc) {
            echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
	}
}