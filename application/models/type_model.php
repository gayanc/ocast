<?php 
/**
 * Price type Model
 * @author : Gayan Chathuranga <gayan.c@eyepax.com> 
 */
class Type_Model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}

	/**
	 * Get All Format Types
	 * @tables   trn_demographic_header(trnDH),trn_demographic_header_lang(trnDHL) 
	 * @tables   trn_demographic_detail(trnDD), trn_demographic_detail_lang(trnDDL)
	 * @return  category Array
	 */
	public function getAll(){
		
	}
}