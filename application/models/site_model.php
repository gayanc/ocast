<?php

class Site_model extends CI_Model {


    function __construct() {
        parent::__construct();
        $this->load->model('Contact_Model');
        $this->load->model('Category_Model');
        $this->load->model("Quickfac_Model");
        $this->load->model("Country_Model");
        $this->load->model("Demographic_Model");
        $this->load->model("Testimonial_Model");
        $this->load->model("Case_Model");
        $this->load->model("Format_Model");
    }

    function getAllActiveSites() {
        $result = null;
        $sql = "select * from trn_site";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result();
        }
        return $result;
    }

    function GetSitesTochechCronResult() {
        $result = null;
        $sql = "select * from trn_site";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result();
        }
        return $result;
    }

    function add_site($data) {
        //insert site to temp table in sign up, once confirm we move data permenantly
        switch ($data->mode) {
            case 'acc_signup':
                $this->db->insert('tmp_account_info', $Data);
                break;
            case 'acc_confirm':
                //$this->db->insert('trn_site', $Data);
                break;
        }
       
    }
    //add user site when google site is connected
    function addUserSite($data){
        try {
            $this->db->insert('set_user_site', $data);
        } catch (Exception $e) {
             echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
    }

    function save_google_site($data)
    {
         try {
                $this->db->insert('trn_site', $data);
                $siteID = $this->db->insert_id();
                $this->session->set_userdata('site_id', $siteID);//store site_id in session 
                $user_site_data = array(
                    'UserID' => $this->session->userdata['user_id'],
                    'SiteID' => $siteID,
                    'CreatedDate' => getDateTime(),
                    'CreatedBy' => $this->session->userdata['session_id'],
                    'LastModifiedDate' => getDateTime(),
                    'LastModifiedBy' => $this->session->userdata['session_id'] 
                );
                $this->addUserSite($user_site_data);
        } catch (Exception $e) {
             echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
    }

    function CrongetAllSites($RequestedSiteId = null) {
        // SELECT *, `trn_site`.`ID` as SiteID, DATEDIFF(date(now()), `trn_site`.`AnalyticsCreatedDate`) as duration FROM (`trn_site`) LEFT JOIN `trn_google_profile` ON `trn_site`.`ProfileID` = `trn_google_profile`.`ID`
        $result = null;
        $this->db->select(" *, `trn_site`.`ID` as SiteID, trn_site.AnalyticsCreatedDate as addeddate ");
        $this->db->from('trn_site');
        $this->db->join('trn_google_profile', 'trn_site.ProfileID = trn_google_profile.ID', 'left');
        if ($RequestedSiteId) {
            $this->db->where('`trn_site`.`ID`', $RequestedSiteId);
        }
        //$this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
        } 
        return $result;
    }

    function update_token($ProfileID, $token) {
        $data = array('Token' => $token);
        $this->db->where('ID', $ProfileID);
        $this->db->update('trn_google_profile', $data);
    }

    function get_token_by_site($SiteID) {
        $result = null;
        $this->db->select('Token');
        $this->db->from('trn_site');
        $this->db->join('trn_google_profile', 'trn_site.ProfileID = trn_google_profile.ID', 'left');
        $this->db->where('trn_site.ID', $SiteID);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
        }
        return $result;
    }

    function update_lastgoogle_date($SiteID, $LastGoogleUpdate) {
        $data = array('LastGoogleUpdate' => $LastGoogleUpdate);
        $this->db->where('ID', $SiteID);
        $this->db->update('trn_site', $data);
    }

    /**
     *  
     * @param    
     * @table    
     * @return 
     * @author : Gayan Chathuranga <gayan.c@eyepax.com>
     */
     public function updateSiteInfo($SiteID,$data){
        $this->db->where('ID', $SiteID);
        $this->db->update('trn_site', $data);
     }


     public function updateSiteLangInfo($SiteID,$data){
        $this->db->where('SiteID', $SiteID);
        $this->db->update('trn_site_lang', $data);
     }

    function get_google_sites() {

        if (!$this->session->userdata('access_token')) {
            redirect(base_url('connectgoogle'), 'location');
        }
        else {
            $client = new Google_Client();
            $accessToken = $this->session->userdata('access_token');
            $obj_service = $this->Marketuser_model->create_google_service($client);
            $this->Marketuser_model->config_google_client($client, $accessToken);
            $account_list = $this->Marketuser_model->get_analytics_accounts($client, $obj_service);
            return $account_list;
        }
    }

    function ExecuteGoogleQuery($ProfileID, $Token, $GoogleSiteID, $report_start_date, $report_end_date, $metrics, $dimensions = null, $sort = null, $start_index = null, $max_results = null, $segment = null, $filter = null) {

        $dimensions = "";
        $TokenIsExpired = "0";
        $optParams = array();
        if ($dimensions != "") {
            $optParams["dimensions"] = $dimensions;
        }

        if ($segment != "") {
            $optParams["segment"] = $segment;
        }

        if ($filter != "") {
            $optParams["filter"] = $filter;
        }
        
        try {
            $client = new Google_Client();
            $obj_service = $this->Marketuser_model->create_google_service($client);
            $this->Marketuser_model->config_google_client($client, $Token);
            $client->setUseObjects(true);

            if ($this->Marketuser_model->is_google_token_expired($client)) {
                $TokenIsExpired = 1;
            }
            $results = $obj_service->data_ga->get('ga:' . $GoogleSiteID, $report_start_date, $report_end_date, $metrics,$optParams);
            if ($TokenIsExpired == 1) {
                $Token = $this->Marketuser_model->get_google_Token($client);
                $this->update_token($ProfileID, $Token);
            }

            if (count($results->getRows()) > 0) {
                return ($results->totalsForAllResults);
            }
        } catch (Exception $e) {
            //  echo $e->getMessage(); 
            return null;
        }
        return null;
    }

    function change_Isdownloading($SiteID, $status) {
        $data = array('Isdownloading' => $status);
        $this->db->where('ID', $SiteID);
        $this->db->update('trn_site', $data);
    }

    
    public function updateSignUpStep($stp='1',$uid){
        $data = array('StepNumber' => $stp);
        $this->db->where('UserID', $uid);
        $this->db->update('sys_registration_history', $data);
    }

    public function insertSignUpStep($stp='3',$uid){
        $data = array('StepNumber' => $stp);
        $this->db->where('UserID', $uid);
        $this->db->insert('sys_registration_history', $data);
    }

    public function getSiteParentData($siteID){
        try {
            $data = array('*');
            $this->db->select($data);
            $this->db->from('trn_site tansST');
            $this->db->join('trn_site_lang tanSTL', 'tansST.ID = tanSTL.SiteID');
            $this->db->where('tansST.ID',  $siteID);
            $this->db->where('tanSTL.LangCode',  lang( 'lcode' ));
            $query = $this->db->get();  
            return $query->result_array();
        } catch (Exception $e) {
            
        }
    }

    public function getSiteCompanyID($siteID){
         try {
            $data = array('CompanyID');
            $this->db->select($data);
            $this->db->from('trn_site tansST');
            $this->db->where('tansST.ID',  $siteID);
            $query = $this->db->get();  
            $compID = $query->result_array();
            return isset($compID[0]['CompanyID']) ? $compID[0]['CompanyID'] : null;;
        } catch (Exception $e) {
            
        }
    }

    public function getSiteData($siteID){
        $compID = $this->getSiteCompanyID($siteID);
        $data = array(
            'master_data' => $this->getSiteParentData($siteID),
            'company_data' => $this->Company_Model->getCompanyData($compID), 
            'contact_data' => $this->Contact_Model->getSiteContact($siteID),
            'country_data' => $this->Country_Model->getSiteCountry($siteID),
            'category_data' => $this->Category_Model->getSiteCategory($siteID),
            'qfact_data' => $this->Quickfac_Model->getSiteQfacts($siteID),
            'testm_data' => $this->Testimonial_Model->getSiteTestimonial($siteID),
            'demog_data' => $this->Demographic_Model->getSiteDemogInfo($siteID),
            'case_data' => $this->Case_Model->getSiteCases($siteID),
            'format_data' => $this->Format_Model->getSiteAllFormats($siteID)
        );
        return $data;
    }

}

?>
