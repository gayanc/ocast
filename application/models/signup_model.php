<?php

/**
 * Signup Model
 * @author : Gayan Chathuranga <gayan.c@eyepax.com> 
 */
class Signup_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * Insert temporary site and company information
     * @param   $lang - default is set to english
     * @table   tmp_account_info
     * @return  
     */
    public function insertData($data, $lang = 'en') {
        try {
            return $this->db->insert('tmp_account_info', $data);
        } catch (Exception $e) {
            echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
    }

    /**
     * Send confirmation mail to the session user(unknown)
     * @param   $lang - default is set to english
     * @table   tmp_account_info
     * @return  
     */
    public function sendConfirmationMail() {
        try {
            $this->db->select('*');
            $this->db->from('tmp_account_info tmpACCInfo');
            $this->db->where('tmpACCInfo.SessionID', $this->session->userdata['session_id']);
            $this->db->where('tmpACCInfo.LangCode', $this->session->userdata['lang']);
            $query = $this->db->get();
            $data = $query->result_array();
            print_r($data);
            $toEmail = $data[0]['UserName'];
            //sending email
            $this->load->library('email');
            $this->email->from('info@ocast.se', 'OCAST Inc');
            $this->email->to($toEmail);
            $this->email->subject('Activate your ocast account');
            $this->email->message('Message Body');
            $this->email->send(
            );
        } catch (Exception $e) {
            echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
    }

    /**
     *  
     * @param    
     * @table    
     * @return 
     * @author : Nadun 
     */
    public function UpdateTmpAccountInfo($ActivationCode) {
        try {
            $this->db->where('ActivationCode', $ActivationCode);
            $this->db->update('tmp_account_info', array('AciveStatus' => 'active'));
        } catch (Exception $e) {
            echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
    }

    function getByUserName($UserName) {
        try {
            $this->db->distinct();
            $this->db->select('*');
            $this->db->from('tmp_account_info');
            $this->db->where('UserName', $UserName);
            $query = $this->db->get();
            $this->db->last_query();
            return $query->result_array();
        } catch (Exception $e) {
            echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
    }

    function getTempUserByName($UserName) {
        try {
            $this->db->distinct();
            $this->db->select('*');
            $this->db->from('tmp_account_info');
            $this->db->where('UserName', $UserName);
            $query = $this->db->get();
            $this->db->last_query();
            return $query->result_array();
        } catch (Exception $e) {
            echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
    }

    function getAccountInfo($UserName) {
        try {
            $data = array(
              'setUSR.*',
              'setUSITE.SiteID'
            );
            $this->db->distinct();
            $this->db->select($data);
            $this->db->from('set_user setUSR');
            $this->db->join('set_user_site setUSITE', 'setUSR.ID = setUSITE.UserID', 'left');
            $this->db->where('setUSR.UserName', $UserName);
            $query = $this->db->get();
            
            return $query->result_array();
        } catch (Exception $e) {
            echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
    }

    function removeUser($id) {
        $this->db->where('UserName', $id);
        $this->db->delete('tmp_account_info');

        $this->db->where('UserName', $id);
        $this->db->delete('set_user');
    }

    function getUserByAcivationcode($ActivationCode) {
        try {

            $data = array('tmp_account_info.ID',
              'tmp_account_info.ActivationCode',
              'tmp_account_info.LangCode',
              'tmp_account_info.UserName',
              'tmp_account_info.Password',
              'tmp_account_info.Fname',
              'tmp_account_info.Lname',
              'tmp_account_info.Company',
              'tmp_account_info.PhoneCountryCode',
              'tmp_account_info.PhoneNumber',
              'tmp_account_info.Address1',
              'tmp_account_info.Address2',
              'tmp_account_info.Zip',
              'tmp_account_info.City',
              'tmp_account_info.CountryID',
              'tmp_account_info.AciveStatus',
              'set_user.ID as user_id',
              'set_user.CompanyID'
              );
            

            $this->db->distinct();
            $this->db->select($data);
            $this->db->from('tmp_account_info');
            $this->db->join('set_user', 'tmp_account_info.UserName = set_user.UserName', 'left');
            $this->db->where('tmp_account_info.ActivationCode', $ActivationCode);
            $query = $this->db->get();
            return $query->result_array();
        } catch (Exception $e) {
            echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
    }

    function IsAddedSite($CompanyId) {
        try {
            
            $CompanyID = $this->session->userdata('$CompanyId');
            $data = array('SiteName', 'URL');
            $this->db->select($data);
            $this->db->from('trn_site');
            $this->db->where('CompanyID', $CompanyID);
            $query = $this->db->get();
            //$this->db->last_query();
            return $query->result_array();
            
        } catch (Exception $e) {
            echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
    }

}

?>
