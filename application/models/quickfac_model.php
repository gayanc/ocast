<?php
/**
 * Qucik Facts Model
 * @author : Gayan Chathuranga <gayan.c@eyepax.com> 
 */
class Quickfac_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    /**
     * Get Degmographic Info
     * @param   $cid - company id
     * @param   $lang - langauge code - 'en' Default
     * @tables   trn_company(trnCMP),trn_company_lang(trnCMPL) 
     * @return  company Array
     */
    public function insertMasterQFactsData($table, $data){
        try {
            $this->db->trans_start();
            $this->db->insert($table, $data);
            $insert_id = $this->db->insert_id();
            $this->db->trans_complete();
            return  $insert_id;
        } catch (Exception $e) {
            echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
        }
    }

    public function insertQFactsLangData($table, $data){
        try {
            return  $this->db->insert($table, $data);
        } catch (Exception $e) {
            echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
        }
    }
    public function getSiteQfacts($siteID){
        try {
                    $data = array('*');
                    $this->db->select($data);
                    $this->db->from('trn_quick_fact tansQFAC');
                    $this->db->join('trn_quick_fact_lang tansQFACL', 'tansQFAC.ID = tansQFACL.QuickFactID');
                    $this->db->where('tansQFAC.SiteID',  $siteID);
                    $this->db->where('tansQFACL.LangCode',  lang( 'lcode' ));
                    $query = $this->db->get();  
                    return $query->result_array();
        } catch (Exception $e) {            
            echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
        }
    }

    function getParentID($siteID){
        try {
                    $data = array('ID');
                    $this->db->select($data);
                    $this->db->from('trn_quick_fact');
                    $this->db->where('SiteID',  $siteID);
                    $query = $this->db->get();  
                    $parentID = $query->result_array();
                    return isset($parentID[0]['ID']) ? $parentID[0]['ID'] : '';
        } catch (Exception $e) {            
            echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
        }    
    }

    function deleteSiteQfacts($table,$lngtbl,$id){        
        try {
            $parentID = $this->getParentID($id);
            $this->db->delete($table, array('SiteID' => $id));
            $this->db->delete($lngtbl, array('QuickFactID' => $parentID));
        } catch (Exception $e) {
            echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
        }
    }
}
?>