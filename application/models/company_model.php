<?php
/**
 * Company Model
 * @author : Gayan Chathuranga <gayan.c@eyepax.com> 
 */
class Company_Model extends CI_Model {

    public $ID;
    public $Name;
    public $Phone;
    public $Address1;
    public $Address2;
    public $Zip;
    public $Country;
    private $CreatedDate;
    private $LastModifiedDate;
    private $LastModifiedBy;
    private $CreatedBy;

    public function __construct() {
        parent::__construct();
    }

    /**
     * Get Degmographic Info
     * @param   $cid - company id
     * @param   $lang - langauge code - 'en' Default
     * @tables   trn_company(trnCMP),trn_company_lang(trnCMPL) 
     * @return  company Array
     */
    public function getCompanyDetails($id, $lang) {
        try {
            $data = array(
                'trnCMP.Phone',
                'trnCMP.Zip',
                'trnCMP.CountryID',
                'trnCMPL.Name',
                'trnCMPL.City',
                'trnCMPL.Address1',
                'trnCMPL.Address2',
            );

            $this->db->select($data);
            $this->db->from('trn_company trnCMP');
            $this->db->join('trn_company_lang', 'trnCMP.ID = trnCMPL.CompanyID');
            $this->db->where('trnCMP.ID', $id);
            $this->db->where('trnCMPL.LangCode', lang( 'lcode' ));
            $this->db->order_by('trnCMP.ID', 'ASC');
            $query = $this->db->get();
            return $query->result_array();
        } catch (Exception $e) {
            echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
    }
    
    /**
     * Save Company details in temporary table
     * @param array $data  
     * @author Nadun <nadun.n@eyepax.com>
     */
    
    function addTmpCompany($data)
    {
         try {
             $this->db->insert('tmp_company', $data);
             return $this->db->insert_id();
             
        } catch (Exception $e) {
            echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
    }
    
    
     /**
     * Save Company details in temporary table which are vary acoording to language
     * @param array $data  
     * @author Nadun <nadun.n@eyepax.com>
     */
    
    function addTmpCompanyLang($data)
    { 
         try {
             $this->db->insert('tmp_company_lang', $data); 
             return $this->db->insert_id();
             
        } catch (Exception $e) {
            echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
    }
    
    function addCompnay($data)
    {
         try {
             $this->db->insert('trn_company', $data);
             return $this->db->insert_id();
             
        } catch (Exception $e) {
            echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
    }
    
    
    function addCompnayLang($data)
    {
         try {
             $this->db->insert('trn_company_lang', $data);
             return $this->db->insert_id();
             
        } catch (Exception $e) {
            echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
    }

    function getCompanyData($id){
        try {
            $data = array(
                'trnCMP.Phone',
                'trnCMP.Zip',
                'trnCMP.CountryCallingCode',
                'trnCMP.CountryID',
                'trnCMPL.Name',
                'trnCMPL.City',
                'trnCMPL.Address1',
                'trnCMPL.Address2',
            );
            $this->db->select($data);
            $this->db->from('trn_company trnCMP');
            $this->db->join('trn_company_lang trnCMPL', 'trnCMP.ID = trnCMPL.CompanyID');
            $this->db->where('trnCMP.ID', $id);
            $this->db->where('trnCMPL.LangCode', lang( 'lcode' ));
            $this->db->order_by('trnCMP.ID', 'ASC');
            $query = $this->db->get();
            return $query->result_array();
        } catch (Exception $e) {
            echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
    }

    function updateCompanyInfo($data,$lang_data){ //main tbl
        try {
            $CI = get_instance();
            $company_id = $CI->session->userdata['userDetails']['CompanyID'];

            $this->db->where('ID',$company_id);
            $this->db->update('trn_company', $data);
                $this->updateCompanyLangInfo($lang_data);
            return true;
        } catch (Exception $e) {
            echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
    }

    function updateCompanyLangInfo($lang_data){ //lang tbl
        try {
            $CI = get_instance();
            $company_id = $CI->session->userdata['userDetails']['CompanyID'];
            $this->db->where('CompanyID', $company_id);
            $query = $this->db->update('trn_company_lang', $lang_data);
            return true;
        } catch (Exception $e) {
            echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
    }

    
    
    
    

}

