<?php

/**
 * Country Model
 * @author : Gayan Chathuranga <gayan.c@eyepax.com> 
 */
class Device_Model extends CI_Model {

    function __construct() {
        parent::__construct();
         
    }

    /**
     * Get Degmographic Info
     * @param   $sid - site id
     * @param   $lang - langauge code - 'en' Default
     * @tables   trn_demographic_header(trnDH),trn_demographic_header_lang(trnDHL) 
     * @tables   trn_demographic_detail(trnDD), trn_demographic_detail_lang(trnDDL)
     * @return  category Array
     */
    public function getSiteDeviceData($sid = 10, $lang = 'en') {

        try {
            $data = array(
                'trnDD.points',
                'trnDHL.HeadLine',
                'trnDDL.Answer'
            );

            $this->db->select($data);
            $this->db->from('trn_demographic_header trnDH, trn_demographic_detail trnDD, trn_demographic_header_lang trnDHL, trn_demographic_detail_lang trnDDL');
            $this->db->join('trn_demographic_header_lang', 'trnDH.ID = trnDHL.DemographicHeaderID');
            $this->db->join('trn_demographic_detail', 'trnDH.ID = trnDD.HeaderID');
            $this->db->where('trnDH.SiteId', $sid);
            $this->db->where('trnDH.SiteId', $sid);
            $this->db->where('trnDHL.LangCode', $lang);
            $this->db->order_by('trnDH.ID', 'ASC');
            $query = $this->db->get();
            return $query->result_array();
        } catch (Exception $e) {
            echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
    }

    /**
     * Get All Devices
     * @param   ''
     * @param   $lang - langauge code - 'en' Default
     * @tables   sys_device(sysD),sys_device_lang(sysDL) 
     * @return  Devices Array
     */
    public function getAll($lang='en') {
        try {
            $data   =   array(
                'sysD.*', 
                'sysDL.*'
            );
            $this->db->select($data);
            $this->db->from('sys_device sysD');
            $this->db->join('sys_device_lang sysDL', 'sysD.ID = sysDL.DeviceID');
            $this->db->where('sysDL.LangCode',  lang('lcode'));
            $this->db->order_by('sysD.SortingOrder', 'ASC');
            $query = $this->db->get(); 
            return $query->result_array();
        } catch (Exception $exc) {
            echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
    }

}