<?php
/**
 * Case Model
 * @author : Gayan Chathuranga <gayan.c@eyepax.com> 
 */
class Case_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    /**
     * insertMasterCaseData
     * @param   $cid - company id
     * @param   $lang - langauge code - 'en' Default
     * @tables   trn_company(trnCMP),trn_company_lang(trnCMPL) 
     * @return  company Array
     */
    public function insertMasterCaseData($table, $data){
        try {
            $this->db->trans_start();
            $this->db->insert($table, $data);
            $insert_id = $this->db->insert_id();
            $this->db->trans_complete();
            return  $insert_id;
        } catch (Exception $e) {
            echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
        }
    }

    public function insertCaseLangData($table, $data){
        try {
            return  $this->db->insert($table, $data);
        } catch (Exception $e) {
            echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
        }
    }

    //trn_case_lang/trn_case
    public function getSiteCases($SiteID){
        try {
            $data = array(
                'tranCASL.*',
                'tranCAS.Image'
            );
            $this->db->select($data);
            $this->db->from('trn_case_lang tranCASL');
            $this->db->join('trn_case tranCAS', 'tranCAS.ID = tranCASL.CaseID');
            $this->db->where('tranCAS.SiteID',  $SiteID);
            $this->db->where('tranCASL.LangCode',  lang( 'lcode' ));
            $this->db->order_by('tranCASL.LastModifiedDate', 'DESC');
            $query = $this->db->get();  
            return $query->result_array();
            } catch (Exception $e) {
                 echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
            }
    
    }

    function getParentID($siteID){
        try {
                    $data = array('ID');
                    $this->db->select($data);
                    $this->db->from('trn_case');
                    $this->db->where('SiteID',  $siteID);
                    $query = $this->db->get();  
                    $parentID = $query->result_array();
                    return isset($parentID[0]['ID']) ? $parentID[0]['ID'] : '';
        } catch (Exception $e) {            
            echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
        }    
    }

    function deleteSiteCases($table,$lngtbl,$id){        
        try {
            $parentID = $this->getParentID($id);
            $this->db->delete($table, array('SiteID' => $id));
            $this->db->delete($lngtbl, array('CaseID' => $parentID));
        } catch (Exception $e) {
            echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
        }
    }
}
?>