<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Marketuser_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->library('google/Google_Client');
    }

    function create_google_service($obj_gclient) {

        $client = $obj_gclient; // Set parameters, Googl API console account details https://code.google.com/apis/console 
        $client->setAccessType($this->config->item('AccessType'));
        $client->setApplicationName($this->config->item('ApplicationName'));
        $client->setClientId($this->config->item('ClientId'));
        $client->setClientSecret($this->config->item('ClientSecret'));
        $client->setRedirectUri($this->config->item('RedirectUri'));
        $client->setDeveloperKey($this->config->item('DeveloperKey'));
        $client->setScopes($this->config->item('Scopes'));
        $service = new Google_AnalyticsService($client);
        return $service;
    }

    function config_google_client($obj_gcliet, $accessToken) { 
        $obj_gcliet->setAccessToken($accessToken);
    }

    function get_google_Token($obj_gcliet) {
        return $obj_gcliet->getAccessToken();
    }

    function is_google_token_expired($obj_gcliet) {
        return $obj_gcliet->isAccessTokenExpired();
    }

    function get_analytics_accounts($obj_gcliet, $obj_service) {
        $service = $obj_service;
        $client = $obj_gcliet;
        $props = $service->management_webproperties->listManagementWebproperties("~all");
        $accounts = $service->management_accounts->listManagementAccounts();
        $client->setUseObjects(true);
        $account = array();
        //$acount_details = array();  
        if (isset($props['items'])) {
            
            foreach ($props['items'] as $item) {
                $accountId = $item["accountId"];
                $webPropertyId = $item["id"];
                $site_name = $item['name'];
                $created = $item['created'];
                // $acount_details["account_name"] = $site_name;
                $profiles = $service->management_profiles->listManagementProfiles($accountId, $webPropertyId);
                $items = $profiles->getItems();
                if (count($items) != 0) {
                    foreach ($items as &$profile) {
                        $prof_id = $profile->getId();  
                        $acount_details =  array('GoogleSiteID'=>$prof_id,'SiteName'=>$site_name,'CreatedDate'=>substr($created, 0, 10));  
                         
                    }
                    $account[] = $acount_details;
                }

                //unset($acount_details);
            }
        }
        else {
            return null;
        }

        $client->setUseObjects(FALSE);
        return $account;
    }

    function update_access_token($access_token) {
        $id = $this->session->userdata('username_id');
        $data = array('access_token' => $access_token);
        $this->db->where('id', $id);
        $this->db->update('set_marketuser', $data);
       // echo $this->db->last_query(); exit;
    }

    function get_user_token() { // Get user token from database.
        $uid = $this->session->userdata('username_id');
        $token = "";
        $query = $this->db->query("select  access_token from set_marketuser where id ='" . $uid . "' and access_token is not null");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $token = $row->access_token;
            }
            return $token;
        }
    }

    function user_activeation($code) {
        $query = $this->db->query("select  id from set_marketuser where activation_code ='" . $code . "' and active_status='pending'");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $id = $row->id;
            }
            $data = array('active_status' => 'active');
            $this->db->where('id', $id);
            return $this->db->update('set_marketuser', $data);
        }
        else {
            return 2;
        }
    }

    function get_login_details($user_name, $password) {

        $query = $this->db->query("select * from set_marketuser where user_name ='" . $user_name . "' and password = '" . md5($password) . "' and active_status='active'");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $id = $row->id;
                $add_session = array('username_id' => $id, 'logged_in' => TRUE);
                $this->session->set_userdata($add_session);
                return 4;
            }
        }
        else {
            return 3;
        }
    }

}

?>
