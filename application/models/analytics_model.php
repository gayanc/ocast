<?php

class Analytics_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function add_analytics($Data) {
        $this->db->insert('trn_analytics', $Data);
    }

    Function GetDataTochechCronResult($SiteID) {
        try {
            $result = null;
            $this->db->select('*');
            $this->db->from('trn_analytics');
            $this->db->order_by('Date','DESC');
            $this->db->where('SiteID', $SiteID);
            $this->db->group_by('Date'); 
            $query = $this->db->get();
            if ($query->num_rows() > 0) { 
                $result = $query->result(); 
                return $result;
            }
        } catch (Exception $e) {
             echo $e->getMessage();
        }
    }

}

?>
