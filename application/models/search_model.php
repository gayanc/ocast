<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Provide data for search queries.
 *
 * @author nadun_n
 */
class search_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function detault_search($start = null, $numpages = null) {
        try {
            $data = array(
              'trn_site.ID as SiteID',
              'trn_site.URL',
              'trn_site.Logo',
              'trn_site.PublishedDate',
              'trn_site.SiteName', 'trn_company_lang.Name as CompanyName',
              'SUM(UniqueVisits) as UniqueVisits',
              'SUM(PageViews) as PageViews',
              'SUM(Visits) as Visits'
            );
            $this->db->select($data);
            $this->db->from('trn_site');
            $this->db->join('trn_analytics', 'trn_site.ID = trn_analytics.SiteID', 'left');
            $this->db->join('trn_site_country', 'trn_site.ID = trn_site_country.SiteID', 'left');
            $this->db->join('trn_site_category', 'trn_site.ID = trn_site_category.SiteID', 'left');
            $this->db->join('trn_site_device', 'trn_site.ID = trn_site_device.SiteID', 'left');
            $this->db->join('trn_format', 'trn_site.ID = trn_format.SiteID', 'left');
            $this->db->join('trn_company_lang', 'trn_site.CompanyID = trn_company_lang.CompanyID', 'left');
            $this->db->where('trn_company_lang.LangCode', lang('lcode'));
            $this->db->where('trn_analytics.date >date_sub(DATE(now()), INTERVAL 30 DAY)');
            /*
              $this->db->where_in('trn_site_country.CountryID','');
              $this->db->where_in('trn_site_category.CategoryID','');
              $this->db->where_in('trn_site_device.DeviceID','');
              $this->db->where_in('trn_format.TypeID',''); */

            $this->db->group_by('trn_site.ID');
            $this->db->order_by('trn_site.ID', 'ASC');
            if ($start > 0 && $numpages > 0) {
                $this->db->limit($numpages, $start);
            }
            else if ($numpages > 0) {
                $this->db->limit($numpages);
            }

            $query = $this->db->get();

            // $this->db->last_query(); 
            return $query->result_array();
        } catch (Exception $exc) {
            echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
    }

    function main_search($dataArr = null) {
        try {
            $country_list = "";
            $category_list = "";
            $gender_list = "";
            $device_list = "";
            $type_list = "";
            $age_from = "";
            $age_to = "";
            $sortingkey = "";
            $rowCount = "";
            $sortingOrder = "";
            $numpages = "";

            $country_list = (isset($dataArr["country_list"]) == TRUE) ? $dataArr["country_list"] : '';
            $category_list = (isset($dataArr["category_list"]) == TRUE) ? $dataArr["category_list"] : '';
            $gender_list = (isset($dataArr["gender_list"]) == TRUE) ? $dataArr["gender_list"] : '';
            $device_list = (isset($dataArr["device_list"]) == TRUE) ? $dataArr["device_list"] : '';
            $type_list = (isset($dataArr["type_list"]) == TRUE) ? $dataArr["type_list"] : '';
            $age_from = (isset($dataArr["age_from"]) == TRUE) ? $dataArr["age_from"] : '';
            $age_to = (isset($dataArr["age_to"]) == TRUE) ? $dataArr["age_to"] : '';
            $sortingkey = (isset($dataArr["sortingkey"]) == TRUE) ? $dataArr["sortingkey"] : '';
            $rowCount = (isset($dataArr["rowCount"]) == TRUE) ? $dataArr["rowCount"] : '';
            $sortingOrder = (isset($dataArr["sortingOrder"]) == TRUE) ? $dataArr["sortingOrder"] : '';
            $numpages = (isset($dataArr["numpages"]) == TRUE) ? $dataArr["numpages"] : NO_OF_REC_IN_SEARCH; 

            $data = array(
              'trn_site.ID as SiteID',
              'trn_site.URL',
              'trn_site.Logo',
              'trn_site.PublishedDate',
              'trn_site.SiteName', 'trn_company_lang.Name as CompanyName',
              'SUM(UniqueVisits) as UniqueVisits',
              'SUM(PageViews) as PageViews',
              'SUM(Visits) as Visits'
            );
            $this->db->select($data);
            $this->db->from('trn_site');
            $this->db->join('trn_analytics', 'trn_site.ID = trn_analytics.SiteID', 'left');
            $this->db->join('trn_site_country', 'trn_site.ID = trn_site_country.SiteID', 'left');
            $this->db->join('trn_site_category', 'trn_site.ID = trn_site_category.SiteID', 'left');
            $this->db->join('trn_site_device', 'trn_site.ID = trn_site_device.SiteID', 'left');
            $this->db->join('trn_format', 'trn_site.ID = trn_format.SiteID', 'left');
            $this->db->join('trn_company_lang', 'trn_site.CompanyID = trn_company_lang.CompanyID', 'left');
            $this->db->where('trn_company_lang.LangCode', lang('lcode'));
            $this->db->where('trn_analytics.date >date_sub(DATE(now()), INTERVAL 30 DAY)');
            if ($country_list != "") {
                $this->db->where_in('trn_site_country.CountryID', $country_list);
            }
            if ($category_list != "") {
                $this->db->where_in('trn_site_category.CategoryID', $category_list);
            }

            if ($device_list != "") {
                $this->db->where_in('trn_site_device.DeviceID', $device_list);
            }

            if ($type_list != "") {
                $this->db->where_in('trn_format.TypeID', $type_list);
            }

            if ($sortingkey != "") {
                $this->db->order_by($sortingkey, $sortingOrder);
            }

           $this->db->group_by('trn_site.ID');

           $this->db->limit($numpages, $rowCount);
            
           $query = $this->db->get();

          // echo $this->db->last_query();
            return $query->result_array();
        } catch (Exception $exc) {
            echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
    }

}

?>
