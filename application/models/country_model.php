<?php

/**
 * Country Model
 * @author : Gayan Chathuranga <gayan.c@eyepax.com> 
 */
class Country_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * Get all country list with its code names - Accending order
     * @param   $lang - default is set to english
     * @table   sys_Country(sysCNT), sys_Country_Lang(sysCNTL)
     * @return  country Array
     */
     public function getAll() {
        try {
            
            $data = array(
                'sysCNT.*',
                'sysCNTL.*'
            );
            $this->db->select($data);
            $this->db->from('sys_country sysCNT');
            $this->db->join('sys_country_lang sysCNTL', 'sysCNT.ID = sysCNTL.CountryID');
            $this->db->where('sysCNTL.LangCode', lang( 'lcode' ));
            $this->db->order_by('sysCNTL.CountryName', 'ASC');
            $this->db->limit(5,4);
            $query = $this->db->get(); 
            
            return $query->result_array();
        } catch (Exception $exc) {
            echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
    }

    /**
     * Get all country calling ID list
     * @param   $lang - default is set to english
     * @table   sys_country(sysCNT)
     * @return  Calling List Array
     */
    public function getCallingID(){
        try {
            
            $data = array(
                'sysCNT.*'
            );
            $this->db->select($data);
            $this->db->from('sys_country sysCNT');
            $this->db->order_by('sysCNT.ID', 'ASC');
            $query = $this->db->get(); 
            
            return $query->result_array();
        } catch (Exception $exc) {

            echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
    }

    public function insertSiteCountry($table, $data){
        try {
            return  $this->db->insert($table, $data);
        } catch (Exception $e) {
            echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
        }
    }

    public function getCountryLangName($ID){
        try {
                    $data = array('*');
                    $this->db->select($data);
                    $this->db->from('sys_country_lang sysCNTL');
                    $this->db->where('sysCNTL.CountryID',  $ID);
                    $this->db->where('sysCNTL.LangCode', lang( 'lcode' ));
                    $query = $this->db->get();  
                    return $query->result_array();                    
        } catch (Exception $e) {            
            echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
        }
    }

    public function getCountryCallingIDByCode($ID){
        try {
            
            $data = array(
                'sysCNT.CallingCode'
            );
            $this->db->select($data);
            $this->db->from('sys_country sysCNT');
            $this->db->where('sysCNT.ID',  $ID);
            $query = $this->db->get(); 
            
            return $query->result_array();
        } catch (Exception $exc) {

            echo 'Exception occured: ' . $e->getCode() . ' - ' . $e->getMessage() . ' in File: ' . $e->getFile() . ', and on line: ' . $e->getLine();
        }
    }

    public function getSiteCountry($siteID){
        try {
                    $objSite = new stdClass();
                    $objSite->countries = array();                  
                    $data = array('*');
                    $this->db->select($data);
                    $this->db->from('trn_site_country tansSCTY');
                    $this->db->where('tansSCTY.SiteID',  $siteID);
                    $query = $this->db->get();
                    $countryHeads = $query->result_array();
                    //print_r($countryHeads);exit;
                    if(count($countryHeads)){
                        foreach ($countryHeads as $key => $value) {
                            $country = $this->getCountryLangName($value['CountryID']);
                            if(count($country)) array_push($objSite->countries,$country);
                        }
                    }
                    return $objSite->countries;

        } catch (Exception $e) {            
            echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
        }
    }

    function deleteSiteCountries($table,$id){
         try {
            $this->db->delete($table, array('SiteID' => $id));
        } catch (Exception $e) {
            echo 'Exception occured: '. $e->getCode().' - '.$e->getMessage().' in File: '.$e->getFile().', and on line: '.$e->getLine();
        }
    }

}

?>
