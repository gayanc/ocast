<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


//define('BASE_UPLOAD_PATH',_DIR);
define('CHAR_LIMIT',1000);
define('AUDI_CHAR_LIMIT',100);
define('DEMO_ANSWER',5);
define('PRODUCT_IMG_UPLOAD_URL',FCPATH.'uploads/product/');
define('CASE_IMG_UPLOAD_URL',FCPATH.'uploads/case/');
define('CONTACT_IMG_UPLOAD_URL', FCPATH.'uploads/contact/');
define('LOGO_IMG_UPLOAD_URL', FCPATH.'uploads/logo/');
define('COMPANY_LOGO_PATH','assets/images/logo');

define('FORMAT_DEFAUL_IMG', 'sampl5.jpg');
define('DEFAUL_COMPANY_LOGO', 'no_image.gif');
/* End of file constants.php */
/* Location: ./application/config/constants.php */

/* OCAST user status */

define('STATE_ACTIVE', 'active');
define('STATE_REVIEW', 'review');
define('STATE_APPROVE', 'approve');
define('STATE_PUBLISH', 'publish');
define('STATE_PENDING', 'pending');

define('NO_OF_QFACTS', '3');
define('NO_OF_TMONIALS', '3');
define('NO_OF_REC_IN_SEARCH', '250');

define('ADMIN_EMAIL','gayan.c@eyepax.com');