
//show close filter items
  
var selText2 = '';
var xx = 0;  
sortingkey = '';
rowCount = '0';
sortingOrder = '1';
search_day = '';
search_month = '';
search_year = '';
search_country_list = new Array() 
search_category_list = new Array() 
search_gender_list = new Array();
search_device_list = new Array();
search_type_list = new Array()
search_age_from = null;
search_age_to = null;
filters_toggle_status = 0;
loaded_site_list = new Array();

$("#countrybox li a").click(function(){
    if ($(this).hasClass('link-disable')) {
        return false;
    }
    $(this).addClass('link-disable');
    var selText2 = $(this).text();
    $("#fil1").append('<p countryId ="'+$(this).attr('id')+'" data-index="'+ $(this).parent('li').index() +'" class="close2">' + selText2 + ' <i class="icon-remove"></i></p>');
    getSearchResult(); 
});

function clear_table()
{  
    $.post(base_url+'search/create_table/', {
        step:"load"
    },
    
    function(data,status){
        // alert("Data: " + response + "\nStatus: " + status);
        var response = $.parseJSON(data)
       
        $.each(response, function(i, item) {
            if(i==0)
            {
                $(".result").html('')
                $(".result").html(response[0]); 
                getSearchResult();  
            }
            if(i=="data")  
            {  
                var json_str = JSON.stringify(response['data']);  
                addrows(json_str)
            }
        }); 
    });   
    
     
}
     
    
 

function getSearchResult()
{
    search_data = new Array();
    search_country_list = [];
    search_category_list = [];
    search_gender_list = [];
    search_device_list = [];
    search_type_list = [];
    $("#fil1 p").each(function(){
        search_country_list.push($(this).attr('countryId'))
    }); 
     
    $("#fil2 p").each(function(){
        search_category_list.push($(this).attr('categoryId'))
    }); 
    
    $("#fil3 p").each(function(){
        search_gender_list.push($(this).attr('gender'))
    }); 
    
    $("#fil4 p").each(function(){
        search_device_list.push($(this).attr('deviceId'))
    }); 
    
    $("#fil5 p").each(function(){
        search_type_list.push($(this).attr('typeId'))
    }); 
     
    // rec_count(); 
    $.post(base_url+'search/get/',   
    {
        country_list: search_country_list, 
        category_list: search_category_list ,
        gender_list:search_gender_list,
        device_list:search_device_list,
        type_list:search_type_list,
        age_from:search_age_from,
        age_to:search_age_to,
        sortingkey:sortingkey,
        rowCount:rowCount,
        sortingOrder:sortingOrder,
        day:search_day,
        month:search_month,
        year:search_year

    }, 
   
    function(data,status){
        // alert("Data: " + response + "\nStatus: " + status); 
        addrows(data);
    });  
      
}

$(document).on('click', 'table th', function(){  
    var keyid_val = $(this).attr('keyid')
    if(keyid_val > 7)
    {
        return 0;
    }
    if(keyid_val == sortingkey)
    {
        if(sortingOrder == "asc")
        {
            sortingOrder = "desc";
        }
        else{
            sortingOrder = "asc";
        } 
    }
    else{
        sortingOrder ="asc"
        sortingkey = keyid_val;  
    } 
    $("table  th").each(function() { 
        $(this).removeClass('headerSortUp') ;
        $(this).removeClass('headerSortDown') ; 
        if($(this).attr('keyid') < 8)
        {
            $(this).addClass('header') ;   
        }  
        if($(this).attr('keyid')== sortingkey) {  
            if(sortingOrder == "asc")
            {
                $(this).removeClass('headerSortDown') ;  
                $(this).addClass('headerSortUp') ; 
            }
            else{ 
                $(this).removeClass('headerSortUp') ;
                $(this).addClass('headerSortDown')  ;  
            } 
        }        
         
    })
    
   getSearchResult(); 
})

function rec_count(){
    rowCount = parseInt(rowCount);
    rowCount = 0;
    $("tbody  tr").each(function() { 
        rowCount = rowCount + 1;
    }) 
    
} 

$(document).on('click', '#catbox_day a', function(){ 
    $("#catbox_day a").each(function(){
        $(this).removeClass('activedrpfield');
    })
    search_day = $(this).attr('idval');
    $(this).addClass('activedrpfield'); 
})

$(document).on('click', '#catbox_week a', function(){ 
    $("#catbox_week a").each(function(){
        $(this).removeClass('activedrpfield');
    })
    search_week = $(this).attr('idval');
    $(this).addClass('activedrpfield'); 
})

$(document).on('click', '#catbox_month a', function(){ 
    $("#catbox_month a").each(function(){
        $(this).removeClass('activedrpfield');
    })
    search_month = $(this).attr('idval');
    $(this).addClass('activedrpfield'); 
})


$(document).on('click', '.catbox a', function(){ 
    loaded_site_list= [];
    $("tbody  tr").each(function() {
        // alert($(this).attr('site_id') ) 
        loaded_site_list.push($(this).attr('site_id'))  
    });
    
    $.post(base_url+'search/get_analytics_by_week/',{
        site_list: loaded_site_list
    },   
    function(data,status){ 
        var data = $.parseJSON(data);
        $.each(data, function(i, item) { 
            var site_id = item['site_id'];
            var percentage = item['percentage']; 
            var unique_visits  = item['unique_visits'];
            var visites  = item['visites'];
            var pageviews  = item['pageviews'];
            var p_v  = item['p_v'];
            var u_v  = item['u_v'];   
            $("[site_id="+site_id+"] [id=percentage]").css("background-color","yellow"); 
            $("[site_id="+site_id+"] [id=percentage]").html(percentage); 
            $("[site_id="+site_id+"] [id=unique_visits]").html(unique_visits); 
            $("[site_id="+site_id+"] [id=visites]").html(visites); 
            $("[site_id="+site_id+"] [id=p_v]").html(p_v); 
            $("[site_id="+site_id+"] [id=u_v]").html(u_v);
            $("[site_id="+site_id+"] [id=pageviews]").html(pageviews);   
        });
    });
     
})
 

function addrows(json_arr)
{
    var data = $.parseJSON(json_arr);
    $('#tbody_result').html('');
    if (! data.length >0)
    {
        return 0;
    }
    $.each(data, function(i, item) {
        var response = data[i];
        var col1 =null;
        var col2 =null; 
        var col3 =null; 
        var col4 =null; 
        var col5 =null;         
        var col6 =null; 
        var col7 =null; 
        var col8 =null; 
        var siteId = null;
        
        $.each(response, function(j, objt) { 
                 
            if(j=="company")
            {
                var company_arr = response["company"];
                col1 = '<td class="tdstyle">';
                col1 +='<div style="float:left">'; 
                col1 +='<img src='+base_url+logo_path+'/'+company_arr["logo"]+'></div>';                  
                col1 +='<div class="tdtitlbt"><h5 class="marginno txtcenttbl">'+company_arr["sitename"];    

                if(company_arr["is_new"]==1)
                {
                    col1 +='<a id="popovr" class="icninfo icnnew asty" data-placement="top" ';
                    col1 +='data-title="Lörem ipsum dolör." data-toggle="tooltip"';
                    col1 +='href="#" data-original-title="" title=""  style="display:block"></a>';
                }  
                col1 +='</h5>'; 
                col1 +='<h6 class="fntsmll marginno"><i>'+company_arr["name"]+'</i></h6></div></td>'; 
            }
            
            if(j=="unique_visits"){ 
                col2 = '<td id="unique_visits">'+response['unique_visits']+'</td>' 
            } 
                 
            if(j=="visites"){ 
                col3 = '<td id="visites" >'+response['visites']+'</td>' 
            } 
                 
                
            if(j=="pageviews")
            {
                col4 = '<td id="pageviews" >'+response['pageviews']+'</td>' 
            }
            if(j=="p_v")
            {
                col5 = '<td class="hidetr" id="p_v">'+response['p_v']+'</td>'       
            }
            if(j=="u_v"){
                   
                col6 = '<td class="hidetr" id="u_v" >'+response['u_v']+'</td>'    
            }
                 
            if(j=="percentage"){
                   
                col7 = '<td class="hidetr" id="percentage" >'+response['percentage']+'</td>'    
            }
            
            if(j =="site_id")
            {
                siteId = response['site_id'];
            }
             
            col8='<td class="hidetr"><i class="icon-heart"></i></td>';
                 
            col9='<td class="hidetr"><img src="'+base_url+'assets/images/icnsarow_07.png"></td>';
        }) 
            
        // 'p_v'=>$p_v, 'u_v'=>$u_v ,'percentage'=>$percentage,'site_id'=>1
            
        $('#tbody_result').append("<tr site_id='"+siteId+"'>"+col1+col2+col3+col4+col5+col6+col7+col8+col9+"</tr>")  
        $("tr:even").addClass("even"); 
    //updateSort();
           
    }); 
}

 
     
function updateSort()
{
    $("table").tablesorter({
        widgets: ["zebra"],
        headers:{
            7: {
                sorter: false
            }, 
            8: {
                sorter: false
            }
        }
    }); 
    $("table").trigger("update");  
    $("tr:even").addClass("even");
}
    
$("#categorybox li a").click(function(){
    if ($(this).hasClass('link-disable')) {
        return false;
    }
    $(this).addClass('link-disable');
    var seText2 = $(this).text();
    $("#fil2").append('<p categoryId ="'+$(this).attr('id')+'"  data-index="'+ $(this).parent('li').index() +'" class="close2">' + seText2 + ' <i class="icon-remove"></i></p>');
    getSearchResult()
});
	
$("#genderbox li a").click(function(){
    if ($(this).hasClass('link-disable')) {
        return false;
    }
    $(this).addClass('link-disable');
    var seText2 = $(this).text();
    $("#fil3").append('<p gender="'+$(this).attr('id')+'" data-index="'+ $(this).parent('li').index() +'" class="close2">' + seText2 + ' <i class="icon-remove"></i></p>');
    getSearchResult()
    
});
	
$("#devicebox li a").click(function(){
    if ($(this).hasClass('link-disable')) {
        return false;
    }
    $(this).addClass('link-disable');
    var seText2 = $(this).text();
    $("#fil4").append('<p deviceId="'+$(this).attr('id')+'" data-index="'+ $(this).parent('li').index() +'" class="close2">' + seText2 + ' <i class="icon-remove"></i></p>');
    getSearchResult()
   
});
	
$("#typbox li a").click(function(){
    if ($(this).hasClass('link-disable')) {
        return false;
    }
    $(this).addClass('link-disable');
    var seText2 = $(this).text();
    $("#fil5").append('<p typeId="'+$(this).attr('id')+'" data-index="'+ $(this).parent('li').index() +'" class="close2">' + seText2 + ' <i class="icon-remove"></i></p>');
    getSearchResult()   
});

$('#set-age').click(function(){
    if ($(this).hasClass('selected')) {
        return false;
    }
    $(this).addClass('selected');
    var seText2 = $('#amount-start').text()+'-'+$('#amount-end').text();
    $("#fil6").append('<p class="close2"> Age (' + seText2 + ')<i class="icon-remove"></i></p>');  
    search_age_from =$('#amount-start').text();
    search_age_to = $('#amount-end').text(); 
    getSearchResult() 
}); 

$(document).on('click', '#fil1 p i.icon-remove', function(){
    xx = $(this).parent('p').data('index');
    $("#countrybox li").eq(xx).find('a').removeClass('link-disable');
    $(this).parent('p').fadeOut(500, function(){
        $(this).remove(); 
        getSearchResult();
    });
   
});
	 
$(document).on('click', '#fil2 p i.icon-remove', function(){
    xx = $(this).parent('p').data('index');
    $("#categorybox li").eq(xx).find('a').removeClass('link-disable');
    $(this).parent('p').fadeOut(500, function(){
        $(this).remove(); 
        getSearchResult();
    });
});
	 
$(document).on('click', '#fil3 p i.icon-remove', function(){
    xx = $(this).parent('p').data('index');
    $("#genderbox li").eq(xx).find('a').removeClass('link-disable');
    $(this).parent('p').fadeOut(500, function(){
        $(this).remove(); 
        getSearchResult();
    });
});
	 
$(document).on('click', '#fil4 p i.icon-remove', function(){
    xx = $(this).parent('p').data('index');
    $("#devicebox li").eq(xx).find('a').removeClass('link-disable');
    $(this).parent('p').fadeOut(500, function(){
        $(this).remove(); 
        getSearchResult();
    });
});
	 
$(document).on('click', '#fil5 p i.icon-remove', function(){
    xx = $(this).parent('p').data('index');
    $("#typbox li").eq(xx).find('a').removeClass('link-disable');
    $(this).parent('p').fadeOut(500, function(){
        $(this).remove(); 
        getSearchResult();
    });
});
	 
$(document).on('click', '#fil6 p i.icon-remove', function(){
    xx = $(this).parent('p').data('index');
    $("#set-age").removeClass('selected');
    $(this).parent('p').fadeOut(500, function(){
        $(this).remove(); 
        search_age_from = null;
        search_age_to = null;
        getSearchResult();
    });
});


//closing all divs
$("#clrall").click(function() {
    $("#filbox p").hide();
    sortingkey = ''; 
    rowCount = ''; 
    sortingOrder = ''; 
    search_day = ''; 
    search_month = '';
    search_year = '';   
    $("#fil1  p").each(function() { 
        var xx = $(this).attr('data-index'); 
        $("#countrybox li").eq(xx).find('a').removeClass('link-disable');  
        $(this).remove(); 
    }) 
    $("#fil2  p").each(function() {
        var xx = $(this).attr('data-index'); 
        $("#categorybox li").eq(xx).find('a').removeClass('link-disable'); 
        $(this).remove(); 
    }) 
    $("#fil3  p").each(function() { 
        var xx = $(this).attr('data-index'); 
        $("#genderbox li").eq(xx).find('a').removeClass('link-disable'); 
        $(this).remove(); 
    }) 
    $("#fil4  p").each(function() { 
        var xx = $(this).attr('data-index'); 
        $("#devicebox li").eq(xx).find('a').removeClass('link-disable'); 
        $(this).remove(); 
    }) 
    $("#fil5  p").each(function() { 
        var xx = $(this).attr('data-index'); 
        $("#typbox li").eq(xx).find('a').removeClass('link-disable'); 
        $(this).remove(); 
    }) 
    getSearchResult();

});
//toggle filter box
$(".filtrlink").click(function() {
    $("#filters").slideToggle("fast");
    if(filters_toggle_status == 0)
    {
         
        filters_toggle_status = 1;
        clear_table();
            
    }
    
});

//age slider script 
$(function() {
    $( "#slider-range" ).slider({
        range: true,
        min: 0,
        max: 100,
        values: [ 25, 75 ],
        slide: function( event, ui ) {
            $( "#amount-start" ).text(  $( "#slider-range" ).slider( "values", 0 ));
            $( "#amount-end" ).text(  $( "#slider-range" ).slider( "values", 1 ));
        }
    });
    $( "#amount-start" ).text(  $( "#slider-range" ).slider( "values", 0 ));
    $( "#amount-end" ).text(  $( "#slider-range" ).slider( "values", 1 ));

});

  
//tooltip 
$(document).ready(function() {
    $('.icninfo').tooltip();
// ('.icninfo').tooltip('hide')

}); 
 
 
//slider 
$(function() {
    $( ".slider" ).slider( {
        value: 50
    } );
});
   
//tabs 1 
$('#myTab a').click(function (e) {
    e.preventDefault()
    $(this).tab('show')
})
        
function resizeMenu(){
    if ($(window).width() <= 750){ // right_appnd
        $('#lftmnu').appendTo('.appn');
    }
    else{
        $('#lftmnu').appendTo('.appndfrmlft');
    }
    return false;
}

//filter link responsive    
function resizeFilter(){
    if ($(window).width() <= 750){
        $('#filterapnd').appendTo('#filtertpbx');
    }
    else{
        $('#filterapnd').appendTo('.appn');
    }
    return false;
}
function resizeSearchbox(){
    if ($(window).width() <= 750){
        $('.custmserch').appendTo('.searchapnd');
    }
    else{
        $('.custmserch').appendTo('.em_div');
    }
    return false;
}
function resizeFormatbox(){
    if ($(window).width() <= 991){
        $('.formt').appendTo('.adformatapnd');
    }
    else{
        $('.formt').appendTo('.adformat');
    }
    return false;
}
function resizeAddscreen(){
    if ($(window).width() <= 991){
        $('.screenadd').appendTo('.adscreenapnd');
    }
    else{
        $('.screenadd').appendTo('.addscreen');
    }
    return false;
}

window.onload = function(){
    resizeMenu();
    resizeFilter();
    resizeSearchbox();
    resizeFormatbox();
    resizeAddscreen();
     
}
window.onresize = function(){
    resizeMenu();
    resizeFilter();
    resizeSearchbox();
    resizeFormatbox();
    resizeAddscreen();
};


//calender/
	
$(function() {

    /*	function showfromto(){
			$('<p class="fromtotxt">From</p>').insertBefore('.ui-datepicker-group-first .ui-datepicker-header');
			$('<p class="fromtotxt">To</p>').insertBefore('.ui-datepicker-group-last .ui-datepicker-header');
		
	}
     */
    $( "#from" ).datepicker({
        defaultDate: "+1w",
        changeMonth: false,
        numberOfMonths: 2,
        showOn: "button",
        buttonImage: "images/icncalndr.gif",
        buttonImageOnly: true,
        onClose: function( selectedDate ) {
            $( "#to" ).datepicker( "option", "minDate", selectedDate );
        }
	  
    });

    $( "#to" ).datepicker({
        defaultDate: "+1w",
        changeMonth: false,
        numberOfMonths: 2,
        showOn: "button",
        buttonImage: "images/icncalndr.gif",
        buttonImageOnly: true,
        onClose: function( selectedDate ) {
            $( "#from" ).datepicker( "option", "maxDate", selectedDate );
        }
	  
    });
	

    $('.ui-datepicker-trigger').click(function(){
        if(!$(".fromtotxt")[0]){
            $('<p class="fromtotxt">From</p>').insertBefore('.ui-datepicker-group-first .ui-datepicker-header');
            $('<p class="fromtotxt">To</p>').insertBefore('.ui-datepicker-group-last .ui-datepicker-header');
        }

    });
	
/* $('#to').next().datepicker().on('click', function() {
		$('<p class="fromtotxt">From</p>').insertBefore('.ui-datepicker-group-first .ui-datepicker-header');
		$('<p class="fromtotxt">To</p>').insertBefore('.ui-datepicker-group-last .ui-datepicker-header');
    });*/

});



/*hover effect calender icon*/
$(function() {
    $(".caltxt")
    .mouseover(function() {
        $(this).find('input').css({
            color:'#74CBDB'
        }); 
        $(this).find('img').attr("src", "images/calicohov.gif");
    })
    .mouseout(function() {
        $(this).find('input').css({
            color:'#656565'
        }); 
        $(this).find('img').attr("src", "images/icncalndr.gif");
    });
});

/*Filter link arrow toggle*/
$(document).ready(function(){
    $(".filtrlink a").click(function() {
        $('.filtrlink i').toggleClass("icon-caret-right icon-caret-down");
    });
});
