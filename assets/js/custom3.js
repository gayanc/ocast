
  //show close filter items
    $("#countrybox li a").click(function(){
		if ($(this).hasClass('link-disable')) {
        	return false;
     	}
		
      $(this).addClass('link-disable');
	  
      var selText2 = $(this).text();
       $("#fil1").append('<p data-index="'+ $(this).parent('li').index() +'" class="close2">' + selText2 + ' <i class="icon-remove"></i></p>');
    
    });
    $("#categorybox li a").click(function(){
      var seText2 = $(this).text();
      $("#fil2").append('<p class="close2">' + seText2 + ' <i class="icon-remove"></i></p>');
    
    });
      $("#genderbox li a").click(function(){
      var seText2 = $(this).text();
      $("#fil3").append('<p class="close2">' + seText2 + ' <i class="icon-remove"></i></p>');
    
    });
      $("#devicebox li a").click(function(){
      var seText2 = $(this).text();
      $("#fil4").append('<p class="close2">' + seText2 + ' <i class="icon-remove"></i></p>');
   
    });
      $("#typbox li a").click(function(){
      var seText2 = $(this).text();
      $("#fil5").append('<p class="close2">' + seText2 + ' <i class="icon-remove"></i></p>');

   
    });

      var xx = 0;

     $(document).on('click', 'p i.icon-remove', function(){
          
          xx = $(this).parent('p').data('index');
          console.log($("#countrybox li").eq(xx).find('a').text());
          $("#countrybox li").eq(xx).find('a').removeClass('link-disable');
          $(this).parent('p').remove();
 
     });


//closing all divs
    $("#clrall").click(function() {
      $("#filbox p").hide();
    });
//toggle filter box
    $(".filtrlink").click(function() {
      $("#filters").slideToggle("fast");
    });

//age slider script 
  $(function() {
    $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: 100,
      values: [ 25, 75 ],
      slide: function( event, ui ) {
    $( "#amount-start" ).text(  $( "#slider-range" ).slider( "values", 0 ));
    $( "#amount-end" ).text(  $( "#slider-range" ).slider( "values", 1 ));
      }
    });
    $( "#amount-start" ).text(  $( "#slider-range" ).slider( "values", 0 ));
    $( "#amount-end" ).text(  $( "#slider-range" ).slider( "values", 1 ));

  });

  
  //tooltip 
      $(document).ready(function() {
      $('.icninfo').tooltip();
     }); 
 
 
 //slider 
       $(function() {
        $( ".slider" ).slider();
      });
   
//tabs 1 
    $('#myTab a').click(function (e) {
      e.preventDefault()
      $(this).tab('show')
    })
     
function resizeMenu(){
        if ($(window).width() <= 750){
         $('#lftmnu').appendTo('.right_appnd');
        }
        else{
          $('#lftmnu').appendTo('.appndfrmlft');
        }
        return false;
      }

//filter link responsive    
function resizeFilter(){
        if ($(window).width() <= 750){
         $('#filterapnd').appendTo('#filtertpbx');
        }
        else{
          $('#filterapnd').appendTo('.appn');
        }
        return false;
        }
function resizeSearchbox(){
        if ($(window).width() <= 750){
         $('.custmserch').appendTo('.searchapnd');
        }
        else{
          $('.custmserch').appendTo('.em_div');
        }
        return false;
        }

window.onload = function(){
      resizeMenu();
      resizeFilter();
      resizeSearchbox();
     
   }
window.onresize = function(){
  resizeMenu();
  resizeFilter();
  resizeSearchbox();
};


//calender
  $(function() {
    $( "#from" ).datepicker({
      defaultDate: "+1w",
      changeMonth: false,
      numberOfMonths: 2,
      showOn: "button",
      buttonImage: "images/icncalndr.gif",
      buttonImageOnly: true,
      onClose: function( selectedDate ) {
        $( "#to" ).datepicker( "option", "minDate", selectedDate );
      }

    });

    $('#from').next().datepicker().bind('click', function() {
    $('<p class="fromtotxt">From</p>').insertBefore('.ui-datepicker-group-first .ui-datepicker-header');
    });
    $('#from').next().datepicker().bind('click', function() {
    $('<p class="fromtotxt">To</p>').insertBefore('.ui-datepicker-group-last .ui-datepicker-header');
    });
    

    $( "#to" ).datepicker({
      defaultDate: "+1w",
      changeMonth: false,
      numberOfMonths: 2,
      showOn: "button",
      buttonImage: "images/icncalndr.gif",
      buttonImageOnly: true,
      onClose: function( selectedDate ) {
        $( "#from" ).datepicker( "option", "maxDate", selectedDate );
      }
    });

    $('#to').next().datepicker().bind('click', function() {
    $('<p class="fromtotxt">From</p>').insertBefore('.ui-datepicker-group-first .ui-datepicker-header');
    });
    $('#to').next().datepicker().bind('click', function() {
    $('<p class="fromtotxt">To</p>').insertBefore('.ui-datepicker-group-last .ui-datepicker-header');
    });
  });


/*hover effect calender icon*/
$(function() {
$(".caltxt")
    .mouseover(function() {
        $(this).find('input').css({color:'#74CBDB'}); 
        $(this).find('img').attr("src", "images/calicohov.gif");
    })
    .mouseout(function() {
        $(this).find('input').css({color:'#656565'}); 
        $(this).find('img').attr("src", "images/icncalndr.gif");
});
});

 