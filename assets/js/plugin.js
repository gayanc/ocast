/* ===================================================
 * Application plugin.js 
 * ===================================================
 * Copyright 2013 Eyepax IT Consulting, Pvt Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://eyepax.com
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */
 $(document).ready(function() {
 //Broswser and Version Detect
 var BrowserDetect = 
    {
        init: function () 
        {
            this.browser = this.searchString(this.dataBrowser) || "Other";
            this.version = this.searchVersion(navigator.userAgent) ||       this.searchVersion(navigator.appVersion) || "Unknown";
        },

        searchString: function (data) 
        {
            for (var i=0 ; i < data.length ; i++)   
            {
                var dataString = data[i].string;
                this.versionSearchString = data[i].subString;

                if (dataString.indexOf(data[i].subString) != -1)
                {
                    return data[i].identity;
                }
            }
        },

        searchVersion: function (dataString) 
        {
            var index = dataString.indexOf(this.versionSearchString);
            if (index == -1) return;
            return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
        },

        dataBrowser: 
        [
            { string: navigator.userAgent, subString: "Chrome",  identity: "Chrome" },
            { string: navigator.userAgent, subString: "MSIE",    identity: "Explorer" },
            { string: navigator.userAgent, subString: "Firefox", identity: "Firefox" },
            { string: navigator.userAgent, subString: "Safari",  identity: "Safari" },
            { string: navigator.userAgent, subString: "Opera",   identity: "Opera" }
        ]

    };
    BrowserDetect.init();
    //Eof Broswser and Version Detect


//###################Textarea Count Limit##################################
  
  function limiting(obj, limit) {
    var cnt = $(".fontsizerm");
    var txt = $(obj).val(); 
    var len = txt.length;
    
    // check if the current length is over the limit
    if(len > limit){
       $(obj).val(txt.substr(0,limit));
       //$(cnt).html(len-1);
       $(cnt).html(limit);
       $("#site_info_desc").addClass("error");
     } 
     else { 
       $(cnt).html(len);
     }
     
     // check if user has less than 20 chars left
     if(limit-len <= 20) {
        $(cnt).addClass("warning");
     }
  }


  $('#site_info_desc').keyup(function(){
    limiting($(this), char_limit);
  });
//###################EOF Textarea Count Limit##################################

//###################Ajax Image Upload##################################

//signup - products and formats
$('#prdfrmtScrnShot').on('change', function(){ 
  $("#preview").html('');
  $(".scrnSht").html('<img src="loader.gif" alt="Uploading...."/>');
  $("#imageform").ajaxForm({
            target: '#preview'
  }).submit();
  });


});

