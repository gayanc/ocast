/* ===================================================
 * Application plugin.js 
 * ===================================================
 * Copyright 2013 Eyepax IT Consulting, Pvt Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://eyepax.com
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */
 $(document).ready(function() {
 //Broswser and Version Detect
 var BrowserDetect = 
    {
        init: function () 
        {
            this.browser = this.searchString(this.dataBrowser) || "Other";
            this.version = this.searchVersion(navigator.userAgent) ||       this.searchVersion(navigator.appVersion) || "Unknown";
        },

        searchString: function (data) 
        {
            for (var i=0 ; i < data.length ; i++)   
            {
                var dataString = data[i].string;
                this.versionSearchString = data[i].subString;

                if (dataString.indexOf(data[i].subString) != -1)
                {
                    return data[i].identity;
                }
            }
        },

        searchVersion: function (dataString) 
        {
            var index = dataString.indexOf(this.versionSearchString);
            if (index == -1) return;
            return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
        },

        dataBrowser: 
        [
            { string: navigator.userAgent, subString: "Chrome",  identity: "Chrome" },
            { string: navigator.userAgent, subString: "MSIE",    identity: "Explorer" },
            { string: navigator.userAgent, subString: "Firefox", identity: "Firefox" },
            { string: navigator.userAgent, subString: "Safari",  identity: "Safari" },
            { string: navigator.userAgent, subString: "Opera",   identity: "Opera" }
        ]

    };
    BrowserDetect.init();
    //Eof Broswser and Version Detect
});


$(document).on('click', '#prdt_type li a', function(){
  console.log($(this).text());
    var selText2 = $(this).text(); 
    var modCnt = $(this).attr('accessKey');
    $("#prdctype"+modCnt+" .txt").html(selText2);
    $("#format_type_id").val($(this).attr('id'));
});

$(document).on('click', '#price_type li a', function(){

    var selText2 = $(this).text();
    var modCnt = $(this).attr('accessKey');
    $("#pricetype"+modCnt+" .txt").html(selText2);
    $("#price_type_id").val($(this).attr('id'));
});

$(document).on('click', '#country_Ext li a', function(){
    var selText2 = $(this).text();
    //var modCnt = $(this).attr('accessKey');
    $("#country_extension .txt").html(selText2);
    $("#cnt_extension_id").val($(this).attr('id'));
});

//Site info
$(document).on('click', '#sinfo_country_Ext li a', function(){
    var selText2 = $(this).text();
    var modCnt = $(this).attr('accessKey');
    $("#sinfo_country_extension"+modCnt+" .txt").html(selText2);
    $("#sptxt_excode"+modCnt+"ID").val($(this).attr('id')); //sptxt_excode' . $modcnt . 'ID
});

//###############categories
var j= 1; 
$("#sit_info_catbox li a").click(function(){
    if ($(this).hasClass('link-disable')) {
        return false;
    }
    $(this).addClass('link-disable');
    var seText2 = $(this).text();
    $("#catapnd").append('<p data-key="'+j+'" data-index="'+ $(this).parent('li').index() +'" class="close2">' + seText2 + ' <i class="icon-remove"></i></p>');
    
    //append category inputs
    input = jQuery('<input type="hidden" id="sptxt_contact_category_'+j+'" name="sptxt_category[]">');    
    jQuery('#apndCatgoryInput').append(input);
    $('#sptxt_contact_category_'+j).val($(this).attr('id'));
    j++;
});

$(document).on('click', '#catapnd p i.icon-remove', function(){
    xx = $(this).parent('p').data('index');
    kk = $(this).parent('p').data('key');
    $("#sit_info_catbox li").eq(xx).find('a').removeClass('link-disable');
    $(this).parent('p').fadeOut(500, function(){
       $(this).remove();
       $("#sptxt_contact_category_"+kk).remove(); //remove element
    });
});
//###############eof categories

$("#sit_info_mcountry li a").click(function(){ //country main
    if ($(this).hasClass('link-disable')) {
        return false;
    }
    //$(this).addClass('link-disable');
    var seText2 = $(this).text();
    $("#mcntryapnd").html('<p data-index="'+ $(this).parent('li').index() +'" class="close2">' + seText2 + ' <i class="icon-remove"></i></p>');
    $('#sptxt_mcountryID').val($(this).attr('id'));
    $('#main_country .txt').html(seText2);
});

$(document).on('click', '#mcntryapnd p i.icon-remove', function(){
    xx = $(this).parent('p').data('index');
    $("#sit_info_mcountry li").eq(xx).find('a').removeClass('link-disable');
    $(this).parent('p').fadeOut(500, function(){
       $(this).remove();
       $('#sptxt_mcountryID').remove();  //remove element
       $('#main_country .txt').html('Main Country');
    });
});

var k =1;
$("#sit_info_othrcountry li a").click(function(){ //country other
    if ($(this).hasClass('link-disable')) {
        return false;
    }
    console.log($(this).attr('id'));
    $(this).addClass('link-disable');
    var seText2 = $(this).text();
    $("#otrcntryapnd").append('<p data-key="'+k+'" data-index="'+ $(this).parent('li').index() +'" class="close2">' + seText2 + ' <i class="icon-remove"></i></p>');
    
    //adding input elements
    input = jQuery('<input type="hidden" id="sptxt_contact_cntothr_'+k+'" name="sptxt_cntry_other[]">');    
    jQuery('#apndInput').append(input);
    $('#sptxt_contact_cntothr_'+k).val($(this).attr('id'));
    k++;
});

$(document).on('click', '#otrcntryapnd p i.icon-remove', function(){
    xx = $(this).parent('p').data('index');
    kk = $(this).parent('p').data('key');
    $("#sit_info_othrcountry li").eq(xx).find('a').removeClass('link-disable');
    $(this).parent('p').fadeOut(500, function(){
       $(this).remove();
       $('#sptxt_contact_cntothr_'+kk).remove(); //remove element
    });
});
