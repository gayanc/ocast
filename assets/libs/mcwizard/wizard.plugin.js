$(document).ready(function() { 

    //using modenizr to detect crossbroser issues
    if(!Modernizr.input.placeholder){
    $('[placeholder]').focus(function() {
      var input = $(this);
      if (input.val() == input.attr('placeholder')) {
        input.val('');
        input.removeClass('placeholder');
      }
        }).blur(function() {
          var input = $(this);
          if (input.val() == '' || input.val() == input.attr('placeholder')) {
            input.addClass('placeholder');
            input.val(input.attr('placeholder'));
          }
        }).blur();
        $('[placeholder]').parents('form').submit(function() {
          $(this).find('[placeholder]').each(function() {
            var input = $(this);
            if (input.val() == input.attr('placeholder')) {
              input.val('');
            }
          })
        });
    }
    /*eof placeholder*/
    $(document).on('click', '.search_wixard .wclose', function( event ){
        //TODO: wizard initialization
           $( "#wizard-form" ).validate().resetForm();
           $("#wizard-form").wizard('backward',20);
           //$("#wizard-form").wizard('destroy');
           //$("#wizard-form").wizard();
            //wizardInitalize();
       $(".search_wixard").hide("slide", { direction: "right" }, 500);
        var t=setTimeout(function(){$('.carditro_wrap').show("slide", { direction: "left" }, 200)},600); 
        var t=setTimeout(function(){
              document.getElementById("mc-wizard").reset();
         },10);         
    });

    $(document).on('click', '#brwnw', function( event ){
         var t=setTimeout(function(){   
           document.getElementById("mc-wizard").reset();
           var t=setTimeout(function(){$(".search_wixard").hide("slide", { direction: "left" }, 500);
            $('#backward').css('display','block');
            $("#wizard-form").wizard('backward',20);
             $('.carditro_wrap').hide("slide", { direction: "left" }, 300);
                var t=setTimeout(function(){$(".search_wixard").show("slide", { direction: "right" }, 500)},400);
             },600);
           });
    });
         
    $("#progressbar").progressbar();

    var annumnthIncome = '';
    var monthlySpend = '';
    var payment_freq = '';
    var payment_freq_rdio = '';
         

    function getValues(selector) {
        var tempValues = {};
        $(selector).each(function() {
            var th = $(this);
            tempValues[th.attr('title')] = th.val();
        });
        return tempValues;
    }
    
    function roundUpToHundred($n){ return Math.round($n/100)*100; }
    /*validations for eValuator*/

    $('#annumincome').blur(function() { 
        annumnthIncome = roundUpToHundred(Math.ceil(parseInt($('#annumincome').val())/12));
        if(Modernizr.input.placeholder){ 
            $('#monthlySpend').prop("placeholder", " < "+ annumnthIncome); $('#fmonthlySpend').prop("placeholder", " < "+ annumnthIncome);
        }else{ 
            $('#monthlySpend').val(" < "+ annumnthIncome); $('#fmonthlySpend').val(" < "+ annumnthIncome);   
        }
        
    });

    $('#monthlySpend').blur(function() {
        monthlySpend = roundUpToHundred(Math.ceil(parseInt($('#monthlySpend').val())));
        if(Modernizr.input.placeholder){ $('#roll_amount').prop("placeholder", " < "+ monthlySpend); }else { $('#roll_amount').val(" < "+ monthlySpend);}
        $('#flsid_'+earnidother).text(monthlySpend); //other spend category
        $('#flsid_'+earnidall).text(monthlySpend);
        $('#hflsid_'+earnidall).val(monthlySpend);
    });

    $('#fmonthlySpend').blur(function() {
        monthlySpend = roundUpToHundred(Math.ceil(parseInt($('#fmonthlySpend').val())));
        if(Modernizr.input.placeholder){ $('#froll_amount').prop("placeholder", " < "+ monthlySpend); }else { $('#froll_amount').val(" < "+ monthlySpend);}
        $('#flsid_'+earnidother).text(monthlySpend);
        $('#flsid_'+earnidall).text(monthlySpend);
        $('#hflsid_'+earnidall).val(monthlySpend);

    });
  
    

    //validate spend categories for monthly spend
    var valOther = parseInt(monthlySpend);
    var fldVal = '';

   
    // $.each(JSON.parse(RewardsBurnedjson), function(i,obj){
    //     tid = obj.tid;
    //     $('.spnt_' + tid).focusout(function(valSpndCat){

    //            var input = $(this);
    //            valSpndCat = $("#" + input[0].id).val(); 
    //            if (!valSpndCat) {
    //             valSpndCat = 0;
    //             }
               
    //            // $("#" + input[0].id).rules( "add", {
    //            //    number: true,
    //            //    max: function() {
    //            //      return Math.ceil(parseInt($('#monthlySpend').val()));
    //            //  },
    //            //  messages: {
    //            //      number: "Number required"
    //            //      //max: "Total is too high"
    //            //    }
    //            // });

    //         if(valSpndCat > monthlySpend){
    //             console.log('larger value');
    //             $("#" + input[0].id).val("");
    //             //add rule
                
    //             $("#" + input[0].id).removeClass('valid');
    //             if(Modernizr.input.placeholder){  $("#" + input[0].id).attr("placeholder","Less value required"); } else { $("#" + input[0].id).val("Less value required");}
    //             $("#" + input[0].id).removeClass("valid").addClass('error');

    //         }else{
    //             var arrtotl =0;
    //             var ttlfldVal= '';

    //             $.each(JSON.parse($.trim(RewardsBurnedjson)), function(i,obj){
 
    //             var tid = obj.tid;
    //             var fldVal = '';
    //                     if(tid != earnidall || tid != earnidother){
    //                         fldVal = $('#flsid_'+ tid).val();
    //                         if(!fldVal){
    //                             fldVal = 0;
    //                         }
    //                         arrtotl = arrtotl + parseInt(fldVal);
    //                     }

    //             });  
    //             var otherVal = monthlySpend-arrtotl;

    //             $("#" + input[0].id).removeClass('error');
    //             if(Modernizr.input.placeholder){ $("#" + input[0].id).attr("placeholder",""); }else { $("#" + input[0].id).val("");}
    //             if(arrtotl>monthlySpend){
    //                     $("#" + input[0].id).removeClass("valid").addClass("error");
    //                     $("#" + input[0].id).val("");
    //                     if(Modernizr.input.placeholder){ $("#" + input[0].id).attr("placeholder","Total is too high"); }else{ $("#" + input[0].id).val("Total is too high");}
    //             }else{
    //                 $('#flsid_'+earnidother).text(otherVal);
    //                 $('#hflsid_'+earnidother).val(otherVal);

    //             }
    //         }   
    //     });
    // });


    //credit card selected or enter
    $('select#card_name').change(function() {
        if ($('select#card-name').val() != "") {
            $(".chosen-single").removeClass('slcterr');
            $( "#ent_card_name" ).rules( "remove" ); $( "#crdapr" ).rules( "remove" );
            $( "#ent_card_name" ).val(''); $( "#crdapr" ).val('');
            
            $(".cus-nw-card").css("display", "none");
            $("#card_name_radio").prop('checked', false);

            $("#ent_card_name").val('');
        }
    });
       
    $('input:radio[name=card_name_radio]').change(function(e) {
        slctval = $('input[name=card_name_radio]:checked').val();
        if(slctval == 'crdnotlst'){

            $(".cus-nw-card").css("display", "block");
            $( "#card_name" ).rules( "remove" ); 

            $( "#ent_card_name" ).rules( "add", {
                required: true
            });    
            $( "#crdapr" ).rules( "add", {
                required: true,
                digits: true
            });
            $(".chosen-select").val('').trigger("chosen:updated");
            $(".chosen-single").removeClass('slcterr');
            //$("select#card_name").prop('selectedIndex', '-1');
        }

        e.preventDefault();
    });         
        
        

    $('select#payment_freq').change(function(e) {
        payment_freq = $('select[name="payment_freq"]').val();
        $('#payment_freq_radio').prop('checked', false);
        e.preventDefault();
    });
        
    $('select#payment_freq_fls').change(function(e) {
        payment_freq = $('select[name="payment_freq_fls"]').val();
        $('#payment_freq_radio_fls').prop('checked', false);
        e.preventDefault();
    });

    $('input:radio[name=payment_freq_radio]').change(function(e) {
        payment_freq_rdio = $('input[name=payment-freq-radio]:checked').val();
        $("#payment_freq").prop('selectedIndex', '-1');
        payment_freq = 0;
        e.preventDefault();
    });        
        

    $('input:radio[name=payment_freq_radio_fls]').change(function(e) {
        payment_freq_rdio = $('input[name=payment_freq_radio_fls]:checked').val();
        $("#payment_freq_fls").prop('selectedIndex', '-1');
        payment_freq = 0;
        e.preventDefault();
    });


    $('input:radio[name=optionCurrBlnce]').change(function(e) {
        slctval = $('input[name=optionCurrBlnce]:checked').val();
        if(slctval == 'balance-transfer'){            
            $( "#transferAmount" ).rules( "add", {
                required: true,
                number: true
            });  
            $(".cus-trf-amnt").css("display", "block");
        }else{
            $( "#transferAmount" ).rules( "remove" ); 
            $(".cus-trf-amnt").css("display", "none");
        }

        e.preventDefault();
    });

            
    $('#bntCard').click(function(e) {
        var card_name = $('#ent_card_name').val();
        var apr = $('#apr').val();
        var string = 'card=' + card_name+'&apr='+apr;

        $.ajax({
            type: "GET",
            dataType: "json",
            url: 'wizard_misc.php',
            data: string,
            beforeSend: function() {
                    $('#mcloading').show();
            },
            success: function(data) {
                if (data.error) {
                    $('#mcloading').removeClass('succ').addClass('err');
                    $('#mcloading').empty().text(data.txt).show().fadeOut(5000);

                } else {
                    $("#ent_card_name").val(card_name);
                    $('#mcloading').removeClass('err').addClass('succ');
                    $('#mcloading').empty().text(data.txt).show().fadeOut(5000);

                }

            }

        });
        e.preventDefault();
    });


    function ajaxWizardStep(step) {
        var dataStr;
        var amntspndTravel = '';
        var amntspndClothes = '';
        var amntspndDiningOut = '';
        var amntspndUtility = '';
        var amntspndGroceries = '';
        var amntspndOthers = '';
        var ovrSeaSpnd = '';
        var domesticSpnd = '';
        var onlineSpnd = '';
        var oflineSpnd = '';

        switch (step) {
            case 'step1':
                dataStr = 'stp=' + step + '&citzPr=' + $('input[name=isCitizen]:checked').val();
                break;
            case 'step2':
                dataStr = 'stp=' + step + '&ageGroup=' + $('input[name=ageGroup]:checked').val();
                break;
            case 'step3':
                dataStr = 'stp=' + step + '&anumIncome=' + $('#annumincome').val();
                break;
            case 'step4':
                dataStr = 'stp=' + step + '&crdStat=' + $('input[name=txtOptionCreditCard]:checked').val(); //bool
                break;
            case 'step5':
                var entCrdName = $('#ent_card_name').val(); var crdApr  = $('#crdapr').val();
                if ($('select[name="card_name"]').val() != "") {
                    dataStr = 'stp=' + step + '&cardName=' + $('select[name="card_name"]').val();
                } else {
                    dataStr = 'stp=' + step + '&entCardName=' + entCrdName +"&apr=" + crdApr; 
                }
                break;

            case 'step6':
                dataStr = 'stp=' + step + '&mnthSpend=' + $('#monthlySpend').val();
                break;

            case 'fstep6':
                step = step.substring(1);
                dataStr = 'stp=' + step + '&mnthSpend=' + $('#fmonthlySpend').val();
                break;

            case 'step7':
                if (payment_freq_rdio != "") {
                    dataStr = 'stp=' + step + '&payFrq=' + payment_freq_rdio;
                } else if (payment_freq != "") {
                    dataStr = 'stp=' + step + '&payFrq=' + payment_freq;
                }
                break;

            case 'fstep7':
                step = step.substring(1);
                if (payment_freq_rdio != "") {
                    dataStr = 'stp=' + step + '&payFrq=' + payment_freq_rdio;
                } else if ( payment_freq != "") {
                    dataStr = 'stp=' + step + '&payFrq=' + payment_freq;
                }
                break;

            case 'step8':
                dataStr = 'stp=' + step + '&rolAmnt=' + $('#roll_amount').val();
                break;

            case 'fstep8':
                step = step.substring(1);
                dataStr = 'stp=' + step + '&rolAmnt=' + $('#froll_amount').val();
                break;

            case 'step9':
                dataStr = 'stp=' + step + '&rolPriod=' + $('#rolloverPeriod').val();
                break;
                
            case 'fstep9':
                step = step.substring(1);
                dataStr = 'stp=' + step + '&rolPriod=' + $('#frolloverPeriod').val();
                break;

            case 'step10':
                dataStr = 'stp=' + step + '&cBlncStat=' + $('input[name=optionCurrBlnce]:checked').val()+'&cBlncAmount='+ $('#transferAmount').val();
                break;

            case 'step11':
                var valuesarr = { };
                valuesarr.rewards = [];

                $.each(JSON.parse($.trim(RewardsBurnedjson)), function(i,obj){
                    tid = obj.tid;
                    var values = $('input[name="rewardscat[step13]['+tid+'][]"]').val();
                    valuesarr.rewards.push({
                        'item_id': tid, 
                        'item_special': values
                    });
                });

                var dataarr = JSON.stringify(valuesarr);
                dataStr = 'stp=' + step + '&catRewards=' + dataarr;
                break;

            case 'step12':
                ovrSeaSpnd = $('#overseaSpnd').val();
                dataStr = 'stp=' + step + '&ovrSeaSpnd=' + ovrSeaSpnd;
                break;

            case 'step13':
                onlineSpnd = $('#onlineSpnd').val();
                dataStr = 'stp=' + step + '&onlineSpnd=' + onlineSpnd;
                break;

        }
        $.ajax({
            type: "GET",
            dataType: "json",
            url: base_domain+'/wizard_misc?p=wizard',
            data: dataStr,
            success: function(data) {
                
            }
        });
            console.log(step);
         return false;
    }
    
    // $('#stp_lst').css("visibility", "hidden").css("display", "");
    $('#stp_lst').css("display", "");
  
  wizardInitalize();
 function wizardInitalize(){
    alert('here');
    $("#mc-wizard").validate({
        onfocusout: false,
        onkeyup: false,
        initialStep: 0,
        rules: {
            rolloverPeriod: {
                required: true,
                max: 36
            },
            frolloverPeriod: {
                required: true,
                max: 36
            },
            annumincome: {
                required: true,
                digits: true
            },
            monthlySpend: {
                required: true,
                digits: true,
                max: function() {
                    return roundUpToHundred(Math.ceil(parseInt($('#annumincome').val())/12));
                }
            },
            fmonthlySpend: {
                required: true,
                digits: true,
                max: function() {
                    return roundUpToHundred(Math.ceil(parseInt($('#annumincome').val())/12));
                }
            },
            roll_amount: {
                required: true,
                max: function() {
                    return Math.ceil(parseInt($('#monthlySpend').val()));
                }
            },
            froll_amount: {
                required: true,
                max: function() {
                    return Math.ceil(parseInt($('#fmonthlySpend').val()));
                }
            },
            optionCurrBlnce: { //current balance
                required: true
            },
            card_name: {
                required: true
            }
        },
        errorPlacement: function(error, element) {
            if (element[0].id == 'card_name'){
                $('.chosen-single').addClass('slcterr');
            }                       
            else{
                element.val(error.text()); 
            }
                      
            
        },
        messages: {
            annumincome:{
                required: "This field is required",
                digits: "Enter valid number"
            },
            fmonthlySpend: {
                required: "Field required",
                digits: "Number required",
                max: function(){
                    return "Max value is "+ roundUpToHundred(Math.ceil(parseInt($('#annumincome').val())/12));   
                }    
            },
            monthlySpend: {
                required: "Field required",
                digits: "Number required",
                max: function(){ 
                    return "Max value is "+ roundUpToHundred(Math.ceil(parseInt($('#annumincome').val())/12));
                }
            },
            frolloverPeriod: {
                max: "Max value is 36"
            },
            rolloverPeriod: {
                max: "Max value is 36"
            },
            ent_card_name:{
                required: "Field required"
            },
            crdapr:{
                required : "Required",
                digits: "Required"
            },
            roll_amount: {
                    max: function(){
                        return "Max value is " + Math.ceil(parseInt($('#monthlySpend').val()))   
                        }             
            },
            froll_amount: {
                    max: function(){
                        return "Max value is " + Math.ceil(parseInt($('#fmonthlySpend').val()))   
                        }             
            },
            overseaSpnd: {
                digits: "Number required"
            },
            onlineSpnd :{
                digits: "Number required"
            }

        }
    });


    // MC wizard wizard with validation
    var stepsPossible = 13;
    $("#wizard-form").wizard({
        stepsWrapper: "#mc-wizard",
        submit: ".submit",        
        beforeSelect: function(event, state) {
           
        },
        afterSelect: function(event, state) {
            
            var step = $(this).wizard('state').step.get(0).accessKey;
            var ctdvl;

            if(step == 'step6' || step == 'fstep6' ){ // one more step ahead
                 ctdvl = $('input[name=txtOptionCreditCard]:checked').val();
                 if(ctdvl == 'step3-slctcrdtcrd'){
                    stepsPossible = 13;
                 }else{
                    stepsPossible = 11;
                 }
            }

            if(step == 'step7' || step == 'fstep7'){
                if($('input[name=payment_freq_radio]:checked').val() == 'parntnode2' || $('input[name=payment_freq_radio_fls]:checked').val() == 'parntnode2')
                stepsPossible = 11;
            }

            if(step == 'fstep8'){
                ctdvl = $('input[name=txtOptionCreditCard]:checked').val();

                if(ctdvl == 'step3-slctcrdtcrd'){
                    stepsPossible = 13;
                }else{
                    stepsPossible = 11;
                }
            }



            if(step == 'step11'){
                if($('input[name=payment_freq_radio]:checked').val() == 'parntnode2'){   
                        stepsPossible = 11;                            
                    }else if($('input[name=payment_freq_radio_fls]:checked').val() == 'parntnode2'){
                        if($('input[name=txtOptionCreditCard]:checked').val() == 'with-out-selected-crdtcrd'){
                         stepsPossible = 9;
                     }else{
                        stepsPossible = 13;
                     }
                }
            }

            //state.stepsPossible
            $("#progressbar").progressbar("value", (100*(state.stepsComplete/stepsPossible)) );

            $('[type=text]').click(function(event) {
                $(this).removeClass("error").addClass("valid");
                $(this).val("");
                event.preventDefault();

            });
            
            //final step button visibility
            $('#stp_lst').css("display", ""); //visiblity not working opera???
            $('#brwnw').css("visibility", "hidden").css("display", "");
            if (step == 'stepLast') {              
                $('#stp_lst').css("display", true);
                $('#stp_lst').text("CALCULATE PERSONALISED  eVALUATOR SCORES");
            }

 

        },
        beforeBackward: function(event, state) {           
            return true;
        },
        beforeForward: function() {
            // var clicks = 0, self = this;
            // jQuery(this).click(function(event){ 
            //    console.log(this);
            // });
            var inputs = $(this).wizard('state').step.find(':input');
            var step = $(this).wizard('state').step.get(0).accessKey;

            $isValid = !inputs.length || !!inputs.valid();
            if($isValid){
                ajaxWizardStep(step);
            }
            return $isValid;

        },
        afterForward: function() {
        },
        afterBackward: function(state){
            var step = $(this).wizard('state').step.get(0).accessKey;
        },    
        transitions: {
            optionCreditCard: function($step, action) {
                var branch = $step.find("[name=txtOptionCreditCard]:checked").val();
                if (!branch) {
                    alert("Please select a value to continue.");
                }
                return branch;
            },
            paymentFrequency: function($step, action) {

                var branch = $step.find("[name=payment_freq_radio]:checked").val();
                if (!branch) {
                    var branch = "payment-frq-tr";
                }
                return branch;
            },                
            currentBalance: function($step, action) {
                var branch = "parntnode2"; 
                if (!branch) {
                    alert("Please select a value to continue.");
                }
                return branch;
            },
            balanceTransfer: function($step, action) {
                var branch = $step.find("[name=blnctransfer]:checked").val();
                if (!branch) {
                    alert("Please select a value to continue.");
                }
                return branch;
            },
            paymentFrequencyFls: function($step, action) {
                var branch = $step.find("[name=payment_freq_radio_fls]:checked").val();
                if (!branch) {
                    var branch = "payment-frq-fl";
                }
                return branch;
            }
        }
    }).wizard('form').submit(function(event) {
        event.preventDefault(); 
    
        var t=setTimeout(function(){
                /*goToBySlider('crdrslt'); */
                var w=setTimeout(function(){
                    $('#end h3').text('Your eValuator scores are calculated below, and the credit cards sorted by those with the best value for you.');
                    $('#backward').css('display','none');
                    $('#stp_lst').css('display','none');
                    $('#brwnw').css("visibility","visible").css('display','block');

                },1000);   
            },600);

    }).validate({
        errorPlacement: function(error, element) {
            if (element.is(':radio') || element.is(':checkbox')) {
                    error.insertAfter(element.next());                
                    //error.insertAfter("label[for='ermsg']");

            } else {

                
                error.insertAfter(element);
            }
        }

    });
    return false;
}

});