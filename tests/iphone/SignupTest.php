<?php
class SignupTest extends PHPUnit_Extensions_Selenium2TestCase
{
	protected $captureScreenshotOnFailure = TRUE;
    protected $screenshotPath = "C:\xampp\htdocs\ocast\tests\iphone\screens";
    protected $screenshotUrl = 'http://localhost/ocast/tests/iphone/screens';

    protected function setUp()
    {
    	//$this->host('localhost');
    	//$this->setPort(4444);
        $this->setBrowser('chrome');
        $this->setBrowserUrl('http://localhost/ocast/');
    }

    public function setUpPage() 
    {
    	$this->currentWindow()->size(array('width' => 320, 'height' => 480));
  	}

 

    public function testSignupScreen()
    {
    	$this->url('signup/new_account');

        //checking top header bar
        $headerBarElement = $this->byCssSelector('.topheaderbar');
        $this->assertEquals('287px', $headerBarElement->css('width'));
        $this->assertEquals('rgb(24, 169, 196) none repeat scroll 0% 0% / auto padding-box border-box', $headerBarElement->css('background'));
        $this->assertEquals('2px 0px 0px', $headerBarElement->css('padding'));
        $this->assertEquals('53px', $headerBarElement->css('height'));


        //checking the navigation open button position
        $element = $this->byId('nav-open-btn');
        $this->assertEquals('absolute', $element->css('position'));
        $this->assertEquals('10px', $element->css('top'));


        //checking page description header and styles
        $descriptionHeader = $this->byCssSelector('.h1mar');
        $this->assertEquals('14px', $descriptionHeader->css('font-size'));
        $this->assertEquals('uppercase', $descriptionHeader->css('text-transform'));
        $this->assertEquals('rgba(101, 101, 101, 1)', $descriptionHeader->css('color'));
        $this->assertEquals('4px', $descriptionHeader->css('margin-bottom'));
        $this->assertEquals('1px solid rgb(165, 165, 165)', $descriptionHeader->css('border-bottom'));
        $this->assertEquals('1px solid rgb(226, 226, 226)', $descriptionHeader->css('border-top'));
        $this->assertEquals('8px 0px', $descriptionHeader->css('padding'));
        $this->assertEquals('open_sansbold', $descriptionHeader->css('font-family'));

        //checking filter link alignment and position
        $filterlink = $this->byId('filtertpbx')->byCssSelector('.navbar-nav > li > a');
        $this->assertEquals('13px', $filterlink->css('font-size'));
        $this->assertEquals('rgba(24, 169, 195, 1)', $filterlink->css('color'));        

    	//checking the search field alignments
    	$searchElement = $this->byCssSelector('.custmserch');
    	$this->assertEquals('50px', $searchElement->css('margin-left'));
    	$this->assertEquals('-4px', $searchElement->css('margin-top'));
    	$this->assertEquals('0px', $searchElement->css('top'));
    	$this->assertEquals('0px', $searchElement->css('left'));

    	//checking footer alignment
    	$footerElement = $this->byCssSelector('.rightsidsm');
    	$this->assertEquals('30px', $footerElement->css('margin-top'), 'Footer top margin is invalid');

        $this->commonLayoutTest($this);
    }

    /**
    * Contains the connect page assertions
    */
    public function testConnect()
    {
        $this->url('/site/connect');

        //echo var_dump($this->getBrowserUrl()); die;

        //checking top header bar
        $headerBarElement = $this->byCssSelector('.topheaderbar');
        $this->assertEquals('287px', $headerBarElement->css('width'));
        $this->assertEquals('rgb(24, 169, 196) none repeat scroll 0% 0% / auto padding-box border-box', $headerBarElement->css('background'));
        $this->assertEquals('2px 0px 0px', $headerBarElement->css('padding'));
        $this->assertEquals('53px', $headerBarElement->css('height'));

        //checking the navigation open button position
        $element = $this->byId('nav-open-btn');
        $this->assertEquals('absolute', $element->css('position'));
        $this->assertEquals('10px', $element->css('top'));

        //checking page description header and styles
        $descriptionHeader = $this->byCssSelector('.h1mar');
        $this->assertEquals('14px', $descriptionHeader->css('font-size'));
        $this->assertEquals('uppercase', $descriptionHeader->css('text-transform'));
        $this->assertEquals('rgba(101, 101, 101, 1)', $descriptionHeader->css('color'));
        $this->assertEquals('4px', $descriptionHeader->css('margin-bottom'));
        $this->assertEquals('1px solid rgb(165, 165, 165)', $descriptionHeader->css('border-bottom'));
        $this->assertEquals('1px solid rgb(226, 226, 226)', $descriptionHeader->css('border-top'));
        $this->assertEquals('8px 0px', $descriptionHeader->css('padding'));
        $this->assertEquals('open_sansbold', $descriptionHeader->css('font-family'));

        //checking filter link alignment and position
        $filterlink = $this->byId('filtertpbx')->byCssSelector('.navbar-nav > li > a');
        $this->assertEquals('13px', $filterlink->css('font-size'));
        $this->assertEquals('rgba(24, 169, 195, 1)', $filterlink->css('color'));
        //$this->commonLayoutTest($this);

        //checking page seperation horizontal borders are invisible
        $hborders = $this->elements($this->using('css selector')->value('*[class="border-tp"]'));
        $this->assertEquals(1, count($hborders));
        $this->assertEquals('none', $hborders[0]->css('display'));

        //checking page side menu is invisible
        $sidemenu = $this->byCssSelector('.appndfrmlft');
        $this->assertEquals('', $sidemenu->text());

        //checking buttons
        $btnConnect = $this->byCssSelector('.orangebtn');
        $this->assertEquals('none', $btnConnect->css('float'));
        $this->assertEquals('10px', $btnConnect->css('margin-top'));
        $this->assertEquals('10px', $btnConnect->css('margin-right'));
        $this->assertEquals('257px', $btnConnect->css('width'));
        $this->assertEquals('rgb(241, 144, 0) none repeat scroll 0% 0% / auto padding-box border-box', $btnConnect->css('background'));

        $btnCancel = $this->byCssSelector('.cancelbtn');
        $this->assertEquals('none', $btnConnect->css('float'));
        $this->assertEquals('10px', $btnConnect->css('margin-top'));
        $this->assertEquals('10px', $btnConnect->css('margin-right'));
        $this->assertEquals('257px', $btnConnect->css('width'));
        $this->assertEquals('rgb(241, 144, 0) none repeat scroll 0% 0% / auto padding-box border-box', $btnConnect->css('background'));

        //checking top and footer description
        $descriptions = $this->elements($this->using('css selector')->value('*[class="fntsmll"]'));
        $this->assertEquals(2, count($descriptions));
        foreach ($descriptions as $description) {
            $this->assertEquals('10px', $description->css('font-size'));
            $this->assertEquals('rgba(143, 143, 143, 1)', $description->css('color'));
        }

        //checking footer is invisible
        $footer = $this->byCssSelector('.footdiv');
        $this->assertEquals('none', $footer->css('display'));

    }

    /**
    * Contains the site info page assertions
    *
    */
    /*public function testSiteInfo()
    {
        $this->url('/site/info');
        $this->commonLayoutTest($this);

        //checking page seperation horizontal borders are invisible
        $hborders = $this->elements($this->using('css selector')->value('*[class="border-tp"]'));
        $this->assertEquals(1, count($hborders));
        $this->assertEquals('none', $hborders[0]->css('display'));

        //checking page description text
        $text = $this->byCssSelector('.padno');
        $this->assertEquals('0px', $text->css('padding'));
        $this->assertEquals('0px', $text->css('margin'));
        $this->assertEquals('rgba(101, 101, 101, 1)', $text->css('color'));

        //checking drop downs common margins and styles
        $dropdowns = $this->elements($this->using('css selector')->value('*[class="btn-group selbx100 selctboxstyle"]'));
        $this->assertEquals(5, count($dropdowns));
        foreach($dropdowns as $dropdown)
        {
            //checking each dropdown first child div
            $firstChildDiv = $dropdown->byXPath("/descendant::div[@class='covr']");
            $this->assertEquals('166.6875px', $firstChildDiv->css('width'));
            $this->assertEquals('36px', $firstChildDiv->css('height'));
            $this->assertEquals('1px solid rgb(211, 211, 211)', $firstChildDiv->css('border'));
            $this->assertEquals('center', $firstChildDiv->css('text-align'));
            $this->assertEquals('3px', $firstChildDiv->css('border-radius'));
            $this->assertEquals('rgba(0, 0, 0, 0) url('.$this->getBrowserUrl().'assets/images/sercrepselect.gif) repeat scroll 0% 0% / auto padding-box border-box', $firstChildDiv->css('background'));

            //checking each child div's styles(usually dropdown default text)
            $firstChildDiv_FirstSiblingDiv = $firstChildDiv->byXPath("/descendant::div[@class='txt']");
            $this->assertEquals('left', $firstChildDiv_FirstSiblingDiv->css('float'));
            $this->assertEquals('left', $firstChildDiv_FirstSiblingDiv->css('text-align'));
            $this->assertEquals('8px', $firstChildDiv_FirstSiblingDiv->css('padding-top'));
            $this->assertEquals('rgba(115, 115, 115, 1)', $firstChildDiv_FirstSiblingDiv->css('color'));
            $this->assertEquals('12px', $firstChildDiv_FirstSiblingDiv->css('font-size'));
            $this->assertEquals('8px', $firstChildDiv_FirstSiblingDiv->css('left'));
            $this->assertEquals('open_sansbold', $firstChildDiv_FirstSiblingDiv->css('font-family'));
            $this->assertEquals('33px', $firstChildDiv_FirstSiblingDiv->css('height'));

            //checking each drop down arrow background block positions and styles
            $firstChildDiv_SecondSiblingDiv = $firstChildDiv->byXPath("/descendant::div[@class='arow']");
            $this->assertEquals('none', $firstChildDiv_SecondSiblingDiv->css('float'));
            $this->assertEquals('rgba(0, 0, 0, 0) url('.$this->getBrowserUrl().'assets/images/blackrep.png) repeat scroll 0% 0% / auto padding-box border-box', $firstChildDiv_SecondSiblingDiv->css('background'));
            $this->assertEquals('rgba(0, 0, 0, 0)', $firstChildDiv_SecondSiblingDiv->css('background-color'));
            $this->assertEquals('36px', $firstChildDiv_SecondSiblingDiv->css('height'));
            $this->assertEquals('32px', $firstChildDiv_SecondSiblingDiv->css('width'));
            $this->assertEquals('7px', $firstChildDiv_SecondSiblingDiv->css('padding'));
            $this->assertEquals('absolute', $firstChildDiv_SecondSiblingDiv->css('position'));
            $this->assertEquals('0px', $firstChildDiv_SecondSiblingDiv->css('right'));
            $this->assertEquals('0px', $firstChildDiv_SecondSiblingDiv->css('top'));

            //checking each dropdowm arrow positions and styles
            $firstChildDiv_SecondSiblingDiv_FirstChildDiv = $firstChildDiv_SecondSiblingDiv->byXPath("/descendant::span[@class='caret']");
            $this->assertEquals('4px solid rgb(250, 250, 250)', $firstChildDiv_SecondSiblingDiv_FirstChildDiv->css('border-top'));

            //checking each drop down first child ul
            $firstChildUL = $dropdown->byXPath("/descendant::ul[@class='dropdown-menu']");
            $this->assertEquals('160px', $firstChildUL->css('min-width'));

            $this->assertEquals('257px', $dropdown->css('width'));
            $this->assertEquals('0px', $dropdown->css('margin-left'));
        }

        //checking "Add new language" button styles and postions
        $btnAddNewLang = $this->elements($this->using('css selector')->value('*[class="btn btn-primary pull-left smallmobilbtn"]'));
        $this->assertEquals('none', $btnAddNewLang[0]->css('float'));
        $this->assertEquals('10px', $btnAddNewLang[0]->css('margin-top'));
        $this->assertEquals('121px', $btnAddNewLang[0]->css('width'));

        //checking edit language version links
        $editLangLink = $this->element($this->using('css selector')->value('*[class="semibold marginno"]'));
        $this->assertEquals('open_sanssemibold', $editLangLink->css('font-family'));
        $this->assertEquals('0px', $editLangLink->css('margin'));
        $this->assertEquals('rgba(143, 143, 143, 1)', $editLangLink->css('color'));
        $links = $this->elements($this->using('xpath')->value('//*[@class="semibold marginno"]/a'));
        foreach($links as $link)
        {
            $this->assertEquals('underline', $link->css('text-decoration'));
            $this->assertEquals('rgba(24, 169, 195, 1)', $link->css('color'));
            $this->assertEquals('open_sanssemibold', $link->css('font-family'));
        }

        //checking section wrapper after the lanuage edit section
        $wrapper = $this->byId('ovrwritbacken');
        $this->assertEquals('20px', $wrapper->css('margin-top'));
        $this->assertEquals('0px', $wrapper->css('padding'));

        //checking first child div
        $wrapperFirstChildDiv = $wrapper->byXPath("/descendant::div[@class='panel panel-default']");
        $this->assertEquals('rgba(255, 255, 255, 1)', $wrapperFirstChildDiv->css('background-color'));
        $this->assertEquals('rgba(143, 143, 143, 1)', $wrapperFirstChildDiv->css('color'));
        $this->assertEquals('13px', $wrapperFirstChildDiv->css('font-size'));
        $this->assertEquals('20px', $wrapperFirstChildDiv->css('margin-bottom'));
        $this->assertEquals('15px 0px', $wrapperFirstChildDiv->css('padding'));

        //checking first child div sibling 01
        $wrapperFirstChildDivSib01 = $wrapperFirstChildDiv->byXPath("/descendant::div[@class='panel-heading']");
        $this->assertEquals('1px solid rgb(165, 165, 165)', $wrapperFirstChildDivSib01->css('border-bottom'));
        $this->assertEquals('1px solid rgb(226, 226, 226)', $wrapperFirstChildDivSib01->css('border-top'));
        $this->assertEquals('rgba(0, 0, 0, 0) none repeat scroll 0% 0% / auto padding-box border-box', $wrapperFirstChildDivSib01->css('background'));
        $this->assertEquals('0px', $wrapperFirstChildDivSib01->css('border-radius'));
        $this->assertEquals('rgba(101, 101, 101, 1)', $wrapperFirstChildDivSib01->css('color'));
        $this->assertEquals('0px', $wrapperFirstChildDivSib01->css('padding-left'));
        $this->assertEquals('8px', $wrapperFirstChildDivSib01->css('padding-bottom'));
        $this->assertEquals('8px', $wrapperFirstChildDivSib01->css('padding-top'));

    }*/


    /**
    * Contains the site info page assertions
    *
    */
    public function testSiteInfo()
    {
        $this->url('/signup/site_info');
        $this->commonLayoutTest($this);

        //getting step IDs container
        $stepContainer = $this->byId('stepids');

        //checking already completed step circles common styles
        $completedCircles = $this->elements($this->using('xpath')->value('//*[@id="stepids"]/div[contains(@class, "col-lg-1") and contains(@class, "linecircl") and contains(@class, "active2")]'));
        $this->assertEquals(2, count($completedCircles));

        //checking circles of further steps
        $furtherCircles = $this->elements($this->using('xpath')->value('//*[@id="stepids"]/div[@class="col-lg-1 linecircl"]'));
        $this->assertEquals(3, count($furtherCircles));

        //checking already completed step circle seperator styles and positions
        $completedCircleSeperators = $this->elements($this->using('xpath')->value('//*[@id="stepids"]/div[@class="col-lg-1 linebrd active"]'));
        $this->assertEquals(2, count($completedCircleSeperators));

        //checking future step circle seperators styles and positions
        $futureCircleSeperators = $this->elements($this->using('xpath')->value('//*[@id="stepids"]/div[@class="col-lg-1 linebrd"]'));
        $this->assertEquals(3, count($futureCircleSeperators));

        //checking completed step text styles and positions
        $completedStepTexts = $this->elements($this->using('xpath')->value('//*[@id="stepids"]/div[contains(@class, "col-lg-1") and contains(@class, "stptxt") and contains(@class, "active")]'));
        $this->assertEquals(3, count($completedStepTexts));

        //checking further steps text styles and positions
        $furtherStepTexts = $this->elements($this->using('xpath')->value('//*[@id="stepids"]/div[contains(@class, "col-lg-1") and contains(@class, "stptxt") and not(contains(@class, "active"))]'));
        $this->assertEquals(3, count($furtherStepTexts));

        //checking site info container positions and styles
        $siteInfoContainer = $this->byId('site_info');
        $childWrapper = $siteInfoContainer->byXPath('div[@class="col-lg-12 padno formmar"]');
        $this->assertEquals('20px', $childWrapper->css('margin-top'));
        $this->assertEquals('0px', $childWrapper->css('padding'));

        //checking alert section elements has necessary classes
        $alertSection = $childWrapper->byXPath('div[1]');
        $this->assertEquals('alert alert-warning', $alertSection->attribute('class'));
        $this->assertEquals('text-align: center;', $alertSection->attribute('style'));

        $alertLink = $alertSection->byXPath('a[1]');
        $this->assertEquals($this->getBrowserUrl().'signup/product_formats', $alertLink->attribute('href'));
        $this->assertEquals('underline', $alertLink->css('text-decoration'));

        $alertText = $alertLink->byXPath('i[1]');
        $this->assertEquals('Skip this step for now', $alertText->text());
        $alertTextArrow = $alertLink->byXPath('i[2]');
        $this->assertEquals('icon-angle-right', $alertTextArrow->attribute('class'));
        $this->assertEquals('font-size: 11px;', $alertTextArrow->attribute('style'));

        //checking category heading count, positions and styles
        $categoryHeadings = $this->elements($this->using('css selector')->value('div[class="panel-heading"]'));
        $this->assertEquals(10, count($categoryHeadings));

        //checking drop downs common margins and styles
        $dropdowns = $this->elements($this->using('css selector')->value('*[class="btn-group selbx100 selctboxstyle"]'));
        $this->assertEquals(4, count($dropdowns));

        //checking primary button styles
        $primaryButtons = $this->elements($this->using('xpath')->value('//button[contains(@class, "btn btn-primary") and contains(@class, "margnrightbtn") and not(contains(@class, "btn-xs"))]'));
        $this->assertEquals(4, count($primaryButtons));

        //checking add another person and add another module button (secondary buttons)
        $addDynaButtons = $this->elements($this->using('xpath')->value('//button[contains(@class, "btn ashbtn margnrightbtn btn-block")]'));
        $this->assertEquals(2, count($addDynaButtons));
        foreach($addDynaButtons as $addDynaButton)
        {
            $this->assertEquals('open_sanssemibold', $addDynaButton->css('font-family'));
            $this->assertEquals('rgba(115, 115, 115, 1)', $addDynaButton->css('color'));
            $this->assertEquals('0px', $addDynaButton->css('border-radius'));
            $this->assertEquals('1px solid rgb(228, 228, 228)', $addDynaButton->css('border'));
            $this->assertEquals('rgba(0, 0, 0, 0) url('.$this->getBrowserUrl().'assets/images/ashbtn.gif) repeat scroll 0% 0% / auto padding-box border-box', $addDynaButton->css('background'));
            $this->assertEquals('12px', $addDynaButton->css('font-size'));
            $this->assertEquals('29px', $addDynaButton->css('height'));
            $this->assertEquals('0px', $addDynaButton->css('padding-top'));
            $this->moveto($addDynaButton);
            $this->assertEquals('rgba(142, 142, 142, 1)', $addDynaButton->css('color'));
        }

        //checking sliders on the page
        $sliderContainers = $this->elements($this->using('xpath')->value('//div[contains(@class, "sliderrange")]'));
        $this->assertEquals(4, count($sliderContainers));

        //here we are dynamically adding another slider on a click event
        $this->byId('filterapnd')->click();
        $this->acceptAlert();
        $sliderContainers = $this->elements($this->using('xpath')->value('//div[contains(@class, "sliderrange") or contains(@class, "rangewid")]'));
        $this->assertEquals(5, count($sliderContainers));

        //asserting slider (Age Span)
        $amountStart = $sliderContainers[0]->byXPath('//descendant::span[@id="amount-start"]');
        $this->assertEquals('rgba(79, 79, 79, 1)', $amountStart->css('color'));
        $this->assertEquals('13px', $amountStart->css('font-size'));
        $this->assertEquals('left', $amountStart->css('float'));
        $this->assertEquals('15px', $amountStart->css('width'));
        $this->assertEquals('10px', $amountStart->css('margin-right'));

        $amountEnd = $sliderContainers[0]->byXPath('//descendant::span[@id="amount-end"]');
        $this->assertEquals('rgba(79, 79, 79, 1)', $amountEnd->css('color'));
        $this->assertEquals('13px', $amountEnd->css('font-size'));
        $this->assertEquals('left', $amountEnd->css('float'));
        $this->assertEquals('15px', $amountEnd->css('width'));
        $this->assertEquals('10px', $amountEnd->css('margin-left'));

    }


    /**
    * Contains the products format page assertions
    *
    */
    public function testProductFormats()
    {
        $this->url('signup/product_formats');
        $this->commonLayoutTest($this);

        //checking already completed step circles common styles
        $completedCircles = $this->elements($this->using('xpath')->value('//*[@id="stepids"]/div[contains(@class, "col-lg-1") and contains(@class, "linecircl") and contains(@class, "active2")]'));
        $this->assertEquals(3, count($completedCircles));

        //checking alert section elements has necessary classes
        $siteInfoContainer = $this->byId('prct_formt');
        $childWrapper = $siteInfoContainer->byXPath('div[@class="col-lg-12 padno formmar"]');
        $alertSection = $childWrapper->byXPath('div[1]');
        $this->assertEquals('alert alert-warning', $alertSection->attribute('class'));
        $this->assertEquals('text-align: center;', $alertSection->attribute('style'));

        $alertLink = $alertSection->byXPath('a[1]');
        $this->assertEquals($this->getBrowserUrl().'signup/add_cases', $alertLink->attribute('href'));
        $this->assertEquals('underline', $alertLink->css('text-decoration'));

        $alertText = $alertLink->byXPath('i[1]');
        $this->assertEquals('Skip this step for now', $alertText->text());
        $alertTextArrow = $alertLink->byXPath('i[2]');
        $this->assertEquals('icon-angle-right', $alertTextArrow->attribute('class'));
        $this->assertEquals('font-size: 11px;', $alertTextArrow->attribute('style'));

        //checking add format button (secondary buttons)
        $addDynaButtons = $this->elements($this->using('xpath')->value('//button[contains(@class, "btn ashbtn margnrightbtn btn-block")]'));
        $this->assertEquals(1, count($addDynaButtons));
        foreach($addDynaButtons as $addDynaButton)
        {
            $this->assertEquals('open_sanssemibold', $addDynaButton->css('font-family'));
            $this->assertEquals('rgba(115, 115, 115, 1)', $addDynaButton->css('color'));
            $this->assertEquals('0px', $addDynaButton->css('border-radius'));
            $this->assertEquals('1px solid rgb(228, 228, 228)', $addDynaButton->css('border'));
            $this->assertEquals('rgba(0, 0, 0, 0) url('.$this->getBrowserUrl().'assets/images/ashbtn.gif) repeat scroll 0% 0% / auto padding-box border-box', $addDynaButton->css('background'));
            $this->assertEquals('12px', $addDynaButton->css('font-size'));
            $this->assertEquals('29px', $addDynaButton->css('height'));
            $this->assertEquals('0px', $addDynaButton->css('padding-top'));
            $this->moveto($addDynaButton);
            $this->assertEquals('rgba(142, 142, 142, 1)', $addDynaButton->css('color'));
        }

        //checking dynamically adding format sections
        $dynaContainer = $this->elements($this->using('xpath')->value('//div[@class="col-lg-12 apndiv"]/div[@class="col-lg-4 clsformat"]'));
        $this->assertEquals(0, count($dynaContainer));
        $this->moveto($addDynaButtons[0]);
        $addDynaButtons[0]->click();
        sleep(1); 
        
        $dynaContainer = $this->elements($this->using('xpath')->value('//div[@class="col-lg-12 apndiv"]/div[contains(@class, "col-lg-4") and contains(@class, "clsformat")]'));
        $this->assertEquals(1, count($dynaContainer));
        $this->assertEquals('21px', $dynaContainer[0]->css('margin-top'));

        //checking siblings (thumbnail) of dynamically added section
        $thumbnail = $dynaContainer[0]->byXPath('form/div/div[@class="col-lg-12 thumbnilimg"]');
        $this->assertEquals('1px dashed rgb(214, 214, 214)', $thumbnail->css('border'));
        $this->assertEquals('105px', $thumbnail->css('min-height'));

        $img = $thumbnail->byXPath('img');
        $this->assertEquals($this->getBrowserUrl().'assets/images/add_screen.png', $img->attribute('src'));
        $this->assertEquals('150', $img->attribute('width'));
        $this->assertEquals('100', $img->attribute('height'));

        $input = $thumbnail->byXPath('input');
        $this->assertEquals('hidden', $input->attribute('type'));
        $this->assertEquals('productScreen1', $input->attribute('id'));
        $this->assertEquals('product[1][screen]', $input->attribute('name'));

        //checking siblings (form) of dynamically added section
        $form = $dynaContainer[0]->byXPath('form/div/div[@class="col-lg-12 "]');
        $this->assertEquals('rgb(245, 245, 245) none repeat scroll 0% 0% / auto padding-box border-box', $form->css('background'));
        $formElements = $this->elements($this->using('xpath')->value('//div[contains(@class, "martp10") and not(contains(@class, "col-lg-12 padno martp10"))]'));
        $this->assertEquals(5, count($formElements));
        foreach($formElements as $formElement)
        {
            $this->assertEquals('10px', $formElement->css('margin-top'));
        }

        //checking siblings (save format) of dynamically added section
        $saveFormatBtn = $this->byXPath('//button[@class="btn btn-small btn-primary saveFormat"]');
        $this->assertEquals('none', $saveFormatBtn->css('float'));
        $this->assertEquals('10px', $saveFormatBtn->css('margin-top'));
        $this->assertEquals('90px', $saveFormatBtn->css('width'));

    }


    /**
    * Contains the add cases page assertions
    *
    */
    public function testAddCases()
    {
        $this->url('signup/add_cases');
        $this->commonLayoutTest($this);

        // asserting case container
        $container = $this->byId('signup_cases');
        $defaultCaseFieldsContainer = $container->byXPath('div[@class = "col-lg-12 padno formmar"]');
        $this->assertEquals('20px', $defaultCaseFieldsContainer->css('margin-top'));
        $this->assertEquals('0px', $defaultCaseFieldsContainer->css('padding'));

        //asserting default title field
        $defaultCaseFieldsContainer = $defaultCaseFieldsContainer->byXPath('/descendant::div[@class = "col-lg-12 padno padleftno formmar"]');
        $titleField = $defaultCaseFieldsContainer->byXPath('div[1]/input');
        $this->assertEquals('txtcases[1][title]', $titleField->attribute('name'));
        $this->assertEquals('Case title', $titleField->attribute('placeholder'));

        //asserting default description
        $descriptionField = $defaultCaseFieldsContainer->byXPath('div[2]/textarea');
        $this->assertEquals('txtcases[1][desc]', $descriptionField->attribute('name'));

        //asserting default add image button
        $defaultButton = $container->byXPath('/descendant::div[@class = "col-lg-12 padno"]/button');
        $this->assertEquals('none', $defaultButton->css('float'));
        $this->assertEquals('10px', $defaultButton->css('margin-top'));
        $this->assertEquals('257px', $defaultButton->css('width'));
        $this->assertEquals('10px', $defaultButton->css('margin-right'));

        //asserting dynamic holder is empty
        $dynamicHolder = $container->byXPath('/descendant::div[@class = "case_apnd"]');
        $dynamicSiblings = $dynamicHolder->elements($this->using('xpath')->value('div'));
        $this->assertEquals(0, count($dynamicSiblings));

        //adding new case
        $newCaseButton = $this->byXPath('//button[@class = "btn ashbtn margnrightbtn btn-block modcase"]');
        $this->moveto($newCaseButton);
        $newCaseButton->click();
        sleep(1);

        //asserting dynamically added elements
        $dynamicHolder = $container->byXPath('/descendant::div[@class = "case_apnd"]');
        $something = $dynamicHolder->byXPath('/descendant::div[@class = "col-lg-12 padno formmar"]');
        $this->assertEquals('20px', $something->css('margin-top'));
        $this->assertEquals('0px', $something->css('padding'));
        $dynamicSiblings = $this->byCssSelector('.case_apnd')->elements($this->using('xpath')->value('div'));
        $this->assertEquals(2, count($dynamicSiblings));

        $dynamicInputHolder = $dynamicSiblings[0]->element($this->using('xpath')->value('div'));
        $this->assertEquals('0px', $dynamicInputHolder->css('padding'));
        $this->assertEquals('20px', $dynamicInputHolder->css('margin-top'));

        $dynamicTitleHolder = $dynamicInputHolder->byXPath('div[1]'); //title holder 
        $this->assertEquals('form-group', $dynamicTitleHolder->attribute('class'));
        $dynamicTitle = $dynamicTitleHolder->byXPath('label');
        $this->assertEquals('Title', $dynamicTitle->text());
        $dynamicTitleInputField = $dynamicTitleHolder->byXPath('input'); //title input field
        $this->assertEquals('Case title', $dynamicTitleInputField->attribute('placeholder'));
        $this->assertEquals('txtcases[2][title]', $dynamicTitleInputField->attribute('name'));
        $this->assertEquals('form-control', $dynamicTitleInputField->attribute('class'));

        $dynamicDescriptionHolder = $dynamicInputHolder->byXPath('div[2]'); //description holder 
        $this->assertEquals('form-group', $dynamicDescriptionHolder->attribute('class'));
        $dynamicDescription = $dynamicDescriptionHolder->byXPath('label');
        $this->assertEquals('Description', $dynamicDescription->text());
        $dynamicDescriptionTextareaField = $dynamicDescriptionHolder->byXPath('textarea'); //description textarea field
        $this->assertEquals('txtcases[2][desc]', $dynamicDescriptionTextareaField->attribute('name'));
        $this->assertEquals('form-control', $dynamicDescriptionTextareaField->attribute('class'));
        $this->assertEquals('4', $dynamicDescriptionTextareaField->attribute('rows'));

        $dynamicAddImageBtnHolder = $dynamicSiblings[1];
        $this->assertEquals('0px', $dynamicAddImageBtnHolder->css('padding'));
        $dynamicButton = $dynamicAddImageBtnHolder->byXPath('button');
        $this->assertContains('btn-primary', $dynamicButton->attribute('class'));
        $this->assertEquals('none', $dynamicButton->css('float'));
        $this->assertEquals('10px', $dynamicButton->css('margin-top'));
        $this->assertEquals('257px', $dynamicButton->css('width'));
        $dynamicHiddenInputField = $dynamicAddImageBtnHolder->byXPath('input');
        $this->assertEquals('hidden', $dynamicHiddenInputField->attribute('type'));
        $this->assertEquals('txtcases[2][img]', $dynamicHiddenInputField->attribute('name'));
        $dynamicImageDescription = $dynamicAddImageBtnHolder->byXPath('h6');
        $this->assertEquals('10px', $dynamicImageDescription->css('font-size'));

    }


    /**
    * Contains the preview site page assertions
    *
    */ 
    public function testPreviewSite()
    {
        $this->url('signup/preview_site');
        $this->commonLayoutTest($this);

        //asserting category section
        $categoryHolder = $this->element($this->using('xpath')->value('//div[contains(@class, "col-lg-8 padno  slectd")]'));
        $this->assertEquals('0px', $categoryHolder->css('padding'));

        //asserting category siblings
        $categoryHeading = $this->element($this->using('xpath')->value('//div[contains(@class, "pull-left fntsmll sansbold categorymob ")]'));
        $this->assertEquals('rgba(191, 191, 191, 1)', $categoryHeading->css('color'));
        $this->assertEquals('none', $categoryHeading->css('float'));
        $this->assertEquals('14px', $categoryHeading->css('font-size'));
        $this->assertEquals('10px', $categoryHeading->css('margin-bottom'));
        $categorySiblings = $categoryHolder->elements($this->using('xpath')->value('div[@id != "null"]'));
        foreach($categorySiblings as $sibling)
        {
            $siblingText = $sibling->byXPath('/descendant::p');
            $this->assertEquals('rgb(145, 145, 145) none repeat scroll 0px 0px / auto padding-box border-box', $siblingText->css('background'));
            $this->assertEquals('20px', $siblingText->css('border-radius'));
            $this->assertEquals('rgba(255, 255, 255, 1)', $siblingText->css('color'));
            $this->assertEquals('left', $siblingText->css('float'));
            $this->assertEquals('10px', $siblingText->css('font-size'));
            $this->assertEquals('19px', $siblingText->css('height'));
            $this->assertEquals('7px', $siblingText->css('margin-right'));
            $this->assertEquals('2px 10px 2px 11px', $siblingText->css('padding'));
            $this->assertEquals('center', $siblingText->css('text-align'));
            $this->assertEquals('-2px', $siblingText->css('margin-top'));
        }

    }



    /**
    * Contains the common layout assertions
    */
    public function commonLayoutTest($scope)
    {
        //checking top header bar
        $headerBarElement = $scope->byCssSelector('.topheaderbar');
        $this->assertEquals('287px', $headerBarElement->css('width'));
        $this->assertEquals('rgb(24, 169, 196) none repeat scroll 0% 0% / auto padding-box border-box', $headerBarElement->css('background'));
        $this->assertEquals('2px 0px 0px', $headerBarElement->css('padding'));
        $this->assertEquals('53px', $headerBarElement->css('height'));

        //checking the navigation open button position
        $element = $scope->byId('nav-open-btn');
        $scope->assertEquals('absolute', $element->css('position'));
        $scope->assertEquals('10px', $element->css('top'));

        //checking page description header and styles
        $descriptionHeader = $scope->byCssSelector('.h1mar');
        $scope->assertEquals('14px', $descriptionHeader->css('font-size'));
        $scope->assertEquals('uppercase', $descriptionHeader->css('text-transform'));
        $scope->assertEquals('rgba(101, 101, 101, 1)', $descriptionHeader->css('color'));
        $scope->assertEquals('4px', $descriptionHeader->css('margin-bottom'));
        $scope->assertEquals('1px solid rgb(165, 165, 165)', $descriptionHeader->css('border-bottom'));
        $scope->assertEquals('1px solid rgb(226, 226, 226)', $descriptionHeader->css('border-top'));
        $scope->assertEquals('8px 0px', $descriptionHeader->css('padding'));
        $this->assertEquals('open_sansbold', $descriptionHeader->css('font-family'));

        //checking step IDs container positions and styles
        $stepContainer = $this->byId('stepids');
        $this->assertEquals('40px 0px 0px', $stepContainer->css('padding'));
        $this->assertEquals('382px', $stepContainer->css('height'));
        $this->assertEquals('25px', $stepContainer->css('margin-bottom'));
        $this->assertEquals('132px', $stepContainer->css('min-height'));

        //checking already completed step circles common styles
        $completedCircles = $this->elements($this->using('xpath')->value('//*[@id="stepids"]/div[contains(@class, "col-lg-1") and contains(@class, "linecircl") and contains(@class, "active2")]'));
        foreach($completedCircles as $circle)
        {
            $result = $circle->byXPath('i');
            $this->assertEquals('icon-ok', $result->attribute('class'));
            $this->assertEquals('rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box', $circle->css('background'));
            $this->assertEquals('rgba(24, 169, 195, 1)', $circle->css('color'));
            $this->assertEquals('2px solid rgb(24, 169, 195)', $circle->css('border'));
            $this->assertEquals('7px', $circle->css('padding-top'));
            $this->assertEquals('11px', $circle->css('padding-left'));
        }

        //checking circles of further steps
        $furtherCircles = $this->elements($this->using('xpath')->value('//*[@id="stepids"]/div[@class="col-lg-1 linecircl"]'));
        foreach($furtherCircles as $furtherCircle)
        {
            $this->assertEquals('center', $furtherCircle->css('text-align'));
            $this->assertEquals('2px solid rgb(213, 226, 232)', $furtherCircle->css('border'));
            $this->assertEquals('43px', $furtherCircle->css('height'));
            $this->assertEquals('43px', $furtherCircle->css('width'));
            $this->assertEquals('40px', $furtherCircle->css('border-radius'));
            $this->assertEquals('rgba(168, 188, 197, 1)', $furtherCircle->css('color'));
            $this->assertEquals('6px', $furtherCircle->css('padding-top'));
            $this->assertEquals('14px', $furtherCircle->css('padding-left'));
            $this->assertEquals('18px', $furtherCircle->css('font-size'));
        }

        //checking already completed step circle seperator styles and positions
        $completedCircleSeperators = $this->elements($this->using('xpath')->value('//*[@id="stepids"]/div[@class="col-lg-1 linebrd active"]'));
        foreach($completedCircleSeperators as $seperator)
        {
            $this->assertEquals('rgba(11, 165, 229, 1)', $seperator->css('border-left-color'));
            $this->assertEquals('rgba(24, 169, 195, 1)', $seperator->css('border-bottom-color'));
            $this->assertEquals('2px solid rgb(11, 165, 229)', $seperator->css('border-left'));
            $this->assertEquals('19px', $seperator->css('height'));
            $this->assertEquals('21px', $seperator->css('left'));
            $this->assertEquals('relative', $seperator->css('position'));
            $this->assertEquals('0px', $seperator->css('top'));
            $this->assertEquals('0px none rgb(24, 169, 195)', $seperator->css('border-bottom'));
        }

        //checking future step circle seperators styles and positions
        $futureCircleSeperators = $this->elements($this->using('xpath')->value('//*[@id="stepids"]/div[@class="col-lg-1 linebrd"]'));
        foreach($completedCircleSeperators as $seperator)
        {
            $this->assertEquals('2px solid rgb(11, 165, 229)', $seperator->css('border-left'));
            $this->assertEquals('19px', $seperator->css('height'));
            $this->assertEquals('21px', $seperator->css('left'));
            $this->assertEquals('relative', $seperator->css('position'));
            $this->assertEquals('0px', $seperator->css('top'));
            $this->assertEquals('0px none rgb(24, 169, 195)', $seperator->css('border-bottom'));
            
        }

        //checking completed step text styles and positions
        $completedStepTexts = $this->elements($this->using('xpath')->value('//*[@id="stepids"]/div[contains(@class, "col-lg-1") and contains(@class, "stptxt") and contains(@class, "active")]'));
        foreach($completedStepTexts as $completedText)
        {
            $this->assertEquals('rgba(24, 169, 195, 1)', $completedText->css('color'));
            $this->assertEquals('relative', $completedText->css('position'));
            //$this->assertEquals('-340px', $completedText->css('top'));
            $this->assertContains(  $completedText->css('top'),
                                    array('-340px', '-341px', '-342px', '-343px', '-344px', '-339px', '-346px'));
            $this->assertEquals('11px', $completedText->css('font-size'));
            $this->assertEquals('61px', $completedText->css('left'));
            $this->assertEquals('46px', $completedText->css('margin-bottom'));
            $this->assertEquals('left', $completedText->css('text-align'));
            $this->assertEquals('257px', $completedText->css('width'));
            $this->assertEquals('0px', $completedText->css('margin-top'));
            //$this->assertEquals('-3px', $completedText->css('right'));
            $this->assertContains(  $completedText->css('right'),
                                    array('auto','0px', '-3px', '4px', '8px', '-9px'));
            $this->assertEquals('break-word', $completedText->css('word-wrap'));
        }


        //checking further steps text styles and positions
        $furtherStepTexts = $this->elements($this->using('xpath')->value('//*[@id="stepids"]/div[contains(@class, "col-lg-1") and contains(@class, "stptxt") and not(contains(@class, "active"))]'));
        foreach($furtherStepTexts as $furtherStepText)
        {
            $this->assertEquals('rgba(168, 188, 197, 1)', $furtherStepText->css('color'));
            $this->assertEquals('relative', $furtherStepText->css('position'));
            //$this->assertEquals('-340px', $furtherStepText->css('top'));
            $this->assertContains(  $furtherStepText->css('top'),
                                    array('-339px', '-340px', '-341px', '-342px', '-343px', '-344px', '-346px'));
            $this->assertEquals('11px', $furtherStepText->css('font-size'));
            $this->assertEquals('61px', $furtherStepText->css('left'));
            $this->assertEquals('46px', $furtherStepText->css('margin-bottom'));
            $this->assertEquals('left', $furtherStepText->css('text-align'));
            $this->assertEquals('257px', $furtherStepText->css('width'));
            $this->assertEquals('0px', $furtherStepText->css('margin-top'));
            $this->assertContains(  $furtherStepText->css('right'),
                                    array('4px', '8px', '-9px', 'auto', '0px'));
            //$this->assertEquals('4px', $furtherStepText->css('right'));
            $this->assertEquals('break-word', $furtherStepText->css('word-wrap'));
        }


        //checking category heading count, positions and styles
        $categoryHeadings = $this->elements($this->using('css selector')->value('div[class="panel-heading"]'));
        foreach($categoryHeadings as $categoryHeading)
        {
            $this->assertEquals('1px solid rgb(165, 165, 165)', $categoryHeading->css('border-bottom'));
            $this->assertEquals('1px solid rgb(226, 226, 226)', $categoryHeading->css('border-top'));
            $this->assertEquals('rgba(0, 0, 0, 0) none repeat scroll 0% 0% / auto padding-box border-box', $categoryHeading->css('background'));
            $this->assertEquals('0px', $categoryHeading->css('border-radius'));
            $this->assertEquals('rgba(101, 101, 101, 1)', $categoryHeading->css('color'));
            $this->assertEquals('0px', $categoryHeading->css('padding-left'));
            $this->assertEquals('8px', $categoryHeading->css('padding-bottom'));
            $this->assertEquals('8px', $categoryHeading->css('padding-top'));
            
        }


        //checking drop downs common margins and styles
        $dropdowns = $this->elements($this->using('css selector')->value('*[class="btn-group selbx100 selctboxstyle"]'));
        foreach($dropdowns as $dropdown)
        {
            //checking each dropdown first child div
            $firstChildDiv = $dropdown->byXPath("/descendant::div[@class='covr']");
            $this->assertEquals('auto', $firstChildDiv->css('width'));
            $this->assertEquals('36px', $firstChildDiv->css('height'));
            $this->assertEquals('1px solid rgb(211, 211, 211)', $firstChildDiv->css('border'));
            $this->assertEquals('center', $firstChildDiv->css('text-align'));
            $this->assertEquals('3px', $firstChildDiv->css('border-radius'));
            $this->assertEquals('rgba(0, 0, 0, 0) url('.$this->getBrowserUrl().'assets/images/sercrepselect.gif) repeat scroll 0% 0% / auto padding-box border-box', $firstChildDiv->css('background'));

            //checking each child div's styles(usually dropdown default text)
            $firstChildDiv_FirstSiblingDiv = $firstChildDiv->byXPath("/descendant::div[@class='txt']");
            $this->assertEquals('left', $firstChildDiv_FirstSiblingDiv->css('float'));
            $this->assertEquals('left', $firstChildDiv_FirstSiblingDiv->css('text-align'));
            $this->assertEquals('8px', $firstChildDiv_FirstSiblingDiv->css('padding-top'));
            $this->assertEquals('rgba(115, 115, 115, 1)', $firstChildDiv_FirstSiblingDiv->css('color'));
            $this->assertEquals('12px', $firstChildDiv_FirstSiblingDiv->css('font-size'));
            $this->assertEquals('8px', $firstChildDiv_FirstSiblingDiv->css('left'));
            $this->assertEquals('open_sansbold', $firstChildDiv_FirstSiblingDiv->css('font-family'));
            $this->assertEquals('33px', $firstChildDiv_FirstSiblingDiv->css('height'));

            //checking each drop down arrow background block positions and styles
            $firstChildDiv_SecondSiblingDiv = $firstChildDiv->byXPath("/descendant::div[@class='arow']");
            $this->assertEquals('none', $firstChildDiv_SecondSiblingDiv->css('float'));
            $this->assertEquals('rgba(0, 0, 0, 0) url('.$this->getBrowserUrl().'assets/images/blackrep.png) repeat scroll 0% 0% / auto padding-box border-box', $firstChildDiv_SecondSiblingDiv->css('background'));
            $this->assertEquals('rgba(0, 0, 0, 0)', $firstChildDiv_SecondSiblingDiv->css('background-color'));
            $this->assertEquals('36px', $firstChildDiv_SecondSiblingDiv->css('height'));
            $this->assertEquals('32px', $firstChildDiv_SecondSiblingDiv->css('width'));
            $this->assertEquals('7px', $firstChildDiv_SecondSiblingDiv->css('padding'));
            $this->assertEquals('absolute', $firstChildDiv_SecondSiblingDiv->css('position'));
            $this->assertEquals('0px', $firstChildDiv_SecondSiblingDiv->css('right'));
            $this->assertEquals('0px', $firstChildDiv_SecondSiblingDiv->css('top'));

            //checking each dropdowm arrow positions and styles
            $firstChildDiv_SecondSiblingDiv_FirstChildDiv = $firstChildDiv_SecondSiblingDiv->byXPath("/descendant::span[@class='caret']");
            $this->assertEquals('4px solid rgb(250, 250, 250)', $firstChildDiv_SecondSiblingDiv_FirstChildDiv->css('border-top'));

            //checking each drop down first child ul
            $firstChildUL = $dropdown->byXPath("/descendant::ul[@class='dropdown-menu']");
            $this->assertEquals('160px', $firstChildUL->css('min-width'));

            //$this->assertEquals('257px', $dropdown->css('width'));
            $this->assertEquals('0px', $dropdown->css('margin-left'));
        }


        //checking primary button styles
        $primaryButtons = $this->elements($this->using('xpath')->value('//button[contains(@class, "btn btn-primary") and contains(@class, "margnrightbtn") and not(contains(@class, "btn-xs"))]'));
        foreach($primaryButtons as $primaryButton)
        {
            $this->assertEquals('none', $primaryButton->css('float'));
            $this->assertEquals('10px', $primaryButton->css('margin-top'));
            $this->assertEquals('257px', $primaryButton->css('width'));
            $this->assertEquals('10px', $primaryButton->css('margin-right'));
            $this->assertEquals('rgba(24, 169, 195, 1)', $primaryButton->css('background-color'));
            $this->moveto($primaryButton);
            $this->assertEquals('rgba(19, 155, 170, 1)', $primaryButton->css('background-color'));

        }


        //checking currently activated circle
        //value('//button[contains(@class, "btn ashbtn margnrightbtn btn-block")]'));
        //$currentCircle = $this->element($stepContainer->using('xpath')->value('//div[contains(@class, "col-lg-1 linecircl active") or contains(@class, "col-lg-1 linecircl marg active")]'));
        $currentCircle = $stepContainer->byXPath("div[contains(concat(' ',@class,' '), ' col-lg-1 linecircl marg  active ') or contains(concat(' ',@class,' '), ' col-lg-1 linecircl active ')]");
        //$currentCircle = $stepContainer->byXPath('div[@class="col-lg-1 linecircl active"]');
        $this->assertEquals('rgb(24, 169, 195) none repeat scroll 0% 0% / auto padding-box border-box', $currentCircle->css('background'));
        $this->assertEquals('rgba(255, 255, 255, 1)', $currentCircle->css('color'));
        $this->assertEquals('0px none rgb(255, 255, 255)', $currentCircle->css('border'));
        $this->assertEquals('8px', $currentCircle->css('padding-top'));

        //checking footer is invisible
        $footer = $scope->byCssSelector('.footdiv');
        $scope->assertEquals('none', $footer->css('display'));
    }

 
}
?>