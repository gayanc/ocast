<?php
class SignupTest extends PHPUnit_Extensions_SeleniumTestCase
{
	/*protected $captureScreenshotOnFailure = TRUE;
    protected $screenshotPath = "C:\xampp\htdocs\ocast\selitest\screens";
    protected $screenshotUrl = 'http://localhost/ocast/selitest/screens';*/

    protected function setUp()
    {
    	//$this->host('localhost');
    	//$this->setPort(4444);
        $this->setBrowser('firefox');
        $this->setBrowserUrl('http://localhost/ocast/');
    }

    /*public function tearDown() 
    {
    	file_put_contents('result.png', base64_decode($this->currentScreenshot()));
  	}*/
 
    /*public function testNavbar()
    {
        $this->url('signup/new_account');
        $element = $this->byCssSelector('.topheaderbar');
        $this->assertEquals('rgba(24, 169, 196, 1)', $element->css('background-color'));
    }*/


    public function testSubmitSignupForm()
    {
    	$this->open('signup/new_account');
    	$this->windowMaximize();

    	$this->assertTitle('Google Analytics | Buyers Audience');
    	$this->type('acctxt_url', 'http://blabla.com');
    	$this->type('acctxt_email', 'bla@bla.com');
    	$this->type('acctxt_lname', 'doe');
    	$this->type('acctxt_cmpname', 'Acme');
    	$this->type('acctxt_addr1', 'Norris Canel Road');
    	$this->type('acctxt_addr2', 'Colombo 10');
    	$this->click("id=sp_svxt_stp1");

    	$this->assertEquals('This field is required.', $this->getAttribute('//*[@id="acctxt_fnameID"]@placeholder'));
    	//selenium.getAttribute("//img/@src");

    	/*"//title[@lang='eng']"
    	""*/
    	//echo var_dump(get_class_methods($this)); die;
    	//$form = $this->byId('acc_info');
    	// $action = $this->attribute('action');
    	// $this->byName('acctxt_url')->value('http://blabla.com');
    	// $this->byName('acctxt_email')->value('bla@bla.com');
    	// $this->byName('acctxt_lname')->value('doe');
    	// $this->byName('acctxt_cmpname')->value('Acme');
    	// $this->byName('acctxt_addr1')->value('Norris Canel Road');
    	// $this->byName('acctxt_addr2')->value('Colombo 10');
    	// $form->submit();

    	// $this->takeScreenshot();
    	//$screen = $this->currentScreenshot();

    	//file_put_contents('result.png', base64_decode($screen));

    	//getcwd()
    	 //$screenshot = $this->currentScreenshot();
    	 //echo var_dump($screen); die;
    	$this->captureEntirePageScreenshot('C:\xampp\htdocs\ocast\selitest\screens\test.png');

    	//$this->captureEntirePageScreenshot(getcwd().'screens\test.png');

    }


    public function testSignupFormForIphone()
    {
    	$this->open('signup/new_account');
    	//$win = $this->window();
    	//$this->runScript("javascript{ window.resizeTo(320,480); }");
    	//echo var_dump(get_class_methods($this)); die;
    	//$this->currentWindow()->size(array('width' => 320, 'height' => 480));
    	$this->captureEntirePageScreenshot('C:\xampp\htdocs\ocast\selitest\screens\iPhoneTest.png');
    }
 
}
?>