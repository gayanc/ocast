<?php 
class CaseTest extends PHPUnit_Extensions_Selenium2TestCase{

	 protected function setUp()
    {
    	//$this->host('localhost');
    	//$this->setPort(4444);
        $this->setBrowser('chrome');
        $this->setBrowserUrl('http://localhost/ocast/');
    }

       public function testNavbar()
    {
        $this->url('signup/new_account');
        $this->currentWindow()->maximize();
        $element = $this->byCssSelector('.topheaderbar');
        $this->assertEquals('rgba(24, 169, 196, 1)', $element->css('background-color'));
    }
}
?>