<?php
class SignupTest extends PHPUnit_Extensions_Selenium2TestCase
{
	protected $captureScreenshotOnFailure = TRUE;
    protected $screenshotPath = "C:\xampp\htdocs\ocast\selitest\screens";
    protected $screenshotUrl = 'http://localhost/ocast/selitest/screens';

    protected function setUp()
    {
    	//$this->host('localhost');
    	//$this->setPort(4444);
        $this->setBrowser('chrome');
        $this->setBrowserUrl('http://localhost/ocast/');
    }

    /*public function tearDown() 
    {
    	file_put_contents('result.png', base64_decode($this->currentScreenshot()));
  	}*/
 
    public function testNavbar()
    {
        $this->url('signup/new_account');
        $this->currentWindow()->maximize();
        $element = $this->byCssSelector('.topheaderbar');
        $this->assertEquals('rgba(24, 169, 196, 1)', $element->css('background-color'));
    }


    public function testSubmitSignupForm()
    {
    	$this->url('signup/new_account');
    	$this->currentWindow()->maximize();
    	$form = $this->byId('acc_info');
    	$action = $form->attribute('action');
    	$this->byName('acctxt_url')->value('http://blabla.com');
    	$this->byName('acctxt_email')->value('bla@bla.com');
    	$this->byName('acctxt_lname')->value('doe');
    	$this->byName('acctxt_cmpname')->value('Acme');
    	$this->byName('acctxt_addr1')->value('Norris Canel Road');
    	$this->byName('acctxt_addr2')->value('Colombo 10');
    	$form->submit();

    	$this->assertEquals('This field is required.', $this->byId('acctxt_fnameID')->attribute('placeholder'));

    	$fp = fopen('C:\xampp\htdocs\ocast\tests\pc\screens\2.jpg','wb');
		fwrite($fp,$this->currentScreenshot());
		fclose($fp);
    }

    public function testCreateProductFormats()
    {
    	$this->currentWindow()->maximize();
    	$this->url('signup/product_formats');
    	$element = $this->byId("apndivcls");
    	$element->click();
    	sleep(1); 
    	$fp = fopen('C:\xampp\htdocs\ocast\tests\pc\screens\2.jpg','wb');
		fwrite($fp,$this->currentScreenshot());
		fclose($fp);
    }
 
}
?>